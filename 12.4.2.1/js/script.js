var Page = function () {
	this.mainContent = null;
    this.page = 1;
    this.mass = [60, 70, 80, 90];
    this.helcopterHeight = [10, 20, 30];
    this.tensionInCable = [700, 800, 900, 1000];
    this.tensionInCableMin = [undefined, 786, 884, 982];
    this.airFriction = [50, 75, 100];
    this.AllslideData = ['mass', 'helcopterHeight', 'tensionInCable', 'airFriction'];
    this.helcopterHeightDefault = 250;
    this.slideMoveIndex = 0;
    this.manMass = 60;
    this.minTensionInCableValue = null;
    this.waterHeight = 30;
    this.enableCheck = function () {};
    this.selectSlideIndex = null;
	this.go = false;
	this.addEventGo();



}
Page.prototype = {
    init: function () {
        this.cleareBackgroundContent();
    	this.addBackgroundContent();
    	this.addHeightText(10);
    	this.addInformationButton();
		// this.enableInformationButton();
	},
    addMainBlock: function (contents) {
        var self = this;
        var content = $('#contentImg');
        if(content.length > 0) {
            self.mainContent = content;
            content.append(contents);
            return true;
        } else {
            console.error('Not found div#contentImg block');
            return false;
        }
    },
    addBackgroundContent: function () {
        var self = this;
        template = '<div class="left-block"></div>\
                    <div class="right-block"></div>';
        if(self.addMainBlock(template)) {
           self.addSlides();
           self.addTabs();
           self.addHelcopter();
           setTimeout(function(){
                if($('.black-arrow').length > 0) return;
                self.addArrows(true,true,true);
                self.changeArrowsHeight();
           },2000);
           $('.tabs-content>div[data-id=1]').click();
        }
    },
    cleareBackgroundContent: function () {
        var self = this;
        var content = $('#contentImg');
        if(content.length > 0) {
            $('#contentImg').html('');
        }else {
            console.error('Not found div#contentImg block');
        }
    },
    addLeftBlock: function (template) {
    	var self = this;
        var content = $('.left-block');
        if(content.length > 0) {
            content.append(template);
            return true;
        } else {
            console.error('Not found div.left-block block');
            return false;
        }
    },
    addOnRightBlock: function (template) {
    	var self = this;
        var content = $('.right-block');
        if(content.length > 0) {
            content.append(template);
            return true;
        } else {
            console.error('Not found div.right-block block');
            return false;
        }
    },
    addHelcopter: function (){
    	var self = this;
    	var template = '<div class="helcopter-block">\
                            <audio loop>\
                              <source src="sounde/helicopter_hovering_overhead cut.wav" type="audio/wav">\
                            </audio>\
    						<div class="helcopter">\
    							<div class="fan1"></div>\
    							<div class="fan2"></div>\
    							<div class="helcopter-body"></div>\
    						</div>\
    						<div class="man-block">\
                                <div class="arrows"></div>\
                                <img src="img/Rope3.png">\
    						</div>\
    					</div>';
    	self.addLeftBlock(template);
    	self.activatedFan();
    },
    addArrows: function (red,blue,yellow,black){
        var redT=blueT=yellowT=blackT='';
        if(red) redT = '<div class="red-arrow">\
                            <div></div><div></div>\
                            <div><i>F</i><span>cable</span></div>\
                        </div>';
        if(blue) blueT = '<div class="blue-arrow">\
                            <div></div><div></div>\
                            <div><i>F</i><span>gravity</span></div>\
                        </div>'
        if(yellow) yellowT = '<div class="yellow-arrow">\
                                <div></div><div></div>\
                                <div><i>F</i><span>air friction</span></div>\
                            </div>';
        if(black) blackT = '<div class="black-arrow">\
                                <div class="body"><div></div><div></div></div>\
                                <div class="text"><i>F</i><span>net</span></div>\
                            </div>';
        var template = redT+blueT+yellowT+blackT;
        $('.man-block .arrows').html(template);
    },
    addMan: function () {
    	var manMass = this.getSlideValue(0)
    	this.manMass = manMass;
    	$('.man-block').removeClass().addClass('man-block').addClass('mass'+manMass);
    },
    getSlideValue: function (dataIndex) {
    	var elem = '.slide_index_'+(dataIndex+1);
    	var slideLeft = parseInt($(elem + ' .slide-button').css('left').replace('px', ''));
    	var data = this[this.AllslideData[dataIndex]];
    	var elWidth = $(elem+' .slide-text').width();
    	var mashtab = (elWidth-40)/(data.length-1);
    	var index = (slideLeft-10)/mashtab;
    	return data[index.toFixed()];
    },
    activatedFan: function () {
    	$('.fan1').addClass('active');
    	$('.fan2').addClass('active');
    },
    disabledFan: function () {
    	$('.fan1').removeClass('active');
    	$('.fan2').removeClass('active');
    },
    setHeightHelcopter: function (height) {
    	var self = this;
    	$('.helcopter').css({top: (self.helcopterHeightDefault-height*8)+'px'})
    },
    addSlides: function () {
    	var self = this;
    	for (var i = 0; i < 4; i++) {
    		self.addSlideBlock(i);
    		self.addEventSlide(i);
    		$('.slide_index_'+(i+1)+' .slide-button').css({left:'10px'});
    	}
    	self.addEventMouseUpSlide();
    },
    addSlideBlock: function (i) {
    	var self = this;
		var template = '<div class="slide-block pointer-cursor slide_index_'+(i+1)+'">\
							<div class="slide-text"></div>\
			    			<div class="slide-button"></div>\
			    			<div class="slide-fill"></div>\
			    		</div>';

		self.addOnRightBlock(template);
		var slideScaleValue = self[self.AllslideData[i]];
		self.addScale('.slide_index_'+(i+1), slideScaleValue);
    },
    addScale: function (elem, data) {
    	if(data && data.length && data.length == 0) return; // if empty  data
    	var active = 'active';
        var elWidth = $(elem+' .slide-text').width();
    	var mashtab = (elWidth-40)/(data.length-1);
    	var template = "";
        var slide_index = elem.split('_')[2];
    	for(var i = 0; i < data.length; i++){
			var left = 10+(mashtab * i);
			if (i === 3 && slide_index == 3) left = (mashtab * i)-10;
            var val = data[i]
            if(val == 1000) val = '1 000';
    		template+='<span class="scale-text '+active+'" style="left: ' + left + 'px">'+val+'</span>';
            active = '';
        }
    	$(elem+' .slide-text').html(template);
    },
    addEventSlide: function (i) {
    	var self = this;
    	$(document).on('mousedown touchstart', '.slide_index_'+(i+1)+' .slide-button', function (e) {
            if(self.go) return;
    		self.slideMove = true;
			self.slideMoveIndex = (i+1);
        })
    	$(document).off('mousemove touchmove', '.slide-block.slide_index_'+(i+1)).on('mousemove touchmove', '.slide-block.slide_index_'+(i+1), function (e) {
			if(self.go) return;
			var clientX = e.originalEvent.touches ? e.originalEvent.touches[0].clientX : e.originalEvent.clientX;
			var r = $('.slide-block.slide_index_'+(i+1))[0].getBoundingClientRect();
			var newLeft = clientX/zoomScale - r.left / zoomScale;
			if(self.slideMoveIndex == (i+1)){
	    		var blockWidth = $('.slide-block.slide_index_'+(i+1)).width();
	    		if(newLeft > blockWidth-20){
	    			newLeft = blockWidth-15;
	    		} else if (newLeft < 5){
	    			newLeft = 0;
	    		}
	    		if(self.slideMove) {
	    			$('.slide_index_'+(i+1)+' .slide-button').css({left: newLeft+'px'});
	    			$('.slide_index_'+(i+1)+' .slide-fill').css({width: newLeft+'px'});
	    			self.slideNewLeft= newLeft;
				}
    		}
    	})
    	$(document).on('click', '.slide-block.slide_index_'+(i+1), function (e) {
            if(self.go) return;
            self.slideMoveIndex = i+1;
    		var left = parseInt(e.offsetX)
    		if($(e.target).hasClass('slide-button')) {
    			left = parseInt($(e.target).css('left').replace('px', ''));
			}
    		self.slideMove = false;
    		var newLeft = self.getCorectPosition(left);
			$('.slide_index_'+self.slideMoveIndex+' .slide-button').css({left: newLeft+'px'});
			// red side
			$('.slide_index_'+self.slideMoveIndex+' .slide-fill').css({width: newLeft+'px'});
			self.changeSlideEvent();
			newLeft = self.getCorectPosition(newLeft);
    		self.slideMoveIndex = 0;
    	})
    },
    addEventMouseUpSlide: function () {
    	var self = this;
    	$(document).on('mouseup touchend', 'body', function (e) {
			if(self.go) return;
			if(!self.slideMove) return
			self.changeSlideEvent();
			var newLeft = self.getCorectPosition();
			$('.slide_index_'+self.slideMoveIndex+' .slide-button').css({left: newLeft+'px'});
			$('.slide_index_'+self.slideMoveIndex+' .slide-fill').css({width: newLeft+'px'});
			self.slideMove = false;
    	})
	},
    setFvalue: function () {
        var mass = this.manMass||this.mass[0];
        var Fcable = this.tensionInCableMin[this.minIndex] || this.getSlideValue(2);
        var FairFriction = this.getSlideValue(3)||this.airFriction[0];
        var Fgravity = Math.round(9.8*mass);
        var formula = "( - "+spaceNumber(Fgravity)+" ) + "+spaceNumber(Fcable)+" + ( - "+spaceNumber(FairFriction)+" )";
        var formulaEqual = ((-Fgravity)+Fcable+(-FairFriction));
        this.fnet = formulaEqual;
        if($('.net-force-block').length == 0) return;
        if(formulaEqual==0){
            $('.man-block .black-arrow').addClass('zero');
        }else if(formulaEqual>0){
            // $('.man-block .black-arrow .body').css({transform:'rotateZ(180deg)'})
        }else{
            $('.man-block .black-arrow .body').css({transform:'rotateZ(0deg)'})
		}
		"<div>"+`$$ { ${formula} } $$`+"</div><div>"+`$$ { ${formulaEqual} } $$`+" N, upwards</div>"
		$('.net-force-block').css({visibility: "hidden"}).html(` <span class='blockSpan forceFirst'> $$ {\\boldsymbol{F}_\\mathrm{net} =  \\boldsymbol{F}_\\mathrm{gravity} + 
									\\boldsymbol{F}_\\mathrm{cable} + \\boldsymbol{F}_\\mathrm{air \\space friction} } $$ </span>
									<span class='blockSpan forceSecond'>$$ { = ${formula} } $$</span>
									<span class='blockSpan forceThird'>$$ { = ${spaceNumber(formulaEqual)} } $$  N, upwards</span>
									`);
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output], function (){
			$('.net-force-block').css({visibility: 'visible'});
		});
    },
    setWvalue: function () {
    	if($('.work-done-block').length == 0) return;
        var mass = this.manMass||this.mass[0];
        var tensionInCable = this.tensionInCableMin[this.minIndex] || this.getSlideValue(2);
        var airFriction = this.getSlideValue(3)||this.airFriction[0];
        var helcopterHeight = this.getSlideValue(1)||this.helcopterHeight[0];
        var mg = Math.round(9.8*mass);
        var Wgravity = mg*helcopterHeight;
        var Wcable = tensionInCable*helcopterHeight;
        var WairFriction = airFriction*helcopterHeight;
        var formula = "( - "+spaceNumber(Wgravity)+" ) + "+spaceNumber(Wcable)+" + ( - "+spaceNumber(WairFriction)+" )";
		var formulaEqual = ((-Wgravity)+Wcable+(-WairFriction));
		$('.work-done-block').css({visibility: "hidden"}).html(`<span class='blockSpan forceFirst'> $$ { W_\\mathrm{net} =  W_\\mathrm{gravity} + W_\\mathrm{cable} 
									+ W_\\mathrm{air \\space friction} } $$ </span>
									<span class='blockSpan forceSecond'>$$ { = ${formula} } $$</span>
									<span class='blockSpan forceThird'>$$ { = \\color{red}{${spaceNumber(formulaEqual)}} } $$  J</span>
									`);
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output], function (){
			$('.work-done-block').css({visibility: 'visible'});
		});
	},
    setEvalue: function () {
    	if($('.kinetic-energy-block').length == 0) return
        var mass = this.manMass||this.mass[0];
        var tensionInCable = this.tensionInCableMin[this.minIndex] || this.getSlideValue(2);
        var airFriction = this.getSlideValue(3)||this.airFriction[0];
        var helcopterHeight = this.getSlideValue(1)||this.helcopterHeight[0];
        var mg = Math.round(9.8*mass);
        var Wgravity = mg*helcopterHeight;
        var Wcable = tensionInCable*helcopterHeight;
        var WairFriction = airFriction*helcopterHeight;
        var Ek = ((-Wgravity)+Wcable+(-WairFriction));
        var vF = Math.round(Math.sqrt(Math.abs(Ek*2/mass))*1000)/1000;
        // var formula = "`1/2 times ` "+mass+" `(` `"+vF+"^2 -`  `0^2` `)`";
        var formulaEqual = Ek
		$('.kinetic-energy-block').css({visibility: "hidden"}).html(`<span class='blockSpan forceFirst'> $$ { \\Delta E_\\mathrm{k} = \\frac{1}{2} m(\\boldsymbol{v}_\\mathrm{f}^2 - 
										 \\boldsymbol{v}_\\mathrm{i}^2) } $$ </span>
										 <span class='blockSpan forceSecond'>$$ { \\Delta E_\\mathrm{k} = \\frac{1}{2} (${mass}) (${vF.toString().replace(".",",")}^2-0^2) } $$</span>
										 <span class='blockSpan forceThird'>$$ { = \\color{red}{${spaceNumber(formulaEqual)}} } $$ J</span>
										 `);
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output], function (){
			$('.kinetic-energy-block').css({visibility: 'visible'});
		});
    },
    addEventGo: function () {
        var self = this;
        $(document).off('click','.go').one('click','.go',function(){
            self.go = true;
            $('audio')[0].play();
            self.setFvalue();
            self.setWvalue();
            setTimeout(function(){
                self.setEvalue();
            },1000);
            self.addArrows(false,false,false,true);
            if(self.fnet > 0){
                self.addArrowPlus();
                self.changeHeightMan(self.getSlideValue(1));
            } else {
                self.setFNetIsNull();
            }
            $('.black-arrow div:nth-child(1) div:nth-child(2)').css({height: (self.fnet/10+5)+'px'});
            self.enableInformationButton();
            $('.go').attr('disabled', true);
            $('.pointer-cursor').removeClass('pointer-cursor');
	        $('.slide-block').addClass('cover-block');
        })
        $('.go').attr('disabled', false);
        // $('.slide-block').addClass('cover-block');
    },
    setFNetIsNull: function () {
        $('.man-block .black-arrow').addClass('zero');
    },
    addEventInformationButton: function() {
        var self = this;
        $('.information-button').on('click',function(){
            self.addInformationPopup();
        })
    },
    addEventCloseButton: function () {
        var self = this;
        $('.information-popup .close-button').on('click',function(){
            self.removePopup('.information-popup');
        })
    },
    changeSlideEvent: function () {
    	switch(this.slideMoveIndex){
    		case 1:
    			this.addMan();
    			this.setMinTensionInCableValue();
    		break;
    		case 2:
    			var slideValue = this.getSlideValue(1);
    			this.setHeightHelcopter(slideValue);
    			this.changeHeightText(slideValue);
                this.changeCableHeight(slideValue);
    		break;
    		case 3:
                this.minIndex = -1
    		break;
    		case 4:
    		break;
    		default:
    			console.error('can you set correct slide index');
    	}
        if(this.go) return;
        this.changeCableHeight(this.getSlideValue(1));
        this.addArrows(true,true,true);
         $('.arrows > div').css('display', 'none');
        this.changeArrowsHeight();
        setTimeout(function () {
        	var arrows = $('.arrows > div');
        	if(arrows.length > 0){
        		arrows.css('display', 'block');
        	}
        }, 2000);
        this.removePopup('.arrow-button');
        this.addEventGo();
    },
    changeArrowsHeight: function () {
    	var self = this;
    	var mass = this.manMass||this.mass[0];
    	var cable = this.tensionInCableMin[this.minIndex] || this.getSlideValue(2);
    	var air = this.getSlideValue(3)||this.airFriction[0];
    	$('.red-arrow div:nth-child(2)').css({height: (cable/25+15)+'px'});
        let bottom = mass*9.8/25+28;
        if(mass == 60) bottom += 5;
    	$('.red-arrow').css({bottom: bottom+'px'});
    	$('.blue-arrow div:nth-child(1)').css({height: (mass*9.8/25+15)+'px'});
    	$('.yellow-arrow div:nth-child(1)').css({height: (air/8+15)+'px'});
    },
    setMinTensionInCableValue: function () {
    	var minIndex = this.mass.indexOf(this.manMass);
        this.minIndex = minIndex;
    	this.setslideButtonPosition(3, this.tensionInCableMin[minIndex]);
    },
    setslideButtonPosition: function (slideIndex, value){
    	var self = this;
    	var elem ='.slide_index_' + slideIndex;
        if(value == undefined){
            $(elem + ' .slide-button').css('left',10 + 'px');
            $(elem + ' .slide-text span').removeClass('active');
            $(elem + ' .slide-text span:nth-child(1)').addClass('active');
            self.minTensionInCableValue = 10;
			$('.min-tooltip').remove();
            $(elem + ' .slide-fill').css('width',0 + 'px');
            return
        }
        var elWidth = $(elem+' .slide-text').width();
        var data = this[this.AllslideData[slideIndex-1]];
        var dataScale = data[data.length - 1] - data[0];
        var mashtab =  (elWidth-40)/dataScale;
        var diferentValue = value - data[0];
        self.minTensionInCableValue = mashtab * diferentValue + 10;
        $(elem + ' .slide-button').css('left',self.minTensionInCableValue + 'px');
        $(elem + ' .slide-fill').css('width',self.minTensionInCableValue + 'px');
        $(elem + ' .slide-text span').removeClass('active scale-disable');
        self.addMinValueText(value, self.minTensionInCableValue, elem);
    },
    addMinValueText: function (value, minTensionInCableValue, elem) {
    	$('.min-tooltip').remove();
		if(value == undefined) return;
		$(elem).append('<span class="min-tooltip active" style="left:' + minTensionInCableValue + 'px;">' + value + '</span>');
    },
    getCorectPosition: function (leftValue = null, f = function (){}) {
    	var self = this;
    	var elem ='.slide_index_'+self.slideMoveIndex;
    	var data = self[self.AllslideData[self.slideMoveIndex-1]];
		var left = parseInt($(elem + ' .slide-button').css('left').replace('px', ''));
    	if(leftValue){
    		left = leftValue
    	}
		var newLeft = 0;
		var elWidth = $(elem+' .slide-text').width();
    	var mashtab = (elWidth-40)/(data.length-1);
    	$(elem + ' .slide-text span').removeClass('active')
    	$(elem + ' span.min-tooltip').removeClass('active')
    	if(self.slideMoveIndex == 3 && self.minTensionInCableValue) {
    		var position = left/mashtab * mashtab;
    		if (position <= self.minTensionInCableValue){
    			newLeft = self.minTensionInCableValue - 10;
				$(elem + ' span.min-tooltip').addClass('active')
				this.minIndex = this.mass.indexOf(this.manMass);
    		} else {
	    		newLeft = Math.round(left/mashtab) * mashtab;
	    		$(elem + ' .slide-text span:nth-child('+(newLeft/mashtab+1)+')').addClass('active')
            }
    	} else {
    		newLeft = Math.round(left/mashtab) * mashtab;
    		$(elem + ' .slide-text span:nth-child('+(newLeft/mashtab+1)+')').addClass('active')
    	}
    	f();
		return newLeft+10;
    },
	addPopupText: function () {
		var template = '<div class="error-popup-texts"></div>';
		this.addMainBlock(template);
		var eroorText = ''
		MathJax.HTML.addElement(document.querySelector('.error-popup-texts'), "p", {id: ''}, [eroorText]);
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, '']);
	},
    addInformationPopup: function () {
        var template = '<div class="information-popup"><div class="close-button"></div></div>';
        this.addMainBlock(template);
        this.addEventCloseButton();
    },
	addTabs: function () {
		var self = this;
		var tamplate = '<div class="tabs-content">\
							<div data-id="1"></div>\
							<div data-id="2"></div>\
							<div data-id="3"></div>\
						</div>';
		self.addOnRightBlock(tamplate);
		self.addTabBody();
		self.addTabsEvent();
	},
	addTabBody: function () {
		var self = this;
		var tamplate = '<div class="tabs-body">\
						</div>';
		self.addOnRightBlock(tamplate);
	},
	addTabsEvent: function () {
		var self = this;
		var lastDataId = null;
		$(document).off('click', '.tabs-content>div[data-id]').on('click', '.tabs-content>div[data-id]', function (e) {
			var tabId = $(e.target).attr('data-id');
			if(lastDataId == tabId) return;
			lastDataId = tabId;
			switch(tabId) {
				case '1':
					self.activeNetForce();
				break;
				case '2':
					self.activeWorkDone();
				break;
				case '3':
					self.activeChangeInKineticEnergy();
				break;
				default:
					console.error('did not send page number', tabId);
			}
		})
	},
	activeNetForce: function () {
		var self = this;
		this.disableTabs();
		$('.tabs-content>div[data-id=1]').addClass('active');
		var template = (`<div class="net-force-block" style="visibility: hidden;"> <span class='blockSpan forceFirst'> $$ {\\boldsymbol{F}_\\mathrm{net} =  \\boldsymbol{F}_\\mathrm{gravity} + 
									\\boldsymbol{F}_\\mathrm{cable} + \\boldsymbol{F}_\\mathrm{air \\space friction} } $$ </span>
									<span class='blockSpan forceSecond'>$$ { = \\quad\\quad\\space\\space\\space + \\qquad\\quad + } $$</span>
									<span class='blockSpan forceThird'>$$ { = \\qquad\\quad } $$  N</span> </div>
									`);
		$('.tabs-body').html(template);
		if(self.go) self.setFvalue();
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output], function (){
            $('.net-force-block').css({visibility: 'visible'});
        });
	},
	activeWorkDone: function () {
		var self = this;
		this.disableTabs();
		$('.tabs-content>div[data-id=2]').addClass('active');
		var template = (` <div class="work-done-block" style="visibility: hidden;"> <span class='blockSpan forceFirst'> $$ { W_\\mathrm{net} =  W_\\mathrm{gravity} + W_\\mathrm{cable} 
									+ W_\\mathrm{air \\space friction} } $$ </span>
									<span class='blockSpan forceSecond'>$$ { =  \\qquad\\quad + \\qquad\\quad +} $$</span>
									<span class='blockSpan forceThird'>$$ { =  \\qquad\\quad} $$  J</span> </div>
									`);
		$('.tabs-body').html(template);
		if(self.go) self.setWvalue();
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output], function (){
            $('.work-done-block').css({visibility: 'visible'});
		});
	},
	activeChangeInKineticEnergy: function () {
		var self = this;
		this.disableTabs();
		$('.tabs-content>div[data-id=3]').addClass('active');
		var template = (`<div class="kinetic-energy-block" style="visibility: hidden;"> <span class='blockSpan forceFirst'> $$ { \\Delta E_\\mathrm{k} = \\frac{1}{2} m(\\boldsymbol{v}_\\mathrm{f}^2 - 
			\\boldsymbol{v}_\\mathrm{i}^2) } $$ </span>
			<span class='blockSpan forceSecond'>$$ { \\Delta E_\\mathrm{k} = \\frac{1}{2} (\\quad ) (\\quad) } $$</span>
			<span class='blockSpan forceThird'>$$ { =  \\quad} $$ J</span> </div>
			`);
		$('.tabs-body').html(template);
        if(self.go) self.setEvalue();
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output], function (){
            $('.kinetic-energy-block').css({visibility: 'visible'});
		});
	},
	disableTabs: function () {
		$('.tabs-content>div[data-id]').removeClass();
		$('.tabs-body').html('');
	},

    addMathText: function (pageHelpText, helpList) {
		var ol = document.createElement('ol');
		ol.className = 'help-list';
		$(pageHelpText).html(ol);
		for(var i = 0; i< helpList.length; i++) {
			var id = 'li'+i+(new Date().getTime());
			MathJax.HTML.addElement(ol, "li", {id: id}, [helpList[i]]);
			MathJax.Hub.Queue(["Typeset", MathJax.Hub, id]);
		}
	},
    changeHeightMan: function (height) {
        var self = this;
        $('.man-block img').css({bottom:height*8+20+'px'});
        $('.black-arrow').css({bottom:height*8+20+'px'});
        setTimeout(function () {
            self.removePopup('.arrow-button');
        },2000);
    },
	// set and get header, description, help list
	setInstructionHelp: function (select) {
		var self = this;
		var helpList = self.getInstructionList();
		$(select || self.pageHelpText).html('');
		self.pageHelpText = select || self.pageHelpText;
		self.addMathText(self.pageHelpText, helpList);
		$(select || self.pageHelpText).parent().removeClass('help_list_' + self.page - 1).addClass('help_list_' + self.page);

	},
	setDescription: function (select, description) {
		var self = this;
		$(select || self.pageDescription).html('');
		var description = self.getDescription();
		var id = Math.random()*(new Date().getTime());
		MathJax.HTML.addElement($(select || self.pageDescription)[0], "span", {id: 'description'}, [description]);
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'description']);
		self.pageDescription = select || self.pageDescription;
		// $(select).parent().removeClass('description_text_' + self.page - 1).addClass('description_text_' + self.page);
	},
	getDescription: function () {
		var self = this;
		var description = "";
		switch (self.page){
			case 1:
				description = "Set the different parameters for the rescue of the soldier. ";
			break;
		}
		return description;
	},
	getInstructionList: function () {
		var self = this;
		var helpList = "";
		switch (self.page){
			case 1:
			helpList = ['A soldier is lifted from the water by a rescue helicopter. The helicopter hovers in one place and the soldier moves vertically.',
						'Use the sliders to set the height of the helicopter above the water, the mass of the soldier, the downwards force of air friction caused by the blades of the helicopter, and the tension in the cable.',
                        'Click GO to start the motion. ',
                        'Observe the net force on the soldier, the net work done on the soldier and the change in the kinetic energy of the soldier.',
                        'Click RESET to start again.'];
			break;
		}
		return helpList;
	},
	setMainHeader: function (select) {
		var self = this;
		var header = self.getMainHeader();
		$(select || self.pageHeader).html(header);
		self.pageHeader = select || self.pageHeader;
		// $(select).parent().removeClass('description_text_' + self.page - 1).addClass('description_text_' + self.page);
	},
	getMainHeader: function () {
		var self = this;
		var header = "Investigate the work-energy theorem in the vertical plane";
		return header;
	},
	//////////
	clearMain : function () {
		var self = this;
		$('.left-block').html('')
		$('.right-block').html('');
		$('.popup-show-block').css('visibility','hidden');

	},
	createPageConclusion : function () {
		var self = this;
		self.addMainBlock('<div id="conclusion"></div>');
		$('#conclusion').append('<div class="pageNumber">1.</div>');
		$('#conclusion').append('<div class="conclusionImage"><img id="nextPage"><img id="conclusionImage"></div>');
		$('#conclusion').append('<div class="pagePagination">\
			<button class="pagePaginationButton" id="pageChange_prev">\<</button>\
			<button class="pagePaginationButton paginationButtonClicked" id="pageChange_1">1</button>\
			<button class="pagePaginationButton" id="pageChange_2">2</button>\
			<button class="pagePaginationButton" id="pageChange_3">3</button>\
			<button class="pagePaginationButton" id="pageChange_next">\></button>\
			</div>')
		$('#conclusionImage').attr('src','img/t1.png');
		self.addEventPagePagination()
	},
	pageAnimate: function (){
		var self = this;
		$('#pageChange_'+self.paginationPageNumber.prevPage).removeClass('paginationButtonClicked');
		$('#pageChange_'+self.paginationPageNumber.nextPage).addClass('paginationButtonClicked');
		$('#nextPage').attr('src','img/t'+self.paginationPageNumber.nextPage+'.png');
		setTimeout(function(){
			$('#nextPage').css('visibility','visible');
		},100)
		$( "#conclusionImage" ).animate({opacity: 0}, 1000, function() {
    		$('#conclusionImage').attr('src','img/t'+self.paginationPageNumber.nextPage+'.png');
			$('#nextPage').css('visibility','hidden');
			$('#conclusionImage').css('opacity','1');
		});
	},
	addEventPagePagination: function (){
		var self = this;
		$(document).on('click','.pagePaginationButton' , function () {
			var buttonVal = $(this).text();
			$('#pageChange_prev').attr('disabled', false);
			$('#pageChange_next').attr('disabled', false);
			switch(buttonVal){
				case "1":
					self.paginationPageNumber.prevPage = self.paginationPageNumber.nextPage;
					self.paginationPageNumber.nextPage = 1;
					$('#pageChange_prev').attr('disabled', true);
					self.pageAnimate();
				break;
				case "2":
					self.paginationPageNumber.prevPage = self.paginationPageNumber.nextPage;
					self.paginationPageNumber.nextPage = 2;
					self.pageAnimate();
				break;
				case "3":
					self.paginationPageNumber.prevPage = self.paginationPageNumber.nextPage;
					self.paginationPageNumber.nextPage = 3;
					$('#pageChange_next').attr('disabled', true);
					self.pageAnimate();
				break;
				case "<":
					if(!(self.paginationPageNumber.nextPage==1)){
						self.paginationPageNumber.prevPage = self.paginationPageNumber.nextPage;
						self.paginationPageNumber.nextPage -=1;
						self.pageAnimate();
					}
				break;
				case ">":
					if(!(self.paginationPageNumber.nextPage==3)){
						self.paginationPageNumber.prevPage = self.paginationPageNumber.nextPage;
						self.paginationPageNumber.nextPage +=1;
						self.pageAnimate();
					}
				break;
			}
			$('.pageNumber').html(self.paginationPageNumber.nextPage+".")
		})
    },
    addArrowPlus: function () {
    	var template = '<img src="img/ArrowBt.png" class="arrow-button1">';
    	this.addLeftBlock(template);
    },
    addInformationButton: function () {
    	var template = '<img src="img/informationBt1.png" class="information-button">';
    	this.addLeftBlock(template);
    },
    // enableArrowButton: function () {
    // 	var elem = $('arrow-button');
    // 	elem.attr('src','/img/informationBt3.png')
    // },
    enableInformationButton: function () {
    	var elem = $('.information-button');
		elem.attr('src','img/informationBt2.png');
		elem.css({cursor: "pointer"});
        this.addEventInformationButton();
    },
    addHeightText: function (height) {
    	var template = '<div class="height-text" ><div></div><p>'+height+' m</p><div></div></div>';
    	this.addLeftBlock(template);
    },
    changeHeightText: function (height) {
    	var self = this;
    	$('.height-text').css('height', self.waterHeight + height * 8);
    	$('.height-text p').text(height+' m');
    },
    changeCableHeight: function (height) {
        var self = this;
        $('.man-block').css('height', self.waterHeight + height * 8+50);
        $('.man-block img').css('bottom','32px')
        $('.black-arrow').css('bottom','20px')
    },
    removePopup:function (namePopup){
        $(namePopup).remove();
    },
}

function spaceNumber(x){
	var z = x.toString();
	if (z.length >= 4) {
		var c = z.substring(0,z.length-3);
		var y = z.substring(z.length-3, z.length);
		return c+"\\space"+y;
	}
	else{
		return z;
	}
}
