var settings = {
    degreeNumber: 1,
    degree: 15,
    mass: 0,
    ready: false,
    go: false
};

var lines = {
    line0: [],
    line1: [],
    line2: [],
    line3: []
};

var checking = {
    check0: false,
    check1: false,
    check2: false,
    check3: false
};

var modals = {
    modal0: false,
    modal1: false,
    modal2: false,
    modal3: false
};

var previousTest = {};

var previousArr = [];

var checkBtns = false;
var slider;
var zoomScale = 1;
var dr = null;
$( function() {
    slider = $(".choose-degree input").off('input').on('input', function (e) {
            $('.range-line').css({width: (e.target.value * 20) + '%'})
            // $(".degree-button").val("$" + e.target.value);
            $('.go-btn').removeClass("opacity");
            if($('.graph-modal').hasClass('show')){
                $('.graph-modal .close').trigger( "click" );
            }
            hideNotifications();
            if(checkBtns){
                setToPosition();
            }

            if(e.target.value == 0){
                settings.degree = 0;
                settings.degreeNumber = 0;
                $('#degree-button').removeClass();
                $('.go-btn').addClass("opacity");
            }
            else if(e.target.value == 1){
                settings.degree = 15;
                settings.degreeNumber = 1;
                $('#degree-button').removeClass();
                if(previousArr.length == 5){
                    $('.reset_helper_text').removeClass('hide');
                    setTimeout(function () {
                        $('.reset_helper_text').addClass('hide');
                    },4000);
                    enableCheck(true);
                }else if(lines.line0.length == 5 && lines.line1.length == 5 && lines.line2.length == 5 && lines.line3.length == 5){
                    enableCheck(false);
                }else{
                    $('.begin_helper_text').removeClass('hide');
                    setTimeout(function () {
                        $('.begin_helper_text').addClass('hide');
                    },4000);
                }
            } else if(e.target.value == 2){
                settings.degree = 25;
                settings.degreeNumber = 2;
                $('#degree-button').removeClass();
                $('#degree-button').addClass('degree-button-25');
                if(previousArr.length == 5){
                    $('.reset_helper_text').removeClass('hide');
                    setTimeout(function () {
                        $('.reset_helper_text').addClass('hide');
                    },4000);
                    enableCheck(true);
                }else if(lines.line0.length == 5 && lines.line1.length == 5 && lines.line2.length == 5 && lines.line3.length == 5){
                    enableCheck(false);
                }else{
                    $('.begin_helper_text').removeClass('hide');
                    setTimeout(function () {
                        $('.begin_helper_text').addClass('hide');
                    },4000);
                }
            }else if(e.target.value == 3){
                settings.degree = 30;
                settings.degreeNumber = 3;
                $('#degree-button').removeClass();
                $('#degree-button').addClass('degree-button-30');
                if(previousArr.length == 5){
                    $('.reset_helper_text').removeClass('hide');
                    setTimeout(function () {
                        $('.reset_helper_text').addClass('hide');
                    },4000);
                    enableCheck(true);
                }else if(lines.line0.length == 5 && lines.line1.length == 5 && lines.line2.length == 5 && lines.line3.length == 5){
                    enableCheck(false);
                }else{
                    $('.begin_helper_text').removeClass('hide');
                    setTimeout(function () {
                        $('.begin_helper_text').addClass('hide');
                    },4000);
                }
            }else if(e.target.value == 4){
                settings.degree = 45;
                settings.degreeNumber = 4;
                $('#degree-button').removeClass();
                $('#degree-button').addClass('degree-button-45');
                if(previousArr.length == 5){
                    $('.reset_helper_text').removeClass('hide');
                    setTimeout(function () {
                        $('.reset_helper_text').addClass('hide');
                    },4000);
                    enableCheck(true);
                }else if(lines.line0.length == 5 && lines.line1.length == 5 && lines.line2.length == 5 && lines.line3.length == 5){
                    enableCheck(false);
                }else{
                    $('.begin_helper_text').removeClass('hide');
                    setTimeout(function () {
                        $('.begin_helper_text').addClass('hide');
                    },4000);
                }
            }else if(e.target.value == 5){
                settings.degree = 60;
                settings.degreeNumber = 5;
                $('#degree-button').removeClass();
                $('#degree-button').addClass('degree-button-60');
                if(previousArr.length == 5){
                    $('.reset_helper_text').removeClass('hide');
                    setTimeout(function () {
                        $('.reset_helper_text').addClass('hide');
                    },4000);
                    enableCheck(true);
                }else if(lines.line0.length == 5 && lines.line1.length == 5 && lines.line2.length == 5 && lines.line3.length == 5){
                    enableCheck(false);
                }else{
                    $('.begin_helper_text').removeClass('hide');
                    setTimeout(function () {
                        $('.begin_helper_text').addClass('hide');
                    },4000);
                }
            }

            moveSurface(settings.degree);
        });
        $(".choose-degree input").off('change').on('change', function(){
            if($( "#degree-button" ).parent().length !== 0){
                countSlide($( "#degree-button" ).parent())
            }
        });

    $("#degree-button").val("$" + $("choose-degree input").val());
    $('.begin_helper_text').removeClass('hide');
    setTimeout(function () {
        $('.begin_helper_text').addClass('hide');
    },4000);
    mass_class = document.getElementsByClassName('mass');
    i=3;
    start=true;

    $('.mass').draggable({
        revert: "invalid",
        stack: ".mass",
        helper: 'clone',
        cursor: 'pointer',
        start: function(event, ui) {
            hideNotifications();
            ui.position.left = ui.originalPosition.left / zoomScale;
			ui.position.top = ui.originalPosition.top / zoomScale;
        },
        drag: function(event, ui) {
    
            var changeLeft = ui.position.left - ui.originalPosition.left; // find change in left
            var newLeft = ui.originalPosition.left + changeLeft; // adjust new left by our zoomScale
    
            var changeTop = ui.position.top - ui.originalPosition.top; // find change in top
            var newTop = ui.originalPosition.top + changeTop; // adjust new top by our zoomScale
    
            ui.position.left = newLeft /zoomScale;
            ui.position.top = newTop /zoomScale;
        },
        stop: function () {
            enableCheck(false);
            previousArr = [];
            previousTest = {};
        }
    });

    $('.rollar-box').droppable({
        accept: ".mass",
        classes: {
            "ui-droppable-hover": "ui-state-hover"
        },
        drop: function(event, ui3) {
            ui3.helper.data('dropped', true);
            var droppable = $(this);
            var draggable = ui3.draggable;
            $(this).addClass("hide-bg");
            $(draggable).addClass('items-hover');
            
            var drag = draggable.clone().draggable({
                stack: ".material-box",
                helper: 'clone',
                cursor: 'pointer',
                start: function(event, ui) {
                    ui.helper.data('dropped', true);
                    hideNotifications();
                    ui.position.left = ui.originalPosition.left / zoomScale;
                    ui.position.top = ui.originalPosition.top / zoomScale;
                    dr = $(ui.helper[0]).clone()
                    dr.appendTo($('#content-box'))
                    $(ui.helper[0]).css('display','none')
                },
                drag: function(event, ui1) {
                    var changeLeft1 = ui1.position.left - ui1.originalPosition.left; // find change in left
                    var newLeft1 = ui1.originalPosition.left + changeLeft1; // adjust new left by our zoomScale
            
                    var changeTop1 = ui1.position.top - ui1.originalPosition.top; // find change in top
                    var newTop1 = ui1.originalPosition.top + changeTop1; // adjust new top by our zoomScale
                    ui1.position.left = newLeft1 / zoomScale;
                    ui1.position.top = newTop1 / zoomScale;
                    
                    var control = (settings.degree/70)*100;
                    dr.css({left: ui1.position.left + (145 + 260*control/100)})
                        .css({top: ui1.position.top+ (400 - 450*control/100)});
                },
                stop: function(event, ui) {
                    hideTicks();
                    dr.remove();
                    dr = null;
                    $('.begin_helper_text').removeClass('hide');
                    setTimeout(function () {
                        $('.begin_helper_text').addClass('hide');
                    },4000);
                    ui.helper[0].remove();
                    $(this).parent().droppable("enable");
                    $(".choose-degree input").attr({disabled: false});
                    $('.chosen').css('width', '20%');
                    this.remove();
                    settings.mass = $('.rollar-box .mass').length;
                    var numbers = lines[`line${settings.mass}`];
                    if(numbers.length == 5){
                        putAllTicks();
                    }
                    $("#checkBtn").addClass("readyToCheck").prop('disabled', false);
                    mass_class[i].style.display="block";
                    i++
                }

            });

            if(i !== 0){
                drag.appendTo(droppable);
                settings.mass = $('.rollar-box .mass').length;
                settings.ready = true;
                hideTicks();
                if(!checkBtns){
                    $('.begin_helper_text').removeClass('hide');
                    setTimeout(function () {
                        $('.begin_helper_text').addClass('hide');
                    },4000);
                }
            }else{
                $(this).droppable("disable");
            }
            $('.box_helper_text').removeClass('hide');
            if(i==0) return;
            i--;
            mass_class[i].style.display="none"
            if(lines['line'+(3-i)].length == 5){
                putAllTicks()
            }
        }
    });

    var setTimeout1 = function (time) {
    }

    $('.go-btn').on('click', function () {
        var previousArr = lines[`line${settings.mass}`];
        if($('.graph-modal').hasClass('show')){
            $('.graph-modal .close').trigger( "click" );
        }

        if(!$(this).hasClass('opacity')){
            if(previousArr.length < 5){
                var check = previousArr.includes(settings.degreeNumber);
                if(previousTest && previousTest.degree !== settings.degreeNumber && !check && settings.degree !== 0) {
                    turnOfMass();
                    hideNotifications();
                    $(".choose-degree input").attr({disabled: 'disabled'});
                    var time = 0;
                    var mass = settings.mass;
                    var degree = settings.degree;


                    if (mass == 0 || mass == 1 || mass == 2 || mass == 3) {
                        if (degree == 0) {
                            time = 0;
                        }
                        else if (degree == 15) {
                            time = 4;
                        }
                        else if (degree == 25) {
                            time = 2;
                        }
                        else if (degree == 30) {
                            time = 1.7
                        }
                        else if (degree == 45) {
                            time = 1.3
                        }
                        else if (degree == 60) {
                            time = 1.1
                        }
                    }

                    if (time > 0 && start) {

                        var movedElement = document.querySelector('.rollar-box');
                        var line = document.querySelector('.pointer .line');
                        movedElement.style.transition = 'all ' + time + 's cubic-bezier(0.72, -0.07, 1, 1) 0s';
                        line.style.transition = 'all ' + time + 's cubic-bezier(0.72, -0.07, 1, 1) 0s';
                        movedElement.style.transform = 'translateX(300%)';
                        $('.pointer .line').addClass('final-line');

                        setTimeout(function () {
            movedElement.style.transition = 'all ' + time * 8 + 's cubic-bezier(0.72, -0.07, 1, 1) 0s';
            // line.style.transition = 'all ' + time * 8 + 's cubic-bezier(0.72, -0.07, 1, 1) 0s';
            $('.pointer .line').removeAttr('style');
            $('.pointer .line').removeClass('final-line');
            $('#rollar_free').css("left", "690px");
            $('.rollar-box').css({"transform": "translateX(0%)", "left": "0"});
            $("#rollar_free").append($(".rollar-box"));
            $(".choose-degree input").attr({disabled: false});
            $(`.check-${settings.degreeNumber}`).show();
            // $('.box_helper_text').removeClass('hide');
            
            // setTimeout(function () {
            //     $('.box_helper_text').addClass('hide');
            // },4000);
            
            if(previousArr.length == 5){
                hideNotifications();
                $(".choose-degree input").attr({disabled: 'disabled'});
                if(lines.line0.length == 5 && lines.line1.length == 5 && lines.line2.length == 5 && lines.line3.length == 5){
                    if(!$('.mass-gradient-0').hasClass('show') || !$('.mass-gradient-1').hasClass('show') || !$('.mass-gradient-2').hasClass('show') || !$('.mass-gradient-3').hasClass('show')){
                        $('.box_helper_text').removeClass('hide');
                        setTimeout(function () {
                            $('.box_helper_text').addClass('hide');
                        },4000);
                    }
                }else{
                    $('.reset_helper_text').removeClass('hide');
                    setTimeout(function () {
                        $('.reset_helper_text').addClass('hide');
                    },4000);
                    enableCheck(true);
                }
            }

            setTimeout(function () {
                $('.begin_helper_text').addClass('hide');
            },4000);
        }, time * 1000);
                        start = false;
                        checkBtns = true;
                        makeCounting();
                        checkHowMuch();
                    }
                } else {
                    showOtherAngle();
                }
            }else if(lines.line0.length == 5 &&
                    lines.line1.length == 5 &&
                    lines.line2.length == 5 &&
                    lines.line3.length == 5){
                // openFinalModal();
            }else{
                showOtherTest();
            }
        }
    });

    $('.graph-btn').on('click', function () {
        if(checkBtns){
            $('.graph-modal').addClass('show');
            checkBtns = false;
            // $('.choose-degree').slider('disable');
            hideNotifications();
        }
    });

    $('.graph-modal .close').on('click', function () {
        $(this).parent().removeClass('show');
        if(lines.line0.length == 5 && lines.line1.length == 5 && lines.line2.length == 5 && lines.line3.length == 5){
            enableCheck(false);
            if($('.mass-gradient-0').hasClass('show') && $('.mass-gradient-1').hasClass('show') && $('.mass-gradient-2').hasClass('show') && $('.mass-gradient-3').hasClass('show')){
                enableCheck(true)
            }
        }else{
            $('.choose-degree input').attr({disabled: false});
            // $('.mass').draggable('enable');
        }
        $('.line-btn').removeClass('clicked');
        $('.gradient-btn').removeClass('clicked');
        checkBtns = true;
        modalMassPieces();
    });

    $('.formula-modal .close').on('click', function () {
        $('.gradient-btn').removeClass('clicked');
        $(this).parent().removeClass('show');
    });

    $('.line-btn').on('click', function () {
        addLine();
    });

    $('.gradient-btn').on('click', function () {
        // removeGradient();
        var check = checking[`check${settings.mass}`];
        if(lines.line0.length == 5 && checking[`check0`]){
            if($('.mass-line-0').hasClass('show') && !modals.modal0){
                addGradient(0);
                $('.gradient-btn').addClass('clicked');
            }
        }
        if(lines.line1.length == 5 && checking[`check1`]){
            if($('.mass-line-1').hasClass('show') && !modals.modal1){
                addGradient(1);
                $('.gradient-btn').addClass('clicked');
            }
        }
        if(lines.line2.length == 5 && checking[`check2`]) {
            if ($('.mass-line-2').hasClass('show') && !modals.modal2) {
                addGradient(2);
                $('.gradient-btn').addClass('clicked');
            }
        }
        if(lines.line3.length == 5 && checking[`check3`]){
            if($('.mass-line-3').hasClass('show') && !modals.modal3){
                addGradient(3);
                $('.gradient-btn').addClass('clicked');
            }
        }

        if(
            (!checking[`check${settings.mass}`] && lines[`line${settings.mass}`].length !== 0) && !$(`.mass-line-${settings.mass}`).hasClass('show') ||
            ((lines.line0.length == 5 && lines.line1.length == 5 && lines.line2.length == 5 && lines.line3.length == 5) && !check)
        ) {
            showGradientError();
        }
        var checkAgain = settings.mass;
        checkAgain++;
        for(var i = 0; i < checkAgain; i++){
            if(previousArr.length == 0 && !$(`.mass-line-${i}`).hasClass('show') && lines[`line${i}`].length == 5){
                showGradientError();
            }
        }
    });

    $("#checkBtn").on('click', function () {
        if($(this).hasClass('accept')){
            if(lines.line0.length == 5 && lines.line1.length == 5 && lines.line2.length == 5 && lines.line3.length == 5){
                if($('.mass-gradient-0').hasClass('show') && $('.mass-gradient-1').hasClass('show') && $('.mass-gradient-2').hasClass('show') && $('.mass-gradient-3').hasClass('show')){
                    openFinalModal();
                    enableCheck(true);
                }
            }else{
                setToPosition();
                enableCheck(false);
            }
            if(previousArr.length == 5){
                hideNotifications();
                if($('.mass-gradient-0').hasClass('show') && $('.mass-gradient-1').hasClass('show') && $('.mass-gradient-2').hasClass('show') && $('.mass-gradient-3').hasClass('show')){
                    enableCheck(true);
                }else{
                    correctMass();
                }
            }
        }
    });

    $('body').on('click', function (e) {
        // e.preventDefault();
        $('.begin_helper_text').addClass('hide');
        $('.box_helper_text').addClass('hide');
        $('.reset_helper_text').addClass('hide');
    });

    $('.btns .no').on('click', function () {
        $('.box_helper_text1').addClass('hide');
        enableCheck(true);
    });

    $('.btns .yes').on('click', function () {
        location.reload();
    });

    $('.mass-cursor').on('click', function () {
        if($(this).hasClass('show')){
            var data = $(this).attr('data-line');
            var number = parseInt(data, 10);
            modals[`modal${number}`] = true;
            $('.mass-cursor').removeClass('opacity');
            if($(this).hasClass(`mass-pieces-${number}`) && $(`.mass-gradient-${number}`).hasClass('show') && !$(`.gradient-btn`).hasClass('clicked')){
                $(`.mass-pieces-${number}`).addClass('opacity');
            }
            addGradient(number);
        }
    })
});

$(`.mass-pieces-0`).on("click", function(){
    $(`.mass-pieces-0`).css({opacity: '0.2'});
    $(`.mass-pieces-1`).css({opacity: '1'});
    $(`.mass-pieces-2`).css({opacity: '1'});
    $(`.mass-pieces-3`).css({opacity: '1'});
})
$(`.mass-pieces-1`).on("click", function(){
    $(this).css({opacity: '0.2'});
    $(`.mass-pieces-0`).css({opacity: '1'});
    $(`.mass-pieces-2`).css({opacity: '1'});
    $(`.mass-pieces-3`).css({opacity: '1'});
})
$(`.mass-pieces-2`).on("click", function(){
    $(this).css({opacity: '0.2'});
    $(`.mass-pieces-1`).css({opacity: '1'});
    $(`.mass-pieces-0`).css({opacity: '1'});
    $(`.mass-pieces-3`).css({opacity: '1'});
})
$(`.mass-pieces-3`).on("click", function(){
    $(this).css({opacity: '0.2'});
    $(`.mass-pieces-1`).css({opacity: '1'});
    $(`.mass-pieces-2`).css({opacity: '1'});
    $(`.mass-pieces-0`).css({opacity: '1'});
})
$('.close').on('click', function(){
    $(`.mass-pieces-0`).css({opacity: '1'});
    $(`.mass-pieces-1`).css({opacity: '1'});
    $(`.mass-pieces-2`).css({opacity: '1'});
    $(`.mass-pieces-3`).css({opacity: '1'});
})

// $(document.body).click( function(e) {
//     hideNotifications();
// });

function addLine() {
    if(lines.line0.length == 5){
        if(!$('.mass-pieces-0-line').hasClass('show')){
            $('.mass-line-0').addClass('show');
            $('.mass-pieces-0').css({opacity: '1', 'pointer-events': 'auto '});
            $('.line-btn').addClass('clicked');
            checking.check0 = true;
        }
    }
    if(lines.line1.length == 5){
        if(!$('.mass-pieces-1-line').hasClass('show')){
            $('.mass-line-1').addClass('show');
            $('.mass-pieces-1').css({opacity: '1', 'pointer-events': 'auto '});
            $('.line-btn').addClass('clicked');
            checking.check1 = true;
        }
    }
    if(lines.line2.length == 5){
        if(!$('.mass-pieces-2-line').hasClass('show')){
            $('.mass-line-2').addClass('show');
            $('.mass-pieces-2').css({opacity: '1', 'pointer-events': 'auto '});
            $('.line-btn').addClass('clicked');
            checking.check2 = true;
        }
    }
    if(lines.line3.length == 5){
        if(!$('.mass-pieces-3-line').hasClass('show')){
            $('.mass-line-3').addClass('show');
            $('.mass-pieces-3').css({opacity: '1', 'pointer-events': 'auto '});
            $('.line-btn').addClass('clicked');
            checking.check3 = true;
        }
    }

    if(lines[`line${settings.mass}`].length !== 5 && lines[`line${settings.mass}`].length !== 0){
        showAlert();
    }
}

function openFinalModal() {
    $('.box_helper_text1').removeClass('hide');
}

function hideTicks() {
    $('.check').hide();
}

function turnOfMass() {
    $('.mass').draggable('disable');
}

function showAlert() {
    $('#responceBox').show();
    $('#responceBox').addClass('toTop');
    $('#responceBox3').hide();
    $('#responceBox3').removeClass('toTop');
    setTimeout(function () {
        $('#responceBox').hide();
        $('.line-btn').removeClass('clicked');
        $('#responceBox').removeClass('toTop');
    },4000)
}

function showOtherAngle() {
    $('#responceBox1').show();
    $('#responceBox1').addClass('toTop');
    setTimeout(function () {
        $('#responceBox1').hide();
        $('#responceBox1').removeClass('toTop');
    },4000)
}

function showGradientError() {
    $('#responceBox3').show();
    $('#responceBox3').addClass('toTop');
    $('#responceBox').hide();
    $('#responceBox').removeClass('toTop');
    setTimeout(function () {
        $('#responceBox3').hide();
        $('#responceBox3').removeClass('toTop');
        $('.gradient-btn').removeClass('clicked');
    },4000)
}

function showOtherTest() {
    $('#responceBox2').show();
    $('#responceBox2').addClass('toTop');
    setTimeout(function () {
        $('#responceBox2').hide();
        $('#responceBox2').removeClass('toTop');
    },4000);
}

function correctMass() {
    settings.degree = 15;
    settings.degreeNumber = 1;
    $('#degree-button').removeClass();
    moveSurface(15);
    setToPosition();
    $('.mass').draggable('enable');
    $('.choose-degree input').attr({disabled: false}).val(1);
    $('.choose-degree .range-line').css({width: 20 + '%'});
}

function addGradient(mass) {
    removeGradient()
    if($(`.mass-line-${mass}`).selector == '.mass-line-0' && $(`.mass-line-${mass}`).hasClass('show')){
        $('.mass-gradient-0').addClass('show');
        $('.formula-modal').addClass('show');
        $('.a').append('<p>3</p>');
        $('.fn').append('<p>2,2</p>');
        $('.result').append('<p>1,36</p>');
        $('.m1').append('<p>1,36</p>');
        $('.m2').append('<p>0,74 kg</p>');
        $('.gradient-btn').addClass('clicked');
        $('.mass-show span').append('Trolley without mass pieces');
        $('.mass-pieces-0-line').addClass('show');
        modals.modal0 = true;
    }
    if($(`.mass-line-${mass}`).selector == '.mass-line-1' && $(`.mass-line-${mass}`).hasClass('show')){
        $('.mass-gradient-1').addClass('show');
        $('.formula-modal').addClass('show');
        $('.a').append('<p>2</p>');
        $('.fn').append('<p>4</p>');
        $('.result').append('<p>0,5</p>');
        $('.m1').append('<p>0,5</p>');
        $('.m2').append('<p>2,0 kg</p>');
        $('.gradient-btn').addClass('clicked');
        $('.mass-show span').append('Trolley with one mass piece');
        $('.mass-pieces-1-line').addClass('show');
        modals.modal1 = true;
    }
    if($(`.mass-line-${mass}`).selector == '.mass-line-2' && $(`.mass-line-${mass}`).hasClass('show')){
        $('.mass-gradient-2').addClass('show');
        $('.formula-modal').addClass('show');
        $('.a').append('<p>1</p>');
        $('.fn').append('<p>3,2</p>');
        $('.result').append('<p>0,31</p>');
        $('.m1').append('<p>0,31</p>');
        $('.m2').append('<p>3,23 kg</p>');
        $('.gradient-btn').addClass('clicked');
        $('.mass-show span').append('Trolley with two mass pieces');
        $('.mass-pieces-2-line').addClass('show');
        modals.modal2 = true;
    }
    if($(`.mass-line-${mass}`).selector == '.mass-line-3' && $(`.mass-line-${mass}`).hasClass('show')) {
        $('.mass-gradient-3').addClass('show');
        $('.formula-modal').addClass('show');
        $('.a').append('<p>1,4</p>');
        $('.fn').append('<p>6,2</p>');
        $('.result').append('<p>0,23</p>');
        $('.m1').append('<p>0,23</p>');
        $('.m2').append('<p>4,35 kg</p>');
        $('.gradient-btn').addClass('clicked');
        $('.mass-show span').append('Trolley with three mass pieces');
        $('.mass-pieces-3-line').addClass('show');
        modals.modal3 = true;
    }
    if(modals.modal0 === true && modals.modal1 === true && modals.modal2 === true && modals.modal3 === true){
        enableCheck(true)
    }
}

function removeGradient() {
    $('.a').empty();
    $('.fn').empty();
    $('.result').empty();
    $('.m1').empty();
    $('.m2').empty();
    $('.mass-show span').empty();

}

function enableCheck(flag){
    if(flag){
        $("#checkBtn").css("opacity","1");
        $("#checkBtn").css("cursor","pointer");
        $("#checkBtn").addClass("accept");
    }else{
        $("#checkBtn").css("opacity",".5");
        $("#checkBtn").css("cursor","default");
        $("#checkBtn").removeClass("accept");
    }
}

function setToPosition() {
    $(".rollar-box").removeAttr('style');
    $(".surface").append($(".rollar-box"));
    start = true;
}

function hideNotifications() {
    $(".begin_helper_text").addClass('hide');
    $(".box_helper_text").addClass('hide');
    $(".reset_helper_text").addClass('hide');
    // $('#responceBox').hide();
    // $('#responceBox1').hide();
}

function moveSurface(degree) {
    if(degree == 0){
        $('.surface').css('-webkit-transform','rotate(0deg)');
    }else if(degree == 15) {
        $('.surface').css('-webkit-transform','rotate(17deg)');
    }else if(degree == 25) {
        $('.surface').css('-webkit-transform','rotate(26deg)');
    }else if(degree == 30) {
        $('.surface').css('-webkit-transform','rotate(32deg)');
    }else if(degree == 45) {
        $('.surface').css('-webkit-transform','rotate(47deg)');
    }else if(degree == 60) {
        $('.surface').css('-webkit-transform','rotate(62deg)');
    }
}

function friction(degree) {
}

function makeCounting() {
    $(`.mass-pieces-${settings.mass}-line`).find(`.dot-${settings.degreeNumber}`).show();
    var numbers = lines[`line${settings.mass}`];
    if(numbers.length !== 0){
        var check = numbers.includes(settings.degreeNumber);
        if(!check){
            lines[`line${settings.mass}`].push(settings.degreeNumber);
        }
    }else{
        lines[`line${settings.mass}`].push(settings.degreeNumber);
    }
    previousArr = numbers;
    previousTest.degree = settings.degreeNumber;
    previousTest.mass =  settings.mass;
}

function checkHowMuch(){
    if(settings.mass == 0){
        $('.mass-pieces-0').addClass('show');
        $('.mass-pieces-0').css({'pointer-events': 'none'});
    }else if(settings.mass == 1){
        $('.mass-pieces-1').addClass('show');
        $('.mass-pieces-1').css({'pointer-events': 'none'});
    }else if(settings.mass == 2){
        $('.mass-pieces-2').addClass('show');
        $('.mass-pieces-2').css({'pointer-events': 'none'});
    }else if(settings.mass == 3){
        $('.mass-pieces-3').addClass('show');
        $('.mass-pieces-3').css({'pointer-events': 'none'});
    }
}

function modalMassPieces() {
    $('.mass-cursor').removeClass('opacity');
    $('.formula-modal').removeClass('show');
}

function countSlide(el) {
    var element = $( el ).attr('style');
    var str = element.slice(6, 15);
    var output = parseInt(str, 10);
    $('.chosen').css( 'width', output+'%' );
}

function removeAllNot() {
    setTimeout(function () {
        hideNotifications();
    }, 4000);
}

function putAllTicks() {
    $('.degree-number .check').show();
}