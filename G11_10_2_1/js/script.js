var sliderCoils;
var sliderArea;
var sliderAngle;
var sliderTime;
var flipCheck = true;
var moveCheck = true;
var resultCheck = false;
var moveInTime;
var moveOutTime;

var parameters = {
    coils: 20,
    area: 2,
    angles: 0,
    time: 5,
    pole: 'N',
    direction: 'in'
};


$(function() {
    addCoilsSlider();
    addAreaSlider();
    addAngleSlider();
    addTimeSlider();
    firstPosition();

    $('.faraday-law').on('click', function (){
        $('.law-modal').show();
    });

    $('.law-modal .close').on('click', function (){
        $('.law-modal').hide();
    });

    $('body').on('click', function (){
        // if(resultCheck){
        //     $('.result').hide();
        // }
    });

    $('.flip').on('click', function () {
        if(flipCheck){
            if($(this).hasClass('rotate')){
                $(this).removeClass('rotate');
                $('.magnet').removeClass('rotate');
                parameters.pole = 'N';
            }else{
                $(this).addClass('rotate');
                $('.magnet').addClass('rotate');
                parameters.pole = 'S';
            }
        }
    });

    $('.move-btn').on('click', function () {
        if(moveCheck){
            $(this).addClass('move-btn-clicked');
            move();
            animateMagnet();
        }
    });

    $("#checkBtn").on('click', function (e) {
        if($(this).hasClass('checked')){
            clearTimeout(moveInTime);
            clearTimeout(moveOutTime);
            removeNiddle();
            reset();
        }
    });
});

function addCoilsSlider() {
    sliderCoils = $( ".choose-coils" ).on('input', function( e ) {
        $( ".coils-button" ).val( "$" + e.target.value );
        $('.move-btn').removeClass('move-btn-clicked');
        if(resultCheck){
            $('.result').hide();
        }
        if(e.target.value == 0){
            parameters.coils = 10;
            $(this).find('.coils-button').removeClass('coils-correction-1');
            $(this).find('.coils-button').removeClass('coils-correction-2');
            $(this).find('.coils-button').removeClass('coils-correction-3');
        }else if(e.target.value == 1){
            parameters.coils = 20;
            $(this).find('.coils-button').addClass('coils-correction-1');
            $(this).find('.coils-button').removeClass('coils-correction-2');
            $(this).find('.coils-button').removeClass('coils-correction-3');
        }else if(e.target.value == 2){
            parameters.coils = 50;
            $(this).find('.coils-button').removeClass('coils-correction-1');
            $(this).find('.coils-button').addClass('coils-correction-2');
            $(this).find('.coils-button').removeClass('coils-correction-3');
        }else if(e.target.value == 3){
            $(this).find('.coils-button').removeClass('coils-correction-1');
            $(this).find('.coils-button').removeClass('coils-correction-2');
            $(this).find('.coils-button').addClass('coils-correction-3');
            parameters.coils = 100;
        }
        $('#main-pic').removeClass();
        $('#main-pic').addClass(`pic-${parameters.area}-${parameters.coils}`);
        //     // $('#rollar_free').empty();
    //     hideNumbers();
    //     hideNotifications();
    });
    $( ".choose-coils" ).on('change', function(){

    })
    // $('.choose-coils').find('a').append('<img src="./img/g_button.png" class="coils-button" alt="">');
    // $( ".coils-button" ).val( "$" + $( "choose-coils" ).slider( "value" ) );
}

function addAreaSlider() {
    sliderArea = $('.choose-area').on('input', function( e ) {
        $( ".area-button" ).val( "$" + e.target.value );
        $('.move-btn').removeClass('move-btn-clicked');
        if(e.target.value == 0){
            $(this).find('.area-button').removeClass('area-correction-2');
            $(this).find('.area-button').removeClass('area-correction-1');
            parameters.area = 1;
        }else if(e.target.value == 1){
            parameters.area = 2;
            $(this).find('.area-button').removeClass('area-correction-2');
            $(this).find('.area-button').addClass('area-correction-1');
        }else if(e.target.value == 2){
            parameters.area = 4;
            $(this).find('.area-button').removeClass('area-correction-1');
            $(this).find('.area-button').addClass('area-correction-2');
        }
        $('#main-pic').removeClass();
        $('#coil-inside').removeClass();
        $('#main-pic').addClass(`pic-${parameters.area}-${parameters.coils}`);
        $('#coil-inside').addClass(`inside-coil-${parameters.area}`);
        //     settings.degree = ui.value;
        //     // $('#rollar_free').empty();
        //     hideNumbers();
        //     hideNotifications();
    });
    $('.choose-area').on('change', function(){

    })
    // $('.choose-area').find('a').append('<img src="./img/y_button.png" class="area-button" alt="">');
    // $( ".area-button" ).val( "$" + $( "choose-area" ).slider( "value" ) );
}

function addAngleSlider() {
    sliderAngle = $( ".choose-angle" ).on('input', function( e ) {
        $( ".angle-button" ).val( "$" + e.target.value );
        $('#magnet-box').removeAttr('style');
        $('.move-btn').removeClass('move-btn-clicked');
        if(e.target.value == 0){
            $(this).find('.angle-button').removeClass('area-correction-2');
            $(this).find('.angle-button').removeClass('area-correction-1');
            $('#magnet-box').removeClass('magnet-30');
            $('#magnet-box').removeClass('magnet-60');
            $('.angle').removeClass('angle-30');
            $('.angle').removeClass('angle-60');
            parameters.angles = 0;
        }else if(e.target.value == 1){
            $(this).find('.angle-button').removeClass('area-correction-2');
            $(this).find('.angle-button').addClass('area-correction-1');
            $('#magnet-box').removeClass('magnet-60');
            $('#magnet-box').addClass('magnet-30');
            $('.angle').removeClass('angle-60');
            $('.angle').addClass('angle-30');
            parameters.angles = 30;
        }else if(e.target.value == 2){
            $(this).find('.angle-button').removeClass('area-correction-1');
            $(this).find('.angle-button').addClass('area-correction-2');
            $('#magnet-box').removeClass('magnet-30');
            $('#magnet-box').addClass('magnet-60');
            $('.angle').removeClass('angle-30');
            $('.angle').addClass('angle-60');
            parameters.angles = 60;
        }
        //     settings.degree = ui.value;
        //     // $('#rollar_free').empty();
        //     hideNumbers();
        //     hideNotifications();
    })
    $( ".choose-angle" ).on('change', function(){

    })
    // $('.choose-angle').find('a').append('<img src="./img/b_button.png" class="angle-button" alt="">');
    // $( ".angle-button" ).val( "$" + $( "choose-angle" ).slider( "value" ) );
}

function addTimeSlider() {
    sliderTime = $( ".choose-time" ).on('input', function( e ) {
        $( ".time-button" ).val( "$" + e.target.value );
        $('.move-btn').removeClass('move-btn-clicked');
        if(e.target.value == 0){
            $(this).find('.time-button').removeClass('area-correction-2');
            $(this).find('.time-button').removeClass('time-correction-1');
            parameters.time = 1;
        }else if(e.target.value == 1){
            $(this).find('.time-button').removeClass('area-correction-2');
            $(this).find('.time-button').addClass('time-correction-1');
            parameters.time = 5;
        }else if(e.target.value == 2){
            $(this).find('.time-button').removeClass('time-correction-1');
            $(this).find('.time-button').addClass('area-correction-2');
            parameters.time = 10;
        }
        //     settings.degree = ui.value;
        //     // $('#rollar_free').empty();
        //     hideNumbers();
        //     hideNotifications();
    })
    $( ".choose-time" ).on('change', function(){

    })
    // $('.choose-time').find('a').append('<img src="./img/pnk_button.png" class="time-button" alt="">');
    // $( ".time-button" ).val( "$" + $( "choose-time" ).slider( "value" ) );
}

// all animation for magnet box
function animateMagnet(){
    var time = parameters.time;
    var angle = parameters.angles;
    var area = parameters.area;
    $(".flip").css({'cursor': 'default'});

    if(angle == 0){
        $('#magnet-box').css({"animation":`animation-0 linear ${time}s infinite`});
    }else if(angle == 30){
        if(area == 1){
            $('#magnet-box').css({"animation":`animation-30-1 linear ${time}s infinite`});
        }else{
            $('#magnet-box').css({"animation":`animation-30 linear ${time}s infinite`});
        }
    }else if(angle == 60){
        if(area == 1){
            $('#magnet-box').css({"animation":`animation-60-1 linear ${time}s infinite`});
        }else if(area == 2){
            $('#magnet-box').css({"animation":`animation-60-2 linear ${time}s infinite`});
        }else if(area == 4){
            $('#magnet-box').css({"animation":`animation-60-4 linear ${time}s infinite`});
        }
    }
}

function move() {
    var time = parameters.time;
    var angle = parameters.angles;
    var direction = parameters.direction;
    var pole = parameters.pole;
    var area = parameters.area;
    resultCheck = false;
    closeAll();
    var formulaRes = formula(pole, direction);
    niddleMove(formulaRes);
    var result = formulaRes.replace('-', '&ndash;').split(".").join(",");
    $('.mjx-chtml').removeAttr('style');
    $('.result .value').empty();
    $('.result .value').append(`${result}`);
    $('.result').show();
    
    enableCheck(true);
    moveInTime = setTimeout(function(){
        if(direction == 'in') {
            parameters.direction = 'out'
        }
        moveOut();
    },time/2*1000);
}

function moveOut() {
    var time = parameters.time;
    var angle = parameters.angles;
    var direction = parameters.direction;
    var pole = parameters.pole;
    var area = parameters.area;
    var movedElement =  document.querySelector('#magnet-box');
    movedElement.style.transition = 'all '+time+'s linear 0s';
    var formulaRes = formula(pole, direction);
    niddleMove(formulaRes);
    var result = formulaRes.replace('-', '&ndash;').split(".").join(",");
    $('.result').show();
    $('.result .value').empty();
    $('.result .value').append(`${result.replace(/\\./g, ',')}`);
    
    moveOutTime = setTimeout(function(){
        if(direction == 'out') {
            parameters.direction = 'in'
        }
        $('.result .value').empty();
        $('.result .value').append('0');
        removeNiddle();
        openMove();
        move();
        resultCheck = true;
    },time/2*1000);
}

function firstPosition() {
    $('.coils-button').addClass('coils-correction-1');
    $('.area-button').addClass('area-correction-1');
    $('.time-button').addClass('time-correction-1');
    $('#main-pic').addClass(`pic-${parameters.area}-${parameters.coils}`);
    sliderCoils.slider( "value", 1 );
    sliderArea.slider( "value", 1 );
    sliderTime.slider( "value", 1 );
}

function enableCheck(flag){
    if(flag){
        $("#checkBtn").css("opacity","1");
        $("#checkBtn").css("cursor","pointer");
        $("#checkBtn").addClass('checked');
    }else{
        $("#checkBtn").css("opacity",".5");
        $("#checkBtn").css("cursor","default");
        $("#checkBtn").removeClass('checked');
    }
}

function formula(pole, direction) {
    var angle = parameters.angles;
    var a = parameters.area;
    var n =  parameters.coils;
    var t =  parameters.time;
    var bi;
    var bf;
    var b;
    var fi;
    var ff;
    var f;
    var e;
    if(pole == 'N'){
        if(direction == 'in'){
            bi = 0.03;
            bf = 0.05;
        }else if(direction == 'out'){
            bi = 0.05;
            bf = 0.03;
        }
    }else if(pole == 'S'){
        if(direction == 'in'){
            bi = 0.05;
            bf = 0.03;
        }else if(direction == 'out'){
            bi = 0.03;
            bf = 0.05;
        }
    }
    b = bf - bi;
    b = b.toFixed(2);
    if(angle == 0){
        f = b * a;
    }else if(angle == 30){
        fi = bi * Math.cos(angle * Math.PI / 180);
        ff = bf * Math.cos(angle * Math.PI / 180);
        f = ff - fi;
    }else if(angle == 60){
        fi = bi * Math.cos(angle * Math.PI / 180);
        ff = bf * Math.cos(angle * Math.PI / 180);
        f = ff - fi;
    }
    e = n * f/t;
    e = e * -1;
    e = e.toFixed(3);
    return e;
}

function niddleMove(number) {
    var check = Math.sign(number);
    var negative;
    if(check == -1){
        negative = number * -1;
    }

    if(check == 1 && number < 0.800){
        removeNiddle();
        $('.niddle').addClass('niddle-1-right');
    }else if(check == 1 && number <= 1.6){
        removeNiddle();
        $('.niddle').addClass('niddle-2-right');
    }else if(check == 1 && number == 4){
        removeNiddle();
        $('.niddle').addClass('niddle-3-right');
    }else if(check == 1 && number == 8){
        removeNiddle();
        $('.niddle').addClass('niddle-4-right');
    }


    if(check == -1 && negative < 0.800){
        removeNiddle();
        $('.niddle').addClass('niddle-1-left');
    }else if(check == -1 && negative <= 1.6){
        removeNiddle();
        $('.niddle').addClass('niddle-2-left');
    }else if(check == -1 && negative == 4){
        removeNiddle();
        $('.niddle').addClass('niddle-3-left');
    }else if(check == -1 && negative == 8){
        removeNiddle();
        $('.niddle').addClass('niddle-4-left');
    }
}

function reset() {
   
    if($(this).hasClass('rotate')){
        $(this).removeClass('rotate');
        $('.magnet').removeClass('rotate');
    }

    $(".flip").css({'cursor': 'pointer'});

    $('#magnet-box').removeAttr('style');

    // $('#magnet-box').removeClass('magnet-60');
    // $('#magnet-box').removeClass('magnet-30');
   
    // $('.angle').removeClass('angle-30');
    // $('.angle').removeClass('angle-60');
    $('.result').hide();
    $('.result .value').empty();
    $('.move-btn').removeClass('move-btn-clicked');
    
    enableCheck(false);
    openAll();
}

function removeNiddle() {
    $('#main-pic .niddle').removeClass('niddle-1-right');
    $('#main-pic .niddle').removeClass('niddle-2-right');
    $('#main-pic .niddle').removeClass('niddle-3-right');
    $('#main-pic .niddle').removeClass('niddle-4-right');
    $('#main-pic .niddle').removeClass('niddle-1-left');
    $('#main-pic .niddle').removeClass('niddle-2-left');
    $('#main-pic .niddle').removeClass('niddle-3-left');
    $('#main-pic .niddle').removeClass('niddle-4-left');
}

function closeAll() {
    flipCheck = false;
    moveCheck = false;
    sliderArea.slider('disable');
    sliderAngle.slider('disable');
    sliderCoils.slider('disable');
    sliderTime.slider('disable');
}

function openAll() {
    flipCheck = true;
    moveCheck = true;
    sliderArea.slider('enable');
    sliderAngle.slider('enable');
    sliderCoils.slider('enable');
    sliderTime.slider('enable');
}

function openMove() {
    moveCheck = true;
}
