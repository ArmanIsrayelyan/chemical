var slideButton = document.querySelector('.slide_button');
var slider = document.querySelector('.slider');
var air_glass_button_container = document.querySelector('.air_glass_button_container');
var items = [...document.querySelectorAll('.o_i'), ...document.querySelectorAll('.o_r')];
var helperTextContainer = document.querySelector('.begin_helper_text');
var clickedButton
var showAlert = false;
var maxLeft = 410;
var minLeft = 0;
var slideButtonCursor;
var percent = 0
var draggable = false;
var mashtab = 1;

slideButton.addEventListener('mousedown', slideButtonTachStart);
slideButton.addEventListener('touchstart', slideButtonTachStart);
slideButton.addEventListener('mouseup',slideButtonTachEnd);
slideButton.addEventListener('touchend',slideButtonTachEnd);
function slideButtonTachMove(event){
    if (!draggable) return;
    var left = parseInt(slideButton.style.left);
    if (left) {
        if (left < minLeft) {
            slideButton.style.left = minLeft + 10 + 'px'

            draggable = false;
            console.log(10);

            return
        } else if (left > maxLeft) {
            slideButton.style.left = maxLeft - 1 + 'px'

            draggable = false;

            return
        }
    }
    var clientX = event.clientX || event.touches[0].clientX;

    percent = (Math.round((100 - getPercentageChange(maxLeft + 45, left)) / 15) * 15)
    slideButton.style.left = clientX/mashtab - offset(slider).left/mashtab + (slideButtonCursor/mashtab||0) + 'px';
}

slider.addEventListener('mousemove', slideButtonTachMove)
slider.addEventListener('touchmove', slideButtonTachMove)


slider.addEventListener('mouseleave', function (event) {
    draggable = false;
    changePercents();
    scrollbarMoveTo();
    lightAnimation();
    showAirCircles();
})

air_glass_button_container.addEventListener('click', function (event) {
    var target = event.target;
    if (!target.closest('.airButton') && !target.closest('.glassButton')) return;

    document.querySelector('.light_from').style.left = '45%';
    setRemoveAnimationElements(false)
    document.querySelector('.left_side.animation').style.visibility = 'visible';

    helperTextContainer.style.top = '200%';

    // lightAnimation()
    var left_side = document.querySelector('.left_side')

    target.classList.add('active')


    var buttons = document.querySelectorAll('.air_glass_button_container  img');
    for (var i = 0; i < buttons.length; i++) {
        var src = buttons[i].getAttribute('src').replace(/\d/, '1');
        buttons[i].setAttribute('src', src)
    }
    var src = target.getAttribute('src').replace(/\d/, '2');
    target.setAttribute('src', src)

    if (target.closest('.airButton')) {
        clickedButton = 1
        $(".triangle_bottom").removeClass("triangle_bottom2");
        left_side.classList.remove('GlassAnimation')
    } else {
        clickedButton = 2
        $(".triangle_bottom").addClass("triangle_bottom2");
        left_side.classList.add('GlassAnimation')
    }
    showContent()
    changePercents();
    scrollbarMoveTo();
    lightAnimation();
    showAirCircles();

    // document.querySelector('.begin_helper_text').classList.add('hide')

    var circles = [...document.querySelectorAll('.circle')];
    circles.forEach(c=>{
        c.style.transition = '0s linear'
    })

    setTimeout(function(){
        circles.forEach(c=>{
            c.style.transition = '1s linear';
        })
    },100)
    setTimeout(function () {

        setRemoveAnimationElements(true)

    },10)

})
document.querySelector('.critical_angle_button').addEventListener('click', function (event) {
    document.querySelector('.critical_angle_container').style.display = 'block';
    document.querySelector('.light_controller').style.display = 'none';

})

document.querySelector('.closeHelperText').addEventListener('click', function (event) {
    if(!event.target.closest('img'))return;
    document.querySelector('.critical_angle_container').style.display = 'none';
    document.querySelector('.light_controller').style.display = 'block';
})



function showAirCircles() {

    var top_percent = document.querySelector('.top_percent');
    var bottom_percent = document.querySelector('.bottom_percent');
    top_percent.style.display = 'block';
    bottom_percent.style.display = 'block';

    var show_percent_container = document.querySelector('.show_percent_container');
    show_percent_container.style.opacity = '1';

    var top_init_rot = -41;
    var bottom_init_rot = -37;

    top_init_rot += percent;

    // bottom_init_rot += Math.floor((percent / 1.52));


    if (percent == 75) {
        top_init_rot -= 4;
        bottom_init_rot -= 4;
    } else if (percent < 15) {
        top_percent.style.display = 'none';
        bottom_percent.style.display = 'none';
    }
    var triangle_top = document.querySelector('.triangle_top > div');
    var triangle_bottom = document.querySelector('.triangle_bottom > div');

    triangle_top.style.transform = `rotate(${top_init_rot - 5}deg)`;



    triangle_bottom.style.transform = `rotate(${bottom_init_rot + +get_n_r_air(percent).replace(',', '.') - 4}deg)`;
    if(clickedButton ==1 )
    triangle_bottom.style.transform = `rotate(${bottom_init_rot + +get_n_r_air(percent).replace(',', '.') - 8.5}deg)`;
    if(percent ==60){
        triangle_top.style.transform = `rotate(14deg)`;

        triangle_bottom.style.transform = `rotate(-9deg)`;
        if(clickedButton ==1 )
        triangle_bottom.style.transform = `rotate(${bottom_init_rot + +get_n_r_air(percent).replace(',', '.') - 8}deg)`;
    }

    if(percent >65){
        triangle_top.style.transform = `rotate(28deg)`;
        if(clickedButton ==1 )
        triangle_bottom.style.transform = `rotate(${bottom_init_rot + +get_n_r_air(percent).replace(',', '.') - 7}deg)`;
    }
    if (percent > 80) {

        show_percent_container.style.opacity = '0';
        triangle_top.style.transform = `rotate(40deg)`;

        triangle_bottom.style.transform = `rotate(40deg)`;
    }


}

function setRemoveAnimationElements(bull) {

    var display = 'none';

    bull ? display = 'block' : null;
    document.querySelector('.light_controller').style.display = display

    document.querySelector('.light_from').style.display = display
    document.querySelector('.out_light_container').style.display = display

}



function slideButtonTachEnd(event){
    draggable = false;

    slideButton.style.transition = 'all 0.2s linear'
    scrollbarMoveTo();
    changePercents();
    lightAnimation();
    showAirCircles();
}

document.addEventListener('click', function(event){
    if(showAlert){
        helperTextContainer.style.top = '200%';

        showAlert = false;

        clearTimeout(hideTimer)
        clearTimeout(showTimer)
    };
})
var showTimer
var hideTimer
function slideButtonTachStart(event){
    if (!clickedButton) {

        helperTextContainer.style.top = '181px';
        clearTimeout(showTimer)
        showTimer = setTimeout(function(){
            showAlert = true
        },500)
        clearTimeout(hideTimer)
        hideTimer =  setTimeout(function(){
            showAlert = false;
            helperTextContainer.style.top = '200%';
        },3000)
        return
    }
    draggable = true;
    slideButtonCursor = +(offset(slideButton).left/mashtab - event.clientX/mashtab);
    slideButton.style.transition = 'all 0s'
}

helperTextContainer.addEventListener('click',function(){
    this.style.top = '200%';
    showAlert = true;

    clearTimeout(hideTimer)
    clearTimeout(showTimer)
})

// .Nair_variables o_i_number


document.addEventListener('click',function(event){
    var target = event.target;
    if(target.closest('#checkBtn')){

        window.location.reload(true)
    }
})
setTimeout(function(){

    slideButtonTachStart()
},300)
