
function getPercentageChange(oldNumber, newNumber) {
    var decreaseValue = oldNumber - newNumber;
    return (decreaseValue / oldNumber) * 100;
}

function offset(el) {
    var rect = el.getBoundingClientRect(),
        scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
        scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
}

function showContent() {
    var items = [...document.querySelectorAll('.o_i'), ...document.querySelectorAll('.o_r')];
    items.forEach(function (elem) {
        elem.style.display = 'none';
    })
    if (clickedButton == 1) {
        items[0].style.display = 'flex';
        if (percent < 80) {
            items[2].style.display = 'flex';
        }

    } else if (clickedButton == 2) {
        items[1].style.display = 'flex';
        items[2].style.display = 'flex';
    }
    changePercents()
}


function changePercents() {
    if (!clickedButton) return;
    if (percent == 88) percent = 90;

    var NGlass_variables = document.querySelector('.NGlass_variables')
    NGlass_variables.style.display = 'block';

    var items = [...document.querySelectorAll('.o_i_number')];

    if (clickedButton == 1) {
        items[0].innerHTML = percent;
        items[2].innerHTML = get_n_r_air(percent)
        document.querySelector('.result_text span:last-child').innerHTML = 'Refraction'
        if (percent > 80) {
            NGlass_variables.style.display = 'none';
            items[0].innerHTML = percent;

            document.querySelector('.result_text span:last-child').innerHTML = 'The light ray glides on the boundary between the two media'
        }

    } else {

        items[1].innerHTML = percent

        items[2].innerHTML = get_i_r_Glass(percent);

        document.querySelector('.result_text span:last-child').innerHTML = 'Total internal reflection';
    }
    document.querySelector('.top_percent .top_percent').innerHTML = percent;
    document.querySelector('.bottom_percent .bottom_percent').innerHTML = get_n_r_air(percent);
    document.querySelector('.show_percent_container_bottom .top_percent .top_percent').innerHTML = percent;
    document.querySelector('.show_percent_container_bottom .bottom_percent .bottom_percent').innerHTML = get_i_r_Glass(percent);

}


function scrollbarMoveTo() {
    percent < 3 ? percent = 2 : null;
    percent > 80 && (percent = 88);


    slideButton.style.left = (percent > 70&&percent <80 ? 74:percent )+ 1.5 + '%';
    percent < 3 ? percent = 0 : null;
}


function lightAnimation() {

    bat = document.querySelector('.light_controller img');
    var show_percent_container_bottom = document.querySelector('.show_percent_container_bottom')

    var out_light_container = document.querySelector('.out_light_container')
    var percent1 = percent;
    if (percent1 > 80) percent1 = 90;

    var rotationPercentTopLight = percent1
    var translateX = 2;
    if (percent1 === 90) {
        translateX = .7;
    }
    if(percent1 === 15) {
        rotationPercentTopLight-=14;
        translateX=3;
        var top = clickedButton === 1 ? '42%' : '41%';
        var bottom = clickedButton === 1 ? '247.9px' : '246.9px';
        document.querySelector('.triangle_top').style.top = top;
        document.querySelector('.triangle_bottom').style.top = bottom;
    }
    if(percent1 === 30) {
        rotationPercentTopLight+=2;
        translateX=3;
        var top = clickedButton === 1 ? '41%' : '40%';
        document.querySelector('.triangle_top').style.top = top;
        document.querySelector('.triangle_bottom').style.top = '247.9px';
    }
    if(percent1 === 45) {
        rotationPercentTopLight+=1;
        translateX=2;
        var bottom = clickedButton === 1 ? '248.9px' : '252.9px';
        document.querySelector('.triangle_top').style.top = '40.5%';
        document.querySelector('.triangle_bottom').style.top = bottom;
    }
    if(percent1 === 60) {
        rotationPercentTopLight+=7;
        translateX=2;
        document.querySelector('.triangle_top').style.top = '40%';
        document.querySelector('.triangle_bottom').style.top = '249.9px';
    }
    if(percent1 === 75) {
        rotationPercentTopLight=79;
        translateX=2;
        document.querySelector('.triangle_top').style.top = '40%';
        document.querySelector('.triangle_bottom').style.top = '249.9px';
    }
    if (clickedButton === 1) {
	    document.querySelector('.light_controller').style.transform = `rotate(${percent1}deg) translateY(-${rotationPercentTopLight}px) translateX(${translateX}px)`;
        document.querySelector('.show_percent_container').style.display = 'block';
        document.querySelector('.show_percent_container_bottom').style.display = 'none';

        var circle3 = document.querySelector('.show_percent_container .triangle_top .circle')
        var circle4 = document.querySelector('.show_percent_container .triangle_bottom .circle');


        setTimeout(function(){
            circle3.style.opacity = '1';
            circle4.style.opacity = '1';
        },10)
        var rotation = setAuthLightHeight(percent1)
        if (rotation == 305) {
            rotation -= 30
        } else if (rotation == 259) {

        } else {
            rotation += 20;
        }
        if (percent1 > 15 && percent1 <= 30) {
            document.querySelector('.light_from').style.left = 47 + '%';
        } else if (percent1 > 30 && percent1 <= 75) {
            document.querySelector('.light_from').style.left = 50 + '%';
            bat.style.left = 2 + 'px';
            rotation += 2;
        } else if (percent1 > 75) {
            rotation += 2;
            document.querySelector('.light_from').style.left = 52 + '%';
            bat.style.left = 4 + 'px';

        } else {
            document.querySelector('.light_from').style.left = '26px';
            rotation = 202;
            bat.style.left = 0;
        }
        document.querySelector('.light_from').style.height = rotation + 'px';


        var bottomLightRotate = +get_n_r_air(percent).replace(',', '.');

        // bottomLightScale = 1 + (((bottomLightRotate + bottomLightRotate / 5) / 100))

        out_light_container.style.transform = `rotate(${bottomLightRotate}deg)`;
        out_light_container.style.height = `${setAuthLightHeight(percent1)}px`;
        // items[0].innerHTML = percent;

        if (percent1 === 90) {
            out_light_container.style.transform = `rotate(${90}deg)`;

            out_light_container.style.top = '49.6%'
            out_light_container.style.left = '48%'
        } else {
            out_light_container.style.top = '49.8%';
            out_light_container.style.left = '305.5px';
        }

    } else {
        if (percent1 === 75 || percent1 === 90) {
            document.querySelector('.out_light_container').style.top = "251.3px"
        } else {
            document.querySelector('.out_light_container').style.top = "250.5px"
        }
        document.querySelector('.light_controller').style.transform = `rotate(${-(180 - percent1)}deg) translateY(0) `;
        if(percent>80)document.querySelector('.light_controller').style.transform = `rotate(${-(180 - percent1)}deg) translateY(0) translateX(.8px)`;
        var glassOutPercent = +get_i_r_Glass(percent).replace(',', '.')
        document.querySelector('.out_light_container').style.transform = `rotate(${-(180 - glassOutPercent)}deg) translateY(0)`;
        // document.querySelector('.out_light_container').style.top = '  49.7%';
        document.querySelector('.out_light_container').style.height = setAuthLightHeightGlass(percent) + 'px';
        document.querySelector('.show_percent_container').style.display = 'none';
        show_percent_container_bottom.style.display = 'block';
        show_percent_container_bottom.style.opacity = '1';
        if (percent === 0) {
            show_percent_container_bottom.style.opacity = '0';
        }
        if (percent < 40) {
            document.querySelector('.show_percent_container').style.display = 'block';
            show_percent_container_bottom.style.opacity = '0';
            document.querySelector('.show_percent_container .top_percent .top_percent').innerHTML = get_i_r_Glass(percent);
            document.querySelector('.show_percent_container .bottom_percent .bottom_percent').innerHTML = percent;
            var circle1 = document.querySelector('.show_percent_container .triangle_top .circle')
            var circle2 = document.querySelector('.show_percent_container .triangle_bottom .circle')
            setTimeout(function () {
                var circle3 = document.querySelector('.show_percent_container_bottom .triangle_top .circle')
                var circle4 = document.querySelector('.show_percent_container_bottom .triangle_bottom .circle');
                circle3.style.opacity = '0';
                circle4.style.opacity = '0';
                circle1.style.opacity = '1';
                circle2.style.opacity = '1';
                circle4.style.transform = 'rotate(-45deg)';
                circle3.style.transform = 'rotate(33deg)';
                if (percent == 15) {
                    circle1.style.transform = 'rotate(-21deg)';
                } else if (percent == 30) {
                    circle1.style.transform = 'rotate(4deg)';
                    circle2.style.transform = 'rotate(-15.94deg)';
                }
            })
        } else {
            document.querySelector('.out_light_container').style.transform = `rotate(${-(glassOutPercent)}deg) translateY(0)`;
            var circle3 = document.querySelector('.show_percent_container .triangle_top .circle')
            var circle4 = document.querySelector('.show_percent_container .triangle_bottom .circle')
            var circle1 = document.querySelector('.show_percent_container_bottom .triangle_top .circle')
            var circle2 = document.querySelector('.show_percent_container_bottom .triangle_bottom .circle');
            circle1.style.opacity = '1';
            circle2.style.opacity = '1';
            circle3.style.opacity = '0';
            circle4.style.opacity = '0';

            setTimeout(function () {
                circle3.style.transform = 'rotate(-44deg)';
                circle4.style.transform = 'rotate(-44deg)';
                if (percent === 45) {
                    circle1.style.transform = 'rotate(270deg)';
                    circle2.style.transform = 'rotate(0deg)';
                } else if (percent === 60) {
                    circle1.style.transform = 'rotate(255deg)';
                    circle2.style.transform = 'rotate(14deg)';
                } else if (percent === 75) {
                    circle1.style.transform = 'rotate(242deg)';
                    circle2.style.transform = 'rotate(28deg)';
                }
                else if (percent > 80) {
                    circle1.style.transform = 'rotate(230deg)';
                    circle2.style.transform = 'rotate(41deg)';
                }
            }, 10)
        }

        if (percent > 80) {
            document.querySelector('.show_percent_container_bottom').style.display = 'none';
        }

    showAirCircles()
        document.querySelector('.light_from').style.height = '279px';
    }
}



function setAuthLightHeight(percent) {
    switch (percent) {
        case 15:
            return 203;
        case 30:
            return 213;
        case 45:
            return 225;
        case 60:
            return 244;
        case 75:
            return 255;
        case 90:
            return 268;
        default:
            return 200
    }
}
function setAuthLightHeightGlass(percent) {
    percent == 88 ? percent = 90 : null;
    switch (percent) {
        case 15:
            return 219;
        case 30:
            return 308;
        case 45:
            return 281;
        case 60:
            return 320;
        case 75:
            return 291;
        case 90:
            return 280;
        default:
            return 205
    }
}

function showBottomTriangle(percent) {
    // var init1 = -45;
    // var init2 = 312;
    // var show_percent_container_bottom = document.querySelector('.show_percent_container_bottom');
    // var circles = show_percent_container_bottom.querySelectorAll('.circle');
    // circles[0].style.transform = `rotate(${init2 - (90 - +get_i_r_Glass(percent).replace(',', '.'))}deg)`;
    // circles[1].style.transform = `rotate(${init1 + (90 - percent)}deg)`;
    // 41deg 50deg
}


function get_n_r_air(percent) {
    switch (percent) {
        case 0:
            return '0'
        case 15:
            return '9,8'
        case 30:
            return '19,2'
        case 45:
            return '27,7'
        case 60:
            return '34,7'
        case 75:
            return '39,5'
        default:
            return '90';
    }
}
function get_i_r_Glass(percent) {
    switch (percent) {
        case 0:
            return '0'
        case 15:
            return '23,2'
        case 30:
            return '49,5'
        case 45:
            return '45'
        case 60:
            return '60'
        case 75:
            return '75'
        default:
            return '90';
    }
}
