window.addEventListener("DOMContentLoaded", function() {
    var triangleBase = document.getElementById("triangle-base");
    var squareBase = document.getElementById("square-base");
    var contentBox = document.getElementById("content-box");
    var whiteArea = document.getElementById("white-area");
    var whiteArea_2 = document.getElementById("white-area-2");
    var disc = document.getElementById("disc");
    var triangle = document.getElementById("triangle");
    var triangleBox = document.getElementById("triangle-box");
    var hb = document.getElementById("hb");
    var count = 12;
    var slantDef = 11;
    var mode = 'triangelBase';


    // toggleScreens
    function loadContent(toggle) {
        if(toggle) {
            contentBox.style.background = "url('./assets/Bg2.png') 0% 0% / 100% no-repeat";
            whiteArea.style.display = "none";
            whiteArea_2.style.display = "block";
            disc.innerHTML = "Set the side of base and slant height of a right pyramid with square base and observe the surface area and volume.";
            triangle.src = "./assets/pyramid2.png";
            $("#triangle").css({
                width: 200,
                height: 210
            });
            hb.style.display = "none";
            $(".height-range").attr({
                min: 12,
                max: 67.08,
                value: 12
            });
            $("#slant-value").html("12");
            $(".a-range").attr({value: 10});
            $("#side-value").html("10");
            $(".heightPyramid-range").attr({value: 10});
            $("#height-value").html("10");

        } else if(!toggle) {
            contentBox.style.background = "url('./assets/Bg1.png') 0% 0% / 100% no-repeat";
            whiteArea.style.display = "block";
            whiteArea_2.style.display = "none";
            disc.innerHTML = "Set the side of base and slant height of a right pyramid with equilateral triangle base and observe the surface area and volume.";
            triangle.src = "./assets/pyramid1.png";
            hb.style.display = "block";
            $(".height-range").attr({
                min: 11,
                max: 62.45,
                value: 11
            });
            $("#slant-value").html("11");
            $(".a-range").attr({value: 10});
            $("#side-value").html("10");
            $(".heightPyramid-range").attr({value: 10});
            $("#height-value").html("10");
        }
    }

    // triangle Base button
    triangleBase.addEventListener("click", function(event) {
        event.target.src = "./assets/EquilateraltrianglebaseBt2.png";
        loadContent(false);
        squareBase.src = "./assets/SquarebaseBt1.png";
        count = 12;
        slantDef = 11;
        $('#range-1').attr('min', Math.ceil(slantDef))
        mode = 'triangleBase';
        $("#h").css({left:'13%', top:'64%'});
        $("#hs").css({left:'90%', top:'60%'});
        $("#hb").css({left: '46%', top:'-23px'});
        $("#a").css({left: '25%', bottom: '87%'});
        $("#triangle-box").css({width: '200px',height:' 210px',top: '55%',left:' 15%'});
        $("#triangle").css({width: '200px',height:' 210px',top: '55%',left:' 15%'});
        getHRange();
        setARange();
        MathJax.Hub.Queue(["Typeset", MathJax.Hub], function () {
            $("#formula-1").css({"visibility": "visible"});
            $("#formula-2").css({"visibility": "visible"});
        })
    });

    // square Base button
    squareBase.addEventListener("click", function(event) {
        event.target.src = "./assets/SquarebaseBt2.png";
        loadContent(true);
        triangleBase.src = "./assets/EquilateraltrianglebaseBt1.png";
        count = 4;
        slantDef = 12;
        $('#range-1').attr('min', Math.floor(slantDef))
        mode = 'squareBase';
        $("#h").css({left: '6%', top:'60%'});
        $("#hs").css({left:'82%', top:'60%'});
        $("#a").css({left: '64%', bottom: '92%'});
        $("#triangle-box").css({width: '200px',height:' 210px',top: '55%',left:' 15%'});
        $("#triangle").css({width: '200px',height:' 210px',top: '55%',left:' 15%'});
        getHRange();
        setARange();
        MathJax.Hub.Queue(["Typeset", MathJax.Hub], function () {
            $("#white-area-2 .formula-1").css({visibility:'visible'})
            $("#white-area-2 .formula-2").css({visibility:'visible'})
        })
    });

    // change dot
    function changeDot(x) {
        x = Number(x);
        return x.toString().replace('.', ',');
    }
// ----------------------------------------------------------------------------------

    // range h(s)
    $(".height-range").on("input", function(e){

        // get all parameters
        var a = $('.a-range').val();

        var slant = $('.height-range').val();
        $("#slant-value").html(changeDot(slant));

        // default slant height
        if(slant < slantDef) {
            $("#slant-value").html(slantDef);
            slant = slantDef
        }

        // when user change hS` he change H too
        var hPyramid = Math.sqrt(Math.abs(Math.pow(slant, 2) - (Math.pow($('.a-range').val(), 2))/count)).toFixed(2);
        $(".heightPyramid-range").val(hPyramid);
        if (hPyramid > 10) {
            $("#height-value").html(changeDot(hPyramid));
        }
        else{
            hPyramid = 10
            $("#height-value").html(hPyramid);
        }

        // change a position
        // if (slant <= 25 && mode == 'triangelBase') {
        //     $('#a').css({top: '-5%'});
        // }
        // else if ( slant > 25 && mode == 'triangelBase' ) {
        //     $('#a').css({top: '4%'});
        // }

        // // change a position square base
        // if (slant <= 30 && mode == 'squareBase') {
        //     $('#a').css({left: '65%', bottom: '100%'});
        // }
        // else if ( slant > 30 && mode == 'squareBase' ) {
        //     $('#a').css({left: '65%', bottom: '100%'});
        // }

        var newSlant = setPyramidHeight(slant);
        $('#triangle').css('height', newSlant);
        $('#triangle-box').css('height', newSlant);

        // calculate for formula area
        surfaceArea_1(a, slant);
        volume_1(a, hPyramid);
        surfaceArea_2(a, slant)
        volume_2(a, hPyramid)

        //change formulas span in area
        var pow_a = Math.pow(a, 2).toString().split(".").join(",");
        var a_hs = Math.round10(1/2 * a * slant, -2).toString().split(".").join(",");

        $(".p_hs").html(changeDot(slant));
        $(".p_H").html(changeDot(hPyramid));
        $(".calc_form").html(`= &nbsp; ${spaceNumber1(pow_a)} + 4 &times; ${spaceNumber1(a_hs)}`);
    });
    function setARange (){
        var H = $(".heightPyramid-range").val();
        var a = $('.a-range').val();
        $("#side-value").html(changeDot(a));

        // when user change a` he change slant height too
        var hS =  Math.sqrt(Math.pow($(".heightPyramid-range").val(), 2) + (Math.pow(a, 2))/count).toFixed(2);
        $('.height-range').val(hS);
        $("#slant-value").html(changeDot(hS));

        var newA = setPyramidWidth(a);
        $('#triangle').css('width', newA);
        $('#triangle-box').css('width', newA);

        // change h possition
        if (a <= 20 && mode == 'triangelBase') {
            $('#h').css({left: '0%'});
        }
        else if (a > 20 && mode == 'triangelBase') {
            $('#h').css({left: '15%'});
        }

        // change h possition square base
        if (a > 20 && mode == 'squareBase') {
            $('#h').css({left: '10%'});
        }
        else if (a <= 20 && mode == 'squareBase') {
            $('#h').css({left: '-10%'});
        }

        setPyramidLeft();
        setPyramidLeftBox();

        // calculate for formula area
        surfaceArea_1(a, hS);
        volume_1(a, H);
        surfaceArea_2(a, hS)
        volume_2(a, H)

        //change formulas span in area
        var pow_a = Math.pow(a, 2).toString().split(".").join(",");
        var a_hs = Math.round10(1/2 * a * hS, -2).toString().split(".").join(",");

        $(".p_a").html(changeDot(a));
        $(".p_hb").html(h_b(a).toString().split(".").join(","));
        $(".p_hs").html(changeDot(hS));
        $(".calc_form").html(`= &nbsp; ${spaceNumber1(pow_a)} + 4 &times; ${spaceNumber1(a_hs)}`);
    }
    function getHRange () {
        // get all slider parameters
        var a = $('.a-range').val();
        var pyramidHeight = $(".heightPyramid-range").val();

        $("#height-value").html(changeDot(pyramidHeight));

        // when user change H` he change slant height too
        var slantHeight =  Math.sqrt(Math.pow(pyramidHeight, 2) + (Math.pow($('.a-range').val(), 2))/count).toFixed(2);
        $('.height-range').val(slantHeight);
        $("#slant-value").html(changeDot(slantHeight));

        var newPyramid = setPyramidHeight(pyramidHeight);
        $('#triangle').css('height', newPyramid);
        $('#triangle-box').css('height', newPyramid);

        // change a position
        // if (pyramidHeight <= 25 && mode == 'triangelBase') {
        //     $('#a').css({bottom: '100%'});
        // }
        // else if ( pyramidHeight > 25 && mode == 'triangelBase' ) {
        //     $('#a').css({bottom: '100%'});
        // }

        // change a position square base
        // if (pyramidHeight <= 30 && mode == 'squareBase') {
        //     $('#a').css({left: '65%', bottom: '100%'});
        // }
        // else if ( pyramidHeight > 30 && mode == 'squareBase' ) {
        //     $('#a').css({left: '65%', bottom: '100%'});
        // }

        // calculate for formula area
        surfaceArea_1(a, slantHeight);
        volume_1(a, pyramidHeight);
        surfaceArea_2(a, slantHeight);
        volume_2(a, pyramidHeight);

        //change formulas span in area
        var pow_a = Math.pow(a, 2).toString().split(".").join(",");
        var a_hs = Math.round10(1/2 * a * slantHeight, -2).toString().split(".").join(",");

        $(".p_hs").html(changeDot(slantHeight));
        $(".p_H").html(changeDot(pyramidHeight));
        $(".calc_form").html(`= &nbsp; ${spaceNumber1(pow_a)} + 4 &times; ${spaceNumber1(a_hs)}`);
    }
    // range a
    $(".a-range").on("input", setARange);

    // range H
    $(".heightPyramid-range").on("input", getHRange);

    // function for set pyramid height
    function setPyramidHeight(slantValue) {
        var newHeight = (200 / 40) * parseInt(slantValue);
        if(newHeight < 90) {
            return 90;
        } else if (newHeight > 270) {
            return 270;
        }
        return newHeight;
    }

    // function for set pyramid width
    function setPyramidWidth(sideVal) {
        var newWidth = (300 / 40) * parseInt(sideVal);
        if(newWidth < 100) {
            return 100;
        } else if (newWidth  > 400) {
            return 400;
        }
        return newWidth;
    }

    //set pyramid position
    function setPyramidLeft() {
        var width = Number(dropPX($("#triangle").css("width")));
        if(width >= 100 && width <= 180) {
            triangle.style.left = "14%";
        } else if(width > 180 && width <= 210){
            triangle.style.left = "13%";
        } else if(width > 210 && width <= 250) {
            triangle.style.left = "12%";
        } else if(width > 250 && width <= 300) {
            triangle.style.left = "11%";
        } else if(width > 300 && width <= 330) {
            triangle.style.left = "10%";
        } else if(width > 330 && width <= 360) {
            triangle.style.left = "9%";
        } else if(width > 360 && width <= 400) {
            triangle.style.left = "8%";
        }
    }

    //set box position
    function setPyramidLeftBox() {
        var width = Number(dropPX($("#triangle-box").css("width")));
        if(width >= 100 && width <= 180) {
            triangleBox.style.left = "14%";
        } else if(width > 180 && width <= 210){
            triangleBox.style.left = "13%";
        } else if(width > 210 && width <= 250) {
            triangleBox.style.left = "12%";
        } else if(width > 250 && width <= 300) {
            triangleBox.style.left = "11%";
        } else if(width > 300 && width <= 330) {
            triangleBox.style.left = "10%";
        } else if(width > 330 && width <= 360) {
            triangleBox.style.left = "9%";
        } else if(width > 360 && width <= 400) {
            triangleBox.style.left = "8%";
        }
    }

    // dropping px from value
    function dropPX(val) {
        return val.substr(0, val.length-2);
    }

    // h_b calculate
    function h_b(a){
        var HB = Number(Math.sqrt(Math.pow(a, 2) - Math.pow(a/2, 2)).toFixed(2));
        return HB;
    }

    // surface area 1 calculate
    function surfaceArea_1(a, hS){
        var hb = h_b(a);
        var surface = Math.round10((a/2 * hb + 3 * (a/2 * hS)), -1);
        var res = spaceNumber(surface).split(".").join(",");
        $(".sur1Res").empty().parent('.spanTxt').css({visibility: 'hidden'});
        $(".sur1Res").append(`$$ { = \\space ${res} } $$`);
        MathJax.Hub.Queue(["Typeset", MathJax.Hub], function () {
            if($(".sur1Res").find('span').length > 0){
                $(".sur1Res").parent('.spanTxt').css({visibility: 'visible'});
            }
        });
    }

    // volume 1 calculate
    function volume_1(a, H){
        var hb = h_b(a);
        var volume = Math.round10((1/3 * a/2 * hb * H), -2);
        var res = spaceNumber(volume).split(".").join(",");
        $(".vol1Res").empty().parent('.spanTxt').css({visibility: 'hidden'});
        $(".vol1Res").append(`$$ { = \\space ${res} } $$`);
        MathJax.Hub.Queue(["Typeset", MathJax.Hub], function () {
            if($(".vol1Res").find('span').length > 0){
                $(".vol1Res").parent('.spanTxt').css({visibility: 'visible'});
            }
        });
    }

    // square surface area 2 calculate
    function surfaceArea_2(a, hS){
        var surface = Math.round10((Math.pow(a, 2) + 4 * (a/2 * hS)), -1);
        var res = spaceNumber(surface).split(".").join(",");
        $(".sur2Res").empty().parent('.spanTxt').css({visibility: 'hidden'});
        $(".sur2Res").append(`$$ { = \\space ${res} } $$`);
        MathJax.Hub.Queue(["Typeset", MathJax.Hub], function () {
            if($(".sur2Res").find('span').length > 0){
                $(".sur2Res").parent('.spanTxt').css({visibility: 'visible'});
            }
        });
    }

    // square volume 2 calculate
    function volume_2(a, H){
        var volume = Math.round10((1/3 * Math.pow(a, 2) * H), -2);
        var res = spaceNumber(volume).split(".").join(",");
        $(".vol2Res").empty().parent('.spanTxt').css({visibility: 'hidden'});
        $(".vol2Res").append(`$$ { = \\space ${res} } $$`);
        MathJax.Hub.Queue(["Typeset", MathJax.Hub], function () {
            if($(".vol2Res").find('span').length > 0){
                $(".vol2Res").parent('.spanTxt').css({visibility: 'visible'});
            }
        });
    }

    // make space if result length > 4
    function spaceNumber1(x){
        var z = x.toString();
        var b =  z.indexOf(',') != -1 ? z.split(',')[1] : z.split('.')[1];
        z = z.indexOf(',') != -1 ? z.split(',')[0] : z.split('.')[0];
        if (z.length >= 4) {
            var c = z.substring(0,z.length-3);
            var d = '';
            if(c.length > 3){
                d = c.substring(0,c.length-3)
                c = c.substring(c.length-3,c.length)
                d+=" ";
            }
            var y = z.substring(z.length-3, z.length);
            if(b) y = y+','+b; 
            return d+c+" "+y;
        } else {
            if(b) return z + ',' + b;
            return z;
        }
    }
    function spaceNumber(x){
        var z = x.toString();
        var b = z.split('.')[1];
        z = z.split('.')[0];
        if (z.length >= 4) {
            var c = z.substring(0,z.length-3);
            var d = '';
            if(c.length > 3){
                d = c.substring(0,c.length-3)
                c = c.substring(c.length-3,c.length)
                d+="\\space";
            }
            var y = z.substring(z.length-3, z.length);
            if(b) y = y+','+b; 
            return d+c+"\\space"+y;
        } else {
            if(b) return z + ',' + b;
            return z;
        }
    }

    // Math random10 function.
    Math.round10 = function(value, exp) {
        return decimalAdjust('round', value, exp);
    };
    function decimalAdjust (type, value, exp) {
        if (typeof exp === 'undefined' || +exp === 0) {
          return Math[type](value);
        }
        value = +value;
        exp = +exp;
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
          return NaN;
        }
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    };

//end
setARange();
getHRange();
});

$(document).ready(function () {
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output], function () {
      $("#formula-1").css({"visibility": "visible"});
      $("#formula-2").css({"visibility": "visible"});
      $("#triangle-box").css({"visibility": "visible"});
    })
  })