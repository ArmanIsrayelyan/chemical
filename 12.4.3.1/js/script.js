var zoomScale = 1;
var Page = function () {
	this.mainContent = null;
    this.page = 1;
    this.massOfCrate = [1, 2, 3, 4, 5];
    this.angleOfIncline = [10, 20, 30, 45];
    this.apliedForce = [0, 2, 4, 6, 8, 10];
    this.AllslideData = ['apliedForce', 'angleOfIncline', 'massOfCrate'];
    this.helcopterHeightDefault = 250;
    this.slideMoveIndex = 0;
    this.manMass = 60;
    this.minTensionInCableValue = null;
    this.waterHeight = 30;
    this.switchButton = false;
    this.CoeffStatic = 0.6;
    this.CoeffKinetic = 0.5;
    this.go = false;
}
Page.prototype = {
    init: function () {
        this.cleareBackgroundContent();
    	this.addBackgroundContent();
        $('div[tab-id=1] div[tab-id-m=1]').click();
        $('div[tab-id=2] div[tab-id-m=1]').click();
        $('.switch-button').click();
    },
    addMainBlock: function (contents) {
        var self = this;
        var content = $('#contentImg');
        if(content.length > 0) {
            self.mainContent = content;
            content.append(contents);
            return true;
        } else {
            console.error('Not found div#contentImg block');
            return false;
        }
    },
    cleareBackgroundContent: function () {
        var self = this;
        var content = $('#contentImg');
        if(content.length > 0) {
            $('#contentImg').html('');
        }else {
            console.error('Not found div#contentImg block');
        }
    },
    addBackgroundContent: function () {
        var self = this;
        template = '<div class="left-block"></div>\
                    <div class="right-block"></div>\
                    <div class="bottom-block"></div>';
        if(self.addMainBlock(template)) {
            self.addSlides();  
            self.addSwitchBlock();
            self.addTabs();
            self.addLeftContent();
            self.addInformationButton();
        }
    },
    addLeftContent: function () {
        var self = this;
        var template = '<div class="triangle"></div>\
                        <div class="box">\
                            <div class="arrows"></div>\
                            <div class="arrow-text"></div>\
                        </div>';
        self.addLeftBlock(template);
        self.changeTriangle(this.getSlideValue(1));
        self.changeBox(this.getSlideValue(2));
        self.addArrows();
        self.addArrowText();
        self.changeSlideEvent();
    },
    getArrowsTamplate: function () {
        var template = '<div class="blue-arrow-text"></div>\
                        <div class="green-arrow-text disabled">\
                            <b>F</b>\
                            <span index><sub>applied</sub></span>\
                            <span class="formulaE"></span>\
                        </div>\
                        <div class="purple-arrow-text disabled">\
                            <b>F</b>\
                            <span index><sub>friction</sub></span>\
                            <span class="formulaE"></span>\
                        </div>';
        return template;
    },
    addArrows: function () {
        var template = '<div class="blue-arrow"><div></div><div></div></div>\
                        <div class="green-arrow disabled"><div></div><div></div></div>\
                        <div class="purple-arrow disabled"><div></div><div></div></div>';
        $('.arrows').append(template);
    },
    addArrowShowValue: function () {
        var template = '<div class="arrow-show-value">'+this.getArrowsTamplate()+'</div>';
        this.addLeftBlock(template);
        var text = '` w `';
        MathJax.HTML.addElement(document.querySelector('.arrow-show-value .blue-arrow-text'), "b", {id: 'omega1'}, [text]);
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'omega1']);
        $('.blue-arrow-text').append('<span equal>=</span> <span class="formulaE"></span>')
        this.calculateArrowText()
    },
    addArrowText: function () {
        var template = this.getArrowsTamplate();
        $('.arrow-text').append(template);
        var text = '` w `';
        MathJax.HTML.addElement(document.querySelector('.blue-arrow-text'), "b", {id: 'omega'}, [text]);
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'omega']);
        $('.blue-arrow-text').append('<span equal>=</span> <span class="formulaE"></span>')
    },
    showArrow: function (select){
        $('.'+select+'-arrow').removeClass('disabled');
        $('.'+select+'-arrow-text').removeClass('disabled');
    },
    hideArrow: function (select){
        $('.'+select+'-arrow').addClass('disabled');
        $('.'+select+'-arrow-text').addClass('disabled');
    },
    showArrowText: function (select){
        $('.'+select+'-arrow-text').removeClass('disabled');
    },
    hideArrowText: function (select){
        $('.'+select+'-arrow-text').addClass('disabled');
    },
    addLeftBlock: function (template) {
    	var self = this;
        var content = $('.left-block');
        if(content.length > 0) {
            content.append(template);
            return true;
        } else {
            console.error('Not found div.left-block block');
            return false;
        }
    },
    addBottomBlock: function (template) {
        var self = this;
        var content = $('.bottom-block');
        if(content.length > 0) {
            content.append(template);
            return true;
        } else {
            console.error('Not found div.bottom-block block');
            return false;
        }
    },
    addOnRightBlock: function (template) {
    	var self = this;
        var content = $('.right-block');
        if(content.length > 0) {
            content.append(template);
            return true;
        } else {
            console.error('Not found div.right-block block');
            return false;
        }
    },
    addSwitchBlock: function () {
        var template = '<div class="switch-block">\
                            <div class="switch-content">\
                                <div class="switch-button"></div>\
                            </div>\
                        </div>';
        this.addBottomBlock(template);
        this.addSwitchEvent();
    },
    getSlideValue: function (dataIndex) {
    	var elem = '.slide_index_'+(dataIndex+1);
    	var slideLeft = parseInt($(elem + ' .slide-button').css('left').replace('px', ''));
    	var data = this[this.AllslideData[dataIndex]];
    	var elWidth = $(elem+' .slide-text').width();
    	var mashtab = (elWidth-44)/(data.length-1);
        var index = Math.round((slideLeft-10)/mashtab);
        while(data[index] == undefined) {
            index--;
            if(index <= 0) {
                index = 0;
                break;
            }
        }
    	return data[index];
    },
    addSlides: function () {
    	var self = this;
    	for (var i = 0; i < 3; i++) {
    		self.addSlideBlock(i);
    		self.addEventSlide(i);
    		$('.slide_index_'+(i+1)+' .slide-button').css({left:'10px'});
    	}
    	self.addEventMouseUpSlide();
    },
    addSlideBlock: function (i) {
    	var self = this;
        var plusText = null;
        if(i==1) plusText='^\circ';
		var template = '<div class="slide-block slide_index_'+(i+1)+'">\
							<div class="slide-text"></div>\
			    			<div class="slide-button"></div>\
			    		</div>';
		self.addBottomBlock(template);
		var slideScaleValue = self[self.AllslideData[i]];
		self.addScale('.slide_index_'+(i+1), slideScaleValue,plusText);
    },
    addScale: function (elem, data,plusText) {
    	if(data && data.length && data.length == 0) return; // if empty  data
    	var active = 'active';
        var elWidth = $(elem+' .slide-text').width();
    	var mashtab = (elWidth-44)/(data.length-1);
    	var template = "";
    	for(var i = 0; i < data.length; i++){
            var textSize = 11
            if(data[i]<10) textSize=17; 
    		var left = textSize+(mashtab * i);
            if(plusText){
                // MathJax.HTML.addElement(document.querySelector(elem).querySelector('.slide-text'), "span", {id: 'degree'+i}, [''+data[i]+''+'`'+plusText+'`']);
                // MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'degree'+i]);
                // $('#degree'+i).addClass('scale-text').css({left: left+'px'}).addClass(active)
                template+='<span class="scale-text '+active+'" style="left: ' + left + 'px">'+data[i]+'<span>&#176;</span></span>';
                active = ''
            }else{
                template+='<span class="scale-text '+active+'" style="left: ' + left + 'px">'+data[i]+'</span>';
                active = '';
            }
        }
        if(template!=='') $(elem+' .slide-text').html(template);
    },
    addSwitchEvent: function (callback = function () {}) {
        var self = this;
        $(document).off('click','.switch-content').on('click','.switch-content', function (e) {
            if(self.go) return;
            var left = $('.switch-button').css('margin-left').replace('px', '');
            if(left == 4) {
                left = 40;
                self.switchButton = true;
                $('.switch-block').css({backgroundImage:'url(./img/down_btn.png)'})
            } else {
                left = 4;
                $('.switch-block').css({backgroundImage:'url(./img/up_btn.png)'})
                self.switchButton = false;
            }
            self.switchButton ? $('.green-arrow').addClass('down') : $('.green-arrow').removeClass('down')
            $('.switch-button').css('margin-left', left);
            setTimeout(function () {
                callback();
            }, 200);
            self.checkArrow();
        })
    },
    activateGoButton: function () {
        $('.go').attr('disabled', false);
        this.addEventGoButton();
    },
    addEventGoButton: function () {
        var self = this;
        $(document).off('click', '.go').one('click', '.go', function () {
            self.showArrow('purple');
            $('.go').attr('disabled', true);
            self.doAnimate();
            self.setTableValue();
            self.enableInformationButton();
            self.go = true;
            $('.switch-content').css({cursor: 'unset'});
            $('.slide-block').addClass('disable_pointter');
        })
    },
    addEventSlide: function (i) {
    	var self = this;
    	$(document).off('mousedown touchstart', '.slide_index_'+(i+1)+' .slide-button').on('mousedown touchstart', '.slide_index_'+(i+1)+' .slide-button', function (e) {
            if(self.go) return;
    		self.slideMove = true;
    		self.slideMoveIndex = (i+1);
    	})
    	$(document).off('mousemove touchmove', '.slide-block.slide_index_'+(i+1)).on('mousemove touchmove', '.slide-block.slide_index_'+(i+1), function (e) {
            if(self.go) return;
            var blockWidth = $('.slide-block.slide_index_'+(i+1)).width();
            var clientX = e.originalEvent.touches ? e.originalEvent.touches[0].clientX : e.originalEvent.clientX;
			var r = $('.slide-block.slide_index_'+(i+1))[0].getBoundingClientRect();
			var newLeft = clientX/zoomScale - r.left / zoomScale;
            if(self.slideMoveIndex == (i+1) && self.slideMove && newLeft < blockWidth-15 && newLeft > 10){
	    		if(newLeft > blockWidth-20){
	    			newLeft = blockWidth-15;
	    		} else if (newLeft < 5){
	    			newLeft = 0;
	    		}
    			$('.slide_index_'+(i+1)+' .slide-button').css({left: newLeft+'px'});
    		}
    	})
    	$(document).off('click','.slide-block.slide_index_'+(i+1)).on('click', '.slide-block.slide_index_'+(i+1), function (e) {
            if(self.go) return;
    		self.slideMoveIndex = i+1;
    		var left = parseInt(e.offsetX)
    		if($(e.target).hasClass('slide-button')) {
    			left = parseInt($(e.target).css('left').replace('px', ''));
    		}
            self.slideMove = false;
    		var newLeft = self.getCorectPosition(left);
    		$('.slide_index_'+self.slideMoveIndex+' .slide-button').css({left: newLeft+'px'});
    		self.changeSlideEvent();
    		self.slideMoveIndex = 0;
    	})
    },
    addEventMouseUpSlide: function () {
    	var self = this;
    	$(document).on('mouseup touchend', 'body', function (e) {
            if(!self.slideMove) return
			var newLeft = self.getCorectPosition();
			$('.slide_index_'+self.slideMoveIndex+' .slide-button').css({left: newLeft+'px'});
    		self.changeSlideEvent();
			self.slideMove = false;
    	})
    },
    getWnet: function (){
        var self = this;
        var WindexP = self.getWIndexParalel();
        var WindexA = self.getWIndexApplied();
        var WindexF = self.getWIndexFriction();
        return (WindexP+WindexA+WindexF);
    },
    getPotencal: function (){
        var self = this;
        var acceleration = this.getAcceleration();
        var degree = this.getSlideValue(1);
        var hI = acceleration == 0? 0 : (Math.round((2.5*Math.tan(Math.PI/180*degree))*100)/100) * (acceleration < 0? 1 : -1);
        return Math.round((self.getSlideValue(2)*9.8*hI)*100)/100;
    },
    getKinetik: function (){
        var self = this;
        return Math.round((0.5*self.getSlideValue(2)*(Math.pow(Math.round(Math.sqrt(Math.abs(2*self.getD(this.getSlideValue(1))*this.getAcceleration()))*1000)/1000,2)))*100)/100;
    },
    minusSign: function (num) {
        if(num<0) return '('+num+')';
        else return num;
    },
    mathJaxQueue(elemName, elemID, callback, elemForCallback){
        $(elemID).css({visibility:"hidden"})
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, elemName], function (){
            if($(elemID).find('span').length > 0) {
                // $(elemID).css({visibility: 'visible'});
                if(callback) callback(elemForCallback);
            }
        });
    },
    setValuePart1: function (){
        var self = this;
        var WindexP = self.getWIndexParalel();
        var WindexA = self.getWIndexApplied();
        var WindexF = self.getWIndexFriction();
        $('div[tab-id=1] div.tab').html('');
        var elem = $('div[tab-id=1] div.tab')[0];
        $(elem).css({visibility:'hidden'})
        $(elem).find('*').css({visibility:'hidden'})
        var elem2  = document.createElement('span');
        elem2.id = 'formulaW';
        elem.append(elem2)
        $(elem2).attr('formula','formula')
        MathJax.HTML.addElement(elem2, "span", {id: 'equal'}, ['`=`']);
        self.mathJaxQueue('equal', '#equal');
        MathJax.HTML.addElement(elem2, "span", {id: 'WindexP',className:'ep'}, ['`' + self.minusSign(WindexP).toString().replace('.',',') + '`']);
        self.mathJaxQueue('WindexP', '#WindexP');
        $(elem2).append('<span class="plus-c"> + </span>');
        MathJax.HTML.addElement(elem2, "span", {id: 'WindexA',className: 'wnc'}, ['`'+ self.minusSign(WindexA).toString().replace('.',',') +'`']);
        self.mathJaxQueue('WindexA', '#WindexA');
        $(elem2).append('<span class="plus-c"> + </span>');
        MathJax.HTML.addElement(elem2, "span", {id: 'WindexF',className: 'wnc'}, ['`'+self.minusSign(WindexF).toString().replace('.',',')+'`']);
        self.mathJaxQueue('WindexF', '#WindexF');
        $(elem2).append('<span class="plus-c"> + 0 + 0</span>');
        var formulaE = Math.round((WindexP+WindexA+WindexF)*100)/100;
        formulaE = formulaE.toString().replace('-', '−').replace('.',',')
        $('div[tab-id=1] div.tab').append('<span formulaEqual class="ek hiddenB u">'+formulaE+' <span style="color: #000"> J</span></span>')
        $('.u').prepend($('<span>`=`</span>').attr('id', 'equal2').css({color: '#000'}));
        self.mathJaxQueue('equal2', '#equal2', (elemForCallback)=>{
            $(elemForCallback).css({visibility:'visible'})
            $(elemForCallback).find('span').css({visibility:'visible'})
        },elem);
    },
    setValuePart2: function(){
        var self = this;
        var massKG = self.getSlideValue(2);
        var g = 9.8;
        var hF = 0;
        var degree = self.getSlideValue(1);
        var acceleration = this.getAcceleration();
        var hI = acceleration == 0? 0 : (Math.round((2.5*Math.tan(Math.PI/180*degree))*100)/100) * (acceleration < 0? -1 : 1);
        var formula = '('+massKG +')('+ g+')('+hF+ (acceleration < 0? '+' : '-');
        var formulaE = Math.round((massKG*g*(hF-hI))*100)/100;
        $('div[tab-id=1] div.tab').html('');
        var elem = $('div[tab-id=1] div.tab')[0];
        $(elem).css({visibility:'hidden'})
        var elem2  = document.createElement('span');
        $(elem2).attr('formulaP','formulaP');
        elem.append(elem2)
        MathJax.HTML.addElement(elem2, "span", {id: 'equal3'}, ['`=`']);
        self.mathJaxQueue('equal3', '#equal3');
        MathJax.HTML.addElement(elem2, "span", {id: 'formulaTab12'}, ['`' + formula.toString().replace('.',',') + '`']);
        self.mathJaxQueue('formulaTab12', '#formulaTab12');
        MathJax.HTML.addElement(elem2, "span", {id: 'formulaTab12h',className: 'hiddenB'}, ['`' + Math.abs(hI).toString().replace('.',',')+ '`']);
        self.mathJaxQueue('formulaTab12h', '#formulaTab12h');
        MathJax.HTML.addElement(elem2, "span", {id: 'formulaTab12p'}, ['`)`']);
        self.mathJaxQueue('formulaTab12p', '#formulaTab12p');
        MathJax.HTML.addElement(elem, "span", {id: 'formulaEqualtab12e'}, ['`'+formulaE.toString().replace('.',',')+'`']);
        self.mathJaxQueue('formulaEqualtab12e', '#formulaEqualtab12e');
        $('#formulaEqualtab12e').addClass('ep').attr('formulaEqualP','formulaEqualP').append('<span style="color: #000"> J</span>')
        $('#formulaEqualtab12e').prepend($('<span>`=`</span>').attr('id', 'equal4').css({color: '#000'}));
        self.mathJaxQueue('equal4','#equal4',(elemForCallback)=>{
            $(elemForCallback).css({visibility:'visible'})
            $(elemForCallback).find('span').css({visibility:'visible'})
        },elem);
    },
    setValuePart3: function(){
        var self = this;
        var massKG = self.getSlideValue(2);
        var vI = 0;
        var vF = Math.round(Math.sqrt(Math.abs(2*self.getD(this.getSlideValue(1))*this.getAcceleration()))*1000)/1000;
        var formula = '`=1/2 ('+massKG+' )( '+vF+'^2 - '+vI+'^2 )`';
        var formulaE = (Math.round((0.5*massKG*(vF*vF-vI*vI))*100)/100).toString().replace('.',',');
         $('div[tab-id=2] div.tab').html('');
         var elem = $('div[tab-id=2] div.tab')[0];
        $(elem).css({visibility:'hidden'})
        MathJax.HTML.addElement(elem, "span", {id: 'formulaP'}, [formula.toString().replace('.',',')]);
        self.mathJaxQueue('formulaP', '#formulaP');
        $('div[tab-id=2] div.tab').append('<span formulaEqualP class="ek hiddenB">'+formulaE+' <span style="color: #000"> J</span></span>')
        $('div[tab-id=2] div.tab span[formulaEqualP]').prepend($('<span>`=`</span>').attr('id', 'equal6').css({color: '#000'}));
        self.mathJaxQueue('equal6', '#equal6',(elemForCallback)=>{
            $(elemForCallback).css({visibility:'visible'})
            $(elemForCallback).find('span').css({visibility:'visible'})
        },elem)
    },
    setValuePart4: function(){
        var self = this;
        var k = self.getKinetik();
        var p = self.getPotencal()
        var formulaE = Math.round((k+p)*100)/100;
        $('div[tab-id=2] div.tab').html('');
        var elem = $('div[tab-id=2] div.tab')[0];
        $(elem).css({visibility:'hidden'})
        var elem2  = document.createElement('span');
        elem2.id = 'formulaW';
        elem.append(elem2)
         MathJax.HTML.addElement(elem2, "span", {id: 'equal7'}, ['`=`']);
        self.mathJaxQueue('equal7', '#equal7');
        MathJax.HTML.addElement(elem2, "span", {id: 'formulaW1',className:'ek hiddenB'}, ['`' + self.minusSign(k).toString().replace('.',',') + '`']);
        self.mathJaxQueue('formulaW1', '#formulaW1');
        $(elem2).append('<span class="plus-c"> + </span>');
        MathJax.HTML.addElement(elem2, "span", {id: 'formulaW2',className: 'ep hiddenB'}, ['`'+ self.minusSign(p).toString().replace('.',',') +'`']);
        self.mathJaxQueue('formulaW2', '#formulaW2');
        // console.log("k:",k,"p:",p);
        MathJax.HTML.addElement(elem, "span", {id: 'formulaEqualW',className: 'wnc hiddenB'}, ['`'+self.minusSign(formulaE).toString().replace('.',',')+'`']);
        self.mathJaxQueue('formulaEqualW', '#formulaEqualW',(elemForCallback)=>{
            $(elemForCallback).css({visibility:'visible'})
            $(elemForCallback).find('span').css({visibility:'visible'})
        },elem);
    },
    setPartValue: function(part){
        var self = this;
        switch(part){
            case 1:
                self.setValuePart1();
            break;
            case 2:
                self.setValuePart2();
            break;
            case 3:
                self.setValuePart3();
            break;
            case 4:
                self.setValuePart4();
            break;
        }
    },
    setTableValue: function () {
        var self = this;
        if($('div[tab-id=1] .tab-menu div[tab-id-m=1]').hasClass('active')){
            self.setValuePart1();
        }else if($('div[tab-id=1] .tab-menu div[tab-id-m=2]').hasClass('active')){
            self.setValuePart2();
        }
        if($('div[tab-id=2] .tab-menu div[tab-id-m=1]').hasClass('active')){
            self.setValuePart3();
        }else if($('div[tab-id=2] .tab-menu div[tab-id-m=2]').hasClass('active')){
            self.setValuePart4();      
        } 
    },
    changeSlideEvent: function () {
    	switch(this.slideMoveIndex){
    		case 1:
                (this.getSlideValue(0) != 0) ? this.showArrow('green') : this.hideArrow('green')
                this.showArrow('purple');
    		break;
    		case 2:
                this.changeTriangle(this.getSlideValue(1));
                this.showArrow('purple');
    		break;
    		case 3:
                this.changeBox(this.getSlideValue(2))
                this.showArrow('purple');
    		break;
        }
        this.checkArrow();
        this.calculateArrowText();
        if(!this.go) this.activateGoButton();
    },
    checkArrow: function () {
        var frictionForceStatic = this.getFrictionForceStatic();
        if(frictionForceStatic < 0) {
            $('.purple-arrow').addClass('down');
        } else {
            $('.purple-arrow').removeClass('down');
        }
    },
    getCorectPosition: function (leftValue = null, f = function (){}) {
    	var self = this;
        var elem ='.slide_index_'+self.slideMoveIndex;
    	var data = self[self.AllslideData[self.slideMoveIndex-1]];
    	var left = 10;
    	if(leftValue){
    		left = leftValue;
    	} else {
            left = parseInt($(elem + ' .slide-button').css('left').replace('px', ''));
        }
		var newLeft = 0;
		var elWidth = $(elem+' .slide-text').width();
    	var mashtab = (elWidth-44)/(data.length-1);
    	$(elem + ' .slide-text span').removeClass('active')
    	newLeft = Math.round(left/mashtab) * mashtab;
        newLeft = (Math.round(newLeft*100)/100)
    	if(left<10) newLeft = 0;
        else if(left>elWidth-10 || left > 270) newLeft=(data.length-1)*mashtab;
        $(elem + ' .slide-text span:nth-child('+Math.round(newLeft/mashtab+1)+')').addClass('active')
    	f();
		return newLeft+10;
    },
	
	addTabs: function () {
		var self = this;
		var tamplate = '<div class="tabs-content">\
                            <div tab-id="1">\
                                <div class="tab-menu">\
                                    <div tab-id-m="1"><span>Net work done</span></div>\
                                    <div tab-id-m="2"><span>Change in potential energy</span></div>\
                                </div>\
                                 <div class="tab"></div>\
							     <div class="tab2"></div>\
							</div>\
                            <div tab-id="2">\
                                <div class="tab-menu">\
                                    <div tab-id-m="1"><span>Change in kinetic energy</span></div>\
                                    <div tab-id-m="2"><span>Work done by non-conservative forces</span></div>\
                                </div>\
                                <div class="tab"></div>\
                                <div class="tab2"></div>\
                            </div>\
						</div>';
		self.addOnRightBlock(tamplate);
		self.addTabsEvent();
	},
	addTabBody: function () {
		var self = this;
		var tamplate = '<div class="tabs-body">\
						</div>';
		self.addOnRightBlock(tamplate);
	},
	addTabsEvent: function () {
		var self = this;
		$(document).off('click', '.tab-menu div').on('click', '.tab-menu div', function (e) {
            var tabId = $(e.target).parent().parent().attr('tab-id');
            var partId = $(e.target).attr('tab-id-m');
            var elem = '.tabs-content div[tab-id='+tabId+']';
			$(elem + ' .tab2').css({
                visibility : 'hidden'
            })
            $(elem+' .tab').html('');
            $(elem+' .tab-menu div').removeClass('active')
            $(elem+' div[tab-id-m='+partId+']').addClass('active');
            var part = 0;
            if(partId=='1' && tabId=='1'){
                // $(elem+' .tab').html($('.math-jax-texts #tabText_1').clone());
                $(elem+' .tab2').html(`<span class='tabs-tab-1'> 
                                                $$
W_{\\mathrm{net}}=W_{w_{ \\|}}+W_{\\mathrm{applied}}+W_{\\mathrm{friction}}+W_{w_{\\perp}}+W_{\\mathrm{normal}}
$$ 
                                            </span>`);
                part = 1;
            }else if(partId=='1' && tabId=='2'){
                $(elem+' .tab2').html(`<span class='tabs-tab-3'>
                                         $$ { \\triangle E_\\mathrm{k} = \\frac{1}{2} m(v_\\mathrm{f}^2 - v_\\mathrm{i}^2) } $$ 
                                        </span> `);
                part = 3;
                // MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output]);
            }else if(partId=='2' && tabId=='1'){
                $(elem+' .tab2').html(`<span class='tabs-tab-2'>
                                         $$ { \\triangle E_\\mathrm{p} =  mg(h_\\mathrm{f} - h_\\mathrm{i}) } $$ 
                                        </span> `);
                part = 2;
                // MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output]);
            }else if(partId=='2' && tabId=='2'){
                $(elem+' .tab2').html(`<span class='tabs-tab-4'> 
                                                $$ { W_\\mathrm{nc} =  \\triangle E_\\mathrm{k} + \\triangle E_\\mathrm{p} } $$ 
                                            </span>`);
                // MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output]);
                part = 4;

            }
            // MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output]);
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output], function (){
                if($('.tab2').find('span').length > 1) {
                    $('.tab2').css({visibility: 'visible'});
                }
            });
            if(self.go) self.setPartValue(part);
            $('.hiddenB').removeClass('hiddenB');
            self.addCssStyle("#formulaP .mjx-mrow:nth-child(3) .mjx-msup:nth-child(2) span[style='padding-top: 0.373em; padding-bottom: 0.373em;']", '{visibility: visible !important;}' );
		})
	},
	addCssStyle: function (selector, text) {
        var style = document.createElement('style');
        var text = document.createTextNode(selector+text);
        $(style).attr('data-type', 'hiddenB');
        style.appendChild(text);
        $('head').append(style);
    },
    addMathText: function (pageHelpText, helpList) {
		var ol = document.createElement('ol');
		ol.className = 'help-list';
		$(pageHelpText).html(ol);
		for(var i = 0; i< helpList.length; i++) {
			var id = 'li'+i+(new Date().getTime());
			MathJax.HTML.addElement(ol, "li", {id: id}, [helpList[i]]);
			MathJax.Hub.Queue(["Typeset", MathJax.Hub, id]);
		}
	},
	// set and get header, description, help list
	setInstructionHelp: function (select) {
		var self = this;
		var helpList = self.getInstructionList();
		$(select || self.pageHelpText).html('');
		self.pageHelpText = select || self.pageHelpText;
		self.addMathText(self.pageHelpText, helpList);
		$(select || self.pageHelpText).parent().removeClass('help_list_' + self.page - 1).addClass('help_list_' + self.page);
		
	},
	setDescription: function (select, description) {
		var self = this;
		$(select || self.pageDescription).html('');
		var description = self.getDescription();
		var id = Math.random()*(new Date().getTime());
		MathJax.HTML.addElement($(select || self.pageDescription)[0], "span", {id: 'description'}, [description]);
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'description']);
		self.pageDescription = select || self.pageDescription;
	},
	getDescription: function () {
		var self = this;
		var description = "";
		switch (self.page){
			case 1:
				description = "Set the different parameters for the crate that is moving on the inclined surface.";
			break;
		}
		return description;
	},
	getInstructionList: function () {
		var self = this;
		var helpList = "";
		switch (self.page){
			case 1:
			helpList = ['The crate can move up or down the inclined surface. The length of the base of the inclined surface is fixed.',
						'Use the sliders to set the mass of the crate and the angle of the incline. Assume that the coefficients of friction stay constant for this surface.',
                        'Decide whether a force must be applied up the incline or down the incline. Use the slider to set the applied force.',
                        'Click GO to start the motion.',
                        'Observe the change in the potential energy of the crate, the change in the kinetic energy of the crate, the net work done on the crate and the work done by non-conservative forces.',
                        'Click RESET to start again.'];
			break;
		}
		return helpList;
	},
	setMainHeader: function (select) {
		var self = this;
		var header = self.getMainHeader();
		$(select || self.pageHeader).html(header);
		self.pageHeader = select || self.pageHeader;
	},
	getMainHeader: function () {
		var self = this;
		var header = "Investigate the work-energy theorem against an incline";
		return header;
	},
	//////////
	clearMain : function () {
		var self = this;
		$('.left-block').html('')
		$('.right-block').html('');
	},
    addEventInformationButton: function() {
        var self = this;
        $('.information-button').off('click').on('click',function(){
            self.addInformationPopup();
        })
    },
    addEventCloseButton: function () {
        var self = this;
        $('.information-popup .close-button').off('click').on('click',function(){
            $('.information-popup').remove();
        })
    },
    addInformationPopup: function () {
        var template = '<div class="information-popup"><div class="close-button"></div></div>';
        this.addMainBlock(template);
        this.addEventCloseButton();
    },
    addInformationButton: function () {
    	var template = '<img src="img/info.png" class="information-button">';
    	this.addLeftBlock(template);
    },
    enableInformationButton: function () {
    	var elem = $('.information-button');
    	elem.attr('src','img/info_.png');
        this.addEventInformationButton();
    },
    changeTriangle: function (degree) {
        var self = this;
        $('.triangle').css({backgroundImage:'url("img/'+degree+'degree_.png")'});
        var side1 = 360;
        var hypotenuse = side1/Math.sin(Math.PI/180*(90-degree))
        var side2 = Math.sqrt(Math.pow(hypotenuse,2)-Math.pow(side1,2))
        var changedPX = 3;
        var bottomPX = 5;
        var leftPX = 165;
        var boxSize = 60;
        if(degree==10) changedPX = 5;
        $('.box').css({bottom:(bottomPX+side2/2+changedPX)+'px',left:(leftPX+side1/2-boxSize/2)+'px',transform:'rotate(-'+degree+'deg)'});
    },
    changeBox: function (mass) {
        var self = this;
        $('.box').css({backgroundImage:'url(img/'+mass+'kg.png)'})
    },
    doAnimate: function (type) {
        var self = this;
        var bottomPX = 7;
        var leftPX = 160;
        var changedPX =  this.getSlideValue(1)/10*5+3;
        var type = self.getAnimate();
        var degree = self.getSlideValue(1)-(self.getSlideValue(1)/10-1);
       if(type == 'Acc up'){
            var side1 = 360;
            boxSize = 60;
            var hypotenuse = side1/Math.sin(Math.PI/180*(90-degree))
            var side2 = Math.sqrt(Math.pow(hypotenuse,2)-Math.pow(side1,2))
            $('.box').css({transition:'1s all',bottom:(bottomPX+side2)+'px',left:(leftPX+side1-boxSize)+'px'})
       }else if(type == 'Acc down'){
            $('.box').css({transition:'1s all',bottom:(bottomPX+changedPX)+'px',left:leftPX+'px'})
       }
       setTimeout(function(){
             $('.box').css({transition:'no'});
             self.hideArrowText('blue');
             self.hideArrowText('green');
             self.hideArrowText('purple');
             self.addArrowShowValue();
             $('.hiddenB').removeClass('hiddenB');
             self.addCssStyle("#formulaP .mjx-mrow:nth-child(3) .mjx-msup:nth-child(2) span[style='padding-top: 0.373em; padding-bottom: 0.373em;']", '{visibility: visible !important;}' );
            $('.green-arrow').css({left: '-21%'});
        },1000);
    },
    calculateArrowText: function () {
        var omega = this.getWeightOrWParallel();
        var applied = this.getSlideValue(0);
        var fFriction = Math.round(this.getFrictionForce()*100)/100;
        $('.blue-arrow-text span.formulaE').html(' = '+omega.toString().replace('.',',')+' N');
        $('.green-arrow-text span.formulaE').html(' = '+applied.toString().replace('.',',')+' N');
        $('.purple-arrow-text span.formulaE').html(' = '+fFriction.toString().replace('.',',')+' N');
        $('.purple-arrow-text').removeClass('another');
        if(this.getSlideValue(1)==45) $('.purple-arrow-text').addClass('another');
        this.changeArrowsHeight(omega, applied, fFriction);
    },
    getFGravity: function () {
        var self = this;
        var mass = self.getSlideValue(2);
        return mass*9.8;
    },
    getFrictionForceStatic: function () {
        var friction = 0;
        // ($D4="UP";$F4-$C4;$F4+$C4)
        var omega = this.getWeightOrWParallel();
        var applied = this.getSlideValue(0);
        if(!this.switchButton){
            friction = omega - applied;
        } else {
            friction = omega + applied;
        }
        return friction;
    },
    getD: function (degree) {
        return this.getAcceleration() == 0? 0 : 5 / (2 * Math.cos( degree * Math.PI / 180));
    },
    getWeightOrWParallel: function (not_round = false) {
        var gravity = this.getFGravity();
        var degree = this.getSlideValue(1);
        var omega = gravity * Math.sin(Math.PI/180*degree);
        if(not_round) return omega;
        var omega = Math.round(omega*100)/100;
        return omega;
    },
    getWeightOrWPerpen: function () {
        var gravity = this.getFGravity();
        var angle = this.getSlideValue(1);
        var perpen = gravity * Math.cos(Math.PI/180*angle);
        var perpen = Math.round(perpen*100)/100;
        return perpen;
    },
    getFStaticFractionMax: function () {
        var FStaticFractionMax = this.getWeightOrWPerpen() * this.CoeffStatic;
        return  Math.round(FStaticFractionMax*100)/100;
    },
    getFrictionForce: function () {
        var frictionForceStatic = this.getFrictionForceStatic();
        var fStaticFractionMax = this.getFStaticFractionMax();
        var fFriction = null;
        if(Math.abs(frictionForceStatic) < fStaticFractionMax) {
            fFriction = Math.abs(frictionForceStatic);
        } else {
            fFriction = this.CoeffKinetic * this.getWeightOrWPerpen();
        }
        return fFriction;
    },
    getDiractionOfFrictionForce: function () {
        var frictionForceStatic = this.getFrictionForceStatic();
        if(frictionForceStatic > 0) {
            return false;
        } else {
            return true;
        }
    },
    getFNet: function () {
        var diractionOfFrictionForce = this.getDiractionOfFrictionForce() //k85
        this.switchButton //D85
        var weightOrWParallel = this.getWeightOrWParallel() //f85
        var aplied = this.getSlideValue(0); // c85
        var frictionForce = this.getFrictionForce(); //j85
        var fNet = null;
        if(diractionOfFrictionForce) {
            if(!this.switchButton){
                fNet = weightOrWParallel - aplied + frictionForce;
            } else {
                fNet = weightOrWParallel + aplied + frictionForce;
            }
        } else {
             if(!this.switchButton){
                fNet = weightOrWParallel - aplied - frictionForce;
            } else {
                fNet = weightOrWParallel + aplied - frictionForce;
            }
        }
        return fNet;
    },
    getAcceleration: function () {
        var fNet = this.getFNet();
        var mass = this.getSlideValue(2);
        // console.log('fNet:' , fNet, 'mass:',mass)
        return fNet/mass;
    },
    getWIndexParalel: function () {
        var omega = this.getWeightOrWParallel(true);
        var degree = this.getSlideValue(1);
        var d = this.getD(degree);
        // console.log("d:",d, 'N:', this.getAcceleration(), 'omega:', omega);
        return this.setDirectionParalel(Math.round(omega*d*100)/100);
    },
    getWIndexApplied: function () {
        var fApplied = this.getSlideValue(0)||0;
        var degree = this.getSlideValue(1);
        var d = this.getD(degree);
        return this.setDirection((Math.round(fApplied*Math.abs(d)*100)/100));
    },
    getWIndexFriction: function () {
        var fFriction = this.getFrictionForce();
        var degree = this.getSlideValue(1);
        var d = this.getD(degree);
        return -(Math.round(fFriction*Math.abs(d)*100)/100);
    },
    getAnimate: function () {
        var FNet = this.getFNet();
        if(FNet > 0){
            return 'Acc down'
        } else if (FNet < 0){
            return 'Acc up'
        } else {
            return 'Stationary'
        }
    },
    setDirection: function (value) {
        if(this.switchButton){
            return -value;
        } else {
            return value;
        }
    },
    setDirectionParalel: function(value){
        if(this.switchButton){
            return value;
        } else {
            return -value;
        }
    },
    changeArrowsHeight: function (omega, applied, fFriction) {
        $('.blue-arrow div:nth-child(2)').css({height: (omega+15)+'px'});
        $('.green-arrow div:nth-child(2)').css({height: (applied+15)+'px'});
        $('.purple-arrow div:nth-child(2)').css({height: (fFriction+15)+'px'});
    },
}