var Page = function () {
		this.pressure = [300, 280, 250, 200, 150, 100];
		this.volume = ['9.5', '10.0', '11.5', '14.0', '19.0', '28.5'];
		this.oneDivideP = ['0.0033', '0.0035', '0.0040', '0.0050', '0.0067', '0.0100'];
		this.pv = [2850, 2800, 2875, 2800, 2850, 2850];
		this.enableCheck = function () {};
		this.initProperty();
	}
Page.prototype = {
	
	init: function () {
		this.cleareBackgroundContent();
		this.addBackgroundContent();
		this.addPopupText();
		this.initProperty();
	},
	initProperty: function () {
		this.index = 0;
		this.calculateiIndex = 1;
		this.plotIndex = 1;
		this.nextStep = true;
		this.page = 1;
		this.pageHeader = "";
		this.pageDescription = "";
		this.pageHelpText = "";
		this.mainContent = null;
		this.nextCalculateControl = true;
		this.paginationPageNumber = {prevPage:null,nextPage:1};
		this.paramsForGraph = {graph1OKnumbers:[],graph2OKnumbers:[]};
		this.pointControl = false;
		this.setTimeEvent = null;
		this.gradientSelect = false;
		this.calculateValue = false;
		this.graph1Line = false;
		this.graph2Line = false;
		this.gradientLine = false;
		this.graph = 1;
		this.inputActive = true;
		$(document).off('click', '.enable-graph');
		$(document).off('click', '.enable-measurements');
		this.graph2textpopup = "";
		this.graph1textpopup = ""
	},
	addMainBlock: function (contents) {
		var self = this;
		var content = $('#contentImg');
		if(content.length > 0) {
			self.mainContent = content;
			content.append(contents);
			return true;
		} else {
			console.error('Not found div#contentImg block');
			return false;
		}
	},
	addBackgroundContent: function () {
		var self = this;
		template = '<div class="left-block"></div>\
					<div class="right-block"></div>'
		if(self.addMainBlock(template)) {
			self.addMeterContent();
			self.addRightContent();
			self.addCalculateContent();
		}
	},
	initMeterButton: function () {
		var self = this;
		$(document).off('click', '.redpipe-meter .buttons').on('click', '.redpipe-meter .buttons', function () {
			// self.nextStep = true
			if(self.index < 5 && self.nextStep) {
				self.removePopup('.popup-show-block');
				self.index++;
				self.setStepPosition();
				self.nextStep = false;
				setTimeout(function() {
					self.setInputPressure(self.index+1);
					self.checkInputPressure(self.index+1);
					self.addPopupValumeHelp(function () {
						clearInterval(self.setTimeEvent);
						self.removePopup('.popup-valume-help');
						self.activateInputValume(self.index+1);
						self.addEventInputValume(self.index+1);
					}) ;
					self.closePopupTwoSeconds('.popup-valume-help', function (){
						self.activateInputValume(self.index+1);
						self.addEventInputValume(self.index+1);
					});
				}, 1000);
			}
		})
	},
	addMeterContent: function () {
		this.cleareLeftContent();
		$('.left-block').append('<div class="redpipe-meter full-screen">\
									<div class="redpipe-block full-screen"></div>\
									<div class="buttons"></div>\
									<div class="meter-block full-screen">\
										<div class="meter-line"></div>\
										<div class="nidle-block full-screen"></div>\
									</div>\
								</div>\
								<div class="wheel-scale full-screen">\
									<div class="scale-block full-screen">\
										<span class="scale-100">100</span>\
										<span class="scale-150">150</span>\
										<span class="scale-200">200</span>\
										<span class="scale-250">250</span>\
										<span class="scale-280">280</span>\
										<span class="scale-300 active">300</span>\
									</div>\
									<div class="wheel-block full-screen"></div>\
								</div>');
		this.initMeterButton();
	},
	addGraphContent: function () {
		this.cleareLeftContent();
		$('.left-block').append('<div class="graphs-block full-screen">\
									<div class="draw-gradient-block" >\
										<div class="draw-button"></div>\
										<div class="gradient-button"></div>\
									</div>\
									<div class="graph graph1 not-active">\
										<div class="points-axis-text"></div>\
										<div class="points"></div>\
									</div>\
									<div class="graph graph2 active">\
										<div class="points-axis-text"></div>\
										<div class="points"></div>\
									</div>\
									<div class="arrow-button"><span></span><span></span></div>\
								</div>');
		this.initArrowButton();
	},
	addEventGraphButton: function (){
		var self = this;
		$(document).off('click', '.graph.enable-graph').one('click', '.graph.enable-graph', function () {
			self.disableInptsEvent();
			self.page = 2
			self.enableMeasurementsButton();
			self.activeGraphButton();
			self.addGraphContent();
			$('.arrow-button').click();
			self.addForGraphTextTable();
			self.showTableRowNumber();
			self.initTableSelectRow();
			// self.addPopupPlot();
			self.setInstructionHelp();
			self.setDescription();
			self.setMainHeader();
			self.addTabsEvent();
		})
	},
	addForGraphTextTable: function () {
		$('table').addClass('already-graph');
		$('table tr:nth-child(1) th:nth-child(2)').prepend(' <br />');
		$('table tr:nth-child(1) th:nth-child(2) span').css({marginTop: '-13px', display: 'inline-block'});
		$('table tr:nth-child(1) th:nth-child(3)').prepend('<span class="for-graph">For graph</span> <br />');
		$('table tr:nth-child(1) th:nth-child(4)').prepend('<span class="for-graph">For graph</span> <br />');
		$('table tr:nth-child(1) th:nth-child(5)').prepend(' <br />');
	},
	addEventDrawButton: function () {
		var self = this;
		self.enableDrawButton();
		$(document).off('click', '.draw-button').on('click', '.draw-button', function () {
			self.activeDrawButton();
			if($('.graphs-block .graph1').hasClass('active')) {
				if(self.paramsForGraph.graph1OKnumbers.length == 6) self.addlineGraph1();
			} else {
				if(self.paramsForGraph.graph2OKnumbers.length == 6){
					self.addlineGraph2();
					if($('.gradient-line').length == 0) {
						self.activeGradientButton();
						self.addEventGradient();
					} else {
						self.addEventGradient();
					}
				}
			}

		})
	},
	addEventGradient: function () {
		var self = this;
		if(self.calculateValue) return;
		$(document).off('click', '.gradient-button').one('click', '.gradient-button', function () {
			if($('.gradient-line').length == 0){
				self.addlineGraphGradient();
				self.enableGradientButton();
			}
			self.removePopup('.plot-block');
			self.disableInptsEvent();
			self.addCalculateBlockContent();
			self.initCloseButton();
			self.gradientSelect = true;
		})
	},
	addRightContent: function () {
		var self = this;
		self.cleareRightContent();
		self.addButtonsContent();
		self.addTableContent();
	},
	cleareBackgroundContent: function () {
		var self = this;
		var content = $('#contentImg');
		if(content.length > 0) {
			$('#contentImg').html('');
		}else {
			console.error('Not found div#contentImg block');
		}
	},
	cleareLeftContent: function () {
		$('.left-block').html('');
	},
	cleareRightContent: function () {
		$('.right-block').html('');
	},
	addButtonsContent: function () {
		var template = '<div class="menu-button">\
								<div class="active-measurements measurements"></div>\
								<div class="not-active-graph graph"></div>\
								<div class="not-active-conclusion conclusion"></div>\
							</div>'
		this.addMainBlock(template)
	},
	addTableContent: function () {
		var tamplate = '<div class="table-content">\
							<table>\
								<thead>\
									<tr class="table-head-row">\
										<th></th>\
										<th></th>\
										<th></th>\
										<th></th>\
										<th></th>\
									</tr>\
								</thead>\
								<tbody>';
		for (var colum = 1; colum <=6; colum++) {
			tamplate+='<tr data-row="'+colum+'"><td>'+colum+'</td>';
			for (var row = 1; row <=4; row++) {
				tamplate+='<td><input type="text" data-id="' + row + '_' + colum + '" disabled /></td>';
			}
			tamplate+='</tr>';
		}
		tamplate+='<tbody>\
				<table>\
			</div>';
		$('.right-block').append(tamplate);
		this.addHeadMathTh($('.table-head-row th:nth-child(2)')[0], "span", {id: 'th1'}, ['Pressure, `p` `(kPa)`']);
		this.addHeadMathTh($('.table-head-row th:nth-child(3)')[0], "span", {id: 'th2'}, ['Volume, `V` `(cm^3)`']);
		this.addHeadMathTh($('.table-head-row th:nth-child(4)')[0], "span", {id: 'th3'}, ['`1/p`']);
		this.addHeadMathTh($('.table-head-row th:nth-child(5)')[0], "span", {id: 'th4'}, ['`pV`']);
		//must not be italic
		this.setStyle('#th1 span[data-mathml]:nth-child(2) span.mjx-mo,#th1 span[data-mathml]:nth-child(2) span.mjx-mi',{fontFamily:'MathJax !important',fontSize:'90px'});
	},
	setStyle: function (selector,params) {
		setTimeout(function(){
			$(selector).css(params);
		},200);
	},
	addHeadMathTh: function (elem, tag, attr, mathText) {
		MathJax.HTML.addElement(elem, tag, attr, mathText);
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, attr.id]);
	},
	addCalculateContent: function () {
		$('.right-block').append('<div class="calculate-button-block">\
									<div class="not-active"></div>\
								</div>');
	},
	addCalculateBlockContent: function () {
		$('.calculate-block').remove();
		$('.right-block').html('<div class="calculate-block">\
									<div class="close-button"></div>\
								</div>');
		calculateText = 'Calculate the gradient of the `V` versus `1/p` graph.';
		MathJax.HTML.addElement($('.calculate-block')[0], "p", {id: 'calculate_block_text'+(Math.random()*10)}, [calculateText]);
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'calculate_block_text']);

		$('.calculate-block').append('<p>Enter your answer below, rounded to the nearest integer.</p>');
		$('.calculate-block').append('<div>\
										<input type="text" >\
										<img src="img/wrong.png" class="status">\
										<span class="show_answer">Show answer</span>\
										<div class="hidden-img "></div>\
									</div>');
		var hiddenImgText = '`m = (\y_\(2) - \y_\(1)) / (\\x_\(2) - \\x_\(1))`';
		var id = 'hidden_img_text'+(Math.random()*10)
        MathJax.HTML.addElement($('.hidden-img')[0], "p", {id: id}, [hiddenImgText]);
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, id]);
		var id = 'hidden_img_text'+(Math.random()*10)
		var hiddenImgText = '`= (28,5 - 19,0) / (0,0100 - 0,0067)`';
        MathJax.HTML.addElement($('.hidden-img')[0], "p", {id: id}, [hiddenImgText]);
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, id]);
		var id = 'hidden_img_text'+(Math.random()*10000000000000000)
		var hiddenImgText = '`=`';
        MathJax.HTML.addElement($('.hidden-img')[0], "p", {id: id, className: 'equal'}, [hiddenImgText]);
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, id]);
		$('.hidden-img p:nth-child(3)').append('<span class=" MJXc-TeX-main-R  c-result">3 000</span>');
		if(this.calculateValue) {
			$('.hidden-img').css({visibility: 'visible'});
			$('.calculate-block img').css({visibility: 'visible'});
			if(self.calculateValue <= 3300 && self.calculateValue >= 2750){
            	$('.calculate-block .status').attr('src','img/right.png');
            }
		} else{
			$('.hidden-img span').css({visibility: 'hidden'})
		}
			this.initEventCalculateBlockInput();
		
	},
	initEventCalculateBlockInput: function () {
		var self = this;
		$(document).off('change','.calculate-block input').on('change','.calculate-block input', function (e) {
			var value = $(e.target).val().replace(',','.');
			$(e.target).val(value.replace('.',','))
			if(value <= 3300 && value >= 2750) {
				$('.calculate-block input').attr('readonly','readonly');
				$('.calculate-block img').attr('src', 'img/right.png');
				self.enableConclusionButton();
				self.addEventConclusion();
				$('.hidden-img span').css({visibility: 'visible'});
				$('.calculate-block span.show_answer').css({visibility: 'hidden'});
				self.calculateValue = $('.calculate-block input').val().replace(',','.');
				value = Math.floor(value/1000)+' '+value.slice(1,value.length)
				$(e.target).val($(e.target).val().replace('.',','))
				self.calculateBlockInput = value;
				self.addPopupNextTab();
				self.closePopupTwoSeconds('.show-instuction');
			} else {
				self.initEventShowAnswer();
				self.calculateValue = $('.calculate-block input').val().replace(',','.')
				$('.calculate-block span.show_answer').css({visibility: 'visible'});
			}
			$('.calculate-block img.status').css({visibility: 'visible'});
			
		})
	},
	initEventShowAnswer: function () {
		var self = this;
		$(document).off('click', '.calculate-block .show_answer').one('click', '.calculate-block .show_answer', function () {
			$('.calculate-block input').attr('readonly','readonly');
			self.calculateBlockInput = '3 000';
			$('.calculate-block img').css({visibility: 'visible'});
			self.enableConclusionButton();
			self.addEventConclusion();
			$('.calculate-block input').val($('.calculate-block input').val().replace('.', ','))
			$('.calculate-block span.show_answer').css({visibility: 'hidden'})
			$('.hidden-img span').css({visibility: 'visible'});
			self.addPopupNextTab(function () {
					self.removePopup('.show-instuction');
			});
			self.closePopupTwoSeconds('.show-instuction');
		})
	},
	addPopupContent: function () {
		var self = this;
		if($('.popup-block').length>0) return;
		var template = '<div class="popup-block tr_intex_'+self.index+'"></div>'
		self.addMainBlock(template);
		$(document).one('click', '#contentImg', function () {
			self.removePopup('.popup-block');
		})
	},
	addPopupShowBlock: function () {
		var self = this;
		self.removePopup('.popup-show-block');
		var template = '<div class="popup-show-block tr_index_'+self.index+'" ></div>'
		self.addMainBlock(template);
		$(document).off('click', '.popup-show-block').on('click', '.popup-show-block', function () {
			self.addPopupContent();
		})
	},
	addPopupValumeHelp: function (f) {
		var self = this;
		var template = '<div class="popup-valume-help">\
						</div>'
		self.addMainBlock(template);
		self.mainContent.one('click', f);
	},
	addPopupCalculateHelp: function (f) {
		var self = this;
		var template = '<div class="popup-calculate-help">\
						</div>'
		self.addMainBlock(template);
			self.mainContent.one('click', f);
		
	},
	addPopupValumeError: function (colume = null, row = null) {
		var self = this;
		self.removePopup('.popup-valume-error');
		var template = '<div class="popup-valume-error"></div>';
		var popupText = '';
		self.addMainBlock(template);
		var attr = {id:"error_popup_id"};
		if(colume <= 2) {
			popupText = $('#volumetextpopup1').clone();
			$('.popup-valume-error').addClass('volume');
			$('.popup-valume-error').append(popupText);
		} else {
			popupText =$('#volumetextpopup2').clone();
			popupText.addClass('calculate-error-text');
			$('.popup-valume-error').append(popupText);
		}
		var buttonBlock = '<div>\
								<input type="button" class="try_again">\
								<input type="button" class="correct_plot correct_value">\
							<div>';
		$('.popup-valume-error').append(buttonBlock);
		$('.popup-valume-error').css({visibility: 'visible'})
		self.addEventTryAgein(colume, row);
		self.addEventCorrectPlotByColuemRow(colume, row);
		self.inputActive = false;
		$('input[data-id]').attr('readonly', 'readonly');
	},
	addEventTryAgein: function(colume, row) {
		var self = this;
		$(document).off('click','.try_again' ).one('click', '.try_again', function () {
			self.inputActive = true;
			self.removePopup('.popup-valume-error');
			$('input[data-id='+colume+'_'+row+']').attr('readonly', false);
		})
		
	},
	addEventCorrectPlotByColuemRow: function(colume, row) {
		var self = this;
		$(document).off('click','.correct_plot' ).on('click','.correct_plot' , function () {
			self.inputActive = true;
			if(colume == 2) {
				self.setInputValume(row );
				self.checkInputValume(row);
			} else if(colume == 3) {
				self.setInput1DivideP(row);
				self.checkInput1DivideP(row);
				self.calculateColume('next_valid');
			} else if(colume == 4) {
				self.setInputPV(row);
				self.checkInputPV(row);
				self.calculateColume('next_valid');
			}
			self.removePopup('.popup-valume-error');
			var input = $('input[data-id='+colume+'_'+row+']');
			input.attr('readonly', 'readonly');
		})
	},
	addPopupPlot: function () {
		var self = this;
		$('.plot-block').remove();
		var template = '<div class="plot-block plot_index_'+self.plotIndex+'">\
							<p>Plot this point.</p>\
						</div>'
		self.addMainBlock(template);
	},
	addPopupNextTab: function (f) {
		var self = this;
		var template = '<div class="show-instuction"><p>Proceed to the next tab.</p></div>'
		self.addMainBlock(template);
		setTimeout(function () {
			self.mainContent.one('click', f);
		}, 500);
	},
	setStepPosition: function () {
		var self = this;
		var whell_value  = 80 - (self.index  * 15.2);
		$('.wheel-block').css({transform: 'rotate('+whell_value + 'deg)'});
		$('.nidle-block').css({transform: 'rotate('+( 65 - (((300 - self.pressure[self.index]) / 5) * 3.3)) + 'deg)'});
		$('.meter-block .meter-line').css({height: 409 - 13.4 * this.volume[self.index]});
		setTimeout(function () {
			$('.scale-block span').removeClass('active');
		}, 500);
		setTimeout(function () {
			$('.scale-'+self.pressure[self.index]).addClass('active');
		}, 1000);
		
	},
	setInput: function (colum, row, value) {
		var self = this;
		var input = $('input[data-id='+colum+'_'+row+']');
		if(self.inputActive){
			input.attr('disabled', false);
			input.attr('readonly', false);
		}
		//input set not-valid
		input.val(value.toString().replace('.', ','));
	},

	// input seter
	setInputPressure: function (row) {
		var self = this;
		self.setInput(1, row, this.pressure[row - 1]);
	},
	setInputValume: function (row) {
		var self = this;
		self.setInput(2, row, this.volume[row - 1]);
	},
	setInput1DivideP: function (row) {
		var self = this;
		self.setInput(3, row, this.oneDivideP[row - 1]);
	},
	setInputPV: function (row) {
		var self = this;
		self.setInput(4, row, this.pv[row - 1]);
	},
	checkAllValue: function () {
		var not_valid_input = $('input.not-valid');
		var allInputs = $('table input');
		var control = false
		for(var i = 0; i < allInputs.length; i++) {
			if($(allInputs[i]).val() == '') {
				control = true;
				break;
			}
		}
		return {
			not_valid_input: not_valid_input,
			control: control,
			allInputs: allInputs,
			index: i
		 }
	},
	// input geter
	checkInput: function (colum, row, value) {
		var self = this;
		value = value.toString();
		if(self.page > 1) return;
		var input = $('input[data-id='+colum+'_'+row+']');
		var valid = self.checkValue(input.val(), value);
		if(valid){
			if(colum !== 1) self.nextStep = true;
			input.removeClass('not-valid')
			var inputs = input.closest('tr').find('input');
			for(var i = 0; i < inputs.length; i++) {
				elem = $(inputs[i]);
				if(elem.val() != '' && !elem.hasClass('not-valid')) {
					elem.attr('readonly', true);
				} 
			}
			if(row == 6 && colum == 2 && self.nextCalculateControl == true) {
				self.nextCalculateControl = false;
				self.nextCalculate();
				$(document).off('click', '.redpipe-meter .buttons')
			}
			if((row == 6 && colum == 4) || (row == 6 && colum == 3 && $('input[data-id=4_6]').val() == self.pv[5])) {
				var allInputs = self.checkAllValue();
				if(allInputs.not_valid_input.length == 0 && !allInputs.control ) {
					self.enableGraphButton();
					self.addEventGraphButton();
					self.addPopupNextTab(function () {
							self.removePopup('.show-instuction');
					});
					self.closePopupTwoSeconds('.show-instuction');
				}
			}
			input.removeClass('not-valid');
			//console.log(input)
			var value = input.val()
			if(value>=1000){
				var num = Math.floor(value/1000).toString();
				value = num+' '+String(value).slice(num.length,String(value).length);
			}
			input.val(value.replace('.', ','));
			input.attr('readonly', 'readonly');
		} else {
			if(self.inputActive){
				input.attr('readonly', false);
			}
			self.nextStep = false
			self.notValidInput(colum, row);
		}
	},

	checkInputPressure: function (row) {
		var self = this;
		self.checkInput(1, row, this.pressure[row - 1]);
	},
	checkInputValume: function (row) {
		var self = this;
		self.checkInput(2, row, this.volume[row - 1]);
	},
	checkInput1DivideP: function (row) {
		var self = this;
		self.checkInput(3, row, this.oneDivideP[row - 1]);
	},
	checkInputPV: function (row) {
		var self = this;
		self.checkInput(4, row, this.pv[row - 1]);
	},
	notValidInput: function (colum, row) {
		var self = this;
		var input = $('input[data-id='+colum+'_'+row+']');
		input.addClass('not-valid');
		self.addPopupValumeError(colum, row);
		$(input).attr('readonly', true);
	},

	activateInputValume: function (row) {
		var self = this; 
		var input = $('input[data-id='+2+'_'+row+']');
		input.val('');
		if(self.inputActive){
			input.attr('disabled', false);
			input.attr('readonly', false);
		}
		self.addPopupShowBlock();

	},
	checkValue: function(value1, value2) {
		value1 = value1.replace('.',',');
		value2 = value2.replace('.',',');
		if(value1 == value2) {
			return true;
		} else {
			return false;
		}
	},
	addEventInputValume: function (row) {
		var self = this;
		var input = $('input[data-id='+2+'_'+row+']');
		$(document).off('change', $('input[data-id='+2+'_'+(row-1)+']'))
		$(document).off('change', input).on('change', input, function () {
			self.checkInputValume(row);
			$(input).attr('readonly', true);
		})
	},
	addEventInputs: function (colume, row) {
		var self = this;
		var input = $('input[data-id='+colume+'_'+row+']');
		$(document).off('change', $('input[data-id='+colume+'_'+(row-1)+']'))
		$(document).off('change', input).on('change', input, function () {
			$(document).off('change', $(this));
			if(colume == 2) {
				self.checkInputValume(row);
			} else if(colume == 3) {
				self.checkInput1DivideP(row);
				self.calculateColume('next_valid');
			} else if(colume == 4) {
				self.checkInputPV(row);
				self.calculateColume('next_valid');
			}
		})
	},
	removePopup: function (select) {
		$(select).remove();
	},
	nextCalculate: function () {
		var self = this;
		self.addCalculateContent();
		self.activateCalculate();
		self.removePopup('.popup-show-block');
	},
	activateCalculate: function () {
		var self = this;
		var calculateButton = $('.calculate-button-block div.not-active')
		calculateButton.removeClass('not-active').addClass('active');
		calculateButton.attr('id' , "calculateButton");
		var columnInputs = $('table tr td:nth-child(4) input, table tr td:nth-child(5) input');
		columnInputs.attr('disabled', false).attr('readonly', true);
		// 
		$(document).off('change', columnInputs).on('change', columnInputs, function (e) {
			if(self.page > 1) {
				$(document).off('change', columnInputs);
				return;
			}
			var position = $(e.target).attr('data-id').split('_');
			self.index = position[1];
			if(position[0] == 3  ) {
				self.checkInput1DivideP(position[1]);
				self.calculateColume('next_valid');
			} else if(position[0] == 4){
				self.checkInputPV(position[1]);
				self.calculateColume('next_valid');
			}
			$(e.target).attr('readonly', true);
		})
		$(document).off('click', '#calculateButton').one('click', '#calculateButton', function(e){
			self.calculateColume();
			$('#calculateButton.active').css({backgroundImage: 'url(img/calculate1_button.png)'});
			$('#calculateButton.active').removeClass('active');
		});
	},
	calculateColume: function (status = null) {
		var self = this;
		if (this.page > 1) return
		var allInputs = self.checkAllValue();
		if(allInputs.not_valid_input.length > 0 || allInputs.control ) {
			var position = null;
			if (allInputs.not_valid_input.length > 0){
				position = allInputs.not_valid_input.attr('data-id').split('_');
			} else {
				position = $(allInputs.allInputs[allInputs.index]).attr('data-id').split('_');
			}
			// self.addPopupValumeError(position[0], position[1]);
			if(self.inputActive)
				$('table tr:nth-child('+self.calculateiIndex+') td:nth-child(4) input, table tr:nth-child('+self.calculateiIndex+') td:nth-child(5) input').attr('readonly', false);
			if(allInputs.not_valid_input.length == 0) {
				if(status != 'next_valid'){
					self.addPopupCalculateHelp(function () {
						self.removePopup('.popup-calculate-help');
					});
					self.closePopupTwoSeconds('.popup-calculate-help');
				}
					
				var input1 = $('table tr:nth-child('+self.calculateiIndex+') td:nth-child(4) input').val();
				var input2 = $('table tr:nth-child('+self.calculateiIndex+') td:nth-child(5) input').val();
				if(input1 && input2){
					self.calculateiIndex++;
					$('input[data-id]').attr('readonly','readonly');
					if(self.inputActive)
						$('table tr:nth-child('+self.calculateiIndex+') td:nth-child(4) input, table tr:nth-child('+self.calculateiIndex+') td:nth-child(5) input').attr('readonly', false);
				}
			}
		} else {
			self.enableGraphButton();
			self.addEventGraphButton();
			//self.enableCheck(true);
		}
	},
	setColumePressureValueme: function () {
		var self = this;
		for(var i = 1; i <= self.pressure.length; i++){
			self.setInputPressure(i);
			self.setInputValume(i);
		}
		self.enableGraphButton();
	},
	activeTableRow: function () {
		var self = this;
		$('.right-block table tr').removeClass('active-calculate');
		$('.right-block table tr:nth-child('+self.plotIndex+')').addClass('active-calculate');
	},
	showTableRowNumber: function () {
		$('.right-block table td:nth-child(1)').css({visibility: 'visible'});
	},
	 hideTableRowNumber: function () {
        $('.right-block table td:nth-child(1)').css({visibility: 'hidden'});
    },
	enableGraphButton: function (){
		$('.menu-button .graph').removeClass('not-active-graph active-graph').addClass('enable-graph');
	},
	disableGraphButton: function (){
		$('.menu-button .graph').removeClass('enable-graph active-graph').addClass('not-active-graph');
	},
	activeGraphButton: function (){
		$('.menu-button .graph').removeClass('enable-graph not-active-graph').addClass('active-graph');
	},
	activeConclusionButton: function (){
		$('.menu-button .conclusion').removeClass('not-active-conclusion active-conclusion').addClass('enable-conclusion');
		this.enableGraphButton();
		this.enableMeasurementsButton();
	},
	disableConclusionButton: function (){
		$('.menu-button .conclusion').removeClass('enable-conclusion active-conclusion').addClass('not-active-conclusion');
	},
	enableConclusionButton: function (){
		$('.menu-button .conclusion').removeClass('enable-conclusion not-active-conclusion').addClass('active-conclusion');
		this.addEventConclusion();
	},
	enableMeasurementsButton: function (){
		$('.menu-button .measurements').removeClass('not-active-measurements active-measurements').addClass('enable-measurements');
	},
	activeMeasurementsButton: function (){
		$('.menu-button .measurements').removeClass('enable-measurements not-active-measurements').addClass('active-measurements');
	},
	activeGradientButton: function (){
		$('.graphs-block .gradient-button').removeClass("enable").addClass('active');
	},
	enableGradientButton: function (){
		$('.graphs-block .gradient-button').removeClass("active").addClass('enable');
	},
	
	disableGradientButton: function (){
		$('.graphs-block .gradient-button').removeClass('active enable')
	},

	activeDrawButton: function (){
		$('.draw-button').removeClass('enable').addClass('active');
	},
	disableDrawButton: function (){
		$('.draw-button').removeClass('enable  active');
		$(document).off('click','.draw-button');
	},
	enableDrawButton: function (){
		$('.draw-button').removeClass('active ').addClass('enable');
		// this.addEventConclusion();
	},

	activateTableColumn: function () {
		$('table tr td:nth-child(3)').addClass('graph-active-td');
		$('table tr th:nth-child(3)').addClass('active-th-x');
		if($('.graphs-block .graph1').hasClass('active')) {
			$('table tr td:nth-child(4)').removeClass('graph-active-td');
			$('table tr td:nth-child(2)').addClass('graph-active-td');
			$('table tr th:nth-child(4)').removeClass('active-th-y');
			$('table tr th:nth-child(2)').addClass('active-th-y');
			
		} else {
			$('table tr td:nth-child(2)').removeClass('graph-active-td');
			$('table tr td:nth-child(4)').addClass('graph-active-td');
			$('table tr th:nth-child(2)').removeClass('active-th-y');
			$('table tr th:nth-child(4)').addClass('active-th-y');
		}
	},
	initArrowButton: function () {
		var self = this;
		$(document).off('click', '.arrow-button').on('click', '.arrow-button', function (){
			$(document).off('click', '.graphs-block .graph1 .points');
			$(document).off('click', '.graphs-block .graph2 .points');
			$('tr.active-calculate').removeClass('active-calculate');
			self.removePopup('.plot-block');
			if($('.graphs-block .graph1').hasClass('active')) {
				self.graph = 2;
				$('.graphs-block .graph1').removeClass('active').addClass('not-active');
				$('.graphs-block .graph2').removeClass('not-active').addClass('active');
				if(self.paramsForGraph.graph2OKnumbers.length < 6) {
					self.disableDrawButton();
					// self.addPopupPlot();
				} else {
					self.addEventDrawButton();
					if($('.graph2-line').length > 0){
						self.activeDrawButton();
						self.activeGradientButton ();
					}
					if($('.gradient-line').length > 0 ){
						self.enableGradientButton();
						self.addCalculateBlockContent();
						self.initCloseButton();
						$('.calculate-block .status').css({visibility: 'visible'});
						if(self.calculateValue){
							$('.calculate-block input').attr('readonly','readonly');
							$('.calculate-block input').val(self.calculateValue);
						}
						if(self.calculateValue <= 3300 && self.calculateValue >= 2750){
	                    	$('.calculate-block .status').css({visibility: 'visible'});
	                    }
					}
					self.removePopup('.plot-block');
					$('tr.active-calculate').removeClass('active-calculate');
				}
			} else {
				self.graph = 1;
				$('.graphs-block .graph1').removeClass('not-active').addClass('active');
				$('.graphs-block .graph2').removeClass('active').addClass('not-active');
				if(self.paramsForGraph.graph1OKnumbers.length < 6) {
					self.disableDrawButton();
					// self.addPopupPlot();
				}else {
					if($('.graph1-line').length > 0) {
						self.activeDrawButton();
					} else {
						self.addEventDrawButton();
					}
				}
				self.disableGradientButton();
				if(self.gradientSelect){
					$('.close-button').click();
				}
			}
			self.activateTableColumn();
		})
	},
	disableInptsEvent: function () {
		$(document).off('change', 'input');
		$(document).off('click', 'input');
	},
	initTableSelectRow: function () {
		var self = this;
		$(document).off('click', '.right-block table tr td:nth-child(1)').on('click', '.right-block table tr td:nth-child(1)', function (e) {
			var indexRow = $(e.target).closest('tr').attr('data-row');
			self.plotIndex = indexRow;
			$('.plot-block').remove();
			if($('.graph.active').hasClass('graph1')){
				if(self.paramsForGraph.graph1OKnumbers.length<6){
					self.activateTableColumn();
					self.initGraphPointsEvent();
					if(self.paramsForGraph.graph1OKnumbers.indexOf(self.plotIndex) == -1) self.addPopupPlot();
					self.activeTableRow();
				}
			}else if(self.paramsForGraph.graph2OKnumbers.length<6){
				if(self.paramsForGraph.graph2OKnumbers.indexOf(self.plotIndex) == -1) self.addPopupPlot();
				self.activateTableColumn();
				self.initGraphPointsEvent();
				self.activeTableRow();
			}
		})
	},
	initGraphPointsEvent: function () {
		var self = this;
		if($('.graphs-block .graph1').hasClass('active')) {
			$(document).off('click', '.graphs-block .graph1 .points').on('click', '.graphs-block .graph1 .points', function (e) {
				if(self.pointControl) return;
				var graph1HasThisPlot = self.paramsForGraph.graph1OKnumbers.find(function(n){
  					return n == self.plotIndex;
  					});
				var corectData = self.getCorectEvent(e)
  				if(graph1HasThisPlot == undefined)	self.addPointsToGraph(corectData.select, corectData.position, self.checkValidePoints(e));
			})
		} else {
			$(document).off('click', '.graphs-block .graph2 .points').on('click', '.graphs-block .graph2 .points', function (e) {
				if(self.pointControl) return;
				var graph2HasThisPlot = self.paramsForGraph.graph2OKnumbers.find(function(n){
  					return n == self.plotIndex;
  					});
				var corectData = self.getCorectEvent(e)
				if(graph2HasThisPlot == undefined)	self.addPointsToGraph(corectData.select, corectData.position, self.checkValidePoints(e));
			})
		}
	},
	addEventTryAgeinGraph: function (point) {
		var self = this;
		$(document).off('click','.graph.try_again').on('click','.graph.try_again', function () {
			self.removePopup('.popup-valume-error');
			point.remove();
			self.pointControl = false;
			self.initTableSelectRow();
		})
	},
	addEventCorrectPlotGraph: function (select, point) {
		var self = this;
		var block_width = $(select).width();
		var block_height = $(select).height();
		var mashtabY = block_height/35;
		$(document).off('click', '.graph.correct_plot_graph').on('click', '.graph.correct_plot_graph', function () {
			self.initTableSelectRow();
			if($(select).parent().hasClass('graph1')) {
				var mashtabX = block_width / 400;
				$(point).css({top: (block_height - mashtabY * self.volume[self.plotIndex - 1]) - 7, left: ((self.pressure[self.plotIndex - 1])*mashtabX) - 7 + 'px'});
				self.paramsForGraph.graph1OKnumbers.push(self.plotIndex);
				self.addPointXYText($('.graphs-block .graph1'), select, {top: (block_height - mashtabY * self.volume[self.plotIndex - 1]) - 7, left: ((self.pressure[self.plotIndex - 1])*mashtabX) + 20 });
				if(self.paramsForGraph.graph1OKnumbers.length == 6) {
					self.addEventDrawButton();
					self.addPopupPlotEnd();
					self.removePopup('.plot-block');
					$('tr.active-calculate').removeClass('active-calculate');
				}
			} else {
				var mashtabX = block_width / 0.01;
				$(point).css({top: (block_height - mashtabY * self.volume[self.plotIndex - 1]) - 5, left: (self.oneDivideP[self.plotIndex - 1]*mashtabX) - 5 + 'px'});
				self.paramsForGraph.graph2OKnumbers.push(self.plotIndex);
				self.addPointXYText($('.graphs-block .graph2'), select, {top: (block_height - mashtabY * self.volume[self.plotIndex - 1]) - 5, left: (self.oneDivideP[self.plotIndex - 1]*mashtabX) - 110 });
				if(self.paramsForGraph.graph2OKnumbers.length == 6) {
					self.addEventDrawButton();
					self.addPopupPlotEnd();
					self.removePopup('.plot-block');
					$('tr.active-calculate').removeClass('active-calculate');
				}
			}
			self.pointControl = false;
			$(point).removeClass('error');
			self.removePopup('.popup-valume-error');
			self.removePopup('.plot-block');
		})
		
	},
	addPopupPlotEnd: function (f=function(){}) {
		var self = this;
		if(self.paramsForGraph.graph1OKnumbers.length==6 
			&& self.paramsForGraph.graph2OKnumbers.length==6) return;
		var template = '<div class="plot-end-popup"></div>';
		self.addMainBlock(template);
		$('.plot-end-popup').append($('#plot-end-text').clone());
		setTimeout(function () {
			self.mainContent.one('click', function () {
				self.removePopup('.plot-end-popup');
			});
		}, 500);
		self.closePopupTwoSeconds('.plot-end-popup');
	},
	addPopupText: function () {
		var template = '<div class="error-popup-texts"></div>';
		this.addMainBlock(template);

		eroorText = 'Enter the correct value in the correct block. Round to four decimal digits.'
		MathJax.HTML.addElement(document.querySelector('.error-popup-texts'), "p", {id: 'volumetextpopup2'}, [eroorText]);
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'volumetextpopup2']);

		eroorText = '`0,5` `cm^3`'
		MathJax.HTML.addElement(document.querySelector('.error-popup-texts'), "p", {id: 'volumetextpopup1'}, [eroorText]);
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'volumetextpopup1']);
		$('#volumetextpopup1').prepend('Measure at the <span class="p-text-bold">top of the meniscus</span>, correct to the nearest' )
	
		var plotEndText = 'Plot the points on the `V` versus `1/p` graph.';
		MathJax.HTML.addElement(document.querySelector('.error-popup-texts'), "p", {id: 'plot-end-text'}, [plotEndText]);
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'plot-end-text']);

	},
	addPopupGraphError: function (select, position, point) {
		var self = this;
		var template = '<div class="popup-valume-error"></div>';
		var eroorText = '';
		var newClass = ''
		self.addMainBlock(template);
		if($('.graph2').hasClass('active')) {

			var eroorText = 'Carefully read the `1/p` value on the horizontal axis and the `V` value on the vertical axis.'
			MathJax.HTML.addElement(document.querySelector('.popup-valume-error'), "p", {id: 'graph2textpopup'}, [eroorText]);
			MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'graph2textpopup']);

			$('.popup-valume-error').removeClass('graph-popup');
			$('.popup-valume-error').addClass('over');
		} else {
			var eroorText = 'Carefully read the `p` value on the horizontal axis and the `V` value on the vertical axis.'
			MathJax.HTML.addElement(document.querySelector('.popup-valume-error'), "p", {id: 'graph1textpopup'}, [eroorText]);
			MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'graph1textpopup']);

			$('.popup-valume-error').removeClass('over');
			$('.popup-valume-error').addClass('graph-popup');

		}
		
		var graphErrorButton ='<div>\
									<input type="button" class="graph try_again">\
									<input type="button" class="graph correct_plot_graph">\
								<div>';

		$('.popup-valume-error').append(graphErrorButton);
		$('.popup-valume-error').addClass(newClass);
		$(document).off('click', '.right-block table tr td:nth-child(1)');
		setTimeout(function () {
            $('.popup-valume-error').css({visibility:'visible'});
        },1500)
		self.addEventTryAgeinGraph(point);
		self.addEventCorrectPlotGraph(select, point);
	},
	getCorectEvent: function (e) {
		var tag = $(e.target).hasClass('points-text') ? $(e.target).closest('.points') : $(e.target);
		var position = {
			left: 0,
			top: 0
		};
		var tagPosition = tag.position()
		if($(e.target).hasClass('points-text')){
			position.left = e.pageX - tagPosition.left;
			position.top = e.pageY - tagPosition.top;
		} else {
			position.left = e.offsetX
			position.top = e.offsetY
		}
		return {
			select: tag,
			position: position
		}
	},
	checkValidePoints: function (e) {
		var self = this;
		var corectData = self.getCorectEvent(e);
		var block_width = $(corectData.select).width();
		var block_height = $(corectData.select).height();
		var valide = false;
		var mashtabY = block_height/35;
		var seteVolume = (block_height-corectData.position.top)/mashtabY;
		if($('.graphs-block .graph1').hasClass('active')) {
				var mashtabX = block_width/400;
				var setePressure = corectData.position.left/mashtabX;
				if(setePressure <= self.pressure[self.plotIndex-1]+1 && setePressure >= self.pressure[self.plotIndex-1]-1 
					&& seteVolume <= +self.volume[self.plotIndex-1]+1 && seteVolume >= +self.volume[self.plotIndex-1] - 1) {
						valide = true;	
						self.paramsForGraph.graph1OKnumbers.push(self.plotIndex);
						self.addPointXYText($('.graphs-block .graph1'), e);
						if(self.paramsForGraph.graph1OKnumbers.length == 6){
							self.addEventDrawButton();
							self.addPopupPlotEnd();
							self.removePopup('.plot-block');
							$('tr.active-calculate').removeClass('active-calculate');
						}
					}
		} else {
			var mashtabX = block_width/0.01;
			var seteOneDivideP = corectData.position.left/mashtabX;
			if(seteOneDivideP <= +self.oneDivideP[self.plotIndex-1]+0.0005 && seteOneDivideP >= +self.oneDivideP[self.plotIndex-1]-0.0005
				&& seteVolume <= +self.volume[self.plotIndex-1]+1 && seteVolume >= +self.volume[self.plotIndex-1] - 1) {
					valide = true;	
					self.paramsForGraph.graph2OKnumbers.push(self.plotIndex);
					self.addPointXYText($('.graphs-block .graph2'), e);
					if(self.paramsForGraph.graph2OKnumbers.length == 6){
						self.addEventDrawButton();
						self.addPopupPlotEnd();
						self.removePopup('.plot-block');
						$('tr.active-calculate').removeClass('active-calculate');
					}
				}
		}
		return valide;
	},
	addPointsToGraph: function (select, position, valide) {
		var self = this;
		var point = $('<div class="point ' + (valide? '' : 'error') + '" style="top:'+(position.top-7)+'px; left:'+(position.left-7)+'px;"></div>')
		$(select).closest('.graph.active').find('.points-axis-text').append(point)
		if(!valide) {
			self.pointControl = true;
			self.addPopupGraphError(select, position, point);
		} else {
			self.pointControl = false;
			var select = null;
			if($('.graphs-block .graph1').hasClass('active')) {
				select = $('.graphs-block .graph1 .points');
			} else {
				select = $('.graphs-block .graph2 .points');
			}
			var block_width = select.width();
			var block_height = select.height();
			var mashtabY = block_height/35;
			if($('.graphs-block .graph1').hasClass('active')) {
				var mashtabX = block_width/400;
				point.css({top: block_height - self.volume[self.plotIndex-1] * mashtabY - 7 + 'px', left: self.pressure[self.plotIndex-1] * mashtabX - 7 + 'px'})
			} else {
				var mashtabX = block_width/0.01;
				point.css({top: block_height - self.volume[self.plotIndex-1] * mashtabY - 7 + 'px', left: self.oneDivideP[self.plotIndex-1] * mashtabX - 7 + 'px'})
			}
			self.removePopup('.plot-block');
		}
	},
	addlineGraph1: function () {
		var self = this;
		var template = '<div class="graph1-line">\
						</div>';
		self.graph1Line = true;
		$('.graph1 .points-axis-text').prepend(template)
	},
	addlineGraph2: function () {
		var self = this;
		var template = '<div class="graph2-line">\
						</div>';
		self.graph2Line = true;
		$('.graph2 .points-axis-text').prepend(template)
	},
	addlineGraphGradient: function () {
		var self = this;
		var template = '<div class="gradient-line">\
						</div>';
		self.gradientLine = true;
		$('.graph2 .points').prepend(template)
	},
	addMathText: function (pageHelpText, helpList) {
		var ol = document.createElement('ol');
		ol.className = 'help-list';
		$(pageHelpText).html(ol);
		for(var i = 0; i< helpList.length; i++) {
			var id = 'li'+i+(new Date().getTime());
			MathJax.HTML.addElement(ol, "li", {id: id}, [helpList[i]]);
			MathJax.Hub.Queue(["Typeset", MathJax.Hub, id]);
		}
	},
	// set and get header, description, help list
	setInstructionHelp: function (select) {
		var self = this;
		var helpList = self.getInstructionList();
		$(select || self.pageHelpText).html('');
		self.pageHelpText = select || self.pageHelpText;
		self.addMathText(self.pageHelpText, helpList);
		$(select || self.pageHelpText).parent().removeClass('help_list_' + self.page - 1).addClass('help_list_' + self.page);
		
	},
	setDescription: function (select, description) {
		var self = this;
		$(select || self.pageDescription).html('');
		var description = self.getDescription();
		var id = Math.random()*(new Date().getTime());
		MathJax.HTML.addElement($(select || self.pageDescription)[0], "span", {id: 'description'}, [description]);
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'description']);
		self.pageDescription = select || self.pageDescription;
		// $(select).parent().removeClass('description_text_' + self.page - 1).addClass('description_text_' + self.page);
	},
	getDescription: function () {
		var self = this;
		var description = "";
		switch (self.page){
			case 1:
				description = "Choose a pressure and measure the volume to complete the table. ";
			break;
			case 2:
				description = "Click on each row number to plot `(p; V)`. Click on the correct spots on the graph paper and draw a curve of best fit. ";
			break;
			case 3:
				description = "Click on play button to see the conclusion that can be drawn from this experiment. ";
			break;
		}
		return description;
	},
	getInstructionList: function () {
		var self = this;
		var helpList = "";
		switch (self.page){
			case 1:
			helpList = ['The initial pressure and volume of the enclosed gas is shown.',
						'Click on the valve to release the pressure.',
						'Measure the new volume of the gas column, correct to one decimal digit. Enter the value in the correct block in the table.',
						'Repeat Steps 2 and 3 until the first two columns of the table are complete.',
						'Click on the CALCULATE button to complete the last two columns of the table.',
						'For each row of the table, calculate `1/p` and `pV`. Enter the values in the correct blocks in the table. Round the `1/p` values to four decimal digits and the `pV` values to the nearest integer.',
						'Go to the GRAPHS tab to continue with the experiment.'];
			break;
			case 2:
				helpList = ['You should first work through the MEASUREMENTS tab before you continue. ',
							'Start with the graph on the top. ',
							'Click on a row number to plot `(p;V)`. Find the `p` value on the horizontal axis and the `V` value on the vertical axis. ',
							'Click on the correct spot on the graph paper to show where the point must be plotted.',
							'Repeat Steps 3 and 4 until all the points are plotted.',
							'Go to the graph behind by using the switch button.',
							'Click on a row number to plot `(1/p \; V)`. Find the `1/p` value on the horizontal axis and the `V` value on the vertical axis.',
							'Click on the correct spot on the graph paper to show where the point must be plotted.',
							'Repeat Steps 7 and 8 until all the points are plotted.',
							'Click on the DRAW button. A curve or line of best fit will be drawn through your plotted points. ',
							'Click on the GRADIENT button for calculations about the gradient of the `V` versus `1/p` graph.',
							'Go to the CONCLUSION tab to continue with the experiment.'];
			break;
			case 3:
				helpList = ['You should first work through the MEASUREMENTS and GRAPHS tabs before you continue.',
							'Click on the CONCLUSION button to see how we can interpret the table and the graphs obtained in this experiment.',
							'Click on the TABLE or GRAPHS button any time when you want to look at the table or the graphs again.',
							'Click on the RESET button to start again. '];
			break;
		}
		return helpList;
	},
	setMainHeader: function (select) {
		var self = this;
		var header = self.getMainHeader();
		$(select || self.pageHeader).html(header);
		self.pageHeader = select || self.pageHeader;
		// $(select).parent().removeClass('description_text_' + self.page - 1).addClass('description_text_' + self.page);
	},
	getMainHeader: function () {
		var self = this;
		var header = "Verify Boyle’s Law";
		return header;
	},
	//////////
	clearMain : function () {
		var self = this;
		$('.left-block').html('')
		$('.right-block').html('');
		$('.popup-show-block').css('visibility','hidden');

	},
	createPageConclusion : function () {
		var self = this;
		self.addMainBlock('<div id="conclusion"></div>');
		$('#conclusion').append('<div class="pageNumber">1.</div>');
		$('#conclusion').append('<div class="conclusionImage"><img id="nextPage"><img id="conclusionImage"></div>');
		$('#conclusion').append('<div class="pagePagination">\
			<button class="pagePaginationButton" id="pageChange_prev">\<</button>\
			<button class="pagePaginationButton paginationButtonClicked" id="pageChange_1">1</button>\
			<button class="pagePaginationButton" id="pageChange_2">2</button>\
			<button class="pagePaginationButton" id="pageChange_3">3</button>\
			<button class="pagePaginationButton" id="pageChange_next">\></button>\
			</div>')
		$('#conclusionImage').attr('src','img/t1.png');
		self.addEventPagePagination()
	},
	addEventConclusion: function(){
		var self = this;
		$(document).off('click','.active-conclusion' ).on('click','.active-conclusion' , function () {
			$('.close-button').trigger('click');
			self.clearMain();
			self.createPageConclusion();
			self.activeConclusionButton();
			self.page = 3;
			self.setInstructionHelp();
			self.setDescription();
			self.setMainHeader();
			self.enableCheck(true);
			self.addTabsEvent();
		})
	},
	pageAnimate: function (){
		var self = this;
		self.animateDo = true;
		$('.pagePaginationButton').removeClass('paginationButtonClicked');
		$('#pageChange_'+self.paginationPageNumber.nextPage).addClass('paginationButtonClicked');
		$('#nextPage').attr('src','img/t'+self.paginationPageNumber.nextPage+'.png');
		setTimeout(function(){
			$('#nextPage').css('visibility','visible');
		},100)
		$( "#conclusionImage" ).animate({opacity: 0}, 1000, function() {
    		$('#conclusionImage').attr('src','img/t'+self.paginationPageNumber.nextPage+'.png');
			$('#nextPage').css('visibility','hidden');
			$('#conclusionImage').css('opacity','1');
			self.animateDo = false;
		});
	},


	addEventPagePagination: function (){
		var self = this;
		self.removePopup('.plot-block')
		$('#pageChange_prev').attr('disabled', true);
		$(document).off('click','.pagePaginationButton').on('click','.pagePaginationButton' , function () {
			if(self.animateDo) return;
			var buttonVal = $(this).text();
			$('#pageChange_prev').attr('disabled', false);
			$('#pageChange_next').attr('disabled', false);
			switch(buttonVal){
				case "1":
					self.paginationPageNumber.prevPage = self.paginationPageNumber.nextPage;
					self.paginationPageNumber.nextPage = 1;
					$('#pageChange_prev').attr('disabled', true);
					self.pageAnimate();
				break;
				case "2":
					self.paginationPageNumber.prevPage = self.paginationPageNumber.nextPage;
					self.paginationPageNumber.nextPage = 2;
					self.pageAnimate();
				break;
				case "3":
					self.paginationPageNumber.prevPage = self.paginationPageNumber.nextPage;
					self.paginationPageNumber.nextPage = 3;
					$('#pageChange_next').attr('disabled', true);
					self.pageAnimate();
				break;
				case "<":
					if(!(self.paginationPageNumber.nextPage==1)){
						self.paginationPageNumber.prevPage = self.paginationPageNumber.nextPage;
						self.paginationPageNumber.nextPage -=1;
						self.pageAnimate();
					}
				break;
				case ">":
					if(!(self.paginationPageNumber.nextPage==3)){
						self.paginationPageNumber.prevPage = self.paginationPageNumber.nextPage;
						self.paginationPageNumber.nextPage +=1;
						self.pageAnimate();
					}
				break;
			}
			if(self.paginationPageNumber.nextPage==1){
				$('#pageChange_prev').attr('disabled', true);
			}else if(self.paginationPageNumber.nextPage==3){
				$('#pageChange_next').attr('disabled', true);
			}
			$('.pageNumber').html(self.paginationPageNumber.nextPage+".")
		})
	},
	insertTableValue: function () {
		var self = this;
		for(var row = 1; row <= self.pressure.length; row++){
			for(var colume = 1; colume < 5; colume++){
				if(colume == 1) {
					self.setInputPressure(row );
				} else if(colume == 2) {
					self.setInputValume(row );
				} else if(colume == 3) {
					self.setInput1DivideP(row);
				} else if(colume == 4) {
					self.setInputPV(row);
				}
			}
		}
		$('input[data-id]').attr('readonly', 'readonly');
	},
	initCloseButton: function () {
		var self = this;
		$(document).off('click','.close-button').on('click','.close-button', function () {
			$(this).parent().remove();
			self.addTableContent();
			self.insertTableValue();
			self.showTableRowNumber();
			self.addForGraphTextTable();
			self.initTableSelectRow();
			self.activateTableColumn();
		})
	},
	addPointXYText: function (graph, e, position = null, index = null) {
		var self = this;
		var tag = e;
		var index = index? index : self.plotIndex;
		if(e.offsetY && !position) {
			position ={
				top: 0,
				left: 0
			} 
			if(graph.hasClass('graph1')) {
				position.top = e.offsetY+(12/index)
				if(index%2) position.left = e.offsetX + 20;
				else position.left = e.offsetX - 110
					if(index == 3) position.top-=20
				position.top+=3
			} else {
				position.top=e.offsetY + (18/index);
				if(index%2) position.left = e.offsetX + 20;
				else position.left = e.offsetX - 130
			}
			tag = e.target;
		} else if (position) {
			if(graph.hasClass('graph2')) {
				position.top+=(18/index);
				if(index%2) position.left+=120;
				else position.left-=20
			} else {
				position.top+=(12/index);
				if(!(index%2)) position.left-=130;
				if(index == 3) position.top-=10
				position.top+=3
			}
		}
		var text = '';
		if(graph.hasClass('graph1')){
			text = '('+ self.pressure[index-1] +' ; '+self.volume[index-1]+')';
		} else {
			text = '('+self.oneDivideP[index-1]+' ; '+self.volume[index-1]+')';
		}
		if(index==5 && graph.hasClass('graph2')){
			position.top += 24;
		}
		text = text.replace(/\./g, ',');
		$(tag).closest('.graph.active').find('.points-axis-text').append('<span class="points-text" style="top:'+(position.top-13)+'px; left:'+position.left+'px">'+text+'</span>');
		// $('.graph.active span[style="top:'+position.top+'px; left:'+position.left+'px"] span.hiddenText').css({visibility:'hidden'});
		// setTimeout(function(){
		// 	$('.graph.active span[style="top:'+position.top+'px; left:'+position.left+'px"] span.hiddenText').css({visibility:'visible'});
		// },700)
	},
	closePopupTwoSeconds: function (select, f = function (){}) {
        var self = this;
        self.setTimeEvent = setTimeout(function () {
            self.removePopup(select);
            f()
        }, 2000);
    },
    addTabsEvent: function () {
        if(this.page == 2) {
            this.addEventTab1();
        }
        if (this.page >= 2){
            this.addEventTab2();
        }
    },
    addEventTab1: function () {
        var self = this
        $(document).off('click', '.enable-measurements').on('click', '.enable-measurements', function () {
        	$('.close-button').trigger('click');
            self.activeMeasurementsButton();
            $('.table-content').remove();
            self.addTableContent();
            self.hideTableRowNumber();
            self.insertTableValue();
            self.addMeterContent();
            self.removePopup('.plot-block');
			$('tr.active-calculate').removeClass('active-calculate');
            $(document).off('click', '.redpipe-meter .buttons');
            $('.wheel-block').css({transform: 'rotate(4deg)'});
			$('.nidle-block').css({transform: 'rotate(-67deg)'});
			$('.meter-block .meter-line').css({height: '27px'});
			$('.scale-100').addClass('active');
			$('.scale-300').removeClass('active');

            self.enableGraphButton();
            $('#conclusion').remove();
            $('.calculate-block').remove();
            $('input[data-id]').attr('readonly','readonly');
            if(self.calculateValue) {
                $('.menu-button .conclusion').removeClass('enable-conclusion not-active-conclusion').addClass('active-conclusion');
            }
            $('#instPanel').removeClass('help_list_2 help_list_3 ')
            var page_number = self.page;
            self.page = 1;
            self.setInstructionHelp('#insTxtPanel');
			self.setDescription('#disc');
			self.setMainHeader('#main_header');
			self.page = page_number;
			if($('.popup-valume-error').length > 0){
                $('.popup-valume-error .try_again').click();
            }
        })
    },
    addEventTab2: function () {
        var self = this
        $(document).off('click', '.enable-graph').on('click', '.enable-graph', function () {
            self.enableMeasurementsButton();
            self.activeGraphButton();
            $('.table-content').remove();
            self.addTableContent();
            self.showTableRowNumber();
            self.insertTableValue();
            self.setInstructionHelp();
            self.setDescription();
            $('.calculate-button-block').remove();
            self.addGraphContent();
            self.addAllPointsToGraph.call(self);
            self.addForGraphTextTable()
            $('#conclusion').remove();
            if (self.gradientSelect && self.graph==2){
                self.activeGradientButton();
                self.removePopup('.plot-block');
				$('tr.active-calculate').removeClass('active-calculate');
				self.addCalculateBlockContent();
                self.initCloseButton();
                if(self.calculateValue){
                	self.initEventShowAnswer();
                	$('.show_answer').click();
                	$('.calculate-block input').val(self.calculateValue.replace('.', ','))
                    $('.calculate-block .status').css({visibility: 'visible'});
                    if(self.calculateValue <= 3300 && self.calculateValue >= 2750){
                    	$('.calculate-block .status').attr('src','img/right.png');
                    }
                	$('.menu-button .conclusion').removeClass('enable-conclusion not-active-conclusion').addClass('active-conclusion');
                }
                self.removePopup('.show-instuction');
            }
            if(self.graph1Line) self.addlineGraph1();
            if(self.graph2Line) self.addlineGraph2();
            if(self.gradientLine){
            	self.addlineGraphGradient();
            	self.activeGradientButton();
            }
            if($('.graphs-block .graph.active').hasClass('graph1')&&self.graph1Line ||
            	$('.graphs-block .graph.active').hasClass('graph2')&&self.graph2Line){
            	self.activeDrawButton();
            }else if($('.graphs-block .graph.active').hasClass('graph1')&&self.paramsForGraph.graph1OKnumbers.length==6 || 
            	$('.graphs-block .graph.active').hasClass('graph2')&&self.paramsForGraph.graph2OKnumbers.length==6 ){
            	self.addEventDrawButton();
            }else {
            	self.disableDrawButton();
            }
            if($('.graphs-block .graph.active').hasClass('graph2')&&self.graph2Line){
            	$('.gradient-button').addClass('active');
            	if(self.gradientLine){
            		$('.gradient-button').removeClass('active').addClass('enable');
            	}
            }
            if($('.graphs-block .graph.active').hasClass('graph1')){
            	$('.gradient-button').removeClass('active enable');
            }
        	$('input[data-id]').attr('readonly','readonly');
        	$('#instPanel').removeClass('help_list_3')
        	var page_number = self.page;
            self.page = 2;
            self.setInstructionHelp('#insTxtPanel');
			self.setDescription('#disc');
			self.setMainHeader('#main_header');
			self.page = page_number;
            // if($('.graphs-block .graph.active').hasClass('graph2')&&self.paramsForGraph.graph2OKnumbers.leng)
        })
    },
    addAllPointsToGraph: function () {
    	var self = this;
		$('.graphs-block .graph1').removeClass('active')
		var select = $('.graphs-block .graph2 .points')
		var block_width = $(select).width();
		var block_height = $(select).height();
		var mashtabY = block_height/35;
		for(var i = 0; i < self.paramsForGraph.graph2OKnumbers.length; i++){
			var point = $('<div class="point" ></div>');
			var index = self.paramsForGraph.graph2OKnumbers[i];
			self.plotIndex = index
			var mashtabX = block_width / 0.01;
			$(point).css({top: (block_height - mashtabY * self.volume[index - 1]) - 5, left: (self.oneDivideP[index - 1]*mashtabX) - 5 + 'px'});
			$('.graphs-block .graph2 .points-axis-text').append(point);
			self.addPointXYText($('.graphs-block .graph2'), select, {top: (block_height - mashtabY * self.volume[index - 1]) - 5, left: (self.oneDivideP[index - 1]*mashtabX) - 110 }, index);
		}
		
		$('.graphs-block .graph2').removeClass('active').addClass('not-active')
		var select = $('.graphs-block .graph1 .points')
		var block_width = $(select).width();
		var block_height = $(select).height();
		var mashtabY = block_height/35;
		for(var i = 0; i < self.paramsForGraph.graph1OKnumbers.length; i++){
			var point = $('<div class="point" ></div>');
			var index = self.paramsForGraph.graph1OKnumbers[i];
			var mashtabX = block_width / 400;
			$(point).css({top: (block_height - mashtabY * self.volume[index - 1]) - 7, left: ((self.pressure[index - 1])*mashtabX) - 7 + 'px'});
			$('.graphs-block .graph1 .points-axis-text').append(point);
			self.addPointXYText($('.graphs-block .graph1').removeClass('not-active').addClass('active'), select, {top: (block_height - mashtabY * self.volume[index - 1]) - 7, left: ((self.pressure[index - 1])*mashtabX) + 20 }, index);
		}
		$('.graphs-block .graph').removeClass('active').addClass('not-active');
		$('.graphs-block .graph'+self.graph).removeClass('not-active').addClass('active');
		self.activateTableColumn();
		if(self.paramsForGraph.graph1OKnumbers.length == 6) {
			self.addEventDrawButton();
			self.removePopup('.plot-block');
			$('tr.active-calculate').removeClass('active-calculate');
		}
    }
}