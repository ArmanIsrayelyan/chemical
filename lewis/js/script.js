currentFunction = '';
currentAnswer = '';
formula = '';
var zoomScale = 1;
$("body").click(function() {
	hideNotifications();
});
$(document).on("click", "#checkBtn", function() {
    window[formula]();
});
$(document).on("click", "#showAnsBtn", function() {
	currentAnswer();
});
function hideNotifications() {
	$("#responceBox").hide().removeClass("toTop");
	$("#responceBoxAns").removeClass("toTop").hide();
	$("#responceAlert").removeClass("toTop").hide();
}
function result(res) {
	if (res) {
		$(".emptyBox").addClass("correctAnswer");
		$("#responceBoxAns").show().addClass('toTop');
		enableCheck(false);
	} else {
		enableCheck(false);
		$("#responceAlert").show().addClass('toTop');
	}
}
function callResponseBox() {
    enableCheck(false);
    $("#responceBox").show().addClass('toTop');
}
function enableCheck(flag) {
	flag ? $("#checkBtn").addClass("readyToCheck").prop('disabled', false) : $("#checkBtn").removeClass("readyToCheck").prop('disabled', true)
}
function answerbr2() {
	renderbr2();
	$(".emptyItems").html(`
		<h1 class="f1 ml-4">Br<sub>2</sub></h1>
		<div class="emptyWrapperN2">
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="11"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="o" style="z-index: 5;"><img src="assets/img/oo.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="21"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="o" style="z-index: 5;"><img src="assets/img/oo.png" alt=""></div></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m6 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="br" style="z-index: 9;">Br</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="22"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="o" style="z-index: 5;"><img src="assets/img/oo.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppable3Hand connectorL ui-droppable correctAnswer" data-position="31"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="x" style="z-index: 5;"><img src="assets/img/ox.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="41"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m6 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="br" style="z-index: 9;">Br</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="42"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="51"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
		</div>
	`);
	$(".emptyBox").addClass("correctAnswer");
}
function answero2() {
	rendero2();
	$(".emptyItems").html(`
		<h1 class="f2 ml-4">O<sub>2</sub></h1>
		<div class="emptyWrapperN2">
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="11"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="o" style="z-index: 5;"><img src="assets/img/oo.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="21"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="o" style="z-index: 5;"><img src="assets/img/oo.png" alt=""></div></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m5 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="o" style="z-index: 9;">O</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="22"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppable3Hand connectorL ui-droppable correctAnswer" data-position="31"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="x" style="z-index: 5;"><img src="assets/img/ox.png" alt=""></div><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="x" style="z-index: 5;"><img src="assets/img/ox.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="41"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m5 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="o" style="z-index: 9;">O</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="42"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="51"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
		</div>
	`);
	$(".emptyBox").addClass("correctAnswer");
}
function answern2() {
	rendern2();
	$(".emptyItems").html(`
		<h1 class="f3 ml-4">N<sub>2</sub></h1>
		<div class="emptyWrapperN2">
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="11"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="o" style="z-index: 5;"><img src="assets/img/oo.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="21"></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m4 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="n" style="z-index: 9;">N</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="22"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppable3Hand connectorL ui-droppable ui-droppable-disabled correctAnswer" data-position="31"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="x" style="z-index: 5;"><img src="assets/img/ox.png" alt=""></div><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="x" style="z-index: 5;"><img src="assets/img/ox.png" alt=""></div><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="x" style="z-index: 5;"><img src="assets/img/ox.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="41"></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m4 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="n" style="z-index: 9;">N</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="42"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="51"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
		</div>
	`);
	$(".emptyBox").addClass("correctAnswer");
}
function answerhcl() {
	renderhcl();
	$(".emptyItems").html(`
		<h1 class="f4 ml-4">HCl</h1>
		<div class="emptyWrapperN2">
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable correctAnswer" data-position="11"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="21"></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m1 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="h" style="z-index: 9;">H</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="22"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppable3Hand connectorL ui-droppable correctAnswer" data-position="31"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="x" style="z-index: 5;"><img src="assets/img/ox.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="41"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m3 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="cl" style="z-index: 9;">Cl</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="42"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="51"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
		</div>
	`);
	$(".emptyBox").addClass("correctAnswer");
}
function answerhcn() {
	renderhcn();
	$(".emptyItems").html(`
		<h1 class="f5 ml-4">HCN</h1>
        <div class="emptyWrapperHcn">
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable correctAnswer" data-position="11"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="21"></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m1 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="h" style="z-index: 9;">H</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="22"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppable3Hand connectorL ui-droppable correctAnswer" data-position="31"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="x" style="z-index: 5;"><img src="assets/img/ox.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="41"></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m2 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="c" style="z-index: 9;">C</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="42"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppable3Hand connectorR ui-droppable ui-droppable-disabled correctAnswer" data-position="51"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="o" style="z-index: 5;"><img src="assets/img/xo.png" alt=""></div><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="o" style="z-index: 5;"><img src="assets/img/xo.png" alt=""></div><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="o" style="z-index: 5;"><img src="assets/img/xo.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="61"></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m4 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="n" style="z-index: 9;">N</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="62"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="71"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="o" style="z-index: 5;"><img src="assets/img/oo.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
		</div>
	`);
	$(".emptyBox").addClass("correctAnswer");
}
function answerh2o() {
	renderh2o();
	$(".emptyItems").html(`
		<h1 class="f6 ml-4">H<sub>2</sub>O</h1>
		<div class="emptyWrapperH2o">
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable correctAnswer" data-position="11"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>

			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="21"></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m1 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="h" style="z-index: 9;">H</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="22"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>

			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="31"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="x" style="z-index: 5;"><img src="assets/img/ox.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable correctAnswer" data-position="32"></div>
			<div class="emptyBox hidden correctAnswer"></div>

			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="41"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="o" style="z-index: 5;"><img src="assets/img/oo.png" alt=""></div></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m5 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="o" style="z-index: 9;">O</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="42"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="o" style="z-index: 5;"><img src="assets/img/xo.png" alt=""></div></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m1 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="h" style="z-index: 9;">H</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="43"></div>

			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="51"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="o" style="z-index: 5;"><img src="assets/img/oo.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable correctAnswer" data-position="52"></div>
			<div class="emptyBox hidden correctAnswer"></div>
		</div>
	`);
	$(".emptyBox").addClass("correctAnswer");
}
function answerof2() {
	renderof2();
	$(".emptyItems").html(`
		<h1 class="f7 ml-4">OF<sub>2</sub></h1>
		<div class="emptyWrapperH2o">
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="11"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>

			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="21"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m8 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="f" style="z-index: 9;">F</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="22"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>

			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="31"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="o" style="z-index: 5;"><img src="assets/img/xo.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="32"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>

			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="41"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="o" style="z-index: 5;"><img src="assets/img/oo.png" alt=""></div></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m5 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="o" style="z-index: 9;">O</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="42"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="x" style="z-index: 5;"><img src="assets/img/ox.png" alt=""></div></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m8 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="f" style="z-index: 9;">F</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="43"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>

			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="51"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="o" style="z-index: 5;"><img src="assets/img/oo.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="52"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
		</div>
	`);
	$(".emptyBox").addClass("correctAnswer");
}
function answerco2() {
	renderco2();
	$(".emptyItems").html(`
		<h1 class="f8 ml-4">CO<sub>2</sub></h1>
        <div class="emptyWrapperHcn">
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="11"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="21"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m5 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="o" style="z-index: 9;">O</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="22"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppable3Hand connectorL ui-droppable correctAnswer" data-position="31"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="o" style="z-index: 5;"><img src="assets/img/xo.png" alt=""></div><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="o" style="z-index: 5;"><img src="assets/img/xo.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="41"></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m2 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="c" style="z-index: 9;">C</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="42"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppable3Hand connectorR ui-droppable correctAnswer" data-position="51"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="x" style="z-index: 5;"><img src="assets/img/ox.png" alt=""></div><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="x" style="z-index: 5;"><img src="assets/img/ox.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="61"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m5 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="o" style="z-index: 9;">O</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="62"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="71"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
		</div>
	`);
	$(".emptyBox").addClass("correctAnswer");
}
function answerhocl() {
	renderhocl();
	$(".emptyItems").html(`
		<h1 class="f9 ml-4">HOCl</h1>
		<div class="emptyWrapperH2o">
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable correctAnswer" data-position="11"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>

			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="21"></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m1 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="h" style="z-index: 9;">H</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="22"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>

			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="31"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="o" style="z-index: 5;"><img src="assets/img/xo.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="32"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>

			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="41"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="o" style="z-index: 5;"><img src="assets/img/oo.png" alt=""></div></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m5 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="o" style="z-index: 9;">O</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="42"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="x" style="z-index: 5;"><img src="assets/img/ox.png" alt=""></div></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m3 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="cl" style="z-index: 9;">Cl</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="43"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>

			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="51"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="o" style="z-index: 5;"><img src="assets/img/oo.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="52"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
		</div>
	`);
	$(".emptyBox").addClass("correctAnswer");
}
function answernh3() {
	rendernh3();
	$(".emptyItems").html(`
		<h1 class="f10 ml-4">NH<sub>3</sub></h1>
        <div class="emptyWrapperNh3">
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable correctAnswer" data-position="11"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>

			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="21"></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m1 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="h" style="z-index: 9;">H</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="22"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>

			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="31"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="x" style="z-index: 5;"><img src="assets/img/ox.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable correctAnswer" data-position="32"></div>
			<div class="emptyBox hidden correctAnswer"></div>

			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="41"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m4 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="n" style="z-index: 9;">N</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="42"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="x" style="z-index: 5;"><img src="assets/img/ox.png" alt=""></div></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m1 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="h" style="z-index: 9;">H</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="43"></div>

			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="51"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="o" style="z-index: 5;"><img src="assets/img/xo.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable correctAnswer" data-position="52"></div>
			<div class="emptyBox hidden correctAnswer"></div>

			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="61"></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m1 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="h" style="z-index: 9;">H</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="62"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>

			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable correctAnswer" data-position="71"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>
		</div>
	`);
	$(".emptyBox").addClass("correctAnswer");
}
function answerbf3() {
	renderbf3();
	$(".emptyItems").html(`
		<h1 class="f11 ml-4">BF<sub>3</sub></h1>
        <div class="emptyWrapperNh3">
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="11"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>

			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="21"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m8 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="f" style="z-index: 9;">F</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="22"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>

			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="31"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="o" style="z-index: 5;"><img src="assets/img/xo.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="32"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>

			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="41"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="o" style="z-index: 5;"><img src="assets/img/oo.png" alt=""></div></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m7 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="b" style="z-index: 9;">B</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="42"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="o" style="z-index: 5;"><img src="assets/img/xo.png" alt=""></div></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m8 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="f" style="z-index: 9;">F</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="43"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>

			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="51"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="x" style="z-index: 5;"><img src="assets/img/ox.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="52"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>

			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="61"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m8 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="f" style="z-index: 9;">F</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="62"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>

			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="71"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="x" style="z-index: 5;"><img src="assets/img/xx.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>
		</div>
	`);
	$(".emptyBox").addClass("correctAnswer");
}
function answerch4() {
	renderch4();
	$(".emptyItems").html(`
		<h1 class="f12 ml-2">CH<sub>4</sub></h1>
		<div class="emptyWrapperCh4 pl-5">
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable correctAnswer" data-position="41"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>

			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="31"></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m1 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="h" style="z-index: 9;">H</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="51"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>

			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable correctAnswer" data-position="21"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="42"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="o" style="z-index: 5;"><img src="assets/img/xo.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable correctAnswer" data-position="61"></div>
			<div class="emptyBox hidden correctAnswer"></div>

			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="11"></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m1 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="h" style="z-index: 9;">H</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="32"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="o" style="z-index: 5;"><img src="assets/img/xo.png" alt=""></div></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m2 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="c" style="z-index: 9;">C</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable ui-droppable-disabled correctAnswer" data-position="52"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="x" data-r="o" style="z-index: 5;"><img src="assets/img/xo.png" alt=""></div></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m1 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="h" style="z-index: 9;">H</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="71"></div>

			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable correctAnswer" data-position="22"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable ui-droppable-disabled correctAnswer" data-position="43"><div class="ui-draggable ui-draggable-handle usedDraggableHand" data-l="o" data-r="x" style="z-index: 5;"><img src="assets/img/ox.png" alt=""></div></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable correctAnswer" data-position="62"></div>
			<div class="emptyBox hidden correctAnswer"></div>

			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="33"></div>
			<div class="emptyBox droppableAtom ui-droppable ui-droppable-disabled correctAnswer"><div class="fillBox m1 ui-draggable ui-draggable-handle usedDraggableAtom" data-name="h" style="z-index: 9;">H</div></div>
			<div class="emptyBox droppableHand cornerX ui-droppable correctAnswer" data-position="53"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>

			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox droppableHand cornerY ui-droppable correctAnswer" data-position="44"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>
			<div class="emptyBox hidden correctAnswer"></div>
		</div>
	`);
	$(".emptyBox").addClass("correctAnswer");
}
function renderbr2() {
	currentFunction = renderbr2;
	currentAnswer = answerbr2;
	$("#mainContent").html(`
		<section id="buildDiagram">
			<div class="section-content">
				<div class="container">
					<div class="overflow-hidden">
						<div class="emptyItems emptyItems21 float-left px-2">
							<h1 class="f1 ml-4">Br<sub>2</sub></h1>
							<div class="emptyWrapperN2">
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="11"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerX" data-position="21"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="22"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppable3Hand connectorL" data-position="31"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerX" data-position="41"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="42"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="51"></div>
								<div class="emptyBox hidden"></div>
							</div>
						</div>
						<div class="fillItems float-right px-2">
							<div class="overflow-hidden">
								<span class="close toHome" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</span>
							</div>
							<div class="fillArrow">
								<img src="assets/img/arrow.png" alt="">
							</div>
							<div class="fillWrapper">
								<div class="fillBox draggableAtom m1" data-name="h">H</div>
								<div class="fillBox draggableAtom m4" data-name="n">N</div>
								<div class="fillBox draggableAtom m7" data-name="b">B</div>
								<div class="fillBox draggableAtom m2" data-name="c">C</div>
								<div class="fillBox draggableAtom m5" data-name="o">O</div>
								<div class="fillBox draggableAtom m8" data-name="f">F</div>
								<div class="fillBox draggableAtom m3" data-name="cl">Cl</div>
								<div class="fillBox draggableAtom m6" data-name="br">Br</div>
							</div>
							<div class="fillConnect">
                                <div class="draggableHand" data-l="x" data-r="o"><img src="assets/img/xo.png" alt=""></div>
                                <div class="draggableHand" data-l="o" data-r="x"><img src="assets/img/ox.png" alt=""></div>
                                <div class="draggableHand" data-l="o" data-r="o"><img src="assets/img/oo.png" alt=""></div>
                                <div class="draggableHand" data-l="x" data-r="x"><img src="assets/img/xx.png" alt=""></div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</section>
    `);
    initDrag(7, -2);
}
function rendero2() {
	currentFunction = rendero2;
	currentAnswer = answero2;
	$("#mainContent").html(`
		<section id="buildDiagram">
			<div class="section-content">
				<div class="container">
					<div class="overflow-hidden">
						<div class="emptyItems emptyItems21 float-left px-2">
							<h1 class="f2 ml-4">O<sub>2</sub></h1>
							<div class="emptyWrapperN2">
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="11"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerX" data-position="21"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="22"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppable3Hand connectorL" data-position="31"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerX" data-position="41"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="42"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="51"></div>
								<div class="emptyBox hidden"></div>
							</div>
						</div>
						<div class="fillItems float-right px-2">
							<div class="overflow-hidden">
								<span class="close toHome" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</span>
							</div>
							<div class="fillArrow">
								<img src="assets/img/arrow.png" alt="">
							</div>
							<div class="fillWrapper">
								<div class="fillBox draggableAtom m1" data-name="h">H</div>
								<div class="fillBox draggableAtom m4" data-name="n">N</div>
								<div class="fillBox draggableAtom m7" data-name="b">B</div>
								<div class="fillBox draggableAtom m2" data-name="c">C</div>
								<div class="fillBox draggableAtom m5" data-name="o">O</div>
								<div class="fillBox draggableAtom m8" data-name="f">F</div>
								<div class="fillBox draggableAtom m3" data-name="cl">Cl</div>
								<div class="fillBox draggableAtom m6" data-name="br">Br</div>
                            </div>
							<div class="fillConnect">
                                <div class="draggableHand" data-l="x" data-r="o"><img src="assets/img/xo.png" alt=""></div>
                                <div class="draggableHand" data-l="o" data-r="x"><img src="assets/img/ox.png" alt=""></div>
                                <div class="draggableHand" data-l="o" data-r="o"><img src="assets/img/oo.png" alt=""></div>
                                <div class="draggableHand" data-l="x" data-r="x"><img src="assets/img/xx.png" alt=""></div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</section>
    `);
    initDrag(6, -2);
}
function rendern2() {
	currentFunction = rendern2;
	currentAnswer = answern2;
	$("#mainContent").html(`
		<section id="buildDiagram">
			<div class="section-content">
				<div class="container">
					<div class="overflow-hidden">
						<div class="emptyItems emptyItems21 float-left px-2">
							<h1 class="f3 ml-4">N<sub>2</sub></h1>
							<div class="emptyWrapperN2">
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="11"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerX" data-position="21"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="22"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppable3Hand connectorL" data-position="31"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerX" data-position="41"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="42"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="51"></div>
								<div class="emptyBox hidden"></div>
							</div>
						</div>
						<div class="fillItems float-right px-2">
							<div class="overflow-hidden">
								<span class="close toHome" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</span>
							</div>
							<div class="fillArrow">
								<img src="assets/img/arrow.png" alt="">
							</div>
							<div class="fillWrapper">
								<div class="fillBox draggableAtom m1" data-name="h">H</div>
								<div class="fillBox draggableAtom m4" data-name="n">N</div>
								<div class="fillBox draggableAtom m7" data-name="b">B</div>
								<div class="fillBox draggableAtom m2" data-name="c">C</div>
								<div class="fillBox draggableAtom m5" data-name="o">O</div>
								<div class="fillBox draggableAtom m8" data-name="f">F</div>
								<div class="fillBox draggableAtom m3" data-name="cl">Cl</div>
								<div class="fillBox draggableAtom m6" data-name="br">Br</div>
                            </div>
							<div class="fillConnect">
                                <div class="draggableHand" data-l="x" data-r="o"><img src="assets/img/xo.png" alt=""></div>
                                <div class="draggableHand" data-l="o" data-r="x"><img src="assets/img/ox.png" alt=""></div>
                                <div class="draggableHand" data-l="o" data-r="o"><img src="assets/img/oo.png" alt=""></div>
                                <div class="draggableHand" data-l="x" data-r="x"><img src="assets/img/xx.png" alt=""></div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</section>
    `);
    initDrag(5, -2);
}
function renderhcl() {
	currentFunction = renderhcl;
	currentAnswer = answerhcl;
	$("#mainContent").html(`
		<section id="buildDiagram">
			<div class="section-content">
				<div class="container">
					<div class="overflow-hidden">
						<div class="emptyItems emptyItems21 float-left px-2">
							<h1 class="f4 ml-4">HCl</h1>
							<div class="emptyWrapperN2">
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="11"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerX" data-position="21"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="22"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppable3Hand connectorL" data-position="31"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerX" data-position="41"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="42"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="51"></div>
								<div class="emptyBox hidden"></div>
							</div>
						</div>
						<div class="fillItems float-right px-2">
							<div class="overflow-hidden">
								<span class="close toHome" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</span>
							</div>
							<div class="fillArrow">
								<img src="assets/img/arrow.png" alt="">
							</div>
							<div class="fillWrapper">
								<div class="fillBox draggableAtom m1" data-name="h">H</div>
								<div class="fillBox draggableAtom m4" data-name="n">N</div>
								<div class="fillBox draggableAtom m7" data-name="b">B</div>
								<div class="fillBox draggableAtom m2" data-name="c">C</div>
								<div class="fillBox draggableAtom m5" data-name="o">O</div>
								<div class="fillBox draggableAtom m8" data-name="f">F</div>
								<div class="fillBox draggableAtom m3" data-name="cl">Cl</div>
								<div class="fillBox draggableAtom m6" data-name="br">Br</div>
                            </div>
							<div class="fillConnect">
                                <div class="draggableHand" data-l="x" data-r="o"><img src="assets/img/xo.png" alt=""></div>
                                <div class="draggableHand" data-l="o" data-r="x"><img src="assets/img/ox.png" alt=""></div>
                                <div class="draggableHand" data-l="o" data-r="o"><img src="assets/img/oo.png" alt=""></div>
                                <div class="draggableHand" data-l="x" data-r="x"><img src="assets/img/xx.png" alt=""></div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</section>
    `);
    initDrag(4, -2);
}
function renderhcn() {
	currentFunction = renderhcn;
	currentAnswer = answerhcn;
	$("#mainContent").html(`
		<section id="buildDiagram">
            <div class="section-content">
                <div class="container">
                    <div class="overflow-hidden">
                        <div class="emptyItems emptyItems31 float-left px-2">
                            <h1 class="f5 ml-4">HCN</h1>
							<div class="emptyWrapperHcn">
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="11"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerX" data-position="21"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="22"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppable3Hand connectorL" data-position="31"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerX" data-position="41"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="42"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppable3Hand connectorR" data-position="51"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerX" data-position="61"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="62"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="71"></div>
								<div class="emptyBox hidden"></div>
							</div>
                        </div>
                        <div class="fillItems float-right px-2">
                            <div class="overflow-hidden">
								<span class="close toHome" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</span>
							</div>
							<div class="fillArrow">
								<img src="assets/img/arrow.png" alt="">
							</div>
                            <div class="fillWrapper">
                                <div class="fillBox draggableAtom m1" data-name="h">H</div>
								<div class="fillBox draggableAtom m4" data-name="n">N</div>
								<div class="fillBox draggableAtom m7" data-name="b">B</div>
								<div class="fillBox draggableAtom m2" data-name="c">C</div>
								<div class="fillBox draggableAtom m5" data-name="o">O</div>
								<div class="fillBox draggableAtom m8" data-name="f">F</div>
								<div class="fillBox draggableAtom m3" data-name="cl">Cl</div>
								<div class="fillBox draggableAtom m6" data-name="br">Br</div>
                            </div>
                            <div class="fillConnect">
                                <div class="draggableHand" data-l="x" data-r="o"><img src="assets/img/xo.png" alt=""></div>
                                <div class="draggableHand" data-l="o" data-r="x"><img src="assets/img/ox.png" alt=""></div>
                                <div class="draggableHand" data-l="o" data-r="o"><img src="assets/img/oo.png" alt=""></div>
                                <div class="draggableHand" data-l="x" data-r="x"><img src="assets/img/xx.png" alt=""></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    `);
    initDrag(5);
}
function renderh2o() {
	currentFunction = renderh2o;
	currentAnswer = answerh2o;
	$("#mainContent").html(`
		<section id="buildDiagram">
			<div class="section-content">
				<div class="container">
					<div class="overflow-hidden">
						<div class="emptyItems emptyItems22 float-left px-2">
							<h1 class="f6 ml-4">H<sub>2</sub>O</h1>
							<div class="emptyWrapperH2o">
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="11"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox hidden"></div>

								<div class="emptyBox droppableHand cornerX" data-position="21"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="22"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox hidden"></div>

								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="31"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="32"></div>
								<div class="emptyBox hidden"></div>

								<div class="emptyBox droppableHand cornerX" data-position="41"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="42"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="43"></div>

								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="51"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="52"></div>
								<div class="emptyBox hidden"></div>
							</div>
						</div>
						<div class="fillItems float-right px-2">
							<div class="overflow-hidden">
								<span class="close toHome" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</span>
							</div>
							<div class="fillArrow">
								<img src="assets/img/arrow.png" alt="">
							</div>
							<div class="fillWrapper">
								<div class="fillBox draggableAtom m1" data-name="h">H</div>
								<div class="fillBox draggableAtom m4" data-name="n">N</div>
								<div class="fillBox draggableAtom m7" data-name="b">B</div>
								<div class="fillBox draggableAtom m2" data-name="c">C</div>
								<div class="fillBox draggableAtom m5" data-name="o">O</div>
								<div class="fillBox draggableAtom m8" data-name="f">F</div>
								<div class="fillBox draggableAtom m3" data-name="cl">Cl</div>
								<div class="fillBox draggableAtom m6" data-name="br">Br</div>
                            </div>
							<div class="fillConnect">
                                <div class="draggableHand" data-l="x" data-r="o"><img src="assets/img/xo.png" alt=""></div>
                                <div class="draggableHand" data-l="o" data-r="x"><img src="assets/img/ox.png" alt=""></div>
                                <div class="draggableHand" data-l="o" data-r="o"><img src="assets/img/oo.png" alt=""></div>
                                <div class="draggableHand" data-l="x" data-r="x"><img src="assets/img/xx.png" alt=""></div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</section>
    `);
    initDrag(4);
}
function renderof2() {
	currentFunction = renderof2;
	currentAnswer = answerof2;
	$("#mainContent").html(`
		<section id="buildDiagram">
			<div class="section-content">
				<div class="container">
					<div class="overflow-hidden">
						<div class="emptyItems emptyItems22 float-left px-2">
							<h1 class="f7 ml-4">OF<sub>2</sub></h1>
							<div class="emptyWrapperH2o">
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="11"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox hidden"></div>

								<div class="emptyBox droppableHand cornerX" data-position="21"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="22"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox hidden"></div>

								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="31"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="32"></div>
								<div class="emptyBox hidden"></div>

								<div class="emptyBox droppableHand cornerX" data-position="41"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="42"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="43"></div>

								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="51"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="52"></div>
								<div class="emptyBox hidden"></div>
							</div>
						</div>
						<div class="fillItems float-right px-2">
							<div class="overflow-hidden">
								<span class="close toHome" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</span>
							</div>
							<div class="fillArrow">
								<img src="assets/img/arrow.png" alt="">
							</div>
							<div class="fillWrapper">
								<div class="fillBox draggableAtom m1" data-name="h">H</div>
								<div class="fillBox draggableAtom m4" data-name="n">N</div>
								<div class="fillBox draggableAtom m7" data-name="b">B</div>
								<div class="fillBox draggableAtom m2" data-name="c">C</div>
								<div class="fillBox draggableAtom m5" data-name="o">O</div>
								<div class="fillBox draggableAtom m8" data-name="f">F</div>
								<div class="fillBox draggableAtom m3" data-name="cl">Cl</div>
								<div class="fillBox draggableAtom m6" data-name="br">Br</div>
                            </div>
							<div class="fillConnect">
                                <div class="draggableHand" data-l="x" data-r="o"><img src="assets/img/xo.png" alt=""></div>
                                <div class="draggableHand" data-l="o" data-r="x"><img src="assets/img/ox.png" alt=""></div>
                                <div class="draggableHand" data-l="o" data-r="o"><img src="assets/img/oo.png" alt=""></div>
                                <div class="draggableHand" data-l="x" data-r="x"><img src="assets/img/xx.png" alt=""></div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</section>
    `);
    initDrag(10);
}
function renderco2() {
	currentFunction = renderco2;
	currentAnswer = answerco2;
	$("#mainContent").html(`
		<section id="buildDiagram">
            <div class="section-content">
                <div class="container">
                    <div class="overflow-hidden">
                        <div class="emptyItems emptyItems31 float-left px-2">
                            <h1 class="f8 ml-4">CO<sub>2</sub></h1>
							<div class="emptyWrapperHcn">
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="11"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerX" data-position="21"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="22"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppable3Hand connectorL" data-position="31"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerX" data-position="41"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="42"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppable3Hand connectorR" data-position="51"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerX" data-position="61"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="62"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="71"></div>
								<div class="emptyBox hidden"></div>
							</div>
                        </div>
                        <div class="fillItems float-right px-2">
                            <div class="overflow-hidden">
								<span class="close toHome" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</span>
							</div>
							<div class="fillArrow">
								<img src="assets/img/arrow.png" alt="">
							</div>
                            <div class="fillWrapper">
                                <div class="fillBox draggableAtom m1" data-name="h">H</div>
								<div class="fillBox draggableAtom m4" data-name="n">N</div>
								<div class="fillBox draggableAtom m7" data-name="b">B</div>
								<div class="fillBox draggableAtom m2" data-name="c">C</div>
								<div class="fillBox draggableAtom m5" data-name="o">O</div>
								<div class="fillBox draggableAtom m8" data-name="f">F</div>
								<div class="fillBox draggableAtom m3" data-name="cl">Cl</div>
								<div class="fillBox draggableAtom m6" data-name="br">Br</div>
                            </div>
                            <div class="fillConnect">
                                <div class="draggableHand" data-l="x" data-r="o"><img src="assets/img/xo.png" alt=""></div>
                                <div class="draggableHand" data-l="o" data-r="x"><img src="assets/img/ox.png" alt=""></div>
                                <div class="draggableHand" data-l="o" data-r="o"><img src="assets/img/oo.png" alt=""></div>
                                <div class="draggableHand" data-l="x" data-r="x"><img src="assets/img/xx.png" alt=""></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    `);
    initDrag(8);
}
function renderhocl() {
	currentFunction = renderhocl;
	currentAnswer = answerhocl;
	$("#mainContent").html(`
		<section id="buildDiagram">
			<div class="section-content">
				<div class="container">
					<div class="overflow-hidden">
						<div class="emptyItems emptyItems22 float-left px-2">
							<h1 class="f9 ml-4">HOCl</h1>
							<div class="emptyWrapperH2o">
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="11"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox hidden"></div>

								<div class="emptyBox droppableHand cornerX" data-position="21"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="22"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox hidden"></div>

								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="31"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="32"></div>
								<div class="emptyBox hidden"></div>

								<div class="emptyBox droppableHand cornerX" data-position="41"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="42"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="43"></div>

								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="51"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="52"></div>
								<div class="emptyBox hidden"></div>
							</div>
						</div>
						<div class="fillItems float-right px-2">
							<div class="overflow-hidden">
								<span class="close toHome" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</span>
							</div>
							<div class="fillArrow">
								<img src="assets/img/arrow.png" alt="">
							</div>
							<div class="fillWrapper">
								<div class="fillBox draggableAtom m1" data-name="h">H</div>
								<div class="fillBox draggableAtom m4" data-name="n">N</div>
								<div class="fillBox draggableAtom m7" data-name="b">B</div>
								<div class="fillBox draggableAtom m2" data-name="c">C</div>
								<div class="fillBox draggableAtom m5" data-name="o">O</div>
								<div class="fillBox draggableAtom m8" data-name="f">F</div>
								<div class="fillBox draggableAtom m3" data-name="cl">Cl</div>
								<div class="fillBox draggableAtom m6" data-name="br">Br</div>
                            </div>
							<div class="fillConnect">
                                <div class="draggableHand" data-l="x" data-r="o"><img src="assets/img/xo.png" alt=""></div>
                                <div class="draggableHand" data-l="o" data-r="x"><img src="assets/img/ox.png" alt=""></div>
                                <div class="draggableHand" data-l="o" data-r="o"><img src="assets/img/oo.png" alt=""></div>
                                <div class="draggableHand" data-l="x" data-r="x"><img src="assets/img/xx.png" alt=""></div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</section>
    `);
    initDrag(7);
}
function rendernh3() {
	currentFunction = rendernh3;
	currentAnswer = answernh3;
	$("#mainContent").html(`
		<section id="buildDiagram">
            <div class="section-content">
                <div class="container">
                    <div class="overflow-hidden">
                        <div class="emptyItems emptyItems32 float-left px-2">
							<h1 class="f10 ml-4">NH<sub>3</sub></h1>
							<div class="emptyWrapperNh3">
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="11"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox hidden"></div>

								<div class="emptyBox droppableHand cornerX" data-position="21"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="22"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox hidden"></div>

								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="31"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="32"></div>
								<div class="emptyBox hidden"></div>

								<div class="emptyBox droppableHand cornerX" data-position="41"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="42"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="43"></div>

								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="51"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="52"></div>
								<div class="emptyBox hidden"></div>

								<div class="emptyBox droppableHand cornerX" data-position="61"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="62"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox hidden"></div>

								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="71"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox hidden"></div>
							</div>
                        </div>
                        <div class="fillItems float-right px-2">
                            <div class="overflow-hidden">
								<span class="close toHome" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</span>
							</div>
							<div class="fillArrow">
								<img src="assets/img/arrow.png" alt="">
							</div>
                            <div class="fillWrapper">
                                <div class="fillBox draggableAtom m1" data-name="h">H</div>
								<div class="fillBox draggableAtom m4" data-name="n">N</div>
								<div class="fillBox draggableAtom m7" data-name="b">B</div>
								<div class="fillBox draggableAtom m2" data-name="c">C</div>
								<div class="fillBox draggableAtom m5" data-name="o">O</div>
								<div class="fillBox draggableAtom m8" data-name="f">F</div>
								<div class="fillBox draggableAtom m3" data-name="cl">Cl</div>
								<div class="fillBox draggableAtom m6" data-name="br">Br</div>
                            </div>
                            <div class="fillConnect">
                                <div class="draggableHand" data-l="x" data-r="o"><img src="assets/img/xo.png" alt=""></div>
                                <div class="draggableHand" data-l="o" data-r="x"><img src="assets/img/ox.png" alt=""></div>
                                <div class="draggableHand" data-l="o" data-r="o"><img src="assets/img/oo.png" alt=""></div>
                                <div class="draggableHand" data-l="x" data-r="x"><img src="assets/img/xx.png" alt=""></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    `);
    initDrag(4, -4);
}
function renderbf3() {
	currentFunction = renderbf3;
	currentAnswer = answerbf3;
	$("#mainContent").html(`
		<section id="buildDiagram">
            <div class="section-content">
                <div class="container">
                    <div class="overflow-hidden">
                        <div class="emptyItems emptyItems32 float-left px-2">
							<h1 class="f11 ml-4">BF<sub>3</sub></h1>
							<div class="emptyWrapperNh3">
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="11"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox hidden"></div>

								<div class="emptyBox droppableHand cornerX" data-position="21"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="22"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox hidden"></div>

								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="31"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="32"></div>
								<div class="emptyBox hidden"></div>

								<div class="emptyBox droppableHand cornerX" data-position="41"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="42"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="43"></div>

								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="51"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="52"></div>
								<div class="emptyBox hidden"></div>

								<div class="emptyBox droppableHand cornerX" data-position="61"></div>
								<div class="emptyBox droppableAtom"></div>
								<div class="emptyBox droppableHand cornerX" data-position="62"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox hidden"></div>

								<div class="emptyBox hidden"></div>
								<div class="emptyBox droppableHand cornerY" data-position="71"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox hidden"></div>
								<div class="emptyBox hidden"></div>
							</div>
                        </div>
                        <div class="fillItems float-right px-2">
                            <div class="overflow-hidden">
								<span class="close toHome" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</span>
							</div>
							<div class="fillArrow">
								<img src="assets/img/arrow.png" alt="">
							</div>
                            <div class="fillWrapper">
                                <div class="fillBox draggableAtom m1" data-name="h">H</div>
								<div class="fillBox draggableAtom m4" data-name="n">N</div>
								<div class="fillBox draggableAtom m7" data-name="b">B</div>
								<div class="fillBox draggableAtom m2" data-name="c">C</div>
								<div class="fillBox draggableAtom m5" data-name="o">O</div>
								<div class="fillBox draggableAtom m8" data-name="f">F</div>
								<div class="fillBox draggableAtom m3" data-name="cl">Cl</div>
								<div class="fillBox draggableAtom m6" data-name="br">Br</div>
                            </div>
                            <div class="fillConnect">
                                <div class="draggableHand" data-l="x" data-r="o"><img src="assets/img/xo.png" alt=""></div>
                                <div class="draggableHand" data-l="o" data-r="x"><img src="assets/img/ox.png" alt=""></div>
                                <div class="draggableHand" data-l="o" data-r="o"><img src="assets/img/oo.png" alt=""></div>
                                <div class="draggableHand" data-l="x" data-r="x"><img src="assets/img/xx.png" alt=""></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    `);
    initDrag(13, -4);
}
function renderch4() {
	currentFunction = renderch4;
	currentAnswer = answerch4;
	$("#mainContent").html(`
		<section id="buildDiagram">
            <div class="section-content">
                <div class="container">
                    <div class="overflow-hidden">
                    	<div class="emptyItemsParent float-left">
                    		<div class="emptyItems emptyItems33">
								<h1 class="f12 ml-4">CH<sub>4</sub></h1>
								<div class="emptyWrapperCh4">
									<div class="emptyBox hidden"></div>
									<div class="emptyBox hidden"></div>
									<div class="emptyBox hidden"></div>
									<div class="emptyBox droppableHand cornerY" data-position="41"></div>
									<div class="emptyBox hidden"></div>
									<div class="emptyBox hidden"></div>
									<div class="emptyBox hidden"></div>

									<div class="emptyBox hidden"></div>
									<div class="emptyBox hidden"></div>
									<div class="emptyBox droppableHand cornerX" data-position="31"></div>
									<div class="emptyBox droppableAtom"></div>
									<div class="emptyBox droppableHand cornerX" data-position="51"></div>
									<div class="emptyBox hidden"></div>
									<div class="emptyBox hidden"></div>

									<div class="emptyBox hidden"></div>
									<div class="emptyBox droppableHand cornerY" data-position="21"></div>
									<div class="emptyBox hidden"></div>
									<div class="emptyBox droppableHand cornerY" data-position="42"></div>
									<div class="emptyBox hidden"></div>
									<div class="emptyBox droppableHand cornerY" data-position="61"></div>
									<div class="emptyBox hidden"></div>

									<div class="emptyBox droppableHand cornerX" data-position="11"></div>
									<div class="emptyBox droppableAtom"></div>
									<div class="emptyBox droppableHand cornerX" data-position="32"></div>
									<div class="emptyBox droppableAtom"></div>
									<div class="emptyBox droppableHand cornerX" data-position="52"></div>
									<div class="emptyBox droppableAtom"></div>
									<div class="emptyBox droppableHand cornerX" data-position="71"></div>

									<div class="emptyBox hidden"></div>
									<div class="emptyBox droppableHand cornerY" data-position="22"></div>
									<div class="emptyBox hidden"></div>
									<div class="emptyBox droppableHand cornerY" data-position="43"></div>
									<div class="emptyBox hidden"></div>
									<div class="emptyBox droppableHand cornerY" data-position="62"></div>
									<div class="emptyBox hidden"></div>

									<div class="emptyBox hidden"></div>
									<div class="emptyBox hidden"></div>
									<div class="emptyBox droppableHand cornerX" data-position="33"></div>
									<div class="emptyBox droppableAtom"></div>
									<div class="emptyBox droppableHand cornerX" data-position="53"></div>
									<div class="emptyBox hidden"></div>
									<div class="emptyBox hidden"></div>

									<div class="emptyBox hidden"></div>
									<div class="emptyBox hidden"></div>
									<div class="emptyBox hidden"></div>
									<div class="emptyBox droppableHand cornerY" data-position="44"></div>
									<div class="emptyBox hidden"></div>
									<div class="emptyBox hidden"></div>
									<div class="emptyBox hidden"></div>
								</div>
	                        </div>
						</div>
                        <div class="fillItems float-right px-2">
                            <div class="overflow-hidden">
								<span class="close toHome" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</span>
							</div>
							<div class="fillArrow">
								<img src="assets/img/arrow.png" alt="">
							</div>
                            <div class="fillWrapper">
                                <div class="fillBox draggableAtom m1" data-name="h">H</div>
								<div class="fillBox draggableAtom m4" data-name="n">N</div>
								<div class="fillBox draggableAtom m7" data-name="b">B</div>
								<div class="fillBox draggableAtom m2" data-name="c">C</div>
								<div class="fillBox draggableAtom m5" data-name="o">O</div>
								<div class="fillBox draggableAtom m8" data-name="f">F</div>
								<div class="fillBox draggableAtom m3" data-name="cl">Cl</div>
								<div class="fillBox draggableAtom m6" data-name="br">Br</div>
                            </div>
                            <div class="fillConnect">
                                <div class="draggableHand" data-l="x" data-r="o"><img src="assets/img/xo.png" alt=""></div>
                                <div class="draggableHand" data-l="o" data-r="x"><img src="assets/img/ox.png" alt=""></div>
                                <div class="draggableHand" data-l="o" data-r="o"><img src="assets/img/oo.png" alt=""></div>
                                <div class="draggableHand" data-l="x" data-r="x"><img src="assets/img/xx.png" alt=""></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    `);
    initDrag(4, -5);
}
$(document).on("click", ".formula .btn", function() {
	formula = $(this).data().name;
	$("#checkBtn").show().prop('disabled', true);
	switch(formula) {
		case 'br2':
			renderbr2();
			break;
		case 'o2':
			rendero2();
			break;
		case 'n2':
			rendern2();
			break;
		case 'hcl':
			renderhcl()
			break;
		case 'hcn':
			renderhcn();
			break;
		case 'h2o':
			renderh2o();
			break;
		case 'of2':
			renderof2();
			break;
		case 'co2':
			renderco2();
			break;
		case 'hocl':
			renderhocl();
			break;
		case 'nh3':
			rendernh3();
			break;
		case 'bf3':
			renderbf3();
			break;
		case 'ch4':
			renderch4();
			break;
	}
});

function initDrag(needHandsP, existHandsP = -3) {
	$('.draggableAtom').draggable({
		revert: "invalid",
		stack: ".draggableAtom",
		helper: 'clone',
		cursor: 'pointer',
		start: function(e, ui) {
			enableCheck(true);
			hideNotifications();
			ui.position.left = ui.originalPosition.left / zoomScale;
			ui.position.top = ui.originalPosition.top / zoomScale;
		},
		drag: function(e, ui){
			var changeLeft = ui.position.left - ui.originalPosition.left; // find change in left
			var newLeft = ui.originalPosition.left + changeLeft; // adjust new left by our zoomScale
			
            var changeTop = ui.position.top - ui.originalPosition.top; // find change in top
            var newTop = ui.originalPosition.top + changeTop ; // adjust new top by our zoomScale
    
            ui.position.left = newLeft / zoomScale ;
            ui.position.top = newTop / zoomScale;
		}
	});

	$('.droppableAtom').droppable({
		accept: ".draggableAtom",
		start: function (e, ui) {
			ui.position.left = ui.originalPosition.left / zoomScale;
			ui.position.top = ui.originalPosition.top / zoomScale;
		},
		drag: function(e, ui){
			var changeLeft = ui.position.left - ui.originalPosition.left; // find change in left
            var newLeft = ui.originalPosition.left + changeLeft; // adjust new left by our zoomScale
    
            var changeTop = ui.position.top - ui.originalPosition.top; // find change in top
            var newTop = ui.originalPosition.top + changeTop; // adjust new top by our zoomScale
    
            ui.position.left = newLeft / zoomScale ;
            ui.position.top = newTop / zoomScale;
		},
		drop: function(event, ui) {
			ui.helper.data('dropped', true);
			var droppable = $(this);
			var draggable = ui.draggable;
			var drag = $('.droppableAtom').has(ui.draggable).length ? draggable : draggable.clone().draggable({
				stack: ".draggableAtom",
				helper: 'clone',
				cursor: 'pointer',
				start: function(event, ui) {
					ui.helper.data('dropped', false);
					hideNotifications();
					ui.position.left = ui.originalPosition.left / zoomScale;
					ui.position.top = ui.originalPosition.top / zoomScale;
				},
				drag: function(e, ui1){
					var changeLeft1 = ui1.position.left - ui1.originalPosition.left; // find change in left
					var newLeft1 = ui1.originalPosition.left + changeLeft1; // adjust new left by our zoomScale
			
					var changeTop1 = ui1.position.top - ui1.originalPosition.top; // find change in top
					var newTop1 = ui1.originalPosition.top + changeTop1; // adjust new top by our zoomScale

					ui1.position.left = newLeft1 / zoomScale;
					ui1.position.top = newTop1 / zoomScale;
				},
				stop: function(event, ui) {
					ui.helper[0].remove();
					$(this).parent().droppable("enable");
					this.remove();
					$("#checkBtn").addClass("readyToCheck").prop('disabled', false);
					checkavailability();
				}
			});
			$(this).droppable("disable");
			drag.appendTo(droppable);
			$(this).children().removeClass("draggableAtom").addClass("usedDraggableAtom");
			checkavailability()
		}
	});
	$('.draggableHand').draggable({
		revert: "invalid",
		stack: ".draggableHand",
		helper: 'clone',
		cursor: 'pointer',
		start: function(e, ui) {
			enableCheck(true);
			hideNotifications();
			ui.position.left = ui.originalPosition.left / zoomScale;
			ui.position.top = ui.originalPosition.top / zoomScale;
		},
		drag: function(e, ui){
			var changeLeft = ui.position.left - ui.originalPosition.left; // find change in left
            var newLeft = ui.originalPosition.left + changeLeft; // adjust new left by our zoomScale
    
            var changeTop = ui.position.top - ui.originalPosition.top; // find change in top
            var newTop = ui.originalPosition.top + changeTop; // adjust new top by our zoomScale
    
            ui.position.left = newLeft / zoomScale;
            ui.position.top = newTop / zoomScale;
		}
	});
	$('.droppableHand').droppable({
		accept: ".draggableHand",
		classes: {
			"ui-droppable-hover": "ui-state-hover"
		},
		start: function (e, ui) {
			ui.position.left = ui.originalPosition.left / zoomScale;
			ui.position.top = ui.originalPosition.top / zoomScale;
		},
		drag: function(e, ui){
			var changeLeft = ui.position.left - ui.originalPosition.left; // find change in left
            var newLeft = ui.originalPosition.left + changeLeft; // adjust new left by our zoomScale
    
            var changeTop = ui.position.top - ui.originalPosition.top; // find change in top
            var newTop = ui.originalPosition.top + changeTop; // adjust new top by our zoomScale
    
            ui.position.left = newLeft / zoomScale;
            ui.position.top = newTop / zoomScale;
		},
		drop: function(event, ui) {
			ui.helper.data('dropped', true);
			var droppable = $(this);
			var draggable = ui.draggable;
			var drag = $('.droppableHand').has(ui.draggable).length ? draggable : draggable.clone().draggable({
				stack: ".draggableHand",
				helper: 'clone',
				cursor: 'pointer',
				start: function(event, ui) {
					ui.helper.data('dropped', false);
					hideNotifications();
					ui.position.left = ui.originalPosition.left / zoomScale;
					ui.position.top = ui.originalPosition.top / zoomScale;
				},
				drag: function(e, ui1){
					var changeLeft1 = ui1.position.left - ui1.originalPosition.left; // find change in left
					var newLeft1 = ui1.originalPosition.left + changeLeft1; // adjust new left by our zoomScale
			
					var changeTop1 = ui1.position.top - ui1.originalPosition.top; // find change in top
					var newTop1 = ui1.originalPosition.top + changeTop1; // adjust new top by our zoomScale
			
					ui1.position.left = newLeft1 / zoomScale;
            		ui1.position.top = newTop1 / zoomScale;
				},
				stop: function(event, ui) {
					ui.helper[0].remove();
					$(this).parent().droppable("enable");
					this.remove();
					$("#checkBtn").addClass("readyToCheck").prop('disabled', false);
					checkavailability();
				}
			});
			$(this).droppable("disable");
			drag.appendTo(droppable);
			$(this).children().removeClass("draggableHand").addClass("usedDraggableHand");
			checkavailability()
		}
	});

	$('.droppable3Hand').droppable({
		accept: ".draggableHand",
		classes: {
			"ui-droppable-hover": "ui-state-hover"
		},
		start: function (e, ui) {
			ui.position.left = ui.originalPosition.left / zoomScale;
			ui.position.top = ui.originalPosition.top / zoomScale;
		},
		drag: function(e, ui){
			var changeLeft = ui.position.left - ui.originalPosition.left; // find change in left
            var newLeft = ui.originalPosition.left + changeLeft; // adjust new left by our zoomScale
    
            var changeTop = ui.position.top - ui.originalPosition.top; // find change in top
            var newTop = ui.originalPosition.top + changeTop; // adjust new top by our zoomScale
    
			ui.position.left = newLeft / zoomScale;
            ui.position.top = newTop / zoomScale;
		},
		drop: function(event, ui) {
			ui.helper.data('dropped', true);
			var droppable = $(this);
			var draggable = ui.draggable;
			var drag = $('.droppable3Hand').has(ui.draggable).length ? draggable : draggable.clone().draggable({
				stack: ".draggableHand",
				helper: 'clone',
				cursor: 'pointer',
				start: function(event, ui) {
					ui.helper.data('dropped', false);
					hideNotifications();
					ui.position.left = ui.originalPosition.left / zoomScale;
					ui.position.top = ui.originalPosition.top / zoomScale;
				},
				stop: function(event, ui)
				{
					ui.helper[0].remove();
					$(this).parent().droppable("enable");
					this.remove();
					$("#checkBtn").addClass("readyToCheck").prop('disabled', false);
					checkavailability();
				},
				drag: function(e, ui1){
					var changeLeft1 = ui1.position.left - ui1.originalPosition.left; // find change in left
					var newLeft1 = ui1.originalPosition.left + changeLeft1 / (( zoomScale)); // adjust new left by our zoomScale
			
					var changeTop1 = ui1.position.top - ui1.originalPosition.top; // find change in top
					var newTop1 = ui1.originalPosition.top + changeTop1 / zoomScale; // adjust new top by our zoomScale
			
					ui1.position.left = newLeft1 / zoomScale;
            		ui1.position.top = newTop1 / zoomScale;
				}
			});
			if ($(this)[0]['childElementCount'] === 2) {
				$(this).droppable("disable");
			}
			drag.appendTo(droppable);
			$(this).children().removeClass("draggableHand").addClass("usedDraggableHand");
			checkavailability();
		}
	});
	function checkavailability() {
		let atoms = $(".droppableAtom");
		for (var i=0; i<atoms.length; i++) {
			if (!atoms[i]['childElementCount']) {
				$("#checkBtn").removeClass("enoughElements");
				return;
			}
		}

		var connects = $(".droppable3Hand")
		for (i=0; i<connects.length; i++) {
			if (!connects[i]['childElementCount']) {
				$("#checkBtn").removeClass("enoughElements");
				return;
			}
		}
		$("#checkBtn").addClass("enoughElements");

	}

}
function hcn() {
    var sign = '';
    var sign2 = '';
    const atoms = $(".droppableAtom").map(function(elem) {
        return $(this).children('.usedDraggableAtom').data("name");
    }).get();
    if ($('*[data-position="31"]').children("div").length === 0 || $('*[data-position="51"]').children("div").length === 0 || atoms.length < 3) {
        callResponseBox();
        return;
    }
    if (atoms[1] !== 'c') {
        result(false);
        return;
    }
    if (atoms[0] === 'h' && atoms[2] === 'n') {
        if (!$('*[data-position="11"]').children("div").length && !$('*[data-position="21"]').children("div").length && !$('*[data-position="22"]').children("div").length && ($('*[data-position="31"]').children("div").length === 1) && ($('*[data-position="51"]').children("div").length > 0)) {
            sign = $('*[data-position="31"]').children("div")[0]['dataset']['r'];
            sign2 = $('*[data-position="51"]').children("div")[0]['dataset']['r'];
            if (!checkC(sign, sign2)) {
                result(false);
                return;
            }
        } else {
            result(false);
            return;
        }
        if ($('*[data-position="61"]').children("div").length + $('*[data-position="62"]').children("div").length + $('*[data-position="71"]').children("div").length === 1) {
            if ($('*[data-position="61"]').children("div").length && $('*[data-position="61"]').children("div")[0]['dataset']['r'] === sign2 && $('*[data-position="61"]').children("div")[0]['dataset']['l'] === sign2 ) {
                result(true);
                return;
            }
            if ($('*[data-position="62"]').children("div").length && $('*[data-position="62"]').children("div")[0]['dataset']['r'] === sign2 && $('*[data-position="62"]').children("div")[0]['dataset']['l'] === sign2 ) {
                result(true);
                return;
            }
            if ($('*[data-position="71"]').children("div").length && $('*[data-position="71"]').children("div")[0]['dataset']['r'] === sign2 && $('*[data-position="71"]').children("div")[0]['dataset']['l'] === sign2 ) {
                result(true);
                return;
            }
        }
        result(false);
        return;
    } else if (atoms[0] === 'n' && atoms[2] === 'h') {
        if (!$('*[data-position="71"]').children("div").length && !$('*[data-position="61"]').children("div").length && !$('*[data-position="62"]').children("div").length && ($('*[data-position="51"]').children("div").length === 1)) {
            sign = $('*[data-position="51"]').children("div")[0]['dataset']['l'];
            sign2 = $('*[data-position="31"]').children("div")[0]['dataset']['l'];
            if (!checkC2(sign, sign2)) {
                result(false);
                return;
            }
        } else {
            result(false);
            return;
        }
        if ($('*[data-position="21"]').children("div").length + $('*[data-position="22"]').children("div").length + $('*[data-position="11"]').children("div").length === 1) {
            if ($('*[data-position="11"]').children("div").length && $('*[data-position="11"]').children("div")[0]['dataset']['r'] === sign2 && $('*[data-position="11"]').children("div")[0]['dataset']['l'] === sign2 ) {
                result(true);
                return;
            }
            if ($('*[data-position="21"]').children("div").length && $('*[data-position="21"]').children("div")[0]['dataset']['r'] === sign2 && $('*[data-position="21"]').children("div")[0]['dataset']['l'] === sign2 ) {
                result(true);
                return;
            }
            if ($('*[data-position="22"]').children("div").length && $('*[data-position="22"]').children("div")[0]['dataset']['r'] === sign2 && $('*[data-position="22"]').children("div")[0]['dataset']['l'] === sign2 ) {
                result(true);
                return;
            }
        }
        result(false);
        return;
    } else {
        result(false);
        return;
    }

    function checkC(sign, sign2){
        if (!$('*[data-position="41"]').children("div").length && !$('*[data-position="42"]').children("div").length && ($('*[data-position="51"]').children("div").length === 3)) {
            for (let i=0; i<$('*[data-position="51"]').children("div").length; i++) {
                if ($('*[data-position="51"]').children("div")[i]['dataset']['l'] !== sign) {
                    return false;
                }
                if ($('*[data-position="51"]').children("div")[i]['dataset']['r'] !== sign2) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    function checkC2(sign, sign2){
        if (!$('*[data-position="41"]').children("div").length && !$('*[data-position="42"]').children("div").length && ($('*[data-position="31"]').children("div").length === 3)) {
            for (let i=0; i<$('*[data-position="31"]').children("div").length; i++) {
                if ($('*[data-position="31"]').children("div")[i]['dataset']['r'] !== sign) {
                    return false;
                }
                if ($('*[data-position="31"]').children("div")[i]['dataset']['l'] !== sign2) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}
function co2() {
    var sign0 = '';
    var sign = '';
    var sign2 = '';
    const atoms = $(".droppableAtom").map(function(elem) {
        return $(this).children('.usedDraggableAtom').data("name");
    }).get();
    if ($('*[data-position="31"]').children("div").length === 0 || $('*[data-position="51"]').children("div").length === 0 || atoms.length < 3) {
        callResponseBox();
        return;
    }
    if (atoms[1] !== 'c') {
        result(false);
        return;
    }
    if (atoms[0] === 'o' && atoms[2] === 'o') {
        if ($('*[data-position="11"]').children("div").length + $('*[data-position="21"]').children("div").length + $('*[data-position="22"]').children("div").length === 2 && ($('*[data-position="31"]').children("div").length === 2)) {
            sign0 = $('*[data-position="31"]').children("div")[0]['dataset']['l'];
            sign = $('*[data-position="31"]').children("div")[0]['dataset']['r'];
            sign2 = $('*[data-position="51"]').children("div")[0]['dataset']['r'];
            if ($('*[data-position="51"]').children("div")[1]['dataset']['r'] !== sign2) {
                result(false);
                return;
            }
            if ($('*[data-position="11"]').children("div").length && ($('*[data-position="11"]').children("div")[0]['dataset']['r'] !== sign0 || $('*[data-position="11"]').children("div")[0]['dataset']['l'] !== sign0)) {
                result(false);
                return;
            }
            if ($('*[data-position="21"]').children("div").length && ($('*[data-position="21"]').children("div")[0]['dataset']['r'] !== sign0 || $('*[data-position="21"]').children("div")[0]['dataset']['l'] !== sign0)) {
                result(false);
                return;
            }
            if ($('*[data-position="22"]').children("div").length && ($('*[data-position="22"]').children("div")[0]['dataset']['r'] !== sign0 || $('*[data-position="22"]').children("div")[0]['dataset']['l'] !== sign0)) {
                result(false);
                return;
            }
            if (!checkC(sign)) {
                result(false);
                return;
            }
            if (!checkO(sign2)) {
                result(false);
                return;
            }
        } else {

            result(false);
            return;
        }
        if ($('*[data-position="61"]').children("div").length + $('*[data-position="62"]').children("div").length + $('*[data-position="71"]').children("div").length === 1) {
            if ($('*[data-position="61"]').children("div").length && ($('*[data-position="61"]').children("div")[0]['dataset']['r'] !== sign2 || $('*[data-position="61"]').children("div")[0]['dataset']['l'] !== sign2)) {
                result(false);
                return;
            }
            if ($('*[data-position="62"]').children("div").length && ($('*[data-position="62"]').children("div")[0]['dataset']['r'] !== sign2 || $('*[data-position="62"]').children("div")[0]['dataset']['l'] !== sign2)) {
                result(false);
                return;
            }
            if ($('*[data-position="71"]').children("div").length && ($('*[data-position="71"]').children("div")[0]['dataset']['r'] !== sign2 || $('*[data-position="71"]').children("div")[0]['dataset']['l'] !== sign2)) {
                result(false);
                return;
            }
        }
        result(true);
        return;
    } else {
        result(false);
        return;
    }

    function checkC(sign){
        if (!$('*[data-position="41"]').children("div").length && !$('*[data-position="42"]').children("div").length && $('*[data-position="51"]').children("div").length === 2 && $('*[data-position="31"]').children("div").length === 2) {
            if ($('*[data-position="51"]').children("div")[0]['dataset']['l'] !== sign || $('*[data-position="51"]').children("div")[1]['dataset']['l'] !== sign || $('*[data-position="31"]').children("div")[0]['dataset']['r'] !== sign || $('*[data-position="31"]').children("div")[1]['dataset']['r'] !== sign) {
                return false;
            }
            return true;
        }
        return false;
    }

    function checkO(sign){
        if ($('*[data-position="71"]').children("div").length + $('*[data-position="61"]').children("div").length + $('*[data-position="62"]').children("div").length === 2 && ($('*[data-position="51"]').children("div").length === 2)) {
            if ($('*[data-position="61"]').children("div").length && ($('*[data-position="61"]').children("div")[0]['dataset']['r'] !== sign || $('*[data-position="61"]').children("div")[0]['dataset']['l'] !== sign)) {
                return false;
            }
            if ($('*[data-position="62"]').children("div").length && ($('*[data-position="62"]').children("div")[0]['dataset']['r'] !== sign || $('*[data-position="62"]').children("div")[0]['dataset']['l'] !== sign)) {
                return false;
            }
            if ($('*[data-position="71"]').children("div").length && ($('*[data-position="71"]').children("div")[0]['dataset']['r'] !== sign || $('*[data-position="71"]').children("div")[0]['dataset']['l'] !== sign)) {
                return false;
            }
            return true;
        }
        return false;
    }
}
function h2o() {
    var sign = '';
    const atoms = $(".droppableAtom").map(function(elem) {
        return $(this).children('.usedDraggableAtom').data("name");
    }).get();
    if ($('*[data-position="31"]').children("div").length === 0 || $('*[data-position="42"]').children("div").length === 0 || atoms.length < 3) {
        callResponseBox();
        return;
    }
    if (atoms[1] !== 'o') {
        result(false);
        return;
    }
    if ($('*[data-position="11"]').children("div").length + $('*[data-position="21"]').children("div").length + $('*[data-position="22"]').children("div").length + $('*[data-position="32"]').children("div").length + $('*[data-position="43"]').children("div").length + $('*[data-position="52"]').children("div").length === 0) {
        if (atoms[0] === 'h' && atoms[2] === 'h') {
            if ($('*[data-position="31"]').children("div").length && $('*[data-position="41"]').children("div").length && $('*[data-position="42"]').children("div").length && $('*[data-position="51"]').children("div").length) {
                sign = $('*[data-position="41"]').children("div")[0]['dataset']['r'];
                if (!($('*[data-position="31"]').children("div")[0]['dataset']['r'] === sign || $('*[data-position="31"]').children("div")[0]['dataset']['l'] === sign)) {
                    result(false);
                    return;
                }
                if (!($('*[data-position="42"]').children("div")[0]['dataset']['r'] === sign || $('*[data-position="42"]').children("div")[0]['dataset']['l'] === sign)) {
                    result(false);
                    return;
                }
                if (!($('*[data-position="41"]').children("div")[0]['dataset']['l'] === sign && $('*[data-position="51"]').children("div")[0]['dataset']['r'] === sign && $('*[data-position="51"]').children("div")[0]['dataset']['l'] === sign)) {
                    result(false);
                    return;
                }
            } else {
                result(false);
                return;
            }
            result(true);
            return;
        } else {
            result(false);
            return;
        }
    }
    result(false);
}
function of2() {
    const atoms = $(".droppableAtom").map(function(elem) {
        return $(this).children('.usedDraggableAtom').data("name");
    }).get();
    if ($('*[data-position="31"]').children("div").length === 0 || $('*[data-position="42"]').children("div").length === 0 || atoms.length < 3) {
        callResponseBox();
        return;
    }
    if (atoms[1] !== 'o') {
        result(false);
        return;
    }
    if (atoms[0] === 'f' && atoms[2] === 'f' && $(".droppableHand").children().length === 10) {
        var sign = $('*[data-position="41"]').children("div")[0]['dataset']['r'];
        var sign1 = '';
        var sign2 = '';
        if ($('*[data-position="41"]').children("div")[0]['dataset']['l'] !== sign || $('*[data-position="51"]').children("div")[0]['dataset']['r'] !== sign || $('*[data-position="51"]').children("div")[0]['dataset']['l'] !== sign) {
            result(false);
            return;
        }
        if (!($('*[data-position="31"]').children("div")[0]['dataset']['r'] === sign || $('*[data-position="31"]').children("div")[0]['dataset']['l'] === sign)) {
            result(false);
            return;
        } else {
            sign1 = $('*[data-position="31"]').children("div")[0]['dataset']['r'] === sign ? $('*[data-position="31"]').children("div")[0]['dataset']['l'] : $('*[data-position="31"]').children("div")[0]['dataset']['r']
        }
        if (!($('*[data-position="42"]').children("div")[0]['dataset']['r'] === sign || $('*[data-position="42"]').children("div")[0]['dataset']['l'] === sign)) {
            result(false);
            return;
        } else {
            sign2 = $('*[data-position="42"]').children("div")[0]['dataset']['r'] === sign ? $('*[data-position="42"]').children("div")[0]['dataset']['l'] : $('*[data-position="42"]').children("div")[0]['dataset']['r']
        }
        if ($('*[data-position="11"]').children("div")[0]['dataset']['r'] !== sign1 || $('*[data-position="11"]').children("div")[0]['dataset']['l'] !== sign1 || $('*[data-position="21"]').children("div")[0]['dataset']['r'] !== sign1 || $('*[data-position="21"]').children("div")[0]['dataset']['l'] !== sign1 || $('*[data-position="22"]').children("div")[0]['dataset']['r'] !== sign1 || $('*[data-position="22"]').children("div")[0]['dataset']['l'] !== sign1 ) {
            result(false);
            return;
        }
        if ($('*[data-position="32"]').children("div")[0]['dataset']['r'] !== sign2 || $('*[data-position="32"]').children("div")[0]['dataset']['l'] !== sign2 || $('*[data-position="43"]').children("div")[0]['dataset']['r'] !== sign2 || $('*[data-position="43"]').children("div")[0]['dataset']['l'] !== sign2 || $('*[data-position="52"]').children("div")[0]['dataset']['r'] !== sign2 || $('*[data-position="52"]').children("div")[0]['dataset']['l'] !== sign2 ) {
            result(false);
            return;
        }
        result(true);
    } else {
        result(false);
    }
}
function hocl() {
    var sign = "";
    var sign1 = "";
    const atoms = $(".droppableAtom").map(function(elem) {
        return $(this).children('.usedDraggableAtom').data("name");
    }).get();
    if ($('*[data-position="31"]').children("div").length === 0 || $('*[data-position="42"]').children("div").length === 0 || atoms.length < 3) {
        callResponseBox();
        return;
    }
    if (atoms[1] !== 'o') {
        result(false);
        return;
    }
    if (atoms[0] === 'h' && atoms[2] === 'cl' && $(".droppableHand").children().length === 7) {
        if ($('*[data-position="11"]').children("div").length + $('*[data-position="21"]').children("div").length + $('*[data-position="22"]').children("div").length === 0) {
            sign = $('*[data-position="41"]').children("div")[0]['dataset']['r'];
            const inFact = $('*[data-position="31"]').children("div")[0]['dataset']['r'] + $('*[data-position="31"]').children("div")[0]['dataset']['l'] + $('*[data-position="41"]').children("div")[0]['dataset']['r'] + $('*[data-position="41"]').children("div")[0]['dataset']['l'] + $('*[data-position="51"]').children("div")[0]['dataset']['r'] + $('*[data-position="51"]').children("div")[0]['dataset']['l'] + $('*[data-position="42"]').children("div")[0]['dataset']['r'] + $('*[data-position="42"]').children("div")[0]['dataset']['l'] + $('*[data-position="32"]').children("div")[0]['dataset']['r'] + $('*[data-position="32"]').children("div")[0]['dataset']['l'] + $('*[data-position="43"]').children("div")[0]['dataset']['r'] + $('*[data-position="43"]').children("div")[0]['dataset']['l'] + $('*[data-position="52"]').children("div")[0]['dataset']['r'] + $('*[data-position="52"]').children("div")[0]['dataset']['l'];
            const shouldBe = sign + sign + sign + sign + sign + sign + sign + sign + sign + sign + sign + sign + sign + sign;
            if (inFact === shouldBe) {
                result(true);
                return;
            } else {
                sign1 = sign === 'x' ? "o" : "x";
                if ($('*[data-position="41"]').children("div")[0]['dataset']['l'] !== sign || $('*[data-position="51"]').children("div")[0]['dataset']['r'] !== sign || $('*[data-position="51"]').children("div")[0]['dataset']['l'] !== sign) {
                    result(false);
                    return;
                }
                if (($('*[data-position="31"]').children("div")[0]['dataset']['r'] === $('*[data-position="31"]').children("div")[0]['dataset']['l']) || ($('*[data-position="42"]').children("div")[0]['dataset']['r'] === $('*[data-position="42"]').children("div")[0]['dataset']['l'])) {
                    result(false);
                    return;
                }
                if ($('*[data-position="32"]').children("div")[0]['dataset']['r'] !== sign1 || $('*[data-position="32"]').children("div")[0]['dataset']['l'] !== sign1 || $('*[data-position="43"]').children("div")[0]['dataset']['r'] !== sign1 || $('*[data-position="43"]').children("div")[0]['dataset']['l'] !== sign1 || $('*[data-position="52"]').children("div")[0]['dataset']['r'] !== sign1 || $('*[data-position="52"]').children("div")[0]['dataset']['l'] !== sign1) {
                    result(false);
                    return;
                }
                result(true);
                return;
            }
        }
        result(false);
    } else if (atoms[0] === 'cl' && atoms[2] === 'h' && $(".droppableHand").children().length === 7) {
        if ($('*[data-position="32"]').children("div").length + $('*[data-position="43"]').children("div").length + $('*[data-position="52"]').children("div").length === 0) {
            sign = $('*[data-position="41"]').children("div")[0]['dataset']['r'];
            const inFact = $('*[data-position="31"]').children("div")[0]['dataset']['r'] + $('*[data-position="31"]').children("div")[0]['dataset']['l'] + $('*[data-position="41"]').children("div")[0]['dataset']['r'] + $('*[data-position="41"]').children("div")[0]['dataset']['l'] + $('*[data-position="51"]').children("div")[0]['dataset']['r'] + $('*[data-position="51"]').children("div")[0]['dataset']['l'] + $('*[data-position="42"]').children("div")[0]['dataset']['r'] + $('*[data-position="42"]').children("div")[0]['dataset']['l'] + $('*[data-position="11"]').children("div")[0]['dataset']['r'] + $('*[data-position="11"]').children("div")[0]['dataset']['l'] + $('*[data-position="21"]').children("div")[0]['dataset']['r'] + $('*[data-position="21"]').children("div")[0]['dataset']['l'] + $('*[data-position="22"]').children("div")[0]['dataset']['r'] + $('*[data-position="22"]').children("div")[0]['dataset']['l'];
            const shouldBe = sign + sign + sign + sign + sign + sign + sign + sign + sign + sign + sign + sign + sign + sign;
            if (inFact === shouldBe) {
                result(true);
                return;
            } else {
                sign1 = sign === 'x' ? "o" : "x";
                if ($('*[data-position="41"]').children("div")[0]['dataset']['l'] !== sign || $('*[data-position="51"]').children("div")[0]['dataset']['r'] !== sign || $('*[data-position="51"]').children("div")[0]['dataset']['l'] !== sign) {
                    result(false);
                    return;
                }
                if ($('*[data-position="31"]').children("div")[0]['dataset']['r'] === $('*[data-position="31"]').children("div")[0]['dataset']['l'] || $('*[data-position="42"]').children("div")[0]['dataset']['r'] === $('*[data-position="42"]').children("div")[0]['dataset']['l']) {
                    result(false);
                    return;
                }
                if ($('*[data-position="11"]').children("div")[0]['dataset']['r'] !== sign1 || $('*[data-position="11"]').children("div")[0]['dataset']['l'] !== sign1 || $('*[data-position="21"]').children("div")[0]['dataset']['r'] !== sign1 || $('*[data-position="21"]').children("div")[0]['dataset']['l'] !== sign1 || $('*[data-position="22"]').children("div")[0]['dataset']['r'] !== sign1 || $('*[data-position="22"]').children("div")[0]['dataset']['l'] !== sign1) {
                    result(false);
                    return;
                }
                result(true);
                return;
            }
        }
        result(false);
    } else {
        result(false);
    }
}
function br2() {
    var sign = "";
    var sign1 = "";
    const atoms = $(".droppableAtom").map(function(elem) {
        return $(this).children('.usedDraggableAtom').data("name");
    }).get();
    if ($('*[data-position="31"]').children("div").length === 0 || atoms.length < 2) {
        callResponseBox();
        return;
    }
    if (atoms[0] === 'br' && atoms[1] === 'br') {
        sign = $('*[data-position="31"]').children("div")[0]['dataset']['l'];
        sign1 = $('*[data-position="31"]').children("div")[0]['dataset']['r'];
        if ($('*[data-position="11"]').children("div").length && $('*[data-position="21"]').children("div").length && $('*[data-position="22"]').children("div").length && $('*[data-position="41"]').children("div").length && $('*[data-position="42"]').children("div").length && $('*[data-position="51"]').children("div").length && $('*[data-position="31"]').children("div").length === 1) {
            if ($('*[data-position="11"]').children("div")[0]['dataset']['l'] !== sign || $('*[data-position="11"]').children("div")[0]['dataset']['r'] !== sign || $('*[data-position="21"]').children("div")[0]['dataset']['l'] !== sign || $('*[data-position="21"]').children("div")[0]['dataset']['r'] !== sign || $('*[data-position="22"]').children("div")[0]['dataset']['l'] !== sign || $('*[data-position="22"]').children("div")[0]['dataset']['r'] !== sign) {
                result(false);
                return;
            }
            if ($('*[data-position="41"]').children("div")[0]['dataset']['l'] !== sign1 || $('*[data-position="41"]').children("div")[0]['dataset']['r'] !== sign1 || $('*[data-position="42"]').children("div")[0]['dataset']['l'] !== sign1 || $('*[data-position="42"]').children("div")[0]['dataset']['r'] !== sign1 || $('*[data-position="51"]').children("div")[0]['dataset']['l'] !== sign1 || $('*[data-position="51"]').children("div")[0]['dataset']['r'] !== sign1) {
                result(false);
                return;
            }
            result(true);
            return;
        }
        result(false);
    } else {
        result(false);
    }
}
function o2() {
    var sign = "";
    var sign1 = "";
    const atoms = $(".droppableAtom").map(function(elem) {
        return $(this).children('.usedDraggableAtom').data("name");
    }).get();
    if ($('*[data-position="31"]').children("div").length === 0 || atoms.length < 2) {
        callResponseBox();
        return;
    }
    if (atoms[0] === 'o' && atoms[1] === 'o') {
        if ($('*[data-position="11"]').children("div").length + $('*[data-position="21"]').children("div").length + $('*[data-position="22"]').children("div").length === 2 && ($('*[data-position="41"]').children("div").length + $('*[data-position="42"]').children("div").length + $('*[data-position="51"]').children("div").length === 2) && $('*[data-position="31"]').children("div").length === 2) {
            sign = $('*[data-position="31"]').children("div")[0]['dataset']['l'];
            sign1 = $('*[data-position="31"]').children("div")[0]['dataset']['r'];
            if ($('*[data-position="11"]').children("div").length && ($('*[data-position="11"]').children("div")[0]['dataset']['r'] !== sign || $('*[data-position="11"]').children("div")[0]['dataset']['l'] !== sign)) {
                result(false);
                return;
            }
            if ($('*[data-position="21"]').children("div").length && ($('*[data-position="21"]').children("div")[0]['dataset']['r'] !== sign || $('*[data-position="21"]').children("div")[0]['dataset']['l'] !== sign)) {
                result(false);
                return;
            }
            if ($('*[data-position="22"]').children("div").length && ($('*[data-position="22"]').children("div")[0]['dataset']['r'] !== sign || $('*[data-position="22"]').children("div")[0]['dataset']['l'] !== sign)) {
                result(false);
                return;
            }
            if ($('*[data-position="41"]').children("div").length && ($('*[data-position="41"]').children("div")[0]['dataset']['r'] !== sign1 || $('*[data-position="41"]').children("div")[0]['dataset']['l'] !== sign1)) {
                result(false);
                return;
            }
            if ($('*[data-position="42"]').children("div").length && ($('*[data-position="42"]').children("div")[0]['dataset']['r'] !== sign1 || $('*[data-position="42"]').children("div")[0]['dataset']['l'] !== sign1)) {
                result(false);
                return;
            }
            if ($('*[data-position="51"]').children("div").length && ($('*[data-position="51"]').children("div")[0]['dataset']['r'] !== sign1 || $('*[data-position="51"]').children("div")[0]['dataset']['l'] !== sign1)) {
                result(false);
                return;
            }
            result(true);
            return;
        }
        result(false);
    } else {
        result(false);
    }
}
function n2() {
    var sign = "";
    var sign1 = "";
    const atoms = $(".droppableAtom").map(function(elem) {
        return $(this).children('.usedDraggableAtom').data("name");
    }).get();
    if ($('*[data-position="31"]').children("div").length === 0 || atoms.length < 2) {
        callResponseBox();
        return;
    }
    if (atoms[0] === 'n' && atoms[1] === 'n') {
        if ($('*[data-position="11"]').children("div").length + $('*[data-position="21"]').children("div").length + $('*[data-position="22"]').children("div").length === 1 && ($('*[data-position="41"]').children("div").length + $('*[data-position="42"]').children("div").length + $('*[data-position="51"]').children("div").length === 1) && $('*[data-position="31"]').children("div").length === 3) {
            sign = $('*[data-position="31"]').children("div")[0]['dataset']['l'];
            sign1 = $('*[data-position="31"]').children("div")[0]['dataset']['r'];
            if ($('*[data-position="31"]').children("div")[1]['dataset']['l'] !== sign || $('*[data-position="31"]').children("div")[2]['dataset']['l'] !== sign || $('*[data-position="31"]').children("div")[1]['dataset']['r'] !== sign1 || $('*[data-position="31"]').children("div")[2]['dataset']['r'] !== sign1) {
                result(false);
                return;
            }
            if ($('*[data-position="11"]').children("div").length && ($('*[data-position="11"]').children("div")[0]['dataset']['r'] !== sign || $('*[data-position="11"]').children("div")[0]['dataset']['l'] !== sign)) {
                result(false);
                return;
            }
            if ($('*[data-position="21"]').children("div").length && ($('*[data-position="21"]').children("div")[0]['dataset']['r'] !== sign || $('*[data-position="21"]').children("div")[0]['dataset']['l'] !== sign)) {
                result(false);
                return;
            }
            if ($('*[data-position="22"]').children("div").length && ($('*[data-position="22"]').children("div")[0]['dataset']['r'] !== sign || $('*[data-position="22"]').children("div")[0]['dataset']['l'] !== sign)) {
                result(false);
                return;
            }
            if ($('*[data-position="41"]').children("div").length && ($('*[data-position="41"]').children("div")[0]['dataset']['r'] !== sign1 || $('*[data-position="41"]').children("div")[0]['dataset']['l'] !== sign1)) {
                result(false);
                return;
            }
            if ($('*[data-position="42"]').children("div").length && ($('*[data-position="42"]').children("div")[0]['dataset']['r'] !== sign1 || $('*[data-position="42"]').children("div")[0]['dataset']['l'] !== sign1)) {
                result(false);
                return;
            }
            if ($('*[data-position="51"]').children("div").length && ($('*[data-position="51"]').children("div")[0]['dataset']['r'] !== sign1 || $('*[data-position="51"]').children("div")[0]['dataset']['l'] !== sign1)) {
                result(false);
                return;
            }
            result(true);
            return;
        }
        result(false);
    } else {
        result(false);
    }
}
function hcl() {
    var sign1 = "";
    const atoms = $(".droppableAtom").map(function(elem) {
        return $(this).children('.usedDraggableAtom').data("name");
    }).get();
    if ($('*[data-position="31"]').children("div").length === 0 || atoms.length < 2) {
        callResponseBox();
        return;
    }
    if (atoms[0] === 'h' && atoms[1] === 'cl') {
        sign1 = $('*[data-position="31"]').children("div")[0]['dataset']['r'];
        if (!$('*[data-position="11"]').children("div").length && !$('*[data-position="21"]').children("div").length && !$('*[data-position="22"]').children("div").length && $('*[data-position="41"]').children("div").length && $('*[data-position="42"]').children("div").length && $('*[data-position="51"]').children("div").length && $('*[data-position="31"]').children("div").length === 1) {
            if ($('*[data-position="41"]').children("div")[0]['dataset']['l'] !== sign1 || $('*[data-position="41"]').children("div")[0]['dataset']['r'] !== sign1 || $('*[data-position="42"]').children("div")[0]['dataset']['l'] !== sign1 || $('*[data-position="42"]').children("div")[0]['dataset']['r'] !== sign1 || $('*[data-position="51"]').children("div")[0]['dataset']['l'] !== sign1 || $('*[data-position="51"]').children("div")[0]['dataset']['r'] !== sign1) {
                result(false);
                return;
            }
            result(true);
            return;
        }
        result(false);
    } else if (atoms[0] === 'cl' && atoms[1] === 'h') {
        sign1 = $('*[data-position="31"]').children("div")[0]['dataset']['l'];
        if (!$('*[data-position="41"]').children("div").length && !$('*[data-position="42"]').children("div").length && !$('*[data-position="51"]').children("div").length && $('*[data-position="11"]').children("div").length && $('*[data-position="21"]').children("div").length && $('*[data-position="22"]').children("div").length && $('*[data-position="31"]').children("div").length === 1) {
            if ($('*[data-position="11"]').children("div")[0]['dataset']['l'] !== sign1 || $('*[data-position="11"]').children("div")[0]['dataset']['r'] !== sign1 || $('*[data-position="21"]').children("div")[0]['dataset']['l'] !== sign1 || $('*[data-position="21"]').children("div")[0]['dataset']['r'] !== sign1 || $('*[data-position="22"]').children("div")[0]['dataset']['l'] !== sign1 || $('*[data-position="22"]').children("div")[0]['dataset']['r'] !== sign1) {
                result(false);
                return;
            }
            result(true);
            return;
        }
        result(false);
    } else {
        result(false);
    }
}
function nh3() {
    var sign = '';
    const atoms = $(".droppableAtom").map(function(elem) {
        return $(this).children('.usedDraggableAtom').data("name");
    }).get();
    if ($('*[data-position="31"]').children("div").length === 0 || $('*[data-position="51"]').children("div").length === 0 || $('*[data-position="42"]').children("div").length === 0 || atoms.length < 4) {
        callResponseBox();
        return;
    }
    if (atoms[1] !== 'n') {
        result(false);
        return;
    }
    if (atoms[0] === 'h' && atoms[2] === 'h'&& atoms[3] === 'h') {
        if (!$('*[data-position="11"]').children("div").length && !$('*[data-position="21"]').children("div").length && !$('*[data-position="22"]').children("div").length && $('*[data-position="31"]').children("div").length && !$('*[data-position="32"]').children("div").length && $('*[data-position="41"]').children("div").length && $('*[data-position="42"]').children("div").length && !$('*[data-position="43"]').children("div").length && $('*[data-position="51"]').children("div").length && !$('*[data-position="52"]').children("div").length && !$('*[data-position="61"]').children("div").length && !$('*[data-position="62"]').children("div").length && !$('*[data-position="71"]').children("div").length) {
            sign = $('*[data-position="41"]').children("div")[0]['dataset']['r'];
            if ($('*[data-position="41"]').children("div")[0]['dataset']['l'] !== sign) {
                result(false);
                return;
            }
            if ($('*[data-position="31"]').children("div")[0]['dataset']['r'] !== sign && $('*[data-position="31"]').children("div")[0]['dataset']['l'] !== sign) {
                result(false);
                return;
            }
            if ($('*[data-position="42"]').children("div")[0]['dataset']['r'] !== sign && $('*[data-position="42"]').children("div")[0]['dataset']['l'] !== sign) {
                result(false);
                return;
            }
            if ($('*[data-position="51"]').children("div")[0]['dataset']['r'] !== sign && $('*[data-position="51"]').children("div")[0]['dataset']['l'] !== sign) {
                result(false);
                return;
            }
            result(true)
        } else {
            result(false);
        }
    } else {
        result(false);
    }
}
function bf3() {
    var sign = '';
    var sign2 = '';
    var sign3 = '';
    var sign4 = '';
    const atoms = $(".droppableAtom").map(function(elem) {
        return $(this).children('.usedDraggableAtom').data("name");
    }).get();
    if ($('*[data-position="31"]').children("div").length === 0 || $('*[data-position="51"]').children("div").length === 0 || $('*[data-position="42"]').children("div").length === 0 || atoms.length < 4) {
        callResponseBox();
        return;
    }
    if (atoms[1] !== 'b') {
        result(false);
        return;
    }
    if (atoms[0] === 'f' && atoms[2] === 'f' && atoms[3] === 'f') {
        sign = $('*[data-position="41"]').children("div")[0]['dataset']['r'];
        sign2 = $('*[data-position="31"]').children("div")[0]['dataset']['r'] === sign ? $('*[data-position="31"]').children("div")[0]['dataset']['l'] : $('*[data-position="31"]').children("div")[0]['dataset']['r'];
        sign3 = $('*[data-position="42"]').children("div")[0]['dataset']['r'] === sign ? $('*[data-position="42"]').children("div")[0]['dataset']['l'] : $('*[data-position="42"]').children("div")[0]['dataset']['r'];
        sign4 = $('*[data-position="51"]').children("div")[0]['dataset']['r'] === sign ? $('*[data-position="51"]').children("div")[0]['dataset']['l'] : $('*[data-position="51"]').children("div")[0]['dataset']['r'];
        if ($('*[data-position="41"]').children("div")[0]['dataset']['l'] !== sign) {
            result(false);
            return;
        }
        if (!($('*[data-position="31"]').children("div")[0]['dataset']['r'] === sign || $('*[data-position="31"]').children("div")[0]['dataset']['l'] === sign)) {
            result(false);
            return;
        }
        if (!($('*[data-position="42"]').children("div")[0]['dataset']['r'] === sign || $('*[data-position="42"]').children("div")[0]['dataset']['l'] === sign)) {
            result(false);
            return;
        }
        if (!($('*[data-position="51"]').children("div")[0]['dataset']['r'] === sign || $('*[data-position="51"]').children("div")[0]['dataset']['l'] === sign)) {
            result(false);
            return;
        }
        if ($('*[data-position="11"]').children("div")[0]['dataset']['r'] !== sign2 || $('*[data-position="11"]').children("div")[0]['dataset']['l'] !== sign2 || $('*[data-position="21"]').children("div")[0]['dataset']['r'] !== sign2 || $('*[data-position="21"]').children("div")[0]['dataset']['l'] !== sign2 || $('*[data-position="22"]').children("div")[0]['dataset']['r'] !== sign2 || $('*[data-position="22"]').children("div")[0]['dataset']['l'] !== sign2) {
            result(false);
            return;
        }
        if ($('*[data-position="32"]').children("div")[0]['dataset']['r'] !== sign3 || $('*[data-position="32"]').children("div")[0]['dataset']['l'] !== sign3 || $('*[data-position="43"]').children("div")[0]['dataset']['r'] !== sign3 || $('*[data-position="43"]').children("div")[0]['dataset']['l'] !== sign3 || $('*[data-position="52"]').children("div")[0]['dataset']['r'] !== sign3 || $('*[data-position="52"]').children("div")[0]['dataset']['l'] !== sign3) {
            result(false);
            return;
        }
        if ($('*[data-position="61"]').children("div")[0]['dataset']['r'] !== sign4 || $('*[data-position="61"]').children("div")[0]['dataset']['l'] !== sign4 || $('*[data-position="62"]').children("div")[0]['dataset']['r'] !== sign4 || $('*[data-position="62"]').children("div")[0]['dataset']['l'] !== sign4 || $('*[data-position="71"]').children("div")[0]['dataset']['r'] !== sign4 || $('*[data-position="71"]').children("div")[0]['dataset']['l'] !== sign4) {
            result(false);
            return;
        }
        result(true)
    } else {
        result(false);
    }
}
function ch4() {
    var sign = '';
    const atoms = $(".droppableAtom").map(function(elem) {
        return $(this).children('.usedDraggableAtom').data("name");
    }).get();
    if ($('*[data-position="32"]').children("div").length === 0 || $('*[data-position="52"]').children("div").length === 0 || $('*[data-position="42"]').children("div").length === 0 || $('*[data-position="43"]').children("div").length === 0 || atoms.length < 5) {
        callResponseBox();
        return;
    }
    if (atoms[2] !== 'c') {
        result(false);
        return;
    }
    if (atoms[0] === 'h' && atoms[1] === 'h'&& atoms[3] === 'h' && atoms[4] === 'h' && $(".droppableHand").children().length === 4) {
        if ($('*[data-position="32"]').children("div").length && $('*[data-position="42"]').children("div").length && $('*[data-position="52"]').children("div").length && $('*[data-position="43"]').children("div").length) {
            if ($('*[data-position="42"]').children("div")[0]['dataset']['r'] === $('*[data-position="42"]').children("div")[0]['dataset']['l']) {
                sign = $('*[data-position="42"]').children("div")[0]['dataset']['r'];
            } else if ($('*[data-position="32"]').children("div")[0]['dataset']['r'] === $('*[data-position="32"]').children("div")[0]['dataset']['l']) {
                sign = $('*[data-position="32"]').children("div")[0]['dataset']['r'];
            } else if ($('*[data-position="43"]').children("div")[0]['dataset']['r'] === $('*[data-position="43"]').children("div")[0]['dataset']['l']) {
                sign = $('*[data-position="43"]').children("div")[0]['dataset']['r'];
            } else if ($('*[data-position="52"]').children("div")[0]['dataset']['r'] === $('*[data-position="52"]').children("div")[0]['dataset']['l']) {
                sign = $('*[data-position="52"]').children("div")[0]['dataset']['r'];
            }
            if (sign) {
                if ($('*[data-position="42"]').children("div")[0]['dataset']['r'] !== sign && $('*[data-position="42"]').children("div")[0]['dataset']['l'] !== sign) {
                    result(false);
                    return;
                }
                if ($('*[data-position="32"]').children("div")[0]['dataset']['r'] !== sign && $('*[data-position="32"]').children("div")[0]['dataset']['l'] !== sign) {
                    result(false);
                    return;
                }
                if ($('*[data-position="43"]').children("div")[0]['dataset']['r'] !== sign && $('*[data-position="43"]').children("div")[0]['dataset']['l'] !== sign) {
                    result(false);
                    return;
                }
                if ($('*[data-position="52"]').children("div")[0]['dataset']['r'] !== sign && $('*[data-position="52"]').children("div")[0]['dataset']['l'] !== sign) {
                    result(false);
                    return;
                }
            } else {
                result(true);
                return;
            }
            result(true)
        } else {
            result(false);
        }
    } else {
        result(false);
    }
}
