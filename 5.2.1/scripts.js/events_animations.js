var mashtab = 1;
(function () {
    // let pastSteps = []
    addClasses()
    document.addEventListener('keydown', function (event) {

        var resetButton = document.querySelector('#checkBtn')
        resetButton.style.opacity = 1;
        resetButton.style.cursor = 'pointer';

        let target = event.target;
        if (!target.closest('.empty.number+td') && !target.closest('.empty.number+td + td')) return;

        // console.log(10)
        event.preventDefault()
        event.stopImmediatePropagation();

        let value = parseInt(target.value);




        if (!isNaN(+event.key)) {
            value = !isNaN(value) ? value + '' + event.key : event.key
            target.value = value + 'º';

        }
        if (event.keyCode == 8) {
            let value = '' + parseInt(target.value);
            if (!isNaN(value)) {
                value = value.substring(0, value.length - 1);
                (value.length > 0) ? value += 'º' : null;

                target.value = value;
            }
        }
        checkAnswers(target)

        return false;
    })


    document.addEventListener('keydown', function (event) {

        let target = event.target;
        if (!target.closest('.sin_i') && !target.closest('.sin_r') && !target.closest('.sin_I_R')) return;

        event.preventDefault()
        event.stopImmediatePropagation();
        let value

        if (!isNaN(+event.key)) {

            value = target.value + '' + event.key;
            value = dropNumbers(value);

            if (value.length > 5) return;

            // value = !isNaN(value) ?dropNumbers(value): event.key
            target.value = value ? value : event.key;

        } else if (event.keyCode == 8) {
            value = target.value
            value = value.substring(0, value.length - 1)
            value = dropNumbers(value);
            if (isNaN(parseInt(value))) value = ' ';
            target.value = value ? isNaN(parseInt(value)) ? '' : value : event.key;
        }
        checkAnswers(target)

        return false;

    })
})()

    + function () {

        document.addEventListener('click', function (event) {
            var target = event.target;
            var input = window.currentInput;

            if (!target.closest('.try_agane') && !target.closest('.show_correct')) return;


            document.querySelector('.click_ignor_container').remove();

            if (input.closest('input')) {
                if (target.closest('.try_agane')) {
                    input.value = '';
                    input.style.color = '';
                    input.focus()
                } else {


                    input.style.color = '';


                    rightDisabledInput(input)
                    if (input.closest('.incidence') || input.closest('.refraction')) {
                        first_to_countRight++
                        allowCompleteButtonClick()
                        input.value = input.getAttribute('data-ans') + 'º';

                        inc_ref_autoCheck(input)

                        if (!document.querySelector('[data-ans="' + input.getAttribute('data-ans') + '"]')) return;

                        var percent = parseInt(document.querySelector('[data-ans="' + input.getAttribute('data-ans') + '"]').closest('td').previousElementSibling.querySelector('input').value)

                        var incidences = document.querySelectorAll('.incidence');

                        if (!incidences[(percent / 15)]) return
                        var nextInput = incidences[(percent / 15)].closest('td').nextElementSibling.querySelector('input');
                        var nextInputVal = parseInt(nextInput.value);
                        var nextInputAns = nextInput.getAttribute('data-ans');

                        console.log(nextInputAns, nextInputVal);

                        // if (nextInputVal != nextInputAns)
                        //     helperAlert(1, 'Enter the <span class="bold">angle of refraction</span> in the table.')
                        // input.setAttribute('disabled',true);
                    } else if (input.closest('.sin_i') || input.closest('.sin_r')) {
                        input.value = input.getAttribute('data-ans');
                        sin_i_r_autoCheck(input)
                    } else {

                        // console.log(input);
                        input.value = input.getAttribute('data-ans');
                        lastRow_autoCheck()
                    }




                }
            } else {

                var circleDiv = input;


                if (target.closest('.try_agane')) {


                    if (currentSelectedTr) {


                        circleDiv.style.background = 'transparent';
                        currentSelectedTr.querySelector('.sin_i').style.color = 'rgb(70, 70, 255)'
                        currentSelectedTr.querySelector('.sin_r').style.color = 'rgb(70, 70, 255)';

                        [...document.querySelectorAll('input')].forEach(elem=>{
                            if(elem.style.color == 'red'){
                                elem.style.color = 'rgb(51, 102, 204)'
                            }
                        })
                    }

                } else {
                    [...document.querySelectorAll('.helper_text_bottom .text')].forEach(elem => {
                        elem.style.display = 'none'
                    });
                    set_right_point(graph_sin_i, graph_sin_r);
                    circleDiv.style.background = 'transparent';

                    if (currentSelectedTr) {
                        currentSelectedTr.querySelector('.sin_i').style.color = 'rgb(70, 70, 255)';
                        currentSelectedTr.querySelector('.sin_r').style.color = 'rgb(70, 70, 255)';

                        currentSelectedTr = ''

                    }
                    [...document.querySelectorAll('input')].forEach(elem=>{
                        if(elem.style.color == 'red'){
                            elem.style.color = 'rgb(51, 102, 204)'
                        }
                    })
                }

            }

        })

        document.querySelector('.graphHiddenLines').addEventListener('click', graph_table_click);

        document.querySelector('.check_gradient_answer').addEventListener('keydown', function (event) {
            var target = event.target;
            value = target.value + '' + event.key;
            value = dropNumbers(value);
            event.preventDefault();
            event.stopImmediatePropagation()
            var correct_icon = document.querySelector('.correct_icon');
            var helper_container = document.querySelector('.helper_container');


            var checker = document.getElementById('checkBtn');



            
            if (!isNaN(+event.key)) {

                value = target.value + '' + event.key;
                value = dropNumbers(value);
                target.value = value;

                var answer = target.getAttribute('data-right');

                correct_icon.style.display = 'none'
                helper_container.style.display = 'none';

                if(value.length>=4){
                    if (value == '0,67' || value == '0,68') {
                        document.querySelector('.helper_link').click();
                        document.querySelector('.correct_icon').style.display = 'block'
                        
                    } else {
                        document.querySelector('.helper_container').style.display = 'flex';
                    }
                }else{
                    document.querySelector('.helper_container').style.display = 'none'
                }


            } else if (event.keyCode == 8) {
                correct_icon.style.display = 'none'
                helper_container.style.display = 'none';
                value = target.value
                value = value.substring(0, value.length - 1)
                value = dropNumbers(value);
                if (isNaN(parseInt(value))) value = ' ';
                target.value = value ? isNaN(parseInt(value)) ? '' : value : event.key;
            }
        })

      
   


        document.querySelector('.helper_link').addEventListener('click', function (event) {
            document.querySelector('.answer').style.display = 'flex';
            if(document.querySelector('.correct_icon').style.display != 'none'){

                document.querySelector('.helper_container').style.display = 'none'
            }else{
                this.style.display = 'none';
            }

         
            helperAlert(1, 'Proceed to the next tab')

            var input = document.querySelector('.check_gradient_answer');
            // input.value = input.getAttribute('data-right')
            input.setAttribute('disabled', true)
            input.style.background = '#fff';
            input.style.color = 'black';
            var ConclusionButton = document.querySelector('.Conclusion img');
            var src = ConclusionButton.getAttribute('src');
            src = src.replace('3', '2');
            ConclusionButton.setAttribute('src', src);

            ConclusionButton.closest('div').classList.remove('disabled');

            document.querySelector('#disc').innerHTML = 'Click on play button to see the conclusion that can be drawn from this experiment. '

            document.querySelector('#insTxtPanel').innerHTML = `
                <ol>
                    <li>You should first work through the MEASUREMENTS and GRAPH tabs before you continue.</li>
                    <li>Click on the CONCLUSION button to see how we can interpret the table and the graph obtained in this experiment. </li>
                    <li>Click on the TABLE or GRAPH button any time when you want to look at the table or the graph again.</li>
                    <li>Click on the RESET button to start again.</li>
                
                </ol>

            `
            var first = true
            ConclusionButton.addEventListener('click', function (event) {
                document.querySelector('.Conclusion_container').style.display = 'flex'
                document.querySelector('#disc').innerHTML = 'Click on play button to see the conclusion that can be drawn from this experiment. '

                document.querySelector('#insTxtPanel').innerHTML = `
                    <ol>
                        <li>You should first work through the MEASUREMENTS and GRAPH tabs before you continue.</li>
                        <li>Click on the CONCLUSION button to see how we can interpret the table and the graph obtained in this experiment. </li>
                        <li>Click on the TABLE or GRAPH button any time when you want to look at the table or the graph again.</li>
                        <li>Click on the RESET button to start again.</li>
                    
                    </ol>
    
                `
                if(first){
                    first = false;
                    
                    showDescription(1)
                }

            })
        })

        document.querySelector('.close_gradient').addEventListener('click', function (event) {

            document.querySelector('.calculate_gradient').style.display = 'none'
        })
        document.querySelector('.slider').addEventListener('click', function (event) {
            var target = event.target;

            if (!target.closest('.slideNumber')) return;
            var container = target.closest('.Conclusion_container')

            var prevB = container.querySelector('.prev');
            var nextB = container.querySelector('.next');
            prevB.classList.remove('disabled');
            nextB.classList.remove('disabled');


            if (target.innerHTML == 1) {
                document.querySelector('.prev').classList.add('disabled')
                prevB.classList.add('disabled');
            } else if (target.innerHTML == 4) {
                nextB.classList.add('disabled');
            }
            if (target.closest('.slideNumber')) {
                var numbers = document.querySelectorAll('.slideNumber');
                [].forEach.call(numbers, function (elem) {
                    elem.classList.remove('active')
                })
                target.classList.add('active')
            }

            // var img = document.querySelector('.description_image img');
            // var src = img.getAttribute('src');
            // src = src.replace(/\d/, target.innerHTML)
            // img.setAttribute('src', src)

            document.querySelector('.page_number').innerHTML = +target.innerHTML + '.';

            showDescription(target.innerHTML)

        })
        document.querySelector('.slider').addEventListener('click', function (event) {

            var target = event.target;
            if (!target.closest('.next') && !target.closest('.prev')) return;
            if (target.closest('.disabled')) return;


            var container = event.currentTarget;
            var active = container.querySelector('.active');




            if (target.closest('.next')) {
                active.nextElementSibling.click()
            } else {
                active.previousElementSibling.click();
            }


        })


        // helperAlert(1,'Proceed to the next tab.')
        document.querySelector('.Graph').addEventListener('click', function (event) {
            document.querySelector('.Conclusion_container').style.display = 'none';


        })

        document.querySelector('.Measuerments').addEventListener('click', function (event) {
            document.querySelector('#insTxtPanel').innerHTML = `
            
                    <ol>
                       
                    <li>When the angle of incidence is zero, light passes straight through the glass block. It is not bent.</li>
                    <li>Use the slider to choose an angle of incidence.</li>
                    <li>Measure the corresponding angle of refraction. Enter the value in the correct block in the table.</li>
                    <li>Repeat Steps 2 and 3 until the first two columns of the table are complete.</li>
                    <li>Click on the COMPLETE TABLE button to complete the next two columns.</li>
                    <li>Calculate
                        
                        
                        
                        <span id="MathJax-Element-13-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mo stretchy=&quot;false&quot;>(</mo><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>i</mi></mrow></msub><mo stretchy=&quot;false&quot;>)</mo></math>" role="presentation" style="font-size: 101%; position: relative;"><span id="MJXc-Node-102" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-103" class="mjx-mrow"><span id="MJXc-Node-104" class="mjx-texatom"><span id="MJXc-Node-105" class="mjx-mrow"><span id="MJXc-Node-106" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-107" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-108" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-109" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;">(</span></span><span id="MJXc-Node-110" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-111" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-112" class="mjx-texatom" style=""><span id="MJXc-Node-113" class="mjx-mrow"><span id="MJXc-Node-114" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span></span></span></span></span><span id="MJXc-Node-115" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;">)</span></span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mo stretchy="false">(</mo><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">i</mi></mrow></msub><mo stretchy="false">)</mo></math></span></span>
                        
                        
                        
                        for each angle of incidence and
                        
                        
                        <span id="MathJax-Element-14-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mo stretchy=&quot;false&quot;>(</mo><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>r</mi></mrow></msub><mo stretchy=&quot;false&quot;>)</mo></math>" role="presentation" style="font-size: 101%; position: relative;"><span id="MJXc-Node-116" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-117" class="mjx-mrow"><span id="MJXc-Node-118" class="mjx-texatom"><span id="MJXc-Node-119" class="mjx-mrow"><span id="MJXc-Node-120" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-121" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-122" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-123" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;">(</span></span><span id="MJXc-Node-124" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-125" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-126" class="mjx-texatom" style=""><span id="MJXc-Node-127" class="mjx-mrow"><span id="MJXc-Node-128" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">r</span></span></span></span></span></span><span id="MJXc-Node-129" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;">)</span></span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mo stretchy="false">(</mo><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">r</mi></mrow></msub><mo stretchy="false">)</mo></math></span></span>
                        
                        
                        
                        for each angle of refraction. Enter the values in the correct blocks in the table, rounded to three decimal digits.</li>
                    <li>Click on the CALCULATE RATIO to complete the last column.</li>
                    <li>Calculate the ratio
                        <span id="MathJax-Element-15-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><mfrac><mrow><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mo stretchy=&quot;false&quot;>(</mo><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>i</mi></mrow></msub><mo stretchy=&quot;false&quot;>)</mo></mrow><mrow><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mo stretchy=&quot;false&quot;>(</mo><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>r</mi></mrow></msub><mo stretchy=&quot;false&quot;>)</mo></mrow></mfrac></math>" role="presentation" style="font-size: 101%; position: relative;"><span id="MJXc-Node-130" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-131" class="mjx-mrow"><span id="MJXc-Node-132" class="mjx-mfrac"><span class="mjx-box MJXc-stacked" style="width: 2.113em; padding: 0px 0.12em;"><span class="mjx-numerator" style="font-size: 70.7%; width: 2.988em; top: -1.665em;"><span id="MJXc-Node-133" class="mjx-mrow" style=""><span id="MJXc-Node-134" class="mjx-texatom"><span id="MJXc-Node-135" class="mjx-mrow"><span id="MJXc-Node-136" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-137" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-138" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-139" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;">(</span></span><span id="MJXc-Node-140" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-141" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-142" class="mjx-texatom" style=""><span id="MJXc-Node-143" class="mjx-mrow"><span id="MJXc-Node-144" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span></span></span></span></span><span id="MJXc-Node-145" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;">)</span></span></span></span><span class="mjx-denominator" style="font-size: 70.7%; width: 2.988em; bottom: -0.958em;"><span id="MJXc-Node-146" class="mjx-mrow" style=""><span id="MJXc-Node-147" class="mjx-texatom"><span id="MJXc-Node-148" class="mjx-mrow"><span id="MJXc-Node-149" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-150" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-151" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-152" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;">(</span></span><span id="MJXc-Node-153" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-154" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-155" class="mjx-texatom" style=""><span id="MJXc-Node-156" class="mjx-mrow"><span id="MJXc-Node-157" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">r</span></span></span></span></span></span><span id="MJXc-Node-158" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;">)</span></span></span></span><span class="mjx-line" style="border-bottom: 1.3px solid; top: -0.289em; width: 2.113em;"></span></span><span class="mjx-vsize" style="height: 1.854em; vertical-align: -0.677em;"></span></span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><mfrac><mrow><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mo stretchy="false">(</mo><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">i</mi></mrow></msub><mo stretchy="false">)</mo></mrow><mrow><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mo stretchy="false">(</mo><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">r</mi></mrow></msub><mo stretchy="false">)</mo></mrow></mfrac></math></span></span> for each row of the table:
                        
                         Enter the values rounded to two decimal digits.</li>
                    <li>Go to the GRAPH tab to continue with the experiment.</li>
                </ol>
            `

            document.querySelector('#disc').innerHTML = `
                Choose an angle of incidence and measure the angle of refraction to complete the table.
            `

            document.querySelector('.Conclusion_container').style.display = 'none';

            if (last_row_countRight > 0) {
                [].forEach.call(document.querySelectorAll('.blueBackground'), function (elem) {
                    elem.remove()
                });

                [].forEach.call(document.querySelectorAll('.for_graph'), function (elem) {
                    elem.style.display = 'none'
                })

                var sin_i = document.querySelectorAll('.sin_i');
                var sin_r = document.querySelectorAll('.sin_r');



                [].forEach.call(sin_i, function (elem, i) {
                    sin_i[i].style.color = 'black';
                    sin_r[i].style.color = 'black';

                    sin_i[i].value = sin_i_right[i]
                    sin_r[i].value = sin_r_right[i]
                })



            }
        })

        document.querySelector('.navigation_buttons.top').onclick = function (event) {
            var target = event.target;
            var navbar = event.currentTarget;
            if (!target.closest('img') || target.closest('.disabled')) return;

            var images = navbar.querySelectorAll('img');
            [].forEach.call(images, function (element) {
                if (element.closest('.disabled')) return;

                var src = element.getAttribute('src');

                var src = src.replace(/\d/, '2')
                element.setAttribute('src', src)
            })
            var src = target.getAttribute('src');

            var src = src.replace(/\d/, '1')
            target.setAttribute('src', src)
            var numbers = [...document.querySelectorAll('.empty.number > div:last-child')];


            if (target.closest('.Conclusion:not(.disabled)')) return;

            if (target.closest('.Graph:not(.disabled)')) {

                numbers.forEach(elem => {
                    elem.style.visibility = 'visible'
                })
            }
            // else {
            //     numbers.forEach(elem=>{
            //         elem.style.visibility = 'hidden'
            //     })
            // }

        }

        document.querySelector('.close_graph_table img').onclick = function (event) {
            event.target.closest('.graph_table').style.zIndex = '-1';
        }

        document.querySelector('.Measuerments').addEventListener('click',function(){
            document.querySelector('.graph_table').style.zIndex = '-1';
            document.querySelector('.calculate_gradient').style.display = 'none';

        })




    }()


var draggable = false;
var position

$(document).off('mousedown', '.line_button_container ').on('mousedown', '.line_button_container ', mouseDown);
$(document).off('touchstart', '.line_button_container ').on('touchstart', '.line_button_container ', touchStart);

function touchStart(e) {
    mouseDown(e);
}

function mouseDown (event) {
    console.log(event.target)
    draggable = true
    console.log("mouseDown")
}

document.body.addEventListener('mouseup', mouseUp) 
document.body.addEventListener('touchend', mouseUp)

function mouseUp (event) {
    console.log("end")

    if (draggable && position && position.left) {
        if (slideButton) {
            slideButton.style.left = position.left;
            slideButton.style.top = position.top;
        }

        draggable = false;
        rotateLight(position.rotate)
        enableInputsFirst2Lines(position.rotate)
        // incidence refraction
    }
    draggable = false
}

function enableInputsFirst2Lines(percent) {
    var incidences = document.querySelectorAll('.incidence');
    var refractions = document.querySelectorAll('.refraction');



    for (var i = 0; i < incidences.length; i++) {
        var inc = incidences[i];
        var incAns = inc.getAttribute('data-ans');
        var ref = refractions[i];
        var refAns = ref.getAttribute('data-ans');

        // console.log(incAns, inc.value);

        if (incAns == percent) {


            inc.value = incAns + 'º';
            inc.style.color = 'black';
            inc.setAttribute('disabled', true)

            inc.style.background = 'rgb(255, 255, 255)'


            if (incAns != parseInt(inc.value))

                incidences[i].removeAttribute('disabled');
            if (refAns != parseInt(ref.value))
                refractions[i].removeAttribute('disabled')
        } else {
            incidences[i].setAttribute('disabled', 'true')
            refractions[i].setAttribute('disabled', 'true')
        }
    }

    if (!incidences[(percent / 15) - 1]) return;
    var nextInput = incidences[(percent / 15) - 1].closest('td').nextElementSibling.querySelector('input');
    var nextInputVal = parseInt(nextInput.value);
    var nextInputAns = nextInput.getAttribute('data-ans');
    // console.log(nextInputVal, nextInputAns)
    if (nextInputVal != nextInputAns)
        helperAlert(1, 'Enter the <span class="bold">angle of refraction</span> in the table.')

}
// slideButton.addEventListener('mouseleave', function (event) {
//     draggable = false
// })

var inputs = document.querySelectorAll('input');
[].forEach.call(inputs, function (elem) {

    elem.onblur = function (event) {


        if (event.sourceCapabilities &&
            event.target.closest('.refraction') ||
            event.target.closest('.sin_i') ||
            event.target.closest('.sin_r') ||
            event.target.closest('.sin_I_R')
        ) {



            if (!elem.value) return;
            if (elem.value == elem.getAttribute('data-ans')) return;

            var errorText

            if (event.target.closest('.refraction')) {
                errorText = 'The angle of refraction is the angle between the refracted ray and the normal. Measure it correctly.'

            } else if (event.target.closest('.sin_i') || event.target.closest('.sin_r')) {
                errorText = 'Set your calculator to degrees mode. Enter the correct value in the correct block. Round to three decimal digits.'
            }
            else if (event.target.closest('.sin_I_R')) {
                errorText = 'Use the rounded values from the previous two columns. Round to two decimal digits.'
            }



            document.querySelector('.hidden').focus()


            varningModal(errorText, elem)
        }

    }
})

// document.addEventListener('blur',function(){
//     alert()
// })

document.addEventListener('click',function(event){
    if(!event.target.closest('#checkBtn')) return;
    event.stopImmediatePropagation()

    if(event.target.style.opacity==1){

        console.log('====================================');
        console.log(1114);
        console.log('====================================');
        window.location.reload(true)
    }
})
var headerHeight = 130;

var a = {
    x: 474, //600
    y: 230
};

var b = {
    x: 554,
    y: 141
};

var c = {
    x: 554,
    y: 320
};

var r = 119; // 600 - a.x

$(document).off('mousemove').on('mousemove', mouseMove);
$(document).off('touchmove').on('touchmove', touchMove);

function touchMove(e) {
    clientY = e.originalEvent.touches[0].clientY;
    mouseMove({clientY})
}
function mouseMove(event){
/*
    r^2 = (a-x)^2 + (b-y)^2
    (a-x)^2 = r^2 - (b-y)^2
    a-x = sqrt(r^2 - (b-y)^2)
    x = a-sqrt(r^2 - (b-y)^2) 
*/
    var clientY = (event.clientY / mashtab - headerHeight - h/mashtab) ;

    if (clientY < 141  ) return;
    if (clientY > 320 ) return;
    if (!draggable) return;

    var clientX = a.x + Math.sqrt(Math.abs(Math.pow(r, 2) - Math.pow( clientY - a.y, 2)));

    slideButton.style.top = clientY + 'px';
    slideButton.style.left = clientX + 'px';

    var fullPercent = c.y - b.y;
    var scrollPercent = 100 - getPercentageChange(fullPercent, clientY - b.y);
    position = getScrollbarPosition(scrollPercent);  
}

function getScrollbarPosition(percent) {

    if (percent < 4) {
        return {
            top: '143px',
            left: '555px',
            rotate: 0
        }
    } else if (percent > 4 && percent < 21) {
        return {
            top: '167px',
            left: '572px',
            rotate: 15
        }
    } else if (percent > 21 && percent < 40) {
        return {
            top: '194px',
            left: '586px',
            rotate: 30
        }
    } else if (percent > 40 && percent < 57) {
        return {
            top: '232px',
            left: '591px',
            rotate: 45
        }
    } else if (percent > 47 && percent < 75) {
        return {
            top: '266px',
            left: '587px',
            rotate: 60
        }
    } else if (percent > 75 && percent < 94) {
        return {
            top: '297px',
            left: '571px',
            rotate: 75
        }
    } else if (percent > 94) {
        return {
            top: '320px',
            left: '554px',
            rotate: 90
        }
    }
}


