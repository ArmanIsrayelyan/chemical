
function offset(el) {
    var rect = el.getBoundingClientRect(),
        scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
        scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
}

let rayBox = document.querySelector('.RayBox');
let lightOut = document.querySelector('.light_from_loop');

let slideButton = document.querySelector('.line_button_container')
let button_container = document.querySelector('.line_button_container');
let sin_i_r_countRight = 0;
let last_row_countRight = 0;
let first_to_countRight = 0;
let graph_table_disabled_elements = [];
let currentSelectedTr;

let graph_sin_i;
let graph_sin_r;
var sin_i_right = ['0,259', '0,500', '0,707', '0,867', '0,966', '1,000'];
var sin_r_right = ['0,174', '0,326', '0,469', '0,574', '0,643', '0,656'];
var sin_I_R_right = ['1,49', '1,53', '1,51', '1,51', '1,50', '1,52'];
var realPercent;







function helperAlert(type, text, endCallback) {


    var disableContainer = document.querySelector('.disable_all');
    disableContainer.style.display = 'block';
    var alertContainer = document.createElement('div');
    alertContainer.classList.add('alertContainer');
    var alertText = document.createElement('p');
    alertText.innerHTML = text;

    if (type != 1) {
        var warning_img = document.createElement('img');
        warning_img.setAttribute('src', './img/warning.JPG');
        alertContainer.appendChild(warning_img);
        alertContainer.classList.add('warning');
    }

    alertContainer.appendChild(alertText)
    wrapper.appendChild(alertContainer);

    setTimeout(function () {

        // alertContainer.style.top = '280px';
        alertContainer.style.opacity = '1';
    })
    setTimeout(function () {

        // alertContainer.style.top = '280px';
        alertContainer.style.opacity = '0';

        setTimeout(function () {
            alertContainer.remove()
            disableContainer.style.display = 'none';


            endCallback ? endCallback() : null;
        }, 1100)
    }, 4000)
}



function rotateLight(deg, active) {
    if (active && deg < 85) {
        deg = +deg
        deg += 15;
        slideButton.style.transition = 'all 0.5s linear'
        // console.log(realPercent)
        position = getScrollbarPosition(realPercent += 18)
        slideButton.style.left = position.left;
        slideButton.style.top = position.top;
        setTimeout(function () {

            slideButton.style.transition = 'all 0s linear'
        }, 0)
    }
    // deg= deg[2] + deg[3];
    // console.log(deg,deg[2] + deg[3]);
    rayBox.setAttribute('class', rayBox.className.replace(/RayBox_rotate_\d{1,2}?/, ''))
    lightOut.setAttribute('class', lightOut.className.replace(/if_light_\d{1,2}?/, ''))
    // button_container.setAttribute('class', button_container.className.replace(/buttonAnimation_../, ''))

    document.querySelector('.light_back_loop').style.transform = `rotate(${180 - deg}deg)`
    rayBox.classList.add('RayBox_rotate_' + deg);
    lightOut.classList.add('if_light_' + deg);
    // console.log(1);
    // button_container.classList.add('buttonAnimation_' + deg)
}
function addClasses() {
    let row = document.querySelectorAll('.task_table tbody tr');


    [].forEach.call(row, elem => {
        // console.log(elment);
        let childes = elem.querySelectorAll('input');
        [].forEach.call(childes, (childes, index) => {
            switch (index) {
                case 0:
                    childes.classList.add('incidence')
                    break;
                case 1:
                    childes.classList.add('refraction')
                    break;
                case 2:
                    childes.classList.add('sin_i')
                    break;
                case 3:
                    childes.classList.add('sin_r')
                    break;
                case 4:
                    childes.classList.add('sin_I_R')
                    break;
            }
        })
    })
}
function rightDisabledInput(elem) {
    elem.setAttribute('disabled', true);
    elem.style.background = '#fff';
    elem.style.color = 'black';
}

function inc_ref_checker(target) {
    var errorText = 'The angle of refraction is the angle between the refracted ray and the normal. Measure it correctly.'
    var answer = target.getAttribute('data-ans');

    var areAnswersTrue = []
    var selfRow = target.closest('tr')
    var value = '' + parseInt(target.value);
    var connection = target.closest('.incidence') ? '.refraction' : '.incidence';
    target.style.color = 'black'


    if (value.length == answer.length) {

        if (value == answer) {
            first_to_countRight++
            allowCompleteButtonClick()
            areAnswersTrue.push(true)
            rightDisabledInput(target)

            if (!document.querySelector('[data-ans="' + answer + '"]')) return;
            var percent = parseInt(document.querySelector('[data-ans="' + answer + '"]').closest('td').previousElementSibling.querySelector('input').value)


            var incidences = document.querySelectorAll('.incidence');

            if (incidences[(percent / 15)]) {
                var nextInput = incidences[(percent / 15)].closest('td').nextElementSibling.querySelector('input');
                var nextInputVal = parseInt(nextInput.value);
                var nextInputAns = nextInput.getAttribute('data-ans');

                // if (nextInputVal != nextInputAns)
                // helperAlert(1, 'Enter the <span class="bold">angle of refraction</span> in the table.')
            }


        } else {
            areAnswersTrue.push(false)


            target.style.color = 'red'

            varningModal(errorText, target)

        }
    }



    var partElement = selfRow.querySelector(connection)
    var partElemValue = '' + parseInt(selfRow.querySelector(connection).value);
    var partElemanswer = selfRow.querySelector(connection).getAttribute('data-ans');


    if (partElemValue.length == partElemanswer.length) {

        if (partElemValue == partElemanswer) {
            areAnswersTrue.push(true)
        } else {
            areAnswersTrue.push(false)

            target.style.color = 'red'


        }
    }

    // console.log(areAnswersTrue);

    if (areAnswersTrue.length > 1) {
        // console.log(areAnswersTrue);

        if (areAnswersTrue[0] && areAnswersTrue[1]) {

            // rotateLight(partElemValue > +answer ? partElemValue : answer, true)




            // openNextTo(target, partElement, selfRow)


            target.setAttribute('disabled', 'true');
            target.style.background = '#fff';
            partElement.setAttribute('disabled', 'true');
            partElement.style.background = '#fff';

        } else {
            // alert()


            // varningModal(text,target)
        }
    }
    if (value.length < answer.length) {

        target.style.color = 'black';

    }

    // }


}

function checkAnswers(target) {

    if (target.closest('.incidence') || target.closest('.refraction')) {
        inc_ref_checker(target)
    } else if (target.closest('.sin_i') || target.closest('.sin_r')) {

        sin_i_r_chacker(target)


    } else if (target.closest('.sin_I_R')) {
        lastRowChacker(target)
    }


}

function lastRowChacker(target) {


    var errorText = 'Use the rounded values from the previous two columns. Round to two decimal digits.';

    var answer = target.getAttribute('data-ans');

    // if(!~pastSteps.indexOf(answer)){
    var value = '' + target.value;

    if (value.length < answer.length) {
        target.style.color = 'black'
    }

    if (value.length == answer.length) {


        if (value == answer) {
            ++last_row_countRight
            rightDisabledInput(target)
            if (last_row_countRight == 6) {
                allowGraphTable()
            } else {
            }

        } else {

            varningModal(errorText, target)

            target.style.color = 'red'
        }

    }
}

function sin_i_r_chacker(target) {
    var answer = target.getAttribute('data-ans').trim() + '';
    var errorText = "Set your calculator to degrees mode. Enter the correct value in the correct block. Round to three decimal digits."

    // if(!~pastSteps.indexOf(answer)){
    var areAnswersTrue = []
    var selfRow = target.closest('tr')
    var value = target.value.trim() + '';
    var connection = target.closest('.sin_i') ? '.sin_r' : '.sin_i';
    target.style.color = 'black'


    if (value.length == answer.length) {



        if (value.replace(',', '') == answer.replace(',', '')) {

            // console.log(1)
            rightDisabledInput(target)
            areAnswersTrue.push(true)
        } else {
            areAnswersTrue.push(false)

            varningModal(errorText, target)
            target.style.color = 'red'
        }
    }


    var partElement = selfRow.querySelector(connection)
    var partElemValue = selfRow.querySelector(connection).value.trim();
    var partElemanswer = selfRow.querySelector(connection).getAttribute('data-ans').trim();


    if (partElemValue.length == partElemanswer.length) {

        if (partElemValue == partElemanswer) {

            // target.setAttribute('disabled',true)
            areAnswersTrue.push(true)
        } else {
            areAnswersTrue.push(false)

            target.style.color = 'red'
        }
    }
    if (areAnswersTrue.length > 1) {

        if (areAnswersTrue[0] && areAnswersTrue[1]) {
            target.setAttribute('disabled', 'true');
            target.style.background = '#fff';
            partElement.setAttribute('disabled', 'true');
            partElement.style.background = '#fff';
            ++sin_i_r_countRight;
            if (sin_i_r_countRight == 6) {
                openLastRow();

            }

        } else {



            // target.style.color = 'red'
        }
    }

}



function openNextTo(target, partElement, parrent) {
    first_to_countRight++

    target.setAttribute('disabled', 'true');
    target.style.background = '#fff';
    partElement.setAttribute('disabled', 'true');
    partElement.style.background = '#fff';

    var nexRow = parrent.nextElementSibling;
    if (nexRow) {
        var inputs = nexRow.querySelectorAll('input');
        var ans = inputs[0].getAttribute('data-ans') + 'º';
        inputs[0].value = ans;
        setTimeout(function () {

            inputs[0].setAttribute('disabled', true);

            inputs[0].style.background = 'rgb(255, 255, 255)';
            // console.log(inputs[0]);
        })
        inputs[0].style.color = 'black';
        inputs[0].removeAttribute('disabled');
        inputs[1].removeAttribute('disabled');
    }
}

function allowCompleteButtonClick() {
    if (first_to_countRight == 6) {

        var complite_button = document.querySelector('.complete_table img');
        var src = '' + complite_button.getAttribute('src');
        src = src.replace(3, 1)
        complite_button.setAttribute('src', src)

        complite_button.addEventListener('mousedown', function (event) {
            // alert();
            src = src.replace(1, 2)
            complite_button.setAttribute('src', src)
        })

        // helperAlert(1,'Proceed to the next tab.')
        complite_button.addEventListener('click', function (event) {

            complite_button.style.display = "none";

            var sin_i = document.querySelectorAll('.sin_i')
            var sin_r = document.querySelectorAll('.sin_r')

            for (var i = 0; i < sin_i.length; i++) {
                sin_r[i].removeAttribute('disabled');
                sin_i[i].removeAttribute('disabled');
            }
            setSinRightanswers()

            helperAlert(1, `Calculate 
            <span id="MathJax-Element-13-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mo stretchy=&quot;false&quot;>(</mo><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>i</mi></mrow></msub><mo stretchy=&quot;false&quot;>)</mo></math>" role="presentation" style="font-size: 101%; position: relative;"><span id="MJXc-Node-102" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-103" class="mjx-mrow"><span id="MJXc-Node-104" class="mjx-texatom"><span id="MJXc-Node-105" class="mjx-mrow"><span id="MJXc-Node-106" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-107" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-108" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-109" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;">(</span></span><span id="MJXc-Node-110" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-111" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-112" class="mjx-texatom" style=""><span id="MJXc-Node-113" class="mjx-mrow"><span id="MJXc-Node-114" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span></span></span></span></span><span id="MJXc-Node-115" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;">)</span></span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mo stretchy="false">(</mo><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">i</mi></mrow></msub><mo stretchy="false">)</mo></math></span></span>
            
            and 
            
           <span id="MathJax-Element-14-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mo stretchy=&quot;false&quot;>(</mo><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>r</mi></mrow></msub><mo stretchy=&quot;false&quot;>)</mo></math>" role="presentation" style="font-size: 101%; position: relative;"><span id="MJXc-Node-116" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-117" class="mjx-mrow"><span id="MJXc-Node-118" class="mjx-texatom"><span id="MJXc-Node-119" class="mjx-mrow"><span id="MJXc-Node-120" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-121" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-122" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-123" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;">(</span></span><span id="MJXc-Node-124" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-125" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-126" class="mjx-texatom" style=""><span id="MJXc-Node-127" class="mjx-mrow"><span id="MJXc-Node-128" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">r</span></span></span></span></span></span><span id="MJXc-Node-129" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;">)</span></span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mo stretchy="false">(</mo><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">r</mi></mrow></msub><mo stretchy="false">)</mo></math></span></span>
            
            for each row of the table. Enter the values rounded to three decimal digits.`)

        })
    }
}
function openLastRow(parrent) {

    var complite_button = document.querySelector('.calculate_ratio img');
    var src = '' + complite_button.getAttribute('src');
    src = src.replace(3, 1)
    complite_button.setAttribute('src', src)


    // helperAlert(1,'Proceed to the next tab.')
    complite_button.addEventListener('mousedown', function (event) {
        // alert();
        src = src.replace(1, 2)
        complite_button.setAttribute('src', src)
    })
    complite_button.addEventListener('click', function (event) {

        complite_button.style.display = "none";

        var sin_i_r = document.querySelectorAll('.sin_I_R')

        for (var i = 0; i < sin_i_r.length; i++) {

            sin_i_r[i].style.background = '#fff';
            sin_i_r[i].style.color = 'black';

            sin_i_r[i].removeAttribute('disabled')
        }
        helperAlert(1, `Calculate the ratio
        <span id="MathJax-Element-15-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><mfrac><mrow><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mo stretchy=&quot;false&quot;>(</mo><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>i</mi></mrow></msub><mo stretchy=&quot;false&quot;>)</mo></mrow><mrow><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mo stretchy=&quot;false&quot;>(</mo><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>r</mi></mrow></msub><mo stretchy=&quot;false&quot;>)</mo></mrow></mfrac></math>" role="presentation" style="font-size: 101%; position: relative;"><span id="MJXc-Node-130" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-131" class="mjx-mrow"><span id="MJXc-Node-132" class="mjx-mfrac"><span class="mjx-box MJXc-stacked" style="width: 2.113em; padding: 0px 0.12em;"><span class="mjx-numerator" style="font-size: 70.7%; width: 2.988em; top: -1.665em;"><span id="MJXc-Node-133" class="mjx-mrow" style=""><span id="MJXc-Node-134" class="mjx-texatom"><span id="MJXc-Node-135" class="mjx-mrow"><span id="MJXc-Node-136" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-137" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-138" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-139" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;">(</span></span><span id="MJXc-Node-140" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-141" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-142" class="mjx-texatom" style=""><span id="MJXc-Node-143" class="mjx-mrow"><span id="MJXc-Node-144" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span></span></span></span></span><span id="MJXc-Node-145" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;">)</span></span></span></span><span class="mjx-denominator" style="font-size: 70.7%; width: 2.988em; bottom: -0.958em;"><span id="MJXc-Node-146" class="mjx-mrow" style=""><span id="MJXc-Node-147" class="mjx-texatom"><span id="MJXc-Node-148" class="mjx-mrow"><span id="MJXc-Node-149" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-150" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-151" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-152" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;">(</span></span><span id="MJXc-Node-153" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-154" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-155" class="mjx-texatom" style=""><span id="MJXc-Node-156" class="mjx-mrow"><span id="MJXc-Node-157" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">r</span></span></span></span></span></span><span id="MJXc-Node-158" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;">)</span></span></span></span><span class="mjx-line" style="border-bottom: 1.3px solid; top: -0.289em; width: 2.113em;"></span></span><span class="mjx-vsize" style="height: 1.854em; vertical-align: -0.677em;"></span></span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><mfrac><mrow><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mo stretchy="false">(</mo><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">i</mi></mrow></msub><mo stretchy="false">)</mo></mrow><mrow><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mo stretchy="false">(</mo><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">r</mi></mrow></msub><mo stretchy="false">)</mo></mrow></mfrac></math></span></span> for each row of the table:
          
         Enter the values rounded to two decimal digits.`)

        // console.log(10);

    })



}
function setSinRightanswers() {




    var sin_i = document.querySelectorAll('.sin_i')
    var sin_r = document.querySelectorAll('.sin_r')

    for (var i = 0; i < sin_i.length; i++) {
        sin_r[i].setAttribute('data-ans', sin_r_right[i]);
        sin_i[i].setAttribute('data-ans', sin_i_right[i]);
        if (sin_I_R_right && sin_I_R_right[i] && sin_I_R_right[i].setAttribute) {

            sin_I_R_right[i].setAttribute('data-ans', sin_I_R_right[i]);
        }
    }
}
function dropNumbers(value) {

    value = value.replace(',', '');
    if (value.length > 1) {

        value = value.replace(/\B/, ',')
    }
    return value;
}


function varningModal(text, element, secondButtonText) {

    var ifPrev = document.querySelector('.click_ignor_container');
    ifPrev && ifPrev.remove();

    var div = document.createElement('div');
    div.classList.add('varnig_container');


    var clickIgnoreDiv = document.createElement('div');
    clickIgnoreDiv.classList.add('click_ignor_container');
    clickIgnoreDiv.appendChild(div)

    // div.style.top = offset(element).top + 'px';
    var content = `

        <div class='varning_icone_container'>
        
            <img src='./assets/img/1.png'/>
        </div>
        <div  class='text_buttons'>
            <p>${text}</p>
            <div>
                <span class='try_agane'>Try again</span>
                <span class='show_correct'>${secondButtonText ? secondButtonText : 'Correct value'}</span>
            
            </div>
        </div>
   `
    div.innerHTML = content;

    document.querySelector('#wrapper').appendChild(clickIgnoreDiv)

    window.currentInput = element;

    element.blur()
}

function inc_ref_autoCheck(target) {
    var selfRow = target.closest('tr');
    var partInput
    if (target.classList.contains('incidence')) {
        partInput = selfRow.querySelector('.refraction');
    } else {
        partInput = selfRow.querySelector('.incidence');
    }
    var partInputValue = parseInt(partInput.value);
    var partInputAnswer = +partInput.getAttribute('data-ans');

    if (partInputValue == partInputAnswer) {
        var percent = +target.getAttribute('data-ans');

        // rotateLight(partInputAnswer > +percent ? partInputAnswer : percent, true)
        // openNextTo(target, partInput, selfRow)
    }


}
function sin_i_r_autoCheck(target) {
    var selfRow = target.closest('tr');
    var partInput
    if (target.classList.contains('sin_i')) {
        partInput = selfRow.querySelector('.sin_r');
    } else {
        partInput = selfRow.querySelector('.sin_i');
    }
    var partInputValue = partInput.value;
    var partInputAnswer = partInput.getAttribute('data-ans');

    // console.log(partInputValue, partInputAnswer)
    if (partInputValue == partInputAnswer) {
        sin_i_r_countRight++;
        if (sin_i_r_countRight == 6) {
            openLastRow()
        }
    }
}

function lastRow_autoCheck(target) {
    last_row_countRight++
    if (last_row_countRight == 6) {
        allowGraphTable()
        helperAlert(1, 'Proceed to the next tab.')
    } else {

    }
}

function allowGraphTable() {
    var button = document.querySelector('.Graph img');
    var src = button.getAttribute('src');
    src = src.replace('3', '2')
    button.setAttribute('src', src);

    button.closest('div').classList.remove('disabled')
    button.onclick = graph_button_click

    document.querySelector('.task_table').onclick = task_table_click
}

function graph_button_click(event) {

    document.querySelector('#disc').innerHTML = 'Click on each row number to plot (<span id="MathJax-Element-22-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><msub><mi>n</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>i</mi></mrow></mrow></msub><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mtext>&amp;#xA0;</mtext><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>i</mi></mrow></msub><mo>=</mo><msub><mi>n</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>r</mi></mrow></mrow></msub><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mtext>&amp;#xA0;</mtext><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>r</mi></mrow></msub></math>" role="presentation" style="font-size: 101%; position: relative;"><span id="MJXc-Node-272" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-273" class="mjx-mrow"><span id="MJXc-Node-281" class="mjx-texatom"><span id="MJXc-Node-282" class="mjx-mrow"><span id="MJXc-Node-283" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-284" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-285" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-286" class="mjx-mtext"><span class="mjx-char MJXc-TeX-main-R" style="margin-top: -0.306em; padding-bottom: 0.313em;">&nbsp;</span></span><span id="MJXc-Node-287" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-288" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-289" class="mjx-texatom" style=""><span id="MJXc-Node-290" class="mjx-mrow"><span id="MJXc-Node-291" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span></span></span></span></span><span id="MJXc-Node-292" class="mjx-mo MJXc-space3"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.065em; padding-bottom: 0.313em;">; </span></span><span id="MJXc-Node-300" class="mjx-texatom"><span id="MJXc-Node-301" class="mjx-mrow"><span id="MJXc-Node-302" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-303" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-304" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-305" class="mjx-mtext"><span class="mjx-char MJXc-TeX-main-R" style="margin-top: -0.306em; padding-bottom: 0.313em;">&nbsp;</span></span><span id="MJXc-Node-306" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-307" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-308" class="mjx-texatom" style=""><span id="MJXc-Node-309" class="mjx-mrow"><span id="MJXc-Node-310" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">r</span></span></span></span></span></span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><msub><mi>n</mi><mrow class="MJX-TeXAtom-ORD"><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">i</mi></mrow></mrow></msub><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mtext>&nbsp;</mtext><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">i</mi></mrow></msub><mo>=</mo><msub><mi>n</mi><mrow class="MJX-TeXAtom-ORD"><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">r</mi></mrow></mrow></msub><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mtext>&nbsp;</mtext><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">r</mi></mrow></msub></math></span></span>). Click on the correct spots on the graph paper and draw a line of best fit.'



    var sinIForGraph = ['0,25', '0,50', '0,70', '0,85', '0,95', '1,00'];
    var sinRForGraph = ['0,15', '0,35', '0,45', '0,55', '0,65', '0,65'];

    var sin_i = document.querySelectorAll('.sin_i')
    var sin_r = document.querySelectorAll('.sin_r')
    var headers = document.querySelectorAll('thead > tr td');

    var blueBackground = document.createElement('div');

    blueBackground.classList.add('blueBackground');


    document.querySelector('.graph_table').style.zIndex = '10'


    headers[3].appendChild(blueBackground)
    var blueBackground = document.createElement('div');

    blueBackground.classList.add('blueBackground');
    blueBackground.classList.add('second');
    headers[4].appendChild(blueBackground)


    var for_graph_text = document.querySelectorAll('.for_graph');
    for_graph_text[0].style.display = 'block';
    for_graph_text[1].style.display = 'block';

    for (var i = 0; i < sinIForGraph.length; i++) {
        sin_i[i].value = sinIForGraph[i];
        sin_r[i].value = sinRForGraph[i];

        sin_i[i].setAttribute('data-graph-value', sinIForGraph[i])
        sin_r[i].setAttribute('data-graph-value', sinRForGraph[i])
    }

    document.querySelector('.navigation_buttons.bottom.graph').style.display = 'flex';




    var instruction = document.querySelector('#insTxtPanel ol');
    instruction.innerHTML = `
    <li>  You should first work through the MEASUREMENTS tab before you continue.</li>
    <li> Click on a row number to plot 
    
        <span id="MathJax-Element-13-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mo stretchy=&quot;false&quot;>(</mo><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>i</mi></mrow></msub><mo stretchy=&quot;false&quot;>)</mo></math>" role="presentation" style="font-size: 101%; position: relative;">( <span id="MJXc-Node-102" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-103" class="mjx-mrow"><span id="MJXc-Node-104" class="mjx-texatom"><span id="MJXc-Node-105" class="mjx-mrow"><span id="MJXc-Node-106" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-107" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-108" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-109" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"> </span></span><span id="MJXc-Node-110" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-111" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-112" class="mjx-texatom" style=""><span id="MJXc-Node-113" class="mjx-mrow"><span id="MJXc-Node-114" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span></span></span></span></span><span id="MJXc-Node-115" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"> ; </span></span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mo stretchy="false">(</mo><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">i</mi></mrow></msub><mo stretchy="false">)</mo></math></span></span><span id="MathJax-Element-14-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mo stretchy=&quot;false&quot;>(</mo><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>r</mi></mrow></msub><mo stretchy=&quot;false&quot;>)</mo></math>" role="presentation" style="font-size: 101%; position: relative;"><span id="MJXc-Node-116" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-117" class="mjx-mrow"><span id="MJXc-Node-118" class="mjx-texatom"><span id="MJXc-Node-119" class="mjx-mrow"><span id="MJXc-Node-120" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-121" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-122" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-123" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"> </span></span><span id="MJXc-Node-124" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-125" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-126" class="mjx-texatom" style=""><span id="MJXc-Node-127" class="mjx-mrow"><span id="MJXc-Node-128" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">r</span></span></span></span></span></span><span id="MJXc-Node-129" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"> </span></span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mo stretchy="false">(</mo><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">r</mi></mrow></msub><mo stretchy="false">)</mo></math></span>)</span></li>
    <li> Find the 
    
    <span id="MathJax-Element-13-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mo stretchy=&quot;false&quot;>(</mo><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>i</mi></mrow></msub><mo stretchy=&quot;false&quot;>)</mo></math>" role="presentation" style="font-size: 101%; position: relative;"><span id="MJXc-Node-102" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-103" class="mjx-mrow"><span id="MJXc-Node-104" class="mjx-texatom"><span id="MJXc-Node-105" class="mjx-mrow"><span id="MJXc-Node-106" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-107" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-108" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-109" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"> </span></span><span id="MJXc-Node-110" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-111" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-112" class="mjx-texatom" style=""><span id="MJXc-Node-113" class="mjx-mrow"><span id="MJXc-Node-114" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span></span></span></span></span><span id="MJXc-Node-115" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"> </span></span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mo stretchy="false">(</mo><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">i</mi></mrow></msub><mo stretchy="false">)</mo></math></span></span>
    
    
    value on the horizontal axis and the 
    
   <span id="MathJax-Element-14-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mo stretchy=&quot;false&quot;>(</mo><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>r</mi></mrow></msub><mo stretchy=&quot;false&quot;>)</mo></math>" role="presentation" style="font-size: 101%; position: relative;"><span id="MJXc-Node-116" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-117" class="mjx-mrow"><span id="MJXc-Node-118" class="mjx-texatom"><span id="MJXc-Node-119" class="mjx-mrow"><span id="MJXc-Node-120" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-121" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-122" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-123" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"> </span></span><span id="MJXc-Node-124" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-125" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-126" class="mjx-texatom" style=""><span id="MJXc-Node-127" class="mjx-mrow"><span id="MJXc-Node-128" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">r</span></span></span></span></span></span><span id="MJXc-Node-129" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"> </span></span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mo stretchy="false">(</mo><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">r</mi></mrow></msub><mo stretchy="false">)</mo></math></span></span>
    
    
    value on the vertical axis.</li>
    <li> Click on the correct spot on the graph paper to show where the point must be plotted.</li>
    <li> Repeat Steps 2 to 4 until all the points are plotted.</li>
    <li> Click on the DRAW LINE button. A line of best fit will be drawn through the plotted points.</li>
    <li> Click on the GRADIENT button for calculations about the gradient of the graph.</li>
    <li> Go to the CONCLUSION tab to continue with the experiment.</li>
    `
}
function task_table_click(event) {
    var target = event.target;
    var tr = target.closest('tr')
    currentSelectedTr = tr
    if (!tr) return;
    var td = tr.querySelector('td')

    var si = document.querySelectorAll('.sin_i');
    var sr = document.querySelectorAll('.sin_r');

    for (let i = 0; i < sr.length; i++) {
        si[i].style.color = 'black'
        sr[i].style.color = 'black'
    }




    // helperAlert(1, 'Plot this point.')

    document.querySelector('.pinkBackground') && document.querySelector('.pinkBackground').remove();


    var actionHelperText = document.createElement('div');
    actionHelperText.classList.add('actionHelperText')
    var pinkBackground = document.createElement('div');
    pinkBackground.classList.add('pinkBackground');
    pinkBackground.style.top = -2 + 'px';



    td.appendChild(pinkBackground);

    [...document.querySelectorAll('.helper_text_bottom .text')].forEach(elem => {
        elem.style.display = 'none';
    });
    td.querySelector('.helper_text_bottom .text').style.display = 'block';
    td.querySelector('.helper_text_bottom ').style.display = 'block';
    var sin_i = tr.querySelector('.sin_i').getAttribute('data-graph-value');
    var sin_r = tr.querySelector('.sin_r').getAttribute('data-graph-value');

    graph_sin_i = sin_i.replace(',', '.');
    graph_sin_r = sin_r.replace(',', '.');

    tr.querySelector('.sin_i').style.color = '#313198';
    tr.querySelector('.sin_r').style.color = '#313198';
}

var disabledClicks = false;
function graph_table_click(event) {
    var errorText = `Carefully read the 
    
    
    <span id="MathJax-Element-13-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mo stretchy=&quot;false&quot;>(</mo><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>i</mi></mrow></msub><mo stretchy=&quot;false&quot;>)</mo></math>" role="presentation" style="font-size: 101%; position: relative;"><span id="MJXc-Node-102" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-103" class="mjx-mrow"><span id="MJXc-Node-104" class="mjx-texatom"><span id="MJXc-Node-105" class="mjx-mrow"><span id="MJXc-Node-106" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-107" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-108" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-109" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"> </span></span><span id="MJXc-Node-110" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-111" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-112" class="mjx-texatom" style=""><span id="MJXc-Node-113" class="mjx-mrow"><span id="MJXc-Node-114" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span></span></span></span></span><span id="MJXc-Node-115" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"></span></span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mo stretchy="false">(</mo><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">i</mi></mrow></msub><mo stretchy="false">)</mo></math></span></span>
    
    
    value on the horizontal axis and the 
    <span id="MathJax-Element-14-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mo stretchy=&quot;false&quot;>(</mo><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>r</mi></mrow></msub><mo stretchy=&quot;false&quot;>)</mo></math>" role="presentation" style="font-size: 101%; position: relative;"><span id="MJXc-Node-116" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-117" class="mjx-mrow"><span id="MJXc-Node-118" class="mjx-texatom"><span id="MJXc-Node-119" class="mjx-mrow"><span id="MJXc-Node-120" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-121" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-122" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-123" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"> </span></span><span id="MJXc-Node-124" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-125" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-126" class="mjx-texatom" style=""><span id="MJXc-Node-127" class="mjx-mrow"><span id="MJXc-Node-128" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">r</span></span></span></span></span></span><span id="MJXc-Node-129" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"></span></span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mo stretchy="false">(</mo><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">r</mi></mrow></msub><mo stretchy="false">)</mo></math></span></span>
    
     value on the vertical axis.`
    if (disabledClicks) return;
    var target = event.target;
    if (!target.closest('.graph_circle') || target.closest('.graph_circle_info')) return;
    var item_position = target.getAttribute('data-item-position');
    var rowPosition = target.closest('[data-row-position]').getAttribute('data-row-position')

    if (~graph_table_disabled_elements.indexOf(target)) return;

    if (!graph_sin_i || !graph_sin_r) return;
    if (!currentSelectedTr) return;


    if (item_position == graph_sin_i && rowPosition == graph_sin_r) {
        [...target.querySelectorAll('.graph_circle_info')].forEach(elem => {
            elem.remove()
        });

        var infoContainer = document.createElement('div');
        infoContainer.classList.add('graph_circle_info')
        infoContainer.innerHTML = (`(${graph_sin_i}; ${graph_sin_r})`).replace(/\./g, ',');

        target.appendChild(infoContainer);

        [...document.querySelectorAll('.helper_text_bottom')].forEach(elem => {
            elem.style.display = 'none';
        })

        if (item_position == '1.00' && rowPosition == '0.65') {
            infoContainer.style.top = '-37px'
            infoContainer.style.left = '-67px'
        }
        if (item_position == '0.95' && rowPosition == '0.65') {
            infoContainer.style.top = '-13px'
            infoContainer.style.left = '-112px'

        }



        set_right_point(graph_sin_i, graph_sin_r);
        enableDrawLine()


    } else {


        target.style.background = 'red';

        currentSelectedTr.querySelector('.sin_i').style.color = 'red'
        currentSelectedTr.querySelector('.sin_r').style.color = 'red'

        disabledClicks = true

        setTimeout(function () {

            varningModal(errorText, target, 'Correct plot');

            disabledClicks = false;
        }, 1000)
        // window.selected_sin_i = document.querySelector('graph_sin_i.replace('.',','));
        // graph_sin_r.replace(',','.');
    }


}

function set_right_point(graph_sin_i, graph_sin_r) {
    var row = document.querySelector('[data-row-position="' + graph_sin_r + '"]');
    var item = row.querySelector('[data-item-position="' + graph_sin_i + '"]')
    item.style.background = '#3366CC';
    graph_table_disabled_elements.push(item);

    unselectRow()
    var infoContainer = document.createElement('div');
    infoContainer.classList.add('graph_circle_info');
    [...item.querySelectorAll('.graph_circle_info')].forEach(elem => {
        elem.remove()
    });
    infoContainer.innerHTML = (`(${graph_sin_i}; ${graph_sin_r})`).replace(/\./g, ',');
    item.appendChild(infoContainer)
    if (graph_sin_i == '1.00' && graph_sin_r == '0.65') {
        infoContainer.style.top = '-33px'
        infoContainer.style.left = '-91px'
    }
    if (graph_sin_i == '0.95' && graph_sin_r == '0.65') {
        infoContainer.style.top = '-10px'
    }
    currentSelectedTr = '';

}

function unselectRow() {
    document.querySelector('.pinkBackground') && document.querySelector('.pinkBackground').remove();
    currentSelectedTr.classList.add('unSelectable')
    currentSelectedTr.addEventListener('click', function (event) {
        event.stopImmediatePropagation();
    })
    enableDrawLine()
}

function enableDrawLine() {
    if (graph_table_disabled_elements.length == 6) {
        var drawLineButton = document.querySelector('.draw_line img');
        var src = drawLineButton.getAttribute('src');
        var src = src.replace('3', '1');
        drawLineButton.setAttribute('src', src);

        drawLineButton.addEventListener('click', function (event) {
            var src = drawLineButton.getAttribute('src');
            var src = src.replace('1', '2');
            drawLineButton.setAttribute('src', src);
            drawLineButton_click();
        })
    }
}

function drawLineButton_click() {
    var line = document.querySelector('.graph_draw_line');
    line.style.border = '2px solid #FFA113';
    line.style.width = '621px';
    var gradient = document.querySelector('.gradient img');

    var src = gradient.getAttribute('src');
    var src = src.replace('3', '1');
    gradient.setAttribute('src', src);


    gradient.addEventListener('click', function (event) {
        src = src.replace('1', '2');
        gradient.setAttribute('src', src);

        drawGraph()

    })

}

function drawGraph() {
    var row = document.querySelector('[data-row-position="0.35"]');
    var item = row.querySelector('[data-item-position="0.50"]');
    var drawGraphDiv = document.createElement('div');
    drawGraphDiv.classList.add('drawGraphDiv')

    drawGraphDiv.style.display = 'block'

    document.querySelector('.calculate_gradient').style.display = 'block';



    item.appendChild(drawGraphDiv)
    drawGraphDiv.style.height = '150px';

    drawGraphDiv.style.width = '214px';
}

function getPercentageChange(oldNumber, newNumber) {
    var decreaseValue = oldNumber - newNumber;
    return (decreaseValue / oldNumber) * 100;
}

function removeHelperAlert() {
    [...document.querySelectorAll('.alertContainer')].forEach(elem => {
        elem.remove();
        document.querySelector('.disable_all').style.display = 'none';
    });

}

document.addEventListener('click', function (event) {
    if (document.querySelector('.disable_all').style.display == 'none') {
        removeHelperAlert()
    }
    if (event.target.closest('.alertContainer')) {

        removeHelperAlert()
    }
})
document.querySelector('.disable_all').addEventListener('click', removeHelperAlert);


function showDescription(slidePage) {

    var pageIndex = slidePage - 1
    var container = document.querySelector('.description__content');

    var content = [
        ['<p>The gradient of the graph remains constant.</p> ', '<p> This tells  us that the ratio  <span id="MathJax-Element-15-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><mfrac><mrow><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mo stretchy=&quot;false&quot;>(</mo><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>i</mi></mrow></msub><mo stretchy=&quot;false&quot;>)</mo></mrow><mrow><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mo stretchy=&quot;false&quot;>(</mo><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>r</mi></mrow></msub><mo stretchy=&quot;false&quot;>)</mo></mrow></mfrac></math>" role="presentation" style="font-size: 101%; position: relative;"><span id="MJXc-Node-130" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-131" class="mjx-mrow"><span id="MJXc-Node-132" class="mjx-mfrac"><span class="mjx-box MJXc-stacked" style="width: 2.113em; padding: 0px 0.12em;"><span class="mjx-numerator" style="font-size: 70.7%; width: 2.988em; top: -1.665em;"><span id="MJXc-Node-133" class="mjx-mrow" style=""><span id="MJXc-Node-134" class="mjx-texatom"><span id="MJXc-Node-135" class="mjx-mrow"><span id="MJXc-Node-136" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-137" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-138" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-139" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"> </span></span><span id="MJXc-Node-140" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-141" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-142" class="mjx-texatom" style=""><span id="MJXc-Node-143" class="mjx-mrow"><span id="MJXc-Node-144" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span></span></span></span></span><span id="MJXc-Node-145" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"></span></span></span></span><span class="mjx-denominator" style="font-size: 70.7%; width: 2.988em; bottom: -0.958em;"><span id="MJXc-Node-146" class="mjx-mrow" style=""><span id="MJXc-Node-147" class="mjx-texatom"><span id="MJXc-Node-148" class="mjx-mrow"><span id="MJXc-Node-149" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-150" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-151" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-152" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"> </span></span><span id="MJXc-Node-153" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-154" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-155" class="mjx-texatom" style=""><span id="MJXc-Node-156" class="mjx-mrow"><span id="MJXc-Node-157" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">r</span></span></span></span></span></span><span id="MJXc-Node-158" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"></span></span></span></span><span class="mjx-line" style="border-bottom: 1.3px solid; top: -0.289em; width: 2.113em;"></span></span><span class="mjx-vsize" style="height: 1.854em; vertical-align: -0.677em;"></span></span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><mfrac><mrow><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mo stretchy="false">(</mo><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">i</mi></mrow></msub><mo stretchy="false">)</mo></mrow><mrow><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mo stretchy="false">(</mo><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">r</mi></mrow></msub><mo stretchy="false">)</mo></mrow></mfrac></math></span></span> remains constant.</p>'],
        [
            `<span id="MathJax-Element-18-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><mfrac><mn>1</mn><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>g</mi><mi mathvariant=&quot;normal&quot;>r</mi><mi mathvariant=&quot;normal&quot;>a</mi><mi mathvariant=&quot;normal&quot;>d</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>e</mi><mi mathvariant=&quot;normal&quot;>n</mi><mi mathvariant=&quot;normal&quot;>t</mi></mrow></mfrac></math>" role="presentation" style="font-size: 101%; position: relative;"><span id="MJXc-Node-211" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-212" class="mjx-mrow"><span id="MJXc-Node-213" class="mjx-mfrac"><span class="mjx-box MJXc-stacked" style="width: 2.698em; padding: 0px 0.12em;"><span class="mjx-numerator" style="font-size: 70.7%; width: 3.815em; top: -1.331em;"><span id="MJXc-Node-214" class="mjx-mn" style=""><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">1</span></span></span>
            <span style="font-family: Roboto; font-size: 21px; padding: 5px 5px;">gradient</span><span class="mjx-line" style="border-bottom: 1.3px solid; top: -0.289em; width: 2.698em;"></span></span><span class="mjx-vsize" style="height: 1.547em; vertical-align: -0.606em;"></span></span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><mfrac><mn>1</mn><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">g</mi><mi mathvariant="normal">r</mi><mi mathvariant="normal">a</mi><mi mathvariant="normal">d</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">e</mi><mi mathvariant="normal">n</mi><mi mathvariant="normal">t</mi></mrow></mfrac></math></span></span> <span id="MathJax-Element-17-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><mfrac><mn>1</mn><mfrac><mrow><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mo stretchy=&quot;false&quot;>(</mo><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>i</mi></mrow></msub><mo stretchy=&quot;false&quot;>)</mo></mrow><mrow><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mo stretchy=&quot;false&quot;>(</mo><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>r</mi></mrow></msub><mo stretchy=&quot;false&quot;>)</mo></mrow></mfrac></mfrac></math>" role="presentation" style="font-size: 101%; position: relative;"><span id="MJXc-Node-180" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-162" class="mjx-mo MJXc-space3"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.065em; padding-bottom: 0.313em;">=</span></span>


            <span id="MJXc-Node-181" class="mjx-mrow"><span id="MJXc-Node-182" class="mjx-mfrac"><span class="mjx-box MJXc-stacked" style="width: 1.87em; padding: 0px 0.12em;"><span class="mjx-numerator" style="font-size: 70.7%; width: 2.644em; top: -1.331em;"><span id="MJXc-Node-183" class="mjx-mn" style=""><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">1</span></span></span><span class="mjx-denominator" style="font-size: 70.7%; width: 2.644em; bottom: -1.858em;"><span id="MJXc-Node-184" class="mjx-mfrac" style=""><span class="mjx-box MJXc-stacked" style="width: 2.204em; padding: 0px 0.12em;"><span class="mjx-numerator" style="font-size: 70.7%; width: 3.117em; top: -1.778em;"><span id="MJXc-Node-185" class="mjx-mrow" style=""><span id="MJXc-Node-186" class="mjx-texatom"><span id="MJXc-Node-187" class="mjx-mrow"><span id="MJXc-Node-188" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-189" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-190" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-191" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"> </span></span><span id="MJXc-Node-192" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-193" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="vertical-align: -0.34em; padding-right: 0.05em;"><span id="MJXc-Node-194" class="mjx-texatom"><span id="MJXc-Node-195" class="mjx-mrow"><span id="MJXc-Node-196" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span></span></span></span></span><span id="MJXc-Node-197" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"></span></span></span></span><span class="mjx-denominator" style="font-size: 70.7%; width: 3.117em; bottom: -0.981em;"><span id="MJXc-Node-198" class="mjx-mrow" style=""><span id="MJXc-Node-199" class="mjx-texatom"><span id="MJXc-Node-200" class="mjx-mrow"><span id="MJXc-Node-201" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-202" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-203" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-204" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"> </span></span><span id="MJXc-Node-205" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-206" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="vertical-align: -0.15em; padding-right: 0.05em;"><span id="MJXc-Node-207" class="mjx-texatom"><span id="MJXc-Node-208" class="mjx-mrow"><span id="MJXc-Node-209" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">r</span></span></span></span></span></span><span id="MJXc-Node-210" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"></span></span></span></span><span class="mjx-line" style="border-bottom: 1px solid; top: -0.292em; width: 2.204em;"></span></span><span class="mjx-vsize" style="height: 1.951em; vertical-align: -0.693em;"></span></span></span><span class="mjx-line" style="border-bottom: 1.3px solid; top: -0.289em; width: 1.87em;"></span></span><span class="mjx-vsize" style="height: 2.255em; vertical-align: -1.314em;"></span></span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><mfrac><mn>1</mn><mfrac><mrow><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mo stretchy="false"> </mo><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">i</mi></mrow></msub><mo stretchy="false">)</mo></mrow><mrow><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mo stretchy="false">(</mo><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">r</mi></mrow></msub><mo stretchy="false">)</mo></mrow></mfrac></mfrac></math></span></span> <span id="MathJax-Element-15-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><mfrac><mrow><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mo stretchy=&quot;false&quot;>(</mo><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>i</mi></mrow></msub><mo stretchy=&quot;false&quot;>)</mo></mrow><mrow><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mo stretchy=&quot;false&quot;>(</mo><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>r</mi></mrow></msub><mo stretchy=&quot;false&quot;>)</mo></mrow></mfrac></math>" role="presentation" style="font-size: 101%; position: relative;"><span id="MJXc-Node-130" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-162" class="mjx-mo MJXc-space3"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.065em; padding-bottom: 0.313em;">=</span></span><span id="MJXc-Node-131" class="mjx-mrow"><span id="MJXc-Node-132" class="mjx-mfrac"><span class="mjx-box MJXc-stacked" style="width: 2.113em; padding: 0px 0.12em;"><span class="mjx-numerator" style="font-size: 70.7%; width: 2.988em; top: -1.665em;"><span id="MJXc-Node-133" class="mjx-mrow" style=""><span id="MJXc-Node-134" class="mjx-texatom"><span id="MJXc-Node-135" class="mjx-mrow"><span id="MJXc-Node-136" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-137" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-138" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-139" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"> </span></span><span id="MJXc-Node-140" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-141" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-142" class="mjx-texatom" style=""><span id="MJXc-Node-143" class="mjx-mrow"><span id="MJXc-Node-144" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span></span></span></span></span><span id="MJXc-Node-145" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"></span></span></span></span><span class="mjx-denominator" style="font-size: 70.7%; width: 2.988em; bottom: -0.958em;"><span id="MJXc-Node-146" class="mjx-mrow" style=""><span id="MJXc-Node-147" class="mjx-texatom"><span id="MJXc-Node-148" class="mjx-mrow"><span id="MJXc-Node-149" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-150" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-151" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-152" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"> </span></span><span id="MJXc-Node-153" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-154" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-155" class="mjx-texatom" style=""><span id="MJXc-Node-156" class="mjx-mrow"><span id="MJXc-Node-157" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">r</span></span></span></span></span></span><span id="MJXc-Node-158" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"></span></span></span></span><span class="mjx-line" style="border-bottom: 1.3px solid; top: -0.289em; width: 2.113em;"></span></span><span class="mjx-vsize" style="height: 1.854em; vertical-align: -0.677em;"></span></span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><mfrac><mrow><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mo stretchy="false"> </mo><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">i</mi></mrow></msub><mo stretchy="false">)</mo></mrow><mrow><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mo stretchy="false">(</mo><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">r</mi></mrow></msub><mo stretchy="false">)</mo></mrow></mfrac></math></span></span>
            `,
            ` <span id="MathJax-Element-18-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><mfrac><mn>1</mn><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>g</mi><mi mathvariant=&quot;normal&quot;>r</mi><mi mathvariant=&quot;normal&quot;>a</mi><mi mathvariant=&quot;normal&quot;>d</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>e</mi><mi mathvariant=&quot;normal&quot;>n</mi><mi mathvariant=&quot;normal&quot;>t</mi></mrow></mfrac></math>" role="presentation" style="font-size: 101%; position: relative;"><span id="MJXc-Node-211" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-212" class="mjx-mrow"><span id="MJXc-Node-213" class="mjx-mfrac"><span class="mjx-box MJXc-stacked" style="width: 2.698em; padding: 0px 0.12em;"><span class="mjx-numerator" style="font-size: 70.7%; width: 3.815em; top: -1.331em;"><span id="MJXc-Node-214" class="mjx-mn" style=""><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">1</span></span></span>
            <span style="font-family: Roboto; font-size: 21px; padding: 5px 5px;">gradient</span><span class="mjx-line" style="border-bottom: 1.3px solid; top: -0.289em; width: 2.698em;"></span></span><span class="mjx-vsize" style="height: 1.547em; vertical-align: -0.606em;"></span></span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><mfrac><mn>1</mn><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">g</mi><mi mathvariant="normal">r</mi><mi mathvariant="normal">a</mi><mi mathvariant="normal">d</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">e</mi><mi mathvariant="normal">n</mi><mi mathvariant="normal">t</mi></mrow></mfrac></math></span></span>
              <span id="MathJax-Element-16-Frame"  class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><mi>m</mi><mo>=</mo><mfrac><mrow><msub><mi>y</mi><mn>2</mn></msub><mo>&amp;#x2212;</mo><msub><mi>y</mi><mn>1</mn></msub></mrow><mrow><msub><mi>x</mi><mn>2</mn></msub><mo>&amp;#x2212;</mo><msub><mi>x</mi><mn>1</mn></msub></mrow></mfrac></math>" role="presentation"><span id="MJXc-Node-159" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-160" class="mjx-mrow"><span id="MJXc-Node-162" class="mjx-mo MJXc-space3"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.065em; padding-bottom: 0.313em;">=</span></span><span id="MJXc-Node-163" class="mjx-mfrac MJXc-space3"><span class="mjx-box MJXc-stacked" style="width: 2.05em; padding: 0px 0.12em;"><span class="mjx-numerator" style="font-size: 70.7%; width: 2.9em; top: -1.453em;"><span id="MJXc-Node-164" class="mjx-mrow" style=""><span id="MJXc-Node-165" class="mjx-msubsup"><span class="mjx-base" style="margin-right: -0.006em;"><span id="MJXc-Node-166" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.251em; padding-bottom: 0.498em; padding-right: 0.006em;">1</span></span></span></span></span></span><span class="mjx-denominator" style="font-size: 70.7%; width: 2.9em; bottom: -0.683em;"><span id="MJXc-Node-172" class="mjx-mrow" style=""><span id="MJXc-Node-173" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-174" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.251em; padding-bottom: 0.313em; font-family: MJXc-TeX-main-R, MJXc-TeX-main-Rw;">0,67</span></span></span></span></span></span><span class="mjx-line" style="border-bottom: 1.3px solid; top: -0.289em; width: 2.05em;"></span></span><span class="mjx-vsize" style="height: 1.51em; vertical-align: -0.483em;"></span></span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><mi>m</mi><mo>=</mo><mfrac><mrow><msub><mi>y</mi><mn>2</mn></msub><mo>−</mo><msub><mi>y</mi><mn>1</mn></msub></mrow><mrow><msub><mi>x</mi><mn>2</mn></msub><mo> − </mo><msub><mi>x</mi><mn>1</mn></msub></mrow></mfrac></math></span></span>
              <span id="MathJax-Element-16-Frame"  class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><mi>m</mi><mo>=</mo><mfrac><mrow><msub><mi>y</mi><mn>2</mn></msub><mo>&amp;#x2212;</mo><msub><mi>y</mi><mn>1</mn></msub></mrow><mrow><msub><mi>x</mi><mn>2</mn></msub><mo>&amp;#x2212;</mo><msub><mi>x</mi><mn>1</mn></msub></mrow></mfrac></math>" role="presentation"><span id="MJXc-Node-159" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-160" class="mjx-mrow"><span id="MJXc-Node-162" class="mjx-mo MJXc-space3"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.065em; padding-bottom: 0.313em;">=</span></span><span id="MJXc-Node-163" class="mjx-mfrac MJXc-space3">1,49</span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><mi>m</mi><mo>=</mo><mfrac><mrow><msub><mi>y</mi><mn>2</mn></msub><mo>−</mo><msub><mi>y</mi><mn>1</mn></msub></mrow><mrow><msub><mi>x</mi><mn>2</mn></msub><mo> − </mo><msub><mi>x</mi><mn>1</mn></msub></mrow></mfrac></math></span></span>
            `,
            '<p>this corresponds with the information in the table.</p>'
        ],
        [`
        
         
        <span id="MathJax-Element-23-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><msub><mi>n</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>a</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>r</mi></mrow></mrow></msub><mo>=</mo><mn>1</mn></math>" role="presentation" style="font-size: 101%; position: relative;"><span id="MJXc-Node-311" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-312" class="mjx-mrow"><span id="MJXc-Node-313" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-314" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.251em; padding-bottom: 0.313em;">n</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-315" class="mjx-texatom" style=""><span id="MJXc-Node-316" class="mjx-mrow"><span id="MJXc-Node-317" class="mjx-texatom"><span id="MJXc-Node-318" class="mjx-mrow"><span id="MJXc-Node-319" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">a</span></span><span id="MJXc-Node-320" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-321" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">r</span></span></span></span></span></span></span></span><span id="MJXc-Node-322" class="mjx-mo MJXc-space3"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.065em; padding-bottom: 0.313em;">=</span></span><span id="MJXc-Node-323" class="mjx-mn MJXc-space3"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">1,00</span></span></span>

        <span class="m_font">and</span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><msub><mi>n</mi><mrow class="MJX-TeXAtom-ORD"><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">a</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">r</mi></mrow></mrow></msub><mo>=</mo><mn>1</mn></math></span></span>
        <span id="MathJax-Element-24-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><msub><mi>n</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>g</mi><mi mathvariant=&quot;normal&quot;>l</mi><mi mathvariant=&quot;normal&quot;>a</mi><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>s</mi></mrow></mrow></msub><mo>=</mo><mn>1</mn><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mo>,</mo></mrow><mn>52</mn></math>" role="presentation" style="font-size: 101%; position: relative;"><span id="MJXc-Node-324" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-325" class="mjx-mrow"><span id="MJXc-Node-326" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-327" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.251em; padding-bottom: 0.313em;">n</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.219em; padding-right: 0.071em;"><span id="MJXc-Node-328" class="mjx-texatom" style=""><span id="MJXc-Node-329" class="mjx-mrow"><span id="MJXc-Node-330" class="mjx-texatom"><span id="MJXc-Node-331" class="mjx-mrow"><span id="MJXc-Node-332" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.56em;">g</span></span><span id="MJXc-Node-333" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.313em;">l</span></span><span id="MJXc-Node-334" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">a</span></span><span id="MJXc-Node-335" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-336" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span></span></span></span></span></span></span><span id="MJXc-Node-337" class="mjx-mo MJXc-space3"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.065em; padding-bottom: 0.313em;">=</span></span><span id="MJXc-Node-338" class="mjx-mn MJXc-space3" style="
    margin-right: 0;
"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">1</span></span><span id="MJXc-Node-339" class="mjx-texatom"><span id="MJXc-Node-340" class="mjx-mrow"><span id="MJXc-Node-341" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="margin-top: -0.183em; padding-bottom: 0.56em;">,</span></span></span></span><span id="MJXc-Node-342" class="mjx-mn"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.374em;">49</span></span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><msub><mi>n</mi><mrow class="MJX-TeXAtom-ORD"><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">g</mi><mi mathvariant="normal">l</mi><mi mathvariant="normal">a</mi><mi mathvariant="normal">s</mi><mi mathvariant="normal">s</mi></mrow></mrow></msub><mo>=</mo><mn>1</mn><mrow class="MJX-TeXAtom-ORD"><mo>,</mo></mrow><mn>52</mn></math></span></span>`

, 
`
        <span  style=' font-family: "Lato";'>From this and the gradient of the graph:<span>

`


,`
        
        
        
        <span id="MathJax-Element-21-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><mfrac><msub><mi>n</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>a</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>r</mi></mrow></mrow></msub><msub><mi>n</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>g</mi><mi mathvariant=&quot;normal&quot;>l</mi><mi mathvariant=&quot;normal&quot;>a</mi><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>s</mi></mrow></mrow></msub></mfrac></math>" role="presentation" style="font-size: 101%; position: relative;"><span id="MJXc-Node-249" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-250" class="mjx-mrow"><span id="MJXc-Node-251" class="mjx-mfrac"><span class="mjx-box MJXc-stacked" style="width: 1.624em; padding: 0px 0.12em;"><span class="mjx-numerator" style="font-size: 70.7%; width: 2.296em; top: -1.257em;"><span id="MJXc-Node-252" class="mjx-msubsup" style=""><span class="mjx-base"><span id="MJXc-Node-253" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.251em; padding-bottom: 0.313em;">n</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-254" class="mjx-texatom" style=""><span id="MJXc-Node-255" class="mjx-mrow"><span id="MJXc-Node-256" class="mjx-texatom"><span id="MJXc-Node-257" class="mjx-mrow"><span id="MJXc-Node-258" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">a</span></span><span id="MJXc-Node-259" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-260" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">r</span></span></span></span></span></span></span></span></span><span class="mjx-denominator" style="font-size: 70.7%; width: 2.296em; bottom: -0.806em;"><span id="MJXc-Node-261" class="mjx-msubsup" style=""><span class="mjx-base"><span id="MJXc-Node-262" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.251em; padding-bottom: 0.313em;">n</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.219em; padding-right: 0.071em;"><span id="MJXc-Node-263" class="mjx-texatom" style=""><span id="MJXc-Node-264" class="mjx-mrow"><span id="MJXc-Node-265" class="mjx-texatom"><span id="MJXc-Node-266" class="mjx-mrow"><span id="MJXc-Node-267" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.56em;">g</span></span><span id="MJXc-Node-268" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.313em;">l</span></span><span id="MJXc-Node-269" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">a</span></span><span id="MJXc-Node-270" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-271" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span></span></span></span></span></span></span></span><span class="mjx-line" style="border-bottom: 1.3px solid; top: -0.289em; width: 1.624em;"></span></span><span class="mjx-vsize" style="height: 1.459em; vertical-align: -0.57em;"></span><span id="MJXc-Node-350" class="mjx-mo MJXc-space3"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.065em; padding-bottom: 0.313em;">=</span></span></span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><mfrac><msub><mi>n</mi><mrow class="MJX-TeXAtom-ORD"><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">a</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">r</mi></mrow></mrow></msub><msub><mi>n</mi><mrow class="MJX-TeXAtom-ORD"><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">g</mi><mi mathvariant="normal">l</mi><mi mathvariant="normal">a</mi><mi mathvariant="normal">s</mi><mi mathvariant="normal">s</mi></mrow></mrow></msub></mfrac></math></span></span>
        <span id="MathJax-Element-15-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><mfrac><mrow><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mo stretchy=&quot;false&quot;>(</mo><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>i</mi></mrow></msub><mo stretchy=&quot;false&quot;>)</mo></mrow><mrow><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mo stretchy=&quot;false&quot;>(</mo><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>r</mi></mrow></msub><mo stretchy=&quot;false&quot;>)</mo></mrow></mfrac></math>" role="presentation" style="font-size: 101%; position: relative;"><span id="MJXc-Node-130" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-131" class="mjx-mrow"><span id="MJXc-Node-132" class="mjx-mfrac"><span class="mjx-box MJXc-stacked" style="width: 2.113em; padding: 0px 0.12em;"><span class="mjx-numerator" style="font-size: 70.7%; width: 2.988em; top: -1.665em;"><span id="MJXc-Node-133" class="mjx-mrow" style=""><span id="MJXc-Node-134" class="mjx-texatom"><span id="MJXc-Node-135" class="mjx-mrow"><span id="MJXc-Node-136" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-137" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-138" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-139" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"> </span></span><span id="MJXc-Node-140" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-141" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-142" class="mjx-texatom" style=""><span id="MJXc-Node-143" class="mjx-mrow"><span id="MJXc-Node-144" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span></span></span></span></span><span id="MJXc-Node-145" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"> </span></span></span></span><span class="mjx-denominator" style="font-size: 70.7%; width: 2.988em; bottom: -0.958em;"><span id="MJXc-Node-146" class="mjx-mrow" style=""><span id="MJXc-Node-147" class="mjx-texatom"><span id="MJXc-Node-148" class="mjx-mrow"><span id="MJXc-Node-149" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-150" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-151" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-152" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"> </span></span><span id="MJXc-Node-153" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-154" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-155" class="mjx-texatom" style=""><span id="MJXc-Node-156" class="mjx-mrow"><span id="MJXc-Node-157" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">r</span></span></span></span></span></span><span id="MJXc-Node-158" class="mjx-mo"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.436em; padding-bottom: 0.622em;"> </span></span></span></span><span class="mjx-line" style="border-bottom: 1.3px solid; top: -0.289em; width: 2.113em;"></span></span><span class="mjx-vsize" style="height: 1.854em; vertical-align: -0.677em;"></span></span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><mfrac><mrow><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mo stretchy="false">(</mo><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">i</mi></mrow></msub><mo stretchy="false">)</mo></mrow><mrow><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mo stretchy="false">(</mo><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">r</mi></mrow></msub><mo stretchy="false">)</mo></mrow></mfrac></math></span></span> `
        ,

            `
            <span id="MathJax-Element-16-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><mi>m</mi><mo>=</mo><mfrac><mrow><msub><mi>y</mi><mn>2</mn></msub><mo>&amp;#x2212;</mo><msub><mi>y</mi><mn>1</mn></msub></mrow><mrow><msub><mi>x</mi><mn>2</mn></msub><mo>&amp;#x2212;</mo><msub><mi>x</mi><mn>1</mn></msub></mrow></mfrac></math>" role="presentation" style="
            font-family: MJXc-TeX-main-R,MJXc-TeX-main-Rw;
        "><span id="MJXc-Node-159" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-160" class="mjx-mrow" style="
            font-family: MJXc-TeX-main-R,MJXc-TeX-main-Rw;
        "><span id="MJXc-Node-163" class="mjx-mfrac MJXc-space3"><span class="mjx-box MJXc-stacked" style="width: 2.05em; padding: 0px 0.12em;"><span class="mjx-numerator" style="font-size: 70.7%;width: 2.9em;top: -1.453em;font-family: MJXc-TeX-main-R,MJXc-TeX-main-Rw;"><span id="MJXc-Node-164" class="mjx-mrow" style=""><span id="MJXc-Node-165" class="mjx-msubsup"><span class="mjx-base" style="margin-right: -0.006em;"><span id="MJXc-Node-166" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.251em;padding-bottom: 0.498em;padding-right: 0.006em;font-family: MJXc-TeX-main-R,MJXc-TeX-main-Rw;">1</span></span></span></span></span></span><span class="mjx-denominator" style="font-size: 70.7%; width: 2.9em; bottom: -0.683em;"><span id="MJXc-Node-172" class="mjx-mrow" style=""><span id="MJXc-Node-173" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-174" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.251em;padding-bottom: 0.313em;font-family: MJXc-TeX-main-R,MJXc-TeX-main-Rw;">1,49</span></span></span></span></span></span><span class="mjx-line" style="border-bottom: 1.3px solid; top: -0.289em; width: 2.05em;"></span></span><span class="mjx-vsize" style="height: 1.51em; vertical-align: -0.483em;"></span></span></span>
                <span id="MathJax-Element-16-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><mi>m</mi><mo>=</mo><mfrac><mrow><msub><mi>y</mi><mn>2</mn></msub><mo>&amp;#x2212;</mo><msub><mi>y</mi><mn>1</mn></msub></mrow><mrow><msub><mi>x</mi><mn>2</mn></msub><mo>&amp;#x2212;</mo><msub><mi>x</mi><mn>1</mn></msub></mrow></mfrac></math>" role="presentation"><span id="MJXc-Node-159" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-162" class="mjx-mo MJXc-space3"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.065em; padding-bottom: 0.313em;">=</span></span>
                <span id="MJXc-Node-351" class="mjx-msubsup MJXc-space3"><span class="mjx-base"><span id="MJXc-Node-352" class="mjx-mn"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.374em;">0,67</span></span></span></span>
                
                
                </span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><mi>m</mi><mo>=</mo><mfrac><mrow><msub><mi>y</mi><mn>2</mn></msub><mo>−</mo><msub><mi>y</mi><mn>1</mn></msub></mrow><mrow><msub><mi>x</mi><mn>2</mn></msub><mo> − </mo><msub><mi>x</mi><mn>1</mn></msub></mrow></mfrac></math></span></span>   </span></span>`
        
        ,
            `<span id="MathJax-Element-25-Frame" class="mjx-chtml MathJax_CHTML" tabindex="-1" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>i</mi></mrow></msub><mo>=</mo><msup><mn>15</mn><mo>&amp;#x2218;</mo></msup></math>" role="presentation" style="font-size: 101%; position: relative;"><span id="MJXc-Node-343" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-344" class="mjx-mrow"><span id="MJXc-Node-345" class="mjx-msubsup"><span class="mjx-base">
        <span id="MJXc-Node-351" class="mjx-msubsup MJXc-space3"><span class="mjx-base"><span id="MJXc-Node-352" class="mjx-mn"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.374em;">0,67</span></span></span></span>
        
        </span></span><span id="MJXc-Node-350" class="mjx-mo MJXc-space3"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.065em; padding-bottom: 0.313em;">=</span></span><span id="MJXc-Node-351" class="mjx-msubsup MJXc-space3"><span class="mjx-base"><span id="MJXc-Node-352" class="mjx-mn"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.374em;">0,67</span></span></span></span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">i</mi></mrow></msub><mo>=</mo><msup><mn>15</mn><mo>∘</mo></msup></math></span></span>`

        ],
        ['<p>This experiment verifies Snell\'s law:  <span style="font-size: 27px !important;">   <span id="MathJax-Element-22-Frame" class="mjx-chtml MathJax_CHTML" tabindex="0" data-mathml="<math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;><msub><mi>n</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>i</mi></mrow></mrow></msub><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mtext>&amp;#xA0;</mtext><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>i</mi></mrow></msub><mo>=</mo><msub><mi>n</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>r</mi></mrow></mrow></msub><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>s</mi><mi mathvariant=&quot;normal&quot;>i</mi><mi mathvariant=&quot;normal&quot;>n</mi></mrow><mtext>&amp;#xA0;</mtext><msub><mi>&amp;#x03B8;</mi><mrow class=&quot;MJX-TeXAtom-ORD&quot;><mi mathvariant=&quot;normal&quot;>r</mi></mrow></msub></math>" role="presentation" style="font-size: 101%; position: relative;"><span id="MJXc-Node-272" class="mjx-math" aria-hidden="true"><span id="MJXc-Node-273" class="mjx-mrow"><span id="MJXc-Node-274" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-275" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.251em; padding-bottom: 0.313em;">n</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-276" class="mjx-texatom" style=""><span id="MJXc-Node-277" class="mjx-mrow"><span id="MJXc-Node-278" class="mjx-texatom"><span id="MJXc-Node-279" class="mjx-mrow"><span id="MJXc-Node-280" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span></span></span></span></span></span></span><span id="MJXc-Node-281" class="mjx-texatom"><span id="MJXc-Node-282" class="mjx-mrow"><span id="MJXc-Node-283" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;"> s</span></span><span id="MJXc-Node-284" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-285" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-286" class="mjx-mtext"><span class="mjx-char MJXc-TeX-main-R" style="margin-top: -0.306em; padding-bottom: 0.313em;">&nbsp;</span></span><span id="MJXc-Node-287" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-288" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-289" class="mjx-texatom" style=""><span id="MJXc-Node-290" class="mjx-mrow"><span id="MJXc-Node-291" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span></span></span></span></span><span id="MJXc-Node-292" class="mjx-mo MJXc-space3"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.065em; padding-bottom: 0.313em;">=</span></span><span id="MJXc-Node-293" class="mjx-msubsup MJXc-space3"><span class="mjx-base"><span id="MJXc-Node-294" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.251em; padding-bottom: 0.313em;">n</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-295" class="mjx-texatom" style=""><span id="MJXc-Node-296" class="mjx-mrow"><span id="MJXc-Node-297" class="mjx-texatom"><span id="MJXc-Node-298" class="mjx-mrow"><span id="MJXc-Node-299" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">r</span></span></span></span></span></span></span></span><span id="MJXc-Node-300" class="mjx-texatom"><span id="MJXc-Node-301" class="mjx-mrow"><span id="MJXc-Node-302" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.189em; padding-bottom: 0.374em;">s</span></span><span id="MJXc-Node-303" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.374em; padding-bottom: 0.313em;">i</span></span><span id="MJXc-Node-304" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">n</span></span></span></span><span id="MJXc-Node-305" class="mjx-mtext"><span class="mjx-char MJXc-TeX-main-R" style="margin-top: -0.306em; padding-bottom: 0.313em;">&nbsp;</span></span><span id="MJXc-Node-306" class="mjx-msubsup"><span class="mjx-base"><span id="MJXc-Node-307" class="mjx-mi"><span class="mjx-char MJXc-TeX-math-I" style="padding-top: 0.498em; padding-bottom: 0.313em;">θ</span></span></span><span class="mjx-sub" style="font-size: 70.7%; vertical-align: -0.212em; padding-right: 0.071em;"><span id="MJXc-Node-308" class="mjx-texatom" style=""><span id="MJXc-Node-309" class="mjx-mrow"><span id="MJXc-Node-310" class="mjx-mi"><span class="mjx-char MJXc-TeX-main-R" style="padding-top: 0.127em; padding-bottom: 0.313em;">r</span></span></span></span></span></span></span></span><span class="MJX_Assistive_MathML" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML"><msub><mi>n</mi><mrow class="MJX-TeXAtom-ORD"><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">i</mi></mrow></mrow></msub><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mtext>&nbsp;</mtext><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">i</mi></mrow></msub><mo>=</mo><msub><mi>n</mi><mrow class="MJX-TeXAtom-ORD"><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">r</mi></mrow></mrow></msub><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">s</mi><mi mathvariant="normal">i</mi><mi mathvariant="normal">n</mi></mrow><mtext>&nbsp;</mtext><msub><mi>θ</mi><mrow class="MJX-TeXAtom-ORD"><mi mathvariant="normal">r</mi></mrow></msub></math></span></span> </span></p>'],
    ]
    var pageContent = content[pageIndex];

    setTimeout(function () {

    }, 500)
    container.innerHTML = ''

    pageContent.forEach((content, index) => {

        var item = document.createElement('div');
        item.classList.add('content__line');

        if (pageIndex == 2) {
            item.classList.add('center');
        }
        item.innerHTML = content;
        container.appendChild(item)
        setTimeout(function () {
            item.style.left = '0';
        }, 1000 * index)
    })


}