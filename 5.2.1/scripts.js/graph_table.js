;+function(){

    var topItemPosition = 0.75

    var graph_container = document.querySelector('.graphHiddenLines');
    for(var i =0;i<16;i++){
        var row = document.createElement('div');

        row.setAttribute('data-row-position',topItemPosition)
        var rightItemPosition = '0.00';
        for(var j = 0;j<21;j++){
            
            var elem = document.createElement('div');
            elem.classList.add('graph_circle')
            elem.setAttribute('data-item-position',rightItemPosition)
            row.appendChild(elem);
            rightItemPosition = (((rightItemPosition*100)+5)/100).toFixed(2)
        }

        topItemPosition = (((topItemPosition*100)-5)/100).toFixed(2)
        graph_container.appendChild(row);
    }
}()