var zoomScale = 1;
var dataDb = {
    '450': {
        '100':{
            'h2': [0.07, 0.12, 0.17, 0.22, 0.06, 0.05, 0.04],
            'n2': [0.40000, 0.39295, 0.38516, 0.37678, 0.40129, 0.40252, 0.40373],
            'hn2': [0.0046732, 0.0103963, 0.0173552, 0.0252706, 0.0037145, 0.0028300, 0.0020280]
        },
        '150':{
            'h2': [0.06, 0.11, 0.16, 0.21, 0.05, 0.04, 0.03],
            'n2': [0.75000, 0.74190, 0.73268, 0.72258, 0.75145, 0.75283, 0.75413],
            'hn2': [0.0050780, 0.0125372, 0.0218562, 0.0326369, 0.0038667, 0.0027693, 0.0018003]
        },
        '200':{
            'h2': [0.05, 0.1, 0.15, 0.2, 0.04, 0.03, 0.02],
            'n2': [1.359, 1.34969, 1.33873, 1.32647, 1.36060, 1.36211, 1.36350],
            'hn2': [0.0052000, 0.0146573, 0.0268177, 0.0410991, 0.0037230, 0.0024195, 0.0013177]
        },
        '250':{
            'h2': [0.04, 0.09, 0.14, 0.19, 0.03, 0.02, 0.01],
            'n2': [3.00000, 2.98848, 2.97433, 2.95814, 3.00189, 3.00360, 3.00508],
            'hn2': [0.0055283, 0.0186221, 0.0360434, 0.0568301, 0.0035918, 0.0019557, 0.0006916]
        },
        '300':{
            'h2': [0.03, 0.08, 0.13, 0.18, 0.02, 0.015, 0.008],
            'n2': [8.00000, 7.98450, 7.96438, 7.94072, 8.00234, 8.00337, 8.00464],
            'hn2': [0.0058636, 0.0255092, 0.0527752, 0.0858573, 0.0031922, 0.0020735, 0.0008077]
        },
        'kc': 0.159
    },
    '500': {
        '100':{
            'h2': [0.073, 0.123, 0.173, 0.223, 0.063, 0.053, 0.043],
            'n2': [0.40079, 0.39507, 0.38889, 0.38235, 0.40186, 0.40291, 0.40393],
            'hn2': [0.0030187, 0.0065550, 0.0108482, 0.0157421, 0.0024234, 0.0018724, 0.0013700]
        },
        '150':{
            'h2': [0.063, 0.113, 0.163, 0.213, 0.053, 0.043, 0.033],
            'n2': [0.75086, 0.74449, 0.73743, 0.72980, 0.75203, 0.75316, 0.75425],
            'hn2': [0.0033126, 0.0079236, 0.0136621, 0.0203024, 0.0025580, 0.0018708, 0.0012586]
        },
        '200':{
            'h2': [0.053, 0.103, 0.153, 0.203, 0.043, 0.033, 0.023],
            'n2': [1.36000, 1.35287, 1.34476, 1.33586, 1.36128, 1.36249, 1.36363],
            'hn2': [0.0034400, 0.0092952, 0.0167778, 0.0255564, 0.0025151, 0.0016917, 0.0009847]
        },
        '250':{
            'h2': [0.043, 0.093, 0.143, 0.193, 0.033, 0.023, 0.013],
            'n2': [3.00088, 2.99237, 2.98229, 2.97098, 3.00233, 3.00368, 3.00515],
            'hn2': [0.0037342, 0.0118606, 0.0225764, 0.0353314, 0.0025112, 0.0014615, 0.0006212]
        },
        '300':{
            'h2': [0.033, 0.083, 0.133, 0.183, 0.023, 0.0153, 0.0083],
            'n2': [8.00086, 7.98985, 7.97610, 7.96021, 8.00261, 8.00382, 8.00480],
            'hn2': [0.0040993, 0.0163404, 0.0331168, 0.0533967, 0.0023855, 0.0012944, 0.0005172]
        },
        'kc': 0.058
    },
    '400': {
        '100':{
            'h2': [0.066, 0.116, 0.166, 0.216, 0.056, 0.046, 0.036],
            'n2': [0.39853, 0.38897, 0.37813, 0.36632, 0.40024, 0.40187, 0.40340],
            'hn2': [0.0075671, 0.0174194, 0.0294013, 0.0429536, 0.0059269, 0.0044214, 0.0030670]
        },
        '150':{
            'h2': [0.056, 0.106, 0.156, 0.206, 0.046, 0.036, 0.026],
            'n2': [0.74845, 0.73708, 0.72373, 0.70886, 0.75041, 0.75225, 0.75395],
            'hn2': [0.0081049, 0.0209460, 0.0370561, 0.0556504, 0.0060419, 0.0041881, 0.0025735]
        },
        '200':{
            'h2': [0.046, 0.006, 0.146, 0.196, 0.036, 0.026, 0.016],
            'n2': [1.35750, 1.36506, 1.32770, 1.30910, 1.35973, 1.36176, 1.36356],
            'hn2': [0.0081263, 0.0003839, 0.0454428, 0.0701871, 0.0056307, 0.0034586, 0.0016707]
        },
        '250':{
            'h2': [0.036, 0.086, 0.136, 0.186, 0.026, 0.016, 0.006],
            'n2': [2.99857, 2.98138, 2.95947, 2.93395, 3.00124, 3.00357, 3.00544],
            'hn2': [0.0083617, 0.0307852, 0.0609960, 0.0971364, 0.0051345, 0.0024796, 0.0005696]
        },
        '300':{
            'h2': [0.026, 0.076, 0.126, 0.176, 0.016, 0.0146, 0.0076],
            'n2': [7.99876, 7.97488, 7.94260, 7.90403, 8.00210, 8.00252, 8.00436],
            'hn2': [0.0083822, 0.0418281, 0.0891094, 0.1467505, 0.0040473, 0.0035280, 0.0013252]
        },
        'kc': 0.500
    },
}


let Page = function () {
	this.page = 1;
    this.pageHelpText = '';
    this.slideMove = false;
    this.slideMoveIndex;
	this.AllslideData = ['pressure', 'temperature'];
    this.pressure = [100, 150, 200, 250, 300];
    this.temperature = [400, 450, 500];
    this.switchValueH2 = 0.05;
    this.switchValueN2 = 1.359;
    this.switchValueHN = 0.0052;
    this.currentTemperature = 450;
    this.currentPressure = 200;
    this.clickEvent = false;
    this.switchIndex = 0;
    this.bottomFormula1Timer = 0;
    this.bottomFormula2Timer = 0;
    this.topFormulaTimer = 0;
    this.equalTimer = 0;
}

Page.prototype = {
    init: function () {
        this.cleareBackgroundContent();
        this.addBackgroundContent();
        this.addFormulaBlock();
        this.addFormula();
        this.setSlideValue(2, this.currentTemperature);
        this.setSlideValue(1, this.currentPressure);
        this.addTopFormula();
        this.addBottomFormula();
        this.checkActive();
    },
    addSlides: function () {
    	var self = this;
    	for (var i = 0; i < 2; i++) {
    		self.addSlideBlock(i);
    		self.addEventSlide(i);
    		$('.slide_index_'+(i+1)+' .slide-button').css({left:'10px'});
    	}
    	self.addEventMouseUpSlide();
    },
    addSlideBlock: function (i) {
    	var self = this;
		var template = '<div class="slide-block pointer-cursor slide_index_'+(i+1)+'">\
							<div class="slide-text"></div>\
			    			<div class="slide-button"></div>\
			    			<div class="slide-fill"></div>\
                            <div class="slide-scall"></div>\
			    		</div>';

		self.addBottomBlock(template);
		var slideScaleValue = self[self.AllslideData[i]];
		self.addScale('.slide_index_'+(i+1), slideScaleValue);
    },
    addBottomBlock: function (template) {
    	var self = this;
        var content = $('.bottom-block');
        if(content.length > 0) {
            content.append(template);
            return true;
        } else {
            console.error('Not found div.right-block block');
            return false;
        }
    },
    addTopBlock: function (template) {
    	var self = this;
        var content = $('.top-block');
        if(content.length > 0) {
            content.append(template);
            return true;
        } else {
            console.error('Not found div.right-block block');
            return false;
        }
    },
    addFormulaBlock: function (){
        this.addTopBlock('<p id="formulatext"></p>');
    },
    addScale: function (elem, data) {
    	if(data && data.length && data.length == 0) return; // if empty  data
    	var active = 'active';
        var elWidth = $(elem+' .slide-text').width();
    	var mashtab = (elWidth-40)/(data.length-1);
    	var template = "";
        var slide_index = elem.split('_')[2];
    	for(var i = 0; i < data.length; i++){
			var left = 10+(mashtab * i);
			if (i === 3 && slide_index == 3) left = (mashtab * i)-10;
            var val = data[i]
            if(val == 1000) val = '1 000';
    		template+='<span class="scale-text '+active+'" style="left: ' + left + 'px">'+val+'</span>';
            active = '';
        }
    	$(elem+' .slide-text').html(template);
    },
    addEventSlide: function (i) {
    	var self = this;
    	$(document).off('mousedown touchstart', '.slide_index_'+(i+1)+' .slide-button').on('mousedown touchstart', '.slide_index_'+(i+1)+' .slide-button', function (e) {
            self.clickEvent = true;
    		self.slideMove = true;
    		self.slideMoveIndex = (i+1);
        })
    	$(document).off('mousemove touchmove', '.slide-block.slide_index_'+(i+1)).on('mousemove touchmove', '.slide-block.slide_index_'+(i+1), function (e) {
    		if(self.slideMoveIndex == (i+1)){
	    		var clientX = e.originalEvent.touches ? e.originalEvent.touches[0].clientX : e.originalEvent.clientX;
                var r = $('.slide-block.slide_index_'+(i+1))[0].getBoundingClientRect();
                var newLeft = clientX/zoomScale - r.left / zoomScale;
	    		var blockWidth = $('.slide-block.slide_index_'+(i+1)).width();
	    		if(newLeft > blockWidth-20){
	    			newLeft = blockWidth-15;
	    		} else if (newLeft < 5){
	    			newLeft = 0;
	    		}
	    		if(self.slideMove) {
	    			$('.slide_index_'+(i+1)+' .slide-button').css({left: newLeft+'px'});
	    			$('.slide_index_'+(i+1)+' .slide-fill').css({width: newLeft+'px'});
	    			self.slideNewLeft= newLeft;
				}
    		}
    	})
    	$(document).off('click', '.slide-block.slide_index_'+(i+1)).on('click', '.slide-block.slide_index_'+(i+1), function (e) {
            if(self.clickEvent){
                self.clickEvent = false;
                return;
            }
            self.slideMoveIndex = i+1;
    		var left = parseInt(e.offsetX)
    		if($(e.target).hasClass('slide-button')) {
    			left = parseInt($(e.target).css('left').replace('px', ''));
    		}
    		self.slideMove = false;
    		var newLeft = self.getCorectPosition(left);
			$('.slide_index_'+self.slideMoveIndex+' .slide-button').css({left: newLeft+'px'});
			// red side
			$('.slide_index_'+self.slideMoveIndex+' .slide-fill').css({width: newLeft+'px'});
			self.changeSlideEvent();
    		self.slideMoveIndex = 0;
    	})
    },
    getSlideValue: function (dataIndex) {
        var elem = '.slide_index_'+(dataIndex);
        var slideLeft = parseInt($(elem + ' .slide-button').css('left').replace('px', ''));
        var data = this[this.AllslideData[dataIndex-1]];
        var elWidth = $(elem+' .slide-text').width();
        var mashtab = (elWidth-40)/(data.length-1);
        var index = (slideLeft-10)/mashtab;
        return data[index.toFixed()];
    },
    setSlideValue: function (slideIndex, value){
        var self = this;
        self.slideMoveIndex  = slideIndex;
        var elem ='.slide_index_' + slideIndex;
        var elWidth = $(elem+' .slide-text').width();
        var data = this[this.AllslideData[slideIndex-1]];
        var dataScale = data[data.length - 1] - data[0];
        var mashtab =  (elWidth-40)/dataScale;
        var diferentValue = value - data[0];
        var left = self.getCorectPosition(mashtab * diferentValue);
        $(elem + ' .slide-button').css('left',left + 'px');
        $(elem + ' .slide-fill').css('width',left + 'px');
        $(elem + ' .slide-text span').removeClass('active scale-disable');
    },
    changeSlideEvent: function () {
        // console.log(this.getSlideValue(1), this.getSlideValue(2));
        this.currentTemperature = this.getSlideValue(2);
        this.currentPressure = this.getSlideValue(1);
        this.calculation();
        this.setEqualValue();
    },
    setEqualValue: function () {
        var kc = dataDb[this.currentTemperature.toString()].kc;
        var value = kc.toFixed(3).replace('.', ',');
        document.querySelector('.equal-value').innerHTML = '';
        $(".equal-value").css("visibility","hidden");
        clearTimeout(this.equalTimer);
        this.equalTimer = setTimeout(function () {
            MathJax.HTML.addElement(document.querySelector('.equal-value'), "span", {id: 'equal_value'}, ['$$ ' + value + ' $$']);
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'equal_value'],
            function () {
                if($('#equal_value span').length > 0) {
                    $(".equal-value").css("visibility",""); // may have to be "visible" rather than ""
                }
            });
        })
       
    },
    getMaxValueIndex: function (value, arr) {
        arr.sort((a, b) => a - b);
        var index = arr.indexOf(value) + 1;
        if(index > 6) return arr[6];
        else return arr[index];
    },
    getMinValueIndex: function (value, arr) {
        arr.sort((a, b) => a - b);
        var index = arr.indexOf(value) - 1;
        if(index < 0) return arr[0];
        else return arr[index];
    },
    removeActiveUp: function () {
        $('.switch .up').removeClass('active');
    },
    addActiveUp: function () {
        $('.switch .up').addClass('active');
    },
    addActiveDown: function () {
        $('.switch .down').addClass('active');
    },
    removeActiveDown: function () {
        $('.switch .down').removeClass('active');
    },
    checkActive: function () {
        var hn2 = dataDb[this.currentTemperature.toString()][this.currentPressure.toString()].hn2.slice().sort((a, b) => a - b).indexOf(this.switchValueHN);
        var n2 = dataDb[this.currentTemperature.toString()][this.currentPressure.toString()].n2.slice().sort((a, b) => a - b).indexOf(this.switchValueN2);
        var h2 = dataDb[this.currentTemperature.toString()][this.currentPressure.toString()].h2.slice().sort((a, b) => a - b).indexOf(this.switchValueH2);
        
        // console.log('hn2=',hn2, 'n2 =', n2, 'h2=', h2 )
        $('.switch .up').addClass('active');
        $('.switch .down').addClass('active');

        if(hn2 > 5) {
           $('.switch_1 .up').removeClass('active');
        } else if(hn2 < 1){
            $('.switch_1 .down').removeClass('active');
        }

        if(h2 > 5) {
           $('.switch_3 .up').removeClass('active');
        } else if(h2 < 1){
            $('.switch_3 .down').removeClass('active');
        }

        if(n2 > 5) {
           $('.switch_2 .up').removeClass('active');
        } else if(n2 < 1){
            $('.switch_2 .down').removeClass('active');
        }
    },
    topFormulaUp: function (){
        var arr = dataDb[this.currentTemperature.toString()][this.currentPressure.toString()].hn2;
        var arr1 = arr.slice();
        this.switchValueHN = this.getMaxValueIndex(this.switchValueHN, arr1, arr);
        this.switchIndex = arr.indexOf(this.switchValueHN)
    },
    topFormuladown: function (){
        var arr = dataDb[this.currentTemperature.toString()][this.currentPressure.toString()].hn2;
        var arr1 = arr.slice();
        this.switchValueHN = this.getMinValueIndex(this.switchValueHN, arr1, arr);
        this.switchIndex = arr.indexOf(this.switchValueHN)
    },
    bottomFormula1Up: function () {
        var arr = dataDb[this.currentTemperature.toString()][this.currentPressure.toString()].n2;
        var arr1 = arr.slice();
        this.switchValueN2 = this.getMaxValueIndex(this.switchValueN2, arr1, arr);
        this.switchIndex = arr.indexOf(this.switchValueN2)
    },
    bottomFormula1Down: function () {
        var arr = dataDb[this.currentTemperature.toString()][this.currentPressure.toString()].n2;
        var arr1 = arr.slice();
        this.switchValueN2 = this.getMinValueIndex(this.switchValueN2, arr1, arr);
        this.switchIndex = arr.indexOf(this.switchValueN2)
    },
    bottomFormula2Up: function () {
        var arr = dataDb[this.currentTemperature.toString()][this.currentPressure.toString()].h2;
        var arr1 = arr.slice();
        this.switchValueH2 = this.getMaxValueIndex(this.switchValueH2, arr1, arr);
        this.switchIndex = arr.indexOf(this.switchValueH2)
    },
    bottomFormula2Down: function () {
        var arr = dataDb[this.currentTemperature.toString()][this.currentPressure.toString()].h2;
        var arr1 = arr.slice();
        this.switchValueH2 = this.getMinValueIndex(this.switchValueH2, arr1, arr);
        this.switchIndex = arr.indexOf(this.switchValueH2)
    },
    calculation: function () {
        this.addActiveUp();
        this.addActiveDown();
        this.setTopFormula();
        this.setBottomFormula1();
        this.setBottomFormula2();
    },
    setTopFormula: function () {
        var hn = dataDb[this.currentTemperature.toString()][this.currentPressure.toString()].hn2[this.switchIndex]
        this.switchValueHN = hn;
        var value = hn.toFixed(4).replace('.', ',');
        document.getElementById('fTop').innerHTML = '';
        $("#fTop").css("visibility","hidden");
        clearTimeout(this.topFormulaTimer);
        this.topFormulaTimer = setTimeout(function () {
            MathJax.HTML.addElement(document.getElementById('fTop'), "span", {id: 'formulaTop'}, ['$$ [\\color{#019A67}{'+value+'}]^{\\color{#019A67}{2}} $$']);
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'formulaTop'],
                function () {
                    if($("#fTop > span > span").length > 0) {
                        $("#fTop").css("visibility",""); // may have to be "visible" rather than ""
                    }
                });
        })
    },
    setBottomFormula1: function () {
        var n2 = dataDb[this.currentTemperature.toString()][this.currentPressure.toString()].n2[this.switchIndex]
        this.switchValueN2 = n2;
        var value = n2.toFixed(4).replace('.', ',');
        document.getElementById('fBottom1').innerHTML = '';
        $("#fBottom1").css("visibility","hidden");
        clearTimeout(this.bottomFormula1Timer);
        this.bottomFormula1Timer = setTimeout(function () {
            MathJax.HTML.addElement(document.getElementById('fBottom1'), "span", {id: 'formulabottom1'}, ['`[\\color{#3467CD}{'+value+'}] `']);
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'formulabottom1'],
                function () {
                    if($("#fBottom1 > span > span").length > 0) {
                        $("#fBottom1").css("visibility",""); // may have to be "visible" rather than ""
                    }
                });
        })
    },
    setBottomFormula2: function () {
        var h2 = dataDb[this.currentTemperature.toString()][this.currentPressure.toString()].h2[this.switchIndex]
        this.switchValueH2 = h2;
        var value = h2.toFixed(4).replace('.', ',');
        document.getElementById('fBottom2').innerHTML = '';
        $("#fBottom2").css("visibility","hidden");
        clearTimeout(this.bottomFormula2Timer);
        this.bottomFormula2Timer = setTimeout(function () {
            MathJax.HTML.addElement(document.getElementById('fBottom2'), "span", {id: 'formulabottom2'}, ['` [\\color{#676767}{'+value+'}]^{\\color{#676767}{3}} `']);
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'formulabottom2'],
                function () {
                    if($("#fBottom2 > span > span").length > 0) {
                        $("#fBottom2").css("visibility",""); // may have to be "visible" rather than ""
                    }
                });
        })
    },
    addTopFormula: function () {
        var template = '<div class="f-top-1"><p id="fTop"></p></div>';
        $('#f1').html(template);
        this.addSwitch('.f-top-1', 1);
        this.addEventSwitch(1, this.topFormulaUp.bind(this), this.topFormuladown.bind(this));
        $("#fTop").css("visibility","hidden");
        clearTimeout(this.topFormulaTimer);
        this.topFormulaTimer = setTimeout(function () {
            MathJax.HTML.addElement(document.getElementById('fTop'), "span", {id: 'formulaTop'}, ['$$ [\\color{#019A67}{0,0052}]^{\\color{#019A67}{2}} $$']);
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'formulaTop'], function () {
                if($("#fTop > span > span").length > 0) {
                    $("#fTop").css("visibility","");
                }
            });
        });
    },
    addBottomFormula: function () {
        var template = '<div class="f-bottom-1"><p id="fBottom1"></p></div>\
                        <div class="f-bottom-2"><p id="fBottom2"></p></div>';
        $('#f2').html(template);

        this.addSwitch('.f-bottom-1', 2);
        this.addEventSwitch(2, this.bottomFormula1Up.bind(this), this.bottomFormula1Down.bind(this));
        $("#fBottom1").css("visibility","hidden");
        clearTimeout(this.bottomFormula1Timer);
        this.bottomFormula1Timer = setTimeout(function () {
            MathJax.HTML.addElement(document.getElementById('fBottom1'), "span", {id: 'formulabottom1'}, ['`[\\color{#3467CD}{1,3590}] `']);
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'formulabottom1'], function () {
                if($("#fBottom1 > span > span").length > 0) {
                    $("#fBottom1").css("visibility","");
                }
            });
        })

        this.addSwitch('.f-bottom-2', 3);
        this.addEventSwitch(3, this.bottomFormula2Up.bind(this), this.bottomFormula2Down.bind(this));
        $("#fBottom2").css("visibility","hidden");
        clearTimeout(this.bottomFormula2Timer);
        this.bottomFormula2Timer = setTimeout(function () {
            MathJax.HTML.addElement(document.getElementById('fBottom2'), "span", {id: 'formulabottom2'}, ['` [\\color{#676767}{0,0500}]^{\\color{#676767}{3}} `']);
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'formulabottom2'], function () {
                if($("#fBottom2 > span > span").length > 0) {
                    $("#fBottom2").css("visibility","");
                }
            });
        });
    },
    addSwitch: function (elm, index) {
        var template = '<div class="switch switch_'+index+' ">\
                            <div class="up active"></div>\
                            <div class="down active"></div>\
                        </div>';
        $(elm).append(template);
    },
    addEventSwitch: function (index, f_up = function (){}, f_down = function (){}) {
        var self = this;
        $(document).off('click', '.switch_'+index+' .up.active').on('click', '.switch_'+index+' .up.active', function(){
            f_up();
            self.calculation();
            self.checkActive();
            // console.log("up")
        });
        $(document).off('click', '.switch_'+index+' .down.active').on('click', '.switch_'+index+' .down.active', function(){
            f_down();
            self.calculation();
            self.checkActive();
            // console.log("down")
        });
    },
    addFormula: function () {
        // var formula = ` \\overrightarrow{\\mathrm{f}}}{[1,3262]^{1} \\stackrel{\\Delta}{\\mathrm{F}}[0,2132]^{3} \\frac{\\mathrm{a}}{\\mathrm{v}}}=0,159  $$`;
        var formula = '$$  \\color{#FF3434}{K_{c}}=\\frac{\\left[\\color{#019A67}{\\mathrm{NH}_{3}}\\right]^{\\color{#019A67}{2}}}{\\left[\\color{#3467CD}{\\mathrm{N}_{2}}\\right]\\left[\\color{#676767}{\\mathrm{H}_{2}}\\right]^{\\color{#676767}{3}}} = $$'
        var fBlock = document.getElementById('formulatext');
        setTimeout(function () {
            MathJax.HTML.addElement(fBlock, "span", {id: 'formula'}, [formula]);
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'formula'], function () {
                $('#formula').css({visibility: 'visible'});
            });
        })
        var template = '<div class="f-second">\
                            <div id="f1"></div>\
                            <div class="line"></div>\
                            <div id="f2"></div>\
                        </div>'
        $(fBlock).append(template)
        MathJax.HTML.addElement(fBlock, "span", {id: 'formula_equal'}, ['$$ = $$']);
        $('#formula_equal').css({visibility: 'hidden'}); 
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'formula_equal'], function () {
            $('#formula_equal').css({visibility: 'visible'}); 
        });

        $(fBlock).append('<div class="equal-value" style="visibility: hidden;"></div>');
        setTimeout(function () {
            MathJax.HTML.addElement(document.querySelector('.equal-value'), "span", {id: 'equal_value'}, ['$$ 0,159 $$']);
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'equal_value'], function () {
                if($('#equal_value span').length > 0) {
                    $('#equal_value').css({visibility: 'visible'});
                }
            });
        })
    },
    addEventMouseUpSlide: function () {
    	var self = this;
    	$(document).off('mouseup touchend', 'body').on('mouseup touchend', 'body', function (e) {
             if(!self.slideMove) return
            var newLeft = self.getCorectPosition();
			$('.slide_index_'+self.slideMoveIndex+' .slide-button').css({left: newLeft+'px'});
			self.changeSlideEvent();
            self.slideMove = false;
    	})
	},
    addMainBlock: function (contents) {
        let self = this;
        let content = $('#contentImg');
        if(content.length > 0) {
            self.mainContent = content;
            content.append(contents);
            return true;
        } else {
            console.error('Not found div#contentImg block');
            return false;
        }
    },
    addBackgroundContent: function () {
        let self = this;
        template = '<div class="top-block"></div>\
                    <div class="bottom-block"></div>'
        if(self.addMainBlock(template)) {
        	self.addSlides();
        }
    },
    cleareBackgroundContent: function () {
		let self = this;
		let content = $('#contentImg');
		if(content.length > 0) {
			$('#contentImg').html('');
		}else {
			console.error('Not found div#contentImg block');
		}
    },
	addMathText: function (pageHelpText, helpList) {
		let ol = document.createElement('ol');
		ol.className = 'help-list';
		$(pageHelpText).html(ol);
		for(let i = 0; i< helpList.length; i++) {
			let id = 'li'+i+(new Date().getTime());
			MathJax.HTML.addElement(ol, "li", {id: id}, [helpList[i]]);
			MathJax.Hub.Queue(["Typeset", MathJax.Hub, id]);
		}
	},
    getCorectPosition: function (leftValue = null, f = function (){}) {
        var self = this;
        var elem ='.slide_index_'+self.slideMoveIndex;
        var data = self[self.AllslideData[self.slideMoveIndex-1]];
        var left = parseInt($(elem + ' .slide-button').css('left').replace('px', ''));
        if(leftValue){
            left = leftValue
        }
        var newLeft = 0;
        var elWidth = $(elem+' .slide-text').width();
        var mashtab = (elWidth-40)/(data.length-1);
        $(elem + ' .slide-text span').removeClass('active')
        $(elem + ' span.min-tooltip').removeClass('active')
        newLeft = Math.round(left/mashtab) * mashtab;
        $(elem + ' .slide-text span:nth-child('+(newLeft/mashtab+1)+')').addClass('active')
        f();
        return newLeft+10;
    },


	// set and get header, description, help list
	setInstructionHelp: function (select) {
		let self = this;
		let helpList = self.getInstructionList();
		$(select || self.pageHelpText).html('');
		self.pageHelpText = select || self.pageHelpText;
		self.addMathText(self.pageHelpText, helpList);
		$(select || self.pageHelpText).parent().removeClass('help_list_' + self.page - 1).addClass('help_list_' + self.page);
		
	},
	getInstructionList: function () {
		let self = this;
		let helpList = "";
		switch (self.page){
			case 1:
			helpList = ['A calculation to determine the value of `K_{c}` for the Haber process, executed at 450 °C and 200 times atmospheric pressure, is shown on the screen.',
						'Use the arrows next to any reagent to increase or decrease its equilibrium concentration. Observe any changes in the calculation.',
						'Use the pressure slider to change the pressure at which the reaction reaches equilibrium. Observe any changes in the calculation.',
						'Use the temperature slider to change the temperature at which the reaction reaches equilibrium. Observe any changes in the calculation.',
						'Remember that the addition or removal of a catalyst will not have any effect on this calculation. It only affects the rate of the forward and reverse reactions.'];
			break;
		}
		return helpList;
	},
	getMainHeader: function () {
		let self = this;
		let header = "Investigate the value of the equilibrium constant";
		return header;
	}
}