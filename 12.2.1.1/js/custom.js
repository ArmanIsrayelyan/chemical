let g = -9.8;
const meterToPxRatio = 160;
var otherDeltaT;
const distances = {
  "1.5": 90,
  "2": 124,
  "2.5": 179,
  "3": 205,
  "3.5": 245,
  "4": 281
};
var direction = 1;
$(document).ready(function() {
  mainInit(direction);
  
//   $('#info-div-text').html(`<span class='blockSpan forceFirst'> $$ { \\begin{aligned}
// \\boldsymbol{v}_{\\mathrm{f}} &=\\boldsymbol{v}_{\\mathrm{i}}+\\boldsymbol{g} \\Delta t \\
// \\Delta \\boldsymbol{y} &=\\boldsymbol{v}_{\\mathrm{i}} \\Delta t+\\frac{1}{2} \\boldsymbol{g} \\Delta t^{2} \\
// \\Delta \\boldsymbol{y} &=\\left(\\frac{\\boldsymbol{v}_{\\mathrm{i}}+\\boldsymbol{v}_{\\mathrm{f}}}{2}\\right) \\Delta t \\
// \\boldsymbol{v}_{\\mathrm{f}}^{2} &=\\boldsymbol{v}_{\\mathrm{i}}^{2}+2 \\boldsymbol{g} \\Delta \\boldsymbol{y}
// \\end{aligned}} $$ </span>
//                   `);
//     MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output], function (){
//       // $('.work-done-block').css({visibility: 'visible'});
//     });
  $(".output__info").on("click", function() {
    openInfoWindow(".info-box");
  });
  $(".info-box__closeIcon").on("click", function() {
    closeInfoWindow(".info-box");
  });

  $(".output__formula").on("click", function() {
    openInfoWindow(".formula-box");
  });
  $(".formula-box__closeIcon").on("click", function() {
    closeInfoWindow(".formula-box");
  });
  $("#content")
    .off("click", "#resetBtn")
    .on("click", "#resetBtn", reloadPage);
});
function reloadPage() {
  location.reload();
}
function mainInit(direction) {
  $("#upDown").on("click", function() {
    direction *= -1;
    changeDirection($(this), direction);
  });
  sliders(direction);
}

function changeDirection(el, direction) {
  direction = -direction;
  $(".elevator__box--js").data(
    "direction",
    -1 * $(".elevator__box--js").data("direction")
  );
  var directionSign = "";
  $(".directionArrow").toggleClass("down");
  if (el.prop("checked")) {
    // direction = -1;
    directionSign = "-";
    addMinusSign(directionSign);
    $(".elevator__box--js").data("upwards", false);
  } else {
    // direction = 1;
    removeMinusSign();
    //   directionSign = "";
    $(".elevator__box--js").data("upwards", true);
  }
  addDirection($(".elevator__box--js").data("direction"));
}

// Change data-attr values
function addDirection(dir) {
  $(".elevator__box--js").data(
    "height",
    -1 * +$(".elevator__box--js").data("height")
  );
  $(".elevator__box--js").data(
    "speed",
    -1 * +$(".elevator__box--js").data("speed")
  );
}

// Sliders
function sliders(dir) {
  initialHeightSlide(dir);
  initialVelocity(dir);
  ballMass(dir);
}
function initialHeightSlide() {
  $(".choose__initialHeight input").off('input').on('input', function(e) {
      let slideDirection = +$(".elevator__box--js").data("direction");
      $(".elevator__box--js").attr("data-elevatorposition", +e.target.value);
      $(".elevator__box--js").data("height", slideDirection * +e.target.value);
      $(".value__height").text(
        Math.abs($(".elevator__box--js").data("height"))
          .toString()
          .replace(".", ",")
      );
    });
  $(".option__carret").val("$" + $("choose-degree input").val());
}
function initialVelocity() {
  $(".choose__initialVelocity input").off('input').on('input', function(e) {
      let slideDirection = +$(".elevator__box--js").data("direction");
      $(".elevator__box--js").data("speed", slideDirection * + e.target.value);
      $(".value__speed").text(
        Math.abs($(".elevator__box--js").data("speed"))
          .toString()
          .replace(".", ",")
      );
    });
  $(".option__carret").val("$" + $("choose-degree input").val());
}
function ballMass() {
  $(".choose__ballMass input").off('input').on('input', function(e) {
      $(".elevator__box--js").data("ballmass", +e.target.value);
      $(".value__ballMass").text(
        $(".elevator__box--js")
          .data("ballmass")
          .toString()
          .replace(".", ",")
      );
    })
  $(".option__carret").val("$" + $("choose-degree input").val());
}

// Main animation
function ballAnimation(upwards) {
  const ELEVATOR = $(".elevator__box--js");
  const HEIGHT_FROM_GROUND = Math.abs(ELEVATOR.data("height"));
  var initialVlocityAbs = Math.abs(ELEVATOR.data("speed"));
  var initialHeightAbs = Math.abs(ELEVATOR.data("height"));
  var ballInitialPosition = -117;
  var groundHeight = distanceFromGround(distances, HEIGHT_FROM_GROUND);

  // Ball
  var ballDown = KUTE.fromTo(
    ".elevator__ball",
    { translateY: ballInitialPosition },
    { translateY: -90},
    {
      duration: 400,
      delay: 0,
      easing: "physicsInOut"
    }
  );
  var ballUp = KUTE.fromTo(
    ".elevator__ball",
    { translateY: -90},
    { translateY: ballInitialPosition },
    {
      duration: 100,
      delay: 0,
      easing: "easeOutBack"
    }
  );
  // Hand
  var handDown = KUTE.fromTo(
    ".elevator__hand",
    { rotateZ: 80, translateX: -25, translateY: 40 },
    { rotateZ: 0, translateX: 0, translateY: 0 },
    {
      duration: 400,
      delay: 0,
      easing: "physicsInOut",
    }
  );
  // Bouncing
  var bounceStart = KUTE.fromTo(
    ".elevator__ball",
    { translateY: groundHeight },
    { translateY: 60 },
    { duration: 400, delay: 0, easing: "easingCubicOut" }
  );

  if (upwards) {
    backToBoy(initialVlocityAbs);
    var step1 = KUTE.fromTo(
      ".elevator__ball",
      { translateY: ballInitialPosition },
      {
        translateY:
          ballInitialPosition -
          maxHeight(initialVlocityAbs, ELEVATOR.data("upwards")) *
            meterToPxRatio
      },
      {
        duration: timeToReachThePlatform(initialVlocityAbs) * 1000,
        delay: 0,
        easing: "easingCubicOut",
        complete: function() {
          var meterValue = maxHeightFromGround(
            initialVlocityAbs,
            initialHeightAbs
          ).toFixed(3);
          $(".options__bottomValue1 input").val(Math.abs(meterValue).toString().split(".").join(","));
        }
      }
    );
    var step2 = KUTE.fromTo(
      ".elevator__ball",
      {
        translateY:
          ballInitialPosition -
          maxHeight(initialVlocityAbs, ELEVATOR.data("upwards")) *
            meterToPxRatio
      },
      {
        translateY: groundHeight
      },
      {
        duration: fallingTime(initialVlocityAbs, initialHeightAbs) * 1000,
        delay: 0,
        easing: "easingCubicIn"
      }
    );
    var bounceEnd = KUTE.fromTo(
      ".elevator__ball",
      { translateY: 60 },
      { translateY: groundHeight },
      {
        duration: 400,
        delay: 0,
        easing: "easingCubicIn",
        complete: function() {
          $(".options__bottomValue2 input").val(
            fullMotionTime(initialVlocityAbs, initialHeightAbs).toFixed(4).toString().split(".").join(",")
          );
          otherDeltaT = fullMotionTime(initialVlocityAbs, initialHeightAbs);
          var x = Math.abs( velocity_ground(initialVlocityAbs, parseFloat(otherDeltaT.toFixed(3)))).toFixed(4);
          $(".options__bottomValue3 input").val(x.toString().split(".").join(","))

          $(".output__formula").removeClass("output__formula--disabled");
          $("#checkBtn").hide();
          $("#resetBtn").removeAttr("hidden");
          fillCalculation();
          clearInterval(ballPoss);
        }
      }
    );
  } else {
    backToBoy(initialVlocityAbs);
    var step1 = KUTE.fromTo(
      ".elevator__ball",
      { translateY: ballInitialPosition },
      {
        translateY:
          ballInitialPosition -
          maxHeight(initialVlocityAbs, ELEVATOR.data("direction")) *
            meterToPxRatio
      },
      {
        duration: Math.abs(timeToReachThePlatform(initialVlocityAbs)) * 1000,
        delay: 0,
        easing: "easingCubicOut",
        complete: function() {
          var meterValue = maxHeightFromGround(
            initialVlocityAbs,
            initialHeightAbs
          ).toFixed(3);
          $(".options__bottomValue1 input").val(Math.abs(meterValue).toString().split(".").join(","));
        }
      }
    );
    var step2 = KUTE.fromTo(
      ".elevator__ball",
      {
        translateY:
          ballInitialPosition -
          maxHeight(initialVlocityAbs, ELEVATOR.data("direction")) *
            meterToPxRatio
      },
      {
        translateY: groundHeight
      },
      {
        duration: fallingTime(initialVlocityAbs, initialHeightAbs) * 1000,
        delay: 0,
        easing: "easingCubicIn"
      }
    );
    var bounceEnd = KUTE.fromTo(
      ".elevator__ball",
      { translateY: 60 },
      { translateY: groundHeight },
      {
        duration: 400,
        delay: 0,
        easing: "easingCubicIn",
        complete: function() {
          $(".options__bottomValue2 input").val(
            fullMotionTime(initialVlocityAbs, initialHeightAbs).toFixed(4).toString().split(".").join(",")
          );

          otherDeltaT = fullMotionTime(initialVlocityAbs, initialHeightAbs);
          var x = Math.abs( velocity_ground(initialVlocityAbs, parseFloat(otherDeltaT.toFixed(3)))).toFixed(4);
          $(".options__bottomValue3 input").val(x.toString().split(".").join(","));

          $(".output__formula").removeClass("output__formula--disabled");
          $("#checkBtn").hide();
          $("#resetBtn").removeAttr("hidden");
          fillCalculation();
          clearInterval(ballPoss)
        }
      }
    );
  }
  handDown.start();
  ballDown.start();
  ballDown.chain(ballUp);
  ballUp.chain(step1);
  step1.chain(step2);
  step2.chain(bounceStart);
  bounceStart.chain(bounceEnd);
}

function distanceFromGround(obj, key) {
  return obj[key];
}

// Formulas
function maxHeight(v_i, direction) {
  var maxHeightRealValue;
  if (direction) {
    maxHeightRealValue = (-1 * (v_i * v_i)) / (2 * g);
  } else {
    maxHeightRealValue = (v_i * v_i) / (2 * g);
  }
  return maxHeightRealValue;
}
function maxHeightFromGround(v_i, y_i) {
  var fullHeight = (-1 * (v_i * v_i)) / (2 * g) + y_i;
  return fullHeight;
}
function velocity_ground(v_i, t_delta) {
  var finalVelocity = v_i + g * t_delta;
  return finalVelocity;
}
function velocity_platform(v_i) {
  return -1 * v_i;
}
function timeToReachThePlatform(v_i) {
  return (-2 * v_i) / g;
}
function fullMotionTime(v_i, y_i) {
  var t_full;
  var rootValue = v_i * v_i - 4 * ((-1 * g) / 2) * (-1 * y_i);
  t_full = (v_i + Math.sqrt(Math.abs(rootValue))) / ((2 * (-1 * g)) / 2);
  return t_full;
}
function fallingTime(v_i, y_i) {
  return fullMotionTime(v_i, y_i) - timeToReachThePlatform(v_i);
}

// Info Popup]
function openInfoWindow(windowSelector) {
  $(windowSelector).show();
  $("#checkBtn, #resetBtn").addClass("popups--hidden");
}
function closeInfoWindow(windowSelector) {
  $(windowSelector).hide();
  $("#checkBtn, #resetBtn").removeClass("popups--hidden");
}

// Add/Remove minus sign
function addMinusSign(sign) {
  // $(".value__height")[0].innerText = sign + $(".value__height")[0].innerText;
  // $(".value__speed")[0].innerText = sign + $(".value__speed")[0].innerText;
  $(".minusSign").show();
  $(".elevator__values").css({"font-size": "22px"});
}
function removeMinusSign() {
  // $(".value__height")[0].innerText = $(".value__height")[0].innerText.replace(
  //   "-",
  //   ""
  // );
  // $(".value__speed")[0].innerText = $(".value__speed")[0].innerText.replace(
  //   "-",
  //   ""
  // );
  $(".minusSign").hide();
  $(".elevator__values").css({"font-size": "25px"});
}
function backToBoy(initialVlocityAbs) {
  setTimeout(function() {
    $(".elevator__dataSpeed").val(initialVlocityAbs);
    $(".elevator__dataTime").val(
        timeToReachThePlatform(initialVlocityAbs).toFixed(2).toString().split(".").join(",")
    );
  }, 2000 * timeToReachThePlatform(initialVlocityAbs));
}
function fillCalculation() {
  var wards = "";
  var g = "";
  const ELEVATOR = $(".elevator__box--js");
  var initialVlocityAbs = Math.abs(ELEVATOR.data("speed"));
  var initialHeightAbs = Math.abs(ELEVATOR.data("height"));
  var initialVlocity = ELEVATOR.data("speed");
  var initialHeight = ELEVATOR.data("height");

  if ($(".directionArrow").hasClass("down")) {
    wards = 'downwards';
    g = "9{,}8";
    g_Num = 9.8;
    var toStr = initialHeight.toString().replace('.', '{,}');
  } else {
    wards = 'upwards ';
    g = "-9{,}8";
    g_Num = -9.8;
    var toStr = '-'+Math.abs(initialHeight).toString().replace('.', '{,}');
  }

 // left side
 $(".wards").html(wards);
 $(".initialHeight").html("$$ { "+ initialHeightAbs.toString().replace('.', '{,}')+" \\space \\mathrm{m} } $$");
 $(".initialVelocity").html("$$ { "+ initialVlocityAbs +" \\space \\mathrm{m}{\\cdot} {\\mathrm{s}^{\\scriptsize{-1}}} } $$");
 $(".g").html("$$ { \\boldsymbol{g} = "+ g +" \\space \\mathrm{m}{\\cdot} {\\mathrm{s}^{\\scriptsize{-2}}} } $$");
 $(".initialHeightL").html("$$ { \\boldsymbol{y}_\\mathrm{i} = "+ initialHeight.toString().replace('.', '{,}')+" \\space \\mathrm{m} } $$");
 $(".initialVelocityL").html("$$ { \\boldsymbol{v}_\\mathrm{i} = "+ initialVlocity +" \\space \\mathrm{m}{\\cdot} {\\mathrm{s}^{\\scriptsize{-1}}} } $$");
 MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output]);

 // right side for first page
 $(".velocities").html("$$ { \\boldsymbol{v}_\\mathrm{f} =- \\boldsymbol{v}_\\mathrm{i} = "+ -1*initialVlocity +" \\space \\mathrm{m}{\\cdot} {\\mathrm{s}^{\\scriptsize{-1}}} } $$");
 $(".platform").html("$$ { "+ initialVlocity +" \\space \\mathrm{m}{\\cdot} {\\mathrm{s}^{\\scriptsize{-1}}} } $$ ");
 $(".velocitiesFormula").html("$$ { \\boldsymbol{v}_\\mathrm{f} = \\boldsymbol{v}_\\mathrm{i} + \\boldsymbol{g} \\Delta t } $$");
 $(".deltaFormula").html("$$ { \\Delta t = \\frac {-2\\boldsymbol{v}_\\mathrm{i}} {\\boldsymbol{g}} } $$");
 var initialVlocity2 = initialVlocity >= 0? initialVlocity : '('+initialVlocity+')';
 $(".deltaT").html("$$ { \\Delta t = \\frac {-2 &times; "+initialVlocity2+"} {"+ g +"} = "+timeToReachThePlatform(initialVlocityAbs).toFixed(2).toString().replace('.', '{,}')+" \\space \\mathrm{s} } $$")
 $(".timeResult").html(`$$ { ${timeToReachThePlatform(initialVlocityAbs).toFixed(2).toString().replace('.', ",")} \\space \\mathrm{s.}} $$`);
 MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output]);

 // right side for second page
 var ydeltaVal = (-1* Math.pow(initialVlocity, 2))/(2*g_Num);
 var yf = initialHeight + ydeltaVal;

 if(yf === parseInt(yf)){
   var yfVal = yf;
 }
 else{
   var yfVal = yf.toFixed(3).toString().replace('.', '{,}')
 }

 var fractionDigits = '';
 if (typeof yfVal === "undefined") {
     fractionDigits = 0
 } else {
     fractionDigits = yfVal.length - 4
 }
 $(".vfPow").html("$$ { \\boldsymbol{v}_\\mathrm{f}^{\\scriptsize{2}} = \\boldsymbol{v}_\\mathrm{i}^{\\scriptsize{2}} + 2\\boldsymbol{g} \\Delta \\boldsymbol{y} } $$");
 $(".yDelta").html("$$ { \\Delta \\boldsymbol{y} = \\frac {-\\boldsymbol{v}_\\mathrm{i}^{\\scriptsize{2}}} {2\\boldsymbol{g}} } $$");
 $(".yDeltaResult").html("$$ { \\Delta \\boldsymbol{y} = \\frac {-1 &times; ("+initialVlocity+")^{\\scriptsize{2}}} {2 &times; {("+ g +")}} = "+ydeltaVal.toFixed(2).toString().replace('.', '{,}') +" \\space \\mathrm{m} } $$");
 $(".initialY").html("$$ { \\boldsymbol{y}_\\mathrm{i} = "+ initialHeight.toString().replace('.', '{,}')+" \\space \\mathrm{m} } $$");

  if(ydeltaVal < 0){
    $(".finalY").html("$$ { \\boldsymbol{y}_\\mathrm{f} = \\boldsymbol{y}_\\mathrm{i} + \\Delta \\boldsymbol{y} = "+ initialHeight.toString().replace('.', '{,}')+" + ("+ydeltaVal.toFixed(3).toString().replace('.', '{,}')+") = "+yfVal+" \\space \\mathrm{m} } $$")
  }
  else{
    $(".finalY").html("$$ { \\boldsymbol{y}_\\mathrm{f} = \\boldsymbol{y}_\\mathrm{i} + \\Delta \\boldsymbol{y} = "+ initialHeight.toString().replace('.', '{,}')+" + "+ydeltaVal.toFixed(3).toString().replace('.', '{,}')+" = "+yfVal+" \\space \\mathrm{m} } $$")
  }

 $(".finalYResult").html(`$$ { ${Math.abs(yf).toFixed(fractionDigits).toString().replace('.', '{,}')} \\space \\mathrm{m.} } $$`)
 MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output]);

var deltaTFormul1 = (initialVlocity + Math.sqrt(Math.pow(initialVlocity, 2) - 4 * (-1* g_Num/2) * (-1*initialHeight)))/(-1*g_Num);
var deltaTFormul2 = (initialVlocity - Math.sqrt(Math.pow(initialVlocity, 2) - 4 * (-1* g_Num/2) * (-1*initialHeight)))/(-1*g_Num);
var mainresformula, mainRes

if(deltaTFormul1 < 0 || deltaTFormul1 < deltaTFormul2){
  mainresformula = deltaTFormul2;
  mainRes = deltaTFormul2
}
else if(deltaTFormul2 < 0 || deltaTFormul2 < deltaTFormul1){
  mainresformula = deltaTFormul1;
  mainRes = deltaTFormul1
}

if(g_Num > 0){
  $(".since").html(" <span class='since'>Since </span> $$ { \\frac{g}{2} > 0 } $$");
  var Val_sqrt = (4*4.9*(-1)*initialHeight).toFixed(1).toString().replace('.', ',')
  var after_sqrt = Math.sqrt(Math.abs(Math.pow(initialVlocity, 2)+(4*4.9* (-1) * initialHeight)))
}
else if(g_Num < 0){
  $(".since").html(" <span class='since'>Since </span> $$ { \\frac{g}{2} < 0 } $$");
  var Val_sqrt = (4*(-1)*4.9*(-1)*initialHeight).toFixed(1).toString().replace('.', ',')
  var after_sqrt = Math.sqrt(Math.abs(Math.pow(initialVlocity, 2)+(4*(-1)*4.9* (-1) * initialHeight)))
}
var is_down = $(".directionArrow").hasClass("down")
var v_i = '\\boldsymbol{v}_\\mathrm{i}'
if(is_down) {
  v_i = '-\\boldsymbol{v}_\\mathrm{i}'
}
var initialVlocityReal = Math.abs(initialVlocity);
var getInitialVlocity = initialVlocity
var sqrtInitialVlocity = initialVlocity
  if(is_down) {
    initialVlocity = `-(${initialVlocity})`;
    sqrtInitialVlocity = `(${sqrtInitialVlocity})`
    v_i = '-\\boldsymbol{v}_\\mathrm{i}'
  } else {
    initialVlocity = Math.abs(initialVlocity)
  }

  var secondAnswer
  var firstAnswer;

  if(deltaTFormul1 < 0){
    var secondAnswer = deltaTFormul1;
    var firstAnswer = deltaTFormul2;
  } else {
    var secondAnswer = deltaTFormul2;
    var firstAnswer = deltaTFormul1;
  }

 $(".deltaYForTime").html("$$ { \\Delta \\boldsymbol{y} = \\boldsymbol{v}_\\mathrm{i} \\Delta t + \\frac{1}{2} \\boldsymbol{g} \\Delta t^{\\scriptsize{2}} } $$");
 $(".deltaTime").html(` $$ { \\Delta t = \\frac {${v_i} &plusmn; \\sqrt{\\boldsymbol{v}_\\mathrm{i}^{\\scriptsize{2}}-4 &times;\\large{(\\frac{\\boldsymbol{g}}{2})}&times;(${is_down? '':'-'}\\boldsymbol{y}_\\mathrm{i})}}  {2 &times; \\large{(\\frac{\\boldsymbol{g}}{2})}} = 
   \\frac {${initialVlocity} &plusmn; \\sqrt{${sqrtInitialVlocity}^{\\scriptsize{2}}-4 &times;(\\frac{${(Math.abs(g_Num)).toString().split(".").join(",")}}{2})&times;(${toStr})}}  {2 &times; (\\frac{${(Math.abs(g_Num)).toString().split(".").join(",")}}{2})} } $$`);
 $(".equalDeltaT").html(`$$ { = \\frac {${initialVlocityReal} &plusmn; \\sqrt{${Math.pow(initialVlocityReal, 2)}+ (${Val_sqrt} ) } }
   {\\mathrm{${(Math.abs(g_Num)).toString().split(".").join(",")}}} = \\frac {${initialVlocityReal} &plusmn; ${after_sqrt.toFixed(4).toString().replace('.', ',')}} 
   {\\mathrm{${(Math.abs(g_Num)).toString().split(".").join(",")}}} = ${firstAnswer.toFixed(3).toString().replace('.', ',')} \\space \\mathrm{s} \\space or \\space ${secondAnswer.toFixed(3).toString().replace('.', ',')} } $$`);
 $(".deltaTimeResult").html(`$$ { ${mainresformula.toFixed(3).toString().replace('.', ',')} \\space \\mathrm{s.} } $$`);

 // left side for third page

 $(".initialHeightF").html("$$ { \\boldsymbol{y}_\\mathrm{f} = "+yfVal+" \\space \\mathrm{m} } $$")
 MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output]);

 // right side for fourth page
 otherDeltaT = fullMotionTime(initialVlocity, initialHeight);
 $(".velocityFinal").html("$$ { \\boldsymbol{v}_\\mathrm{f} = \\boldsymbol{v}_\\mathrm{i} + \\boldsymbol{g} \\Delta t  } $$");
 $(".velFormula").html(`$$ { \\boldsymbol{v}_\\mathrm{f} = (${getInitialVlocity}) + (${g}) &times; ${mainRes.toFixed(3).toString().replace('.', ',')}  } $$`);
 $(".velocityEqual").html(`$$ { = ${Math.abs(velocity_ground(initialVlocityAbs, parseFloat(mainRes.toFixed(3)))).toFixed(4).toString().replace('.', ',')} \\space \\mathrm{m}{\\cdot} {\\mathrm{s}^{\\scriptsize{-1}}} } $$`);
 $(".velocityResult").html("$$ { "+Math.abs( velocity_ground(initialVlocityAbs, parseFloat(mainRes.toFixed(3)))).toFixed(4).toString().replace('.', ',')+" \\space \\mathrm{m}{\\cdot} {\\mathrm{s}^{\\scriptsize{-1}}} } $$");
 // left side for fourth page
 $(".deltaTL").html("$$ { \\Delta t = "+ mainRes.toFixed(3).toString().split(".").join(",")+" \\space \\mathrm{s} } $$")
 MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output]);

}

var ballPoss = setInterval(function(){
  var pos = $(".elevator__ball").position().top;
  if(pos < -250){
    $(".elevator__ball").css("visibility", "hidden");
  }
  else{
    $(".elevator__ball").css("visibility", "visible");
  }
},10)

$(document).ready(function () {
  MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output], function () {
    $(".elevator__values").css({"display": "block"});
  })
})