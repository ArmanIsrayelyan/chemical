let Page = function () {
    this.page = 1;
    this.pageHeader = "";
    this.pageDescription = "";
    this.pageHelpText = "";
    this.mainContent = null;
	this.enableCheck = function () {};
	this.speed = 40;
	this.massDriver = 50;
	this.delta_t = {Inflatedairbag: 0.25, Faultyairbag: 0.01, No_airbag: 0.005}; // no airbag 0.01, 0,25 s for inflated airbag, 0,005 faulty airbag
	this.p_value = 0;
}
Page.prototype = {
    init: function () {
		var self = this;
        this.cleareBackgroundContent();
		this.addBackgroundContent();
		$('slide-block-1 input').val(self.speed);
		$('slide-block-2 input').val(self.massDriver);
		$('.momentum').click();
	},
	cleareBackgroundContent: function () {
		let self = this;
		let content = $('#contentImg');
		if(content.length > 0) {
			$('#contentImg').html('');
		}else {
			console.error('Not found div#contentImg block');
		}
    },
    cleareLeftContent: function () {
		$('.left-block').html('');
	},
	cleareRightContent: function () {
		$('.right-block').html('');
    },
    addMainBlock: function (contents) {
        let self = this;
        let content = $('#contentImg');
        if(content.length > 0) {
            self.mainContent = content;
            content.append(contents);
            return true;
        } else {
            console.error('Not found div#contentImg block');
            return false;
        }
    },
    addBackgroundContent: function () {
        let self = this;
        template = '<div class="left-block"></div>\
                    <div class="right-block"></div>'
        if(self.addMainBlock(template)) {
			self.addRightBlock();
			self.addLeftBlock();
			self.addSlideBlock(1, 40, 160, 20, self.setSpeed.bind(self));
			self.addSlideBlock(2, 50, 100, 5, self.setMassDriver.bind(self));
			self.addTabEvent();
			self.addAirbagBlock();
			self.addEventAirbagButton();
			self.addEventGo();
        }
	},
	addLeftBlock: function () {
		template = '<div class="left-content">\
						<img src="img/Arrow+.png" />\
						<div class="airbag-block"></div>\
						<div class="video-block">\
							<video width="100%" height="100%" >\
								<source src="img/videos/Inflatedairbag.webm" type="video/webm">\
							</video>\
						</div>\
					</div>'
		$('.left-block').html(template);
	},
	addAirbagBlock: function () {
		var template = '<div>\
							<div class="airbag-slider">\
							</div>\
							<input id="airbag_slider" type="range" min="0" max="100" step="50" value=100>\
						</div>\
						<div>\
							<button id="No_airbag" index="0"></button>\
							<button id="Faultyairbag" index="1"></button>\
							<button id="Inflatedairbag" index="2" class="active"></button>\
						</div>';
		$('.airbag-block').append(template);
	},
	addEventAirbagButton: function () {
		var self = this;
		$('.airbag-block button').on('click', function () {
			$('.airbag-block button').each(function (index, item) {
				$(item).removeClass('active');
			})
			var src = "img/videos/" + $(this).attr('id') + ".webm";
			$('.video-block source').attr('src', src);
			$('.video-block video')[0].load();
			$('#airbag_slider').val(50*$(this).attr('index'));
			$(this).addClass('active');
			self.addFormula();
		})
		$('#airbag_slider').off('input').on('input', function (){
			if($(this).val()==100){
				$('.airbag-block button:nth-child(3)').trigger( "click" );
			} else if($(this).val()==50){
				$('.airbag-block button:nth-child(2)').trigger( "click" );
			} else {
				$('.airbag-block button:nth-child(1)').trigger( "click" );
			}
		})
	},
	addEventGo: function () {
		var self = this
		$(document).off('click', '#checkBtn').one('click', '#checkBtn', function () {
			$('.video-block video')[0].play();
			$(this).css({opacity: '0.5', cursor: 'unset'})
			self.addFormula();
			$('.airbag-block button').off('click').css({cursor: 'unset'});
			$('.slide-block-1 input').attr('disabled', 'disabled')
			$('.slide-block-2 input').attr('disabled', 'disabled')
			$('#resetBtn').attr('disabled', false);
			$('#airbag_slider').attr('disabled', 'disabled');
			self.addEventReset();
		})
	},
	addEventReset: function () {
		$('#resetBtn').one('click', function () {
			window.location = window.location;
		})
	},
	addRightBlock: function () {
		template = '<div class="slide-block-1"></div>\
					<div class="slide-block-2"></div>\
					<div class="tab-block">\
						<div>\
							<div class="momentum active "></div>\
							<div class="impuls "></div>\
						</div>\
					<div class="tab-content"></div>\
					</div>';
		$('.right-block').append(template);
	},
	addTabEvent: function () {
		var self = this;
		$('.tab-block > div > div').off('click').on('click', function () {
			$('.tab-block > div > div').each(function (index, item) {
				$(item).removeClass('active');
			})
			$(this).addClass('active');
			self.addFormula();
		})
	},
	addSlideBlock: function (i, min, max, step, f = function () {}) {
        var self = this;
        var template = '<input id="range-'+i+'" type="range" class="radius" min="'+min+'" max="'+max+'" step="'+step+'" value="1">';

		$('.slide-block-'+i).append(template);
        self.addEventInput(i, f);
	},
	addEventInput: function (i, f) {
		var self = this;
		$('#range-'+i).off('change').on('change', function (e) {
			f(e)
			self.addFormula();
		})
	},
	setSpeed: function (e) {
		let value = e.target.value;
		if(this.speed == value) return;
		this.speed = value;
	},
	setMassDriver: function (e) {
		let value = e.target.value;
		if(this.massDriver == value) return;
		this.massDriver = value;
	},
	addFormula: function () {
		var self = this;
		if($('.momentum').hasClass('active')){
			self.addMomentumFormula();
		} else if ($('.impuls').hasClass('active')){
			self.addSpeedFormula();
		}
	},
	addMathJax: function (tag, formula, id) {
		$(tag).html('').css({visibility: 'hidden'});

		MathJax.HTML.addElement($(tag)[0], "p", {id: id}, [formula]);
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, id], function () {
			$(tag).css({visibility: 'visible'});
		});
	},
	addMomentumFormula: function () {
		var speed = '\\quad',  mass = '\\quad', page_value='\\quad';
		if(this.speed) speed = Number((this.speed*5/18).toFixed(2));
		if(this.massDriver) mass = Number((this.massDriver*6.8/100).toFixed(2));
		if(this.speed && this.massDriver){
			this.p_value = Number((0 - speed * mass).toFixed(2))
			page_value = this.p_value.toString().replace('.', ',');
		}
		var formula = '$$\\begin{aligned} \\Delta \\boldsymbol{p} &=\\boldsymbol{p}_{\\class{sub-value}{\\mathrm{f}}}-\\boldsymbol{p}_{\\class{sub-value}{\\mathrm{i}}} \\\\ &=0-m \\boldsymbol{v}_{\\class{sub-value}{\\mathrm{i}}} \\\\ &=0-('+spaceNumber(mass)+')('+spaceNumber(speed)+') \\\\\ &='+spaceNumber(page_value)+' \\begin{array}{c}{\\mathrm{kg} \\cdot \\mathrm{m} \\cdot \\mathrm{s}^{\\class{sub-value}{-1}}}\\end{array} \\end{aligned}$$';
		this.addMathJax('.tab-content', formula, 'momentum_block');
	},
	addSpeedFormula: function () {
		var delta_t = this.delta_t[$('.airbag-block button.active').attr('id')];
		var speed = '\\quad',  mass = '\\quad', page_value='\\quad';
		if(this.speed) speed = Number((this.speed*5/18).toFixed(2));
		if(this.massDriver) mass = Number((this.massDriver*6.8/100).toFixed(2));
		if(this.speed && this.massDriver){
			page_value = this.p_value = Number((0 - speed * mass).toFixed(2))
		}
		var f_net = Number((this.p_value/delta_t).toFixed(2));
		var formula = '$$\\begin{aligned} \\Delta \\boldsymbol{p}=\\text { Impulse } &=\\boldsymbol{F}_{\\class{sub-net}{\\text {net }}} \\Delta t \\\\ '+spaceNumber(page_value)+'&=\\boldsymbol{F}_{\\class{sub-net}{\\text {net }}}  \\times('+delta_t.toString().replace('.', ',')+') \\\\ \\boldsymbol{F}_{\\class{sub-net}{\\text {net }}} =& '+spaceNumber(page_value/delta_t)+'\\space\\text { N } \\\\=& \\space'+spaceNumber(Math.abs(f_net))+'\\space \\text{N} \\textit { onto the driver. } \\end{aligned}$$';
		this.addMathJax('.tab-content', formula, 'speed_block');
	},
    removePopup:function (namePopup){
    	$(namePopup).remove();
    },
    addMathText: function (pageHelpText, helpList) {
		let ol = document.createElement('ol');
		ol.className = 'help-list';
		$(pageHelpText).html(ol);
		for(let i = 0; i< helpList.length; i++) {
			let id = 'li'+i+(new Date().getTime());
			MathJax.HTML.addElement(ol, "li", {id: id}, [helpList[i]]);
			MathJax.Hub.Queue(["Typeset", MathJax.Hub, id]);
		}
	},
	// set and get header, description, help list
	setInstructionHelp: function (select) {
		let self = this;
		let helpList = self.getInstructionList();
		$(select || self.pageHelpText).html('');
		self.pageHelpText = select || self.pageHelpText;
		self.addMathText(self.pageHelpText, helpList);
		$(select || self.pageHelpText).parent().removeClass('help_list_' + self.page - 1).addClass('help_list_' + self.page);
		
	},
	setDescription: function (select, description) {
		let self = this;
		$(select || self.pageDescription).html('');
		var description = self.getDescription();
		let id = Math.random()*(new Date().getTime());
		MathJax.HTML.addElement($(select || self.pageDescription)[0], "span", {id: 'description'}, [description]);
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'description']);
		self.pageDescription = select || self.pageDescription;
		// $(select).parent().removeClass('description_text_' + self.page - 1).addClass('description_text_' + self.page);
	},
	getDescription: function () {
		let self = this;
		let description = "";
		switch (self.page){
			case 1:
				description = "Set the speed of the car, mass of the driver and condition of the airbag.";
			break;
		}
		return description;
	},
	getInstructionList: function () {
		let self = this;
		let helpList = "";
		switch (self.page){
			case 1:
			helpList = ['This simulation shows you what happens to the head of a driver when the car is in a head-on collision.',
						'Use the slider to set the speed at which the car is driving.',
						'Use the slider to set the mass of the driver. On average, a human head makes up about `6,8%` of the mass of the body. ',
						'Choose whether you want to observe a scenario without an airbag, with a faulty airbag that does not inflate all the way or with an inflated airbag.',
						'Click GO to see how the head of the driver comes to a stop. Observe the net force exerted on the head of the driver.',
						'Click RESET to start again.',
						'Enter other speeds, masses or airbag conditions and observe how the net force is affected.'];
			break;
		}
		return helpList;
	},
	setMainHeader: function (select) {
		let self = this;
		let header = self.getMainHeader();
		$(select || self.pageHeader).html(header);
		self.pageHeader = select || self.pageHeader;
		// $(select).parent().removeClass('description_text_' + self.page - 1).addClass('description_text_' + self.page);
	},
	getMainHeader: function () {
		let self = this;
		let header = "Investigate the impulse-momentum theorem";
		return header;
	},
	//////////
	clearMain : function () {
		let self = this;
		$('.left-block').html('')
		$('.right-block').html('');
		$('.popup-show-block').css('visibility','hidden');

	},
	createPageConclusion : function () {
		let self = this;
		self.addMainBlock('<div id="conclusion"></div>');
		$('#conclusion').append('<div class="pageNumber">1.</div>');
		$('#conclusion').append('<div class="conclusionImage"><img id="nextPage"><img id="conclusionImage"></div>');
		$('#conclusion').append('<div class="pagePagination">\
			<button class="pagePaginationButton" id="pageChange_prev" disabled="disabled">\<</button>\
			<button class="pagePaginationButton paginationButtonClicked" id="pageChange_1">1</button>\
			<button class="pagePaginationButton" id="pageChange_2">2</button>\
			<button class="pagePaginationButton" id="pageChange_3">3</button>\
			<button class="pagePaginationButton" id="pageChange_next">\></button>\
			</div>')
		$('#conclusionImage').attr('src','img/t1.png');
		self.addEventPagePagination()
	},
	addEventConclusion: function(){
		let self = this;
		$(document).off('click','.conclusion' ).on('click','.conclusion' , function () {
			self.clearMain();
			self.createPageConclusion();
			self.activeConclusionButton();
			self.page = 3;
			self.setInstructionHelp();
			self.setDescription();
			//self.setMainHeader();
			self.enableCheck(true);
            self.addTabsEvent();
		})
	},
	pageAnimate: function (){
		let self = this;
        self.animateDo = true
		$('#pageChange_'+self.paginationPageNumber.prevPage).removeClass('paginationButtonClicked');
		$('#pageChange_'+self.paginationPageNumber.nextPage).addClass('paginationButtonClicked');
		$('#nextPage').attr('src','img/t'+self.paginationPageNumber.nextPage+'.png');
		setTimeout(function(){
			$('#nextPage').css('visibility','visible');
		},100)
		$( "#conclusionImage" ).animate({opacity: 0}, 1000, function() {
    		$('#conclusionImage').attr('src','img/t'+self.paginationPageNumber.nextPage+'.png');
			$('#nextPage').css('visibility','hidden');
			$('#conclusionImage').css('opacity','1');
            self.animateDo = false;
		});
	},


	addEventPagePagination: function (){
		let self = this;
		$(document).off('click','.pagePaginationButton' ).on('click','.pagePaginationButton' , function () {
            if(self.animateDo) return;
			let buttonVal = $(this).text();
			$('#pageChange_prev').attr('disabled', false);
			$('#pageChange_next').attr('disabled', false);
			switch(buttonVal){
				case "1":
					self.paginationPageNumber.prevPage = self.paginationPageNumber.nextPage;
					self.paginationPageNumber.nextPage = 1;
					$('#pageChange_prev').attr('disabled', true);
					self.pageAnimate();
				break;
				case "2":
					self.paginationPageNumber.prevPage = self.paginationPageNumber.nextPage;
					self.paginationPageNumber.nextPage = 2;
					self.pageAnimate();
				break;
				case "3":
					self.paginationPageNumber.prevPage = self.paginationPageNumber.nextPage;
					self.paginationPageNumber.nextPage = 3;
					$('#pageChange_next').attr('disabled', true);
					self.pageAnimate();
				break;
				case "<":
					if(!(self.paginationPageNumber.nextPage==1)){
						self.paginationPageNumber.prevPage = self.paginationPageNumber.nextPage;
						self.paginationPageNumber.nextPage -=1;
						self.pageAnimate();
					}
                    if(self.paginationPageNumber.nextPage==1){
                        $('#pageChange_prev').attr('disabled', true);
                    }
				break;
				case ">":
					if(!(self.paginationPageNumber.nextPage==3)){
						self.paginationPageNumber.prevPage = self.paginationPageNumber.nextPage;
						self.paginationPageNumber.nextPage +=1;
						self.pageAnimate();
                       
					}
                    if(self.paginationPageNumber.nextPage==3){
                         $('#pageChange_next').attr('disabled', true);
                    }
				break;
			}
			$('.pageNumber').html(self.paginationPageNumber.nextPage+".")
		})
    }
}
function spaceNumber(x){
	var z = x.toString();
	z = z.replace(',', '.');
	z = Number(Number(z).toFixed(2)).toString();
	var zb = z.split('.');
	var b = zb[1];
        z = zb[0];
	if (Math.abs(z).toString().length >= 4) {
		var c = z.substring(0,z.length-3);
		var d = '';
		if(c.length > 3){
			d = c.substring(0,c.length-3)
			c = c.substring(c.length-3,c.length)
			d+="\\space";
		}
		var y = z.substring(z.length-3, z.length);
		if(b) y = y+','+b; 
		return d+c+"\\space"+y;
	} else {
		if(b) return z + ',' + b;
		return z;
	}
}