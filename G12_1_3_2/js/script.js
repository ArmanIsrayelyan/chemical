var mashtab = 1;
$(document).ready(function(){
    var gobtn = false;
    var beforeMomentum_content = $('#beforeMomentum').html();
    var beforeEnergy_content = $('#beforeEnergy').html();
    var afterMomentum_content = $('#afterMomentum').html();
    var afterEnergy_content = $('#afterEnergy').html();
    // For calculating formulas.
    var orangeBall = { mass: 0.165, direction: 1, speed: 0 };
    var blueBall = { mass: 0.165, direction: -1, speed: 0 };
    var options = {mode: 'inelastic', collis: false, twoBallDir: 0, click: false};
    var borderBump;
    var noColl = false
    // Play this audio when detect collision..
    var audio = new Audio();
    audio.src = "./assets/SnookerBall.mp3";

    // Detected click on document.
    $(document).click(function(){
        options.click = true;
    });

    // TimeOut for direction popup and add ball mass.
    setTimeout(function() {
        var ckeckPopup = setInterval(function(){
            if (options.click === false) {
                $("#changeDir").fadeIn();
                clearInterval(ckeckPopup);
            } else{
                $("#changeDir").fadeOut();
                clearInterval(ckeckPopup);
            }
        }, 10)
    }, 1000);
        
    // Hide direction popup.
    setTimeout(function() {
        $("#changeDir").fadeOut();
    }, 4000);

    // Add ball mass. 
    setTimeout(function() {
        $(".orangeMass").html('0,165');
        $(".blueMass").html('0,165');
    }, 2000);
    
    // Choose Mode (default Inelastic).
    $(document).off('click', "#chooseMode").on('click', "#chooseMode",function() {
        if(gobtn) return;
        $(this).toggleClass('inelastic').toggleClass('elastic');
        if ($(this).attr("class") == "inelastic") {
            options.mode = "inelastic";
        } else {
            options.mode = "elastic";
        }
    });

    // Choose blue ball speed (default 0).
    $(".r1").on("input", function() {
        $("#changeDir").fadeOut();
        $(".orangeMass").html('0,165');
        $(".blueMass").html('0,165');

        var speed = $(this).val();
        orangeBall.speed = parseInt(speed);

        if (speed < 10) {
            $("#ballSpeed_1").html("0" + speed);
        } else {
            $("#ballSpeed_1").html(speed);
        } 
    });

    // Choose orange ball speed (default 0).
    $(".r2").on("input",function() {
        $("#changeDir").fadeOut();
        $(".orangeMass").html('0,165');
        $(".blueMass").html('0,165');

        var speed = $(this).val();
        blueBall.speed = parseInt(speed);

        if (speed < 10) {
            $("#ballSpeed_2").html("0" + speed);
        } else {
            $("#ballSpeed_2").html(speed);
        } 
    });

    // Change direction for blue ball (default +1).
    $(".changeDirectionB_1").click(function() {
        $(".directionBall_1").toggleClass('dir1').toggleClass('dir2');
        $(".orangeMass").html('0,165');
        $(".blueMass").html('0,165');
        $("#changeDir").fadeOut();

        if (orangeBall.direction === 1) {
            orangeBall.direction = -1;
            $("#ball1").css({"left": "700px"});
            $("#a1").css({"left": "715px", "transform": 'rotate(180deg)'});
        }  else {
            orangeBall.direction = 1;
            $("#ball1").css({"left": "300px"});
            $("#a1").css({"left": "315px", "transform": 'rotate(360deg)'});
        }

        if (orangeBall.direction === -1 && blueBall.direction === 1){
            $("#ball1").css({"left": "500px"});
            $("#a1").css({"left": "515px", "transform": 'rotate(180deg)'});

            $("#ball2").css({"left": "700px"});
            $("#a2").css({"left": "715px", "transform": 'rotate(180deg)'});
        }
        BefMomentumFormula();
        BefEnergyFormula();
    });

    // Change direction for orange ball (default -1).
    $(".changeDirectionB_2").click(function() {
        $(".directionBall_2").toggleClass('dir1').toggleClass('dir2');
        $(".orangeMass").html('0,165');
        $(".blueMass").html('0,165');
        $("#changeDir").fadeOut();

        if (blueBall.direction === -1) {
            blueBall.direction = 1;
            $("#ball2").css({"left": "500px"});
            $("#a2").css({"left": "515px", "transform": 'rotate(180deg)'});
        } else {
            blueBall.direction = -1;
            $("#ball2").css({"left": "900px"});
            $("#a2").css({"left": "915px", "transform": 'rotate(360deg)'});
        }

        if (orangeBall.direction === -1 && blueBall.direction === 1){
            $("#ball2").css({"left": "700px"});
            $("#a2").css({"left": "715px", "transform": 'rotate(180deg)'});

            $("#ball1").css({"left": "500px"});
            $("#a1").css({"left": "515px", "transform": 'rotate(180deg)'});
        }
        BefMomentumFormula();
        BefEnergyFormula();
    });

    // Formula view part - Before default Momentum and Energy. 
    $("#beforeColEnergy").click(function() {
        $(this).removeClass("blackEnergyBtn").addClass("whiteEnergyBtn");
        $("#beforeColMom").removeClass("whiteMomentBtn").addClass("blackMomentBtn");
        $("#beforeMomentum").css({"visibility": "hidden"});
        $("#beforeEnergy").css({"visibility": "visible"});

        // for after
        $('#afterColEnergy').removeClass("blackEnergyBtn").addClass("whiteEnergyBtn");
        $("#afterColMom").removeClass("whiteMomentBtn").addClass("blackMomentBtn");
        $("#afterMomentum").css({"visibility": "hidden"});
        $("#afterEnergy").css({"visibility": "visible"});

    });
    $("#beforeColMom").click(function() {
        $(this).removeClass("blackMomentBtn").addClass("whiteMomentBtn");
        $("#beforeColEnergy").removeClass("whiteEnergyBtn").addClass("blackEnergyBtn");
        $("#beforeMomentum").css({"visibility": "visible"});
        $("#beforeEnergy").css({"visibility": "hidden"});

        // for after
        $("#afterColMom").removeClass("blackMomentBtn").addClass("whiteMomentBtn");
        $("#afterColEnergy").removeClass("whiteEnergyBtn").addClass("blackEnergyBtn");
        $("#afterMomentum").css({"visibility": "visible"});
        $("#afterEnergy").css({"visibility": "hidden"});


    });

    // Formula view part - After default Momentum and Energy.
    $("#afterColEnergy").click(function() {
        $(this).removeClass("blackEnergyBtn").addClass("whiteEnergyBtn");
        $("#afterColMom").removeClass("whiteMomentBtn").addClass("blackMomentBtn");
        $("#afterMomentum").css({"visibility": "hidden"});
        $("#afterEnergy").css({"visibility": "visible"});

        // for before
        $("#beforeColEnergy").removeClass("blackEnergyBtn").addClass("whiteEnergyBtn");
        $("#beforeColMom").removeClass("whiteMomentBtn").addClass("blackMomentBtn");
        $("#beforeMomentum").css({"visibility": "hidden"});
        $("#beforeEnergy").css({"visibility": "visible"});
    });
    $("#afterColMom").click(function() {
        $(this).removeClass("blackMomentBtn").addClass("whiteMomentBtn");
        $("#afterColEnergy").removeClass("whiteEnergyBtn").addClass("blackEnergyBtn");
        $("#afterMomentum").css({"visibility": "visible"});
        $("#afterEnergy").css({"visibility": "hidden"});

        // for before
        $("#beforeColMom").removeClass("blackMomentBtn").addClass("whiteMomentBtn");
        $("#beforeColEnergy").removeClass("whiteEnergyBtn").addClass("blackEnergyBtn");
        $("#beforeMomentum").css({"visibility": "visible"});
        $("#beforeEnergy").css({"visibility": "hidden"});
    });
    
    $('.r1').off('change').on('change', function (){
        BefMomentumFormula();
        BefEnergyFormula()
    })
    $('.r2').off('change').on('change', function (){
        BefMomentumFormula();
        BefEnergyFormula()
    })

    // Before Moment formula.
    function BefMomentumFormula() {
        var S1 = orangeBall.speed * orangeBall.direction;
        var S2 = blueBall.speed * blueBall.direction;
        var MV1 = Math.round10((S1*orangeBall.mass), -3);
        var MV2 = Math.round10((S2*blueBall.mass), -3);
        var Sum = Math.round10((MV1 + MV2), -3);
        $(".orangeS").html(`$$ { (${S1}) } $$`);
        $(".blueS").html(`$$ { (${S2}) } $$`);
        
        $(".befMomMV").empty();
        if (MV1 < 0 && MV2 < 0) {
            $(".befMomMV").html(`$$ { = (${changeDot(MV1)}) \\space + \\space (${changeDot(MV2)}) } $$`);
        }
        else if (MV1 < 0) {
            $(".befMomMV").html(`$$ { = (${changeDot(MV1)}) \\space + \\space ${changeDot(MV2)} } $$`);
        }
        else if (MV2 < 0) {
            $(".befMomMV").html(`$$ { = ${changeDot(MV1)} \\space + \\space (${changeDot(MV2)}) } $$`);
        }
        else{
            $(".befMomMV").html(`$$ { = ${changeDot(MV1)} \\space + \\space ${changeDot(MV2)} } $$`);
        }

        $(".beforeMomSum").html(`$$ { ${changeDot(Sum)} } $$`);
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output]);
    };
    
    // Before Energy formula.
    function BefEnergyFormula() {
        var S1 = orangeBall.speed * orangeBall.direction;
        var S2 = blueBall.speed * blueBall.direction;
        var MV1 = Math.round10(0.5*orangeBall.mass*Math.pow(S1, 2), -3);
        var MV2 = Math.round10(0.5*blueBall.mass*Math.pow(S2, 2), -3);
        var Sum = Math.round10((MV1 + MV2), -3);
        $(".orangeEnergyS").html(`$$ { (${S1}) } $$`);
        $(".blueEnergyS").html(`$$ { (${S2}) } $$`);

        $(".befEnMV").empty();
        $(".befEnMV").html(`$$ { = (${changeDot(MV1)}) \\space + \\space (${changeDot(MV2)}) } $$`);

        if (MV1 < 0) {
            $(".befEnMV").html(`$$ { = (${changeDot(MV1)}) \\space + \\space ${changeDot(MV2)} } $$`);
        }
        else if (MV2 < 0) {
            $(".befEnMV").html(`$$ { = ${changeDot(MV1)} \\space + \\space (${changeDot(MV2)}) } $$`);
        }
        else if (MV1 < 0 && MV2 < 0) {
            $(".befEnMV").html(`$$ { = (${changeDot(MV1)}) \\space + \\space (${changeDot(MV2)}) } $$`);
        }
        else{
            $(".befEnMV").html(`$$ { = ${changeDot(MV1)} \\space + \\space ${changeDot(MV2)} } $$`);
        }

        $(".befEnMV").css({'font-size': '23px'})
        $(".beforeEnergySum").css({'font-size': '23px'})

        $(".beforeEnergySum").html(`$$ { ${changeDot(Sum)} } $$`);
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output]);
    };
    
    // Function for Solution 1 and 2.
    function Solution() {
        var S1 = orangeBall.speed * orangeBall.direction; // V1 B
        var S2 = blueBall.speed * blueBall.direction;  // V2 D
        var V1F, V2F;
        if(noColl) {
            V1F = S1
            V2F = S2
        } else {
            var a = 1+orangeBall.mass/blueBall.mass; // K
            var b = Number((-2*(orangeBall.mass*S1 + blueBall.mass*S2)/blueBall.mass).toFixed(1)); //  L
            var c = Number((Math.pow((orangeBall.mass*S1 + blueBall.mass*S2), 2) / (orangeBall.mass*blueBall.mass) - Math.pow(S1, 2) - blueBall.mass* (Math.pow(S2, 2)/orangeBall.mass)).toFixed(1)); // M
            var d = Number(Math.sqrt(Math.pow(b, 2) - 4*a*c).toFixed(1)); // N
            // console.log({a, b, c, d})
            // var momentum_befor = orangeBall.mass*S1 + blueBall.mass*S2 // E
            var Sol1 = Number(((-b + d) / (2*a)).toFixed(1)); // O
            var Sol2 = Number(((-b - d) / (2*a)).toFixed(1)); // P

            if(options.mode == "inelastic"){
                if(orangeBall.direction == blueBall.direction && Math.abs(Sol2) >= Math.abs(Sol1)){
                    V2F = Math.ceil(Math.abs(Sol1) - (20/100)* Math.abs(Sol1));
                    V2F*=(Sol1 > 0)? 1 : -1;
                    V1F = Math.ceil((orangeBall.mass*S1+blueBall.mass*S2-orangeBall.mass*V2F)/blueBall.mass);

                } else if(orangeBall.direction == blueBall.direction && Math.abs(Sol2) <= Math.abs(Sol1)){
                    V1F = Math.ceil(Math.abs(Sol2) - (20/100) * Math.abs(Sol2));
                    V1F*=(Sol2 > 0)? 1 : -1;
                    V2F = Math.ceil((orangeBall.mass*S1+blueBall.mass*S2-orangeBall.mass*V1F)/blueBall.mass);
                } 
                else if(Math.abs(Sol2) >= Math.abs(Sol1)){
                    V1F = Math.floor(Math.abs(Sol2) - (20/100) * Math.abs(Sol2));
                    V1F*=(Sol2 > 0)? 1 : -1;
                    V2F = Math.ceil((orangeBall.mass*S1+blueBall.mass*S2-orangeBall.mass*V1F)/blueBall.mass);
                } else {
                    V2F = Math.floor(Math.abs(Sol1) - (20/100)* Math.abs(Sol1));
                    V2F*=(Sol1 > 0)? 1 : -1;
                    V1F = Math.floor((orangeBall.mass*S1+blueBall.mass*S2-orangeBall.mass*V2F)/blueBall.mass);
                }
            } else {
                V1F = Math.round((-b - d) / (2*a));
                V2F = Math.round(-b + d) / (2*a);
            }
        }
        // console.log({V1: V1F, V2: V2F, sol1: Sol1, sol2: Sol2})
        return {V1: V1F, V2: V2F};
    };

    // After Momentum Formula
    function AfterMomentumFormula() {
        detectColl()
        var sol = Solution();
        var MV1 = Math.round10((sol.V1*orangeBall.mass), -3);
        var MV2 = Math.round10((sol.V2*blueBall.mass), -3);
        var Sum = Math.round10((MV1 + MV2), -3);
        $(".orangeAfterMomS").html(`$$ { (${sol.V1}) } $$`);
        $(".blueAfterMomS").html(`$$ { (${sol.V2}) } $$`);

        $(".afterMomMV").empty();
        $(".afterMomMV").html(`$$ { = (${changeDot(MV1)}) \\space + \\space (${changeDot(MV2)}) } $$`);
        
        if (MV1 < 0 && MV2 < 0) {
            $(".afterMomMV").html(`$$ { = (${changeDot(MV1)}) \\space + \\space (${changeDot(MV2)}) } $$`);
        }
        else if (MV1 < 0) {
            $(".afterMomMV").html(`$$ { = (${changeDot(MV1)}) \\space + \\space ${changeDot(MV2)} } $$`);
        }
        else if (MV2 < 0) {
            $(".afterMomMV").html(`$$ { = ${changeDot(MV1)} \\space + \\space (${changeDot(MV2)}) } $$`);
        }
        else{
            $(".afterMomMV").html(`$$ { = ${changeDot(MV1)} \\space + \\space ${changeDot(MV2)} } $$`);
        }

        $(".afterMomSum").html(`$$ { ${changeDot(Sum)} } $$`);
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output]);
    };
   
    // Before Energy formula 
    function AfterEnergyFormula() {
        detectColl()
        var sol = Solution();
        var MV1 = Math.round10(0.5*orangeBall.mass*Math.pow(sol.V1, 2), -3);
        var MV2 = Math.round10(0.5*blueBall.mass*Math.pow(sol.V2, 2), -3);
        var Sum = Math.round10((MV1 + MV2), -3);
        $(".orangeAfterEnergyS").html(`$$ { (${sol.V1}) } $$`);
        $(".blueAfterEnergyS").html(`$$ { (${sol.V2}) } $$`);

        $(".afterEnMV").empty();
        $(".afterEnMV").html(`$$ { = (${changeDot(MV1)}) \\space + \\space (${changeDot(MV2)}) } $$`);

        if (MV1 < 0) {
            $(".afterEnMV").html(`$$ { = (${changeDot(MV1)}) \\space + \\space ${changeDot(MV2)} } $$`);
        }
        else if (MV2 < 0) {
            $(".afterEnMV").html(`$$ { = ${changeDot(MV1)} \\space + \\space (${changeDot(MV2)}) } $$`);
        }
        else if (MV1 < 0 && MV2 < 0) {
            $(".afterEnMV").html(`$$ { = (${changeDot(MV1)}) \\space + \\space (${changeDot(MV2)}) } $$`);
        }
        else{
            $(".afterEnMV").html(`$$ { = ${changeDot(MV1)} \\space + \\space ${changeDot(MV2)} } $$`);
        }
        
        $(".afterEnergySum").html(`$$ { ${changeDot(Sum)} } $$`);
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output]);
    };
function detectColl() {
    if (orangeBall.direction === 1 && blueBall.direction === 1 && orangeBall.speed <= blueBall.speed) {
        $("#noColl").fadeIn();
        noColl = true;
    }
    else if (orangeBall.direction === -1 && blueBall.direction === -1 && orangeBall.speed >= blueBall.speed) {
        $("#noColl").fadeIn();
        noColl = true;
    }
    else if (orangeBall.direction === -1 && blueBall.direction === 1){
        $("#noColl").fadeIn();
        noColl = true;
    }

    if (orangeBall.speed === 0 && blueBall.speed === 0) {
        $("#noSpeed").fadeIn();
    }
}
    // Go btn click
    $("#goBtn").click(function(e) {
        gobtn = true;
        detectColl()
        if (orangeBall.speed === 0 && blueBall.speed === 0) {
            $("#noSpeed").fadeIn();
        }
        else {
            var sol = Solution();
            AfterMomentumFormula();
            AfterEnergyFormula();

            $(".r1").prop("disabled", true);
            $(".r2").prop("disabled", true);
            $(".changeDirectionB_1").css("pointer-events", "none");
            $(".changeDirectionB_2").css("pointer-events", "none");
            $("#goBtn").css({"pointer-events": "none", 'opacity': 0.5});

            $("#a1").css({display: "none"});
            $("#a2").css({display: "none"});

            moveBall();

            // Detect collision for borderline.
            borderBump = setInterval(function(){
                if (detectBorder()) {
                    clearInterval(borderBump);
                    $("#ball2").stop();
                }
            },10)
            $(".b1").rotateBall(orangeBall.direction, orangeBall.speed, sol.V1);
            $(".b2").rotateBall(blueBall.direction, blueBall.speed, sol.V2);
        }
        e.stopPropagation();

        // Hide popup after 2 seconds.
        var hidePopup = setInterval(function(){
            if ($('#noSpeed').css('display') === 'block') { 
                $('#noSpeed').fadeOut();
                clearInterval(hidePopup);
            }
        }, 4000);
    });

    // Hide hint.
    $(document.body).click(function() {
        $('#noColl').fadeOut();
        $('#noSpeed').fadeOut();
        $("#changeDir").fadeOut();
    });

    // Reset button function.
    $("#resetBtn").click(function() {
        noColl = false;
        gobtn = false;
        $(".r1").prop("disabled", false).val(0);
        $(".r2").prop("disabled", false).val(0);
        $(".changeDirectionB_1").css("pointer-events", "auto");
        $(".changeDirectionB_2").css("pointer-events", "auto");
        $("#goBtn").css({"pointer-events": "auto", "opacity": 1});

        $("#ballSpeed_1").html("00");
        $("#ballSpeed_2").html("00");
        $("#a1").css({display: "block", "left": "315px", "transform": 'rotate(360deg)'});
        $("#a2").css({display: "block", "left": "915px", "transform": 'rotate(360deg)'});

        $(".directionBall_1").removeClass('dir2').addClass('dir1');
        $(".directionBall_2").removeClass('dir1').addClass('dir2');

        $("#ball1").stop().css({left: '300px'});
        $("#ball2").stop().css({left: '900px'});
        orangeBall.speed = 0;
        blueBall.speed = 0;
        orangeBall.direction =  1;
        blueBall.direction =  -1;
        options.collis = false;
        clearInterval(borderBump);
        setTimeout(function () {
            $(".ball").attr('style', '');
        }, 100);
        $(".orangeS, .blueS, .blueMV").html('(&emsp;)');
        $(".orangeAfterMomS, .blueAfterMomS, .blueAfterMomMV").html('(&emsp;)');
        $(".beforeMomSum, .beforeEnergySum, .afterMomSum, .afterEnergySum, .orangeMV, .orangeAfterMomMV").html('&emsp;&emsp;');
        $(".orangeEnergyS, .blueEnergyS, .orangeAfterEnergyS, .blueAfterEnergyS").html('(&emsp;)');
        $(".blueEnergyMV, .blueAfterEnergyMV").html('(&emsp;&emsp;)');
        $(".orangeEnergyMV, .orangeAfterEnergyMV").html('(&emsp;)');
        $(".befMomMV").html(`$$ { = ${'&emsp;&emsp;'} \\space + \\space (${'&emsp;&emsp;'}) } $$`)
        $(".afterMomMV").html(`$$ { = ${'&emsp;&emsp;'} \\space + \\space (${'&emsp;&emsp;'}) } $$`);
        $(".befEnMV").html(`$$ { = (${'&emsp;&emsp;'}) \\space + \\space (${'&emsp;&emsp;'}) } $$`);
        $(".afterEnMV").html(`$$ { = (${'&emsp;&emsp;'}) \\space + \\space (${'&emsp;&emsp;'}) } $$`);
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output]);

        
        setTimeout(function() {
            $(".orangeMass").html('0,165');
            $(".blueMass").html('0,165');
        }, 2000);
    });

    // Detect borderline collision function.
    function detectBorder() {
       var bump2 = $("#ball2").position().left;
        if(bump2+80 <= 10 ){
            return true;
        }
        else {
             return false;
        }
    }

    // Move ball.
    function moveBall() {
        var x1 = $("#ball1").position().left / mashtab;
        var x2 = $("#ball2").position().left / mashtab;

        if (orangeBall.speed != 0) {
            var t1 = Math.floor((1000/orangeBall.speed)*100/2);
            if(orangeBall.direction > 0)
                var possb1 = Math.floor(x1+1000);
            else 
                var possb1 = Math.floor(x1-1000);
            var options1 = {
                duration: t1,
                easing: 'linear',
                complete: function(){
                    if(orangeBall.direction === -1 && blueBall.direction === -1 && orangeBall.speed > blueBall.speed){
                        $("#ball1").stop();
                    }
                    else if(orangeBall.direction === -1 && blueBall.direction === 1) {
                        $("#ball1").stop();
                    }
                    else{
                        $("#ball1, #ball2").stop();
                        clearInterval(borderBump);
                    }
                }
            };

            $("#ball1").animate({
                left: possb1+"px"
            }, options1);
        }

        if (blueBall.speed != 0) {
            var t2 = Math.floor((1000/blueBall.speed)*100/2);
            if(blueBall.direction > 0)
                var possb2 = Math.floor(x2+1000);
            else 
                var possb2 = Math.floor(x2-1000);
            var options2 = {
                duration: t2,
                easing: 'linear',
                complete: function(){
                    if(orangeBall.direction === 1 && blueBall.direction === 1 && orangeBall.speed < blueBall.speed){
                        $("#ball2").stop();
                    }
                    else if(orangeBall.direction === -1 && blueBall.direction === 1) {
                        $("#ball2").stop();
                    }
                    else{
                        $("#ball1, #ball2").stop();
                        clearInterval(borderBump);
                    }
                }
             };
     
             $("#ball2").animate({
                 left: possb2+"px"
             }, options2);
        }

        var inter = setInterval(function() {
            var coll = collision();
            if (coll == true) {
                clearInterval(inter);
                var newX1 = $("#ball1").position().left / mashtab;
                var newX2 = $("#ball2").position().left / mashtab;
                afterCollisionMove(newX1, newX2);
            }
        }, 10)
    };

    // Move ball after collision.
    function afterCollisionMove(afterX1, afterX2) {
        $("#ball1").stop()
        $("#ball2").stop()
        detectColl()
        var sol = Solution();
        var p1 = 0;
        var p2 = 0;
        if(sol.V1 > 0){
            p1 = afterX1 + 1000;
        } else {
            p1 = afterX1 - 1000;
        }
        if(sol.V2 > 0){
            p2 = afterX2 + 1000;
        } else {
            p2 = afterX2 - 1000;
        }

        // Formula t = S/V (where S = 500, V = ball speed)
        if(sol.V2 != 0)
            var t2 = Math.floor(1000/Math.abs(sol.V2))*100/2;
        if(sol.V1 != 0)
            var t1 = Math.floor(1000/Math.abs(sol.V1))*100/2;
// console.log('t2', t2, 't1', t2)
        // if(options.mode == "inelastic"){
            audio.play();
        // }
        var options1 = {
            duration: t1,
            easing: 'linear',
            complete: function(){
                $("#ball1").stop()
                clearInterval(borderBump);
            }
        };
        var options2 = {
           duration: t2,
           easing: 'linear',
           complete: function(){
               $("#ball2").stop();
               clearInterval(borderBump);
           }
        };

        if(sol.V1 == 0){
            $("#ball1").stop()
        } else {
            $("#ball1").animate({
                left: p1 + "px"
            }, options1);
        }
        
        if(sol.V2 == 0){
            $("#ball2").stop()
        } else {
             $("#ball2").animate({
                left: p2 + "px"
            }, options2);
        }
    };

    // Animation rotate ball.
    $.fn.rotateBall = function(dir1, speed, speed2) {
        var self = this;
        var elie = this;
        var degree = 0;

        // Formula V = (2*Pi*R/T) (where R-radius in centimeters, T-period, V-ball speed)
        var t = Math.floor((2*3.14*2.65)/Math.abs(speed));
        r = (2*3.14*2.65)/Math.abs(speed);
        self.interForRotate = setInterval(function(){
            options.collis = collision();
            if(options.collis){
                 dir1 = 1;
                if(speed2 < 0){
                   dir1 = -1
                }
                t = Math.floor((2*3.14*2.65)/Math.abs(speed2));
                r = (2*3.14*2.65)/Math.abs(speed2);
                clearInterval(self.interForRotate);
                rotate(t, r);
            }
        }, 10);

         rotateBefore(t, r);
         function rotateBefore(time, r ) {
             if(collision()) return;
            time = Math.abs(time);
            if(time==1) {
                if (dir1 === -1) {
                    if (degree < 0) degree = 360;
                    degree-=(Math.abs(1-r)-0.1);
                }
                else {
                    if (degree > 360) degree = 0;
                    degree+=(Math.abs(1-r)-0.1);
                }
            }  
            elie.css( {WebkitTransform: 'rotate(' + degree + 'deg)','-moz-transform': 'rotate(' + degree + 'deg)'});  
            if ($("#ball1").is(':animated') || $("#ball2").is(':animated')) {
                setTimeout(function() {
                    if (dir1 === -1) {
                        if (degree < 0) degree = 360;
                        --degree; rotateBefore(time, r);
                    }
                    else {
                        if (degree > 360) degree = 0;
                        ++degree; rotateBefore(time, r)
                    }
                }, time);
            }
            else {
                $(".b1").css({"transform": ''});
                $(".b2").css({"transform": ''});
                return;
            }
            
            if($("#ball1").is(':animated') == false ){
                $(".b1").css({"transform": ''});
                return;
            }
            if($("#ball2").is(':animated') == false ){
                $(".b2").css({"transform": ''});
                return;
            }
        }

        function rotate(time, r ) {
            time = Math.abs(time);
            if(time==1) {
                if (dir1 === -1) {
                    if (degree < 0) degree = 360;
                    degree-=(Math.abs(1-r)-0.1);
                }
                else {
                    if (degree > 360) degree = 0;
                    degree+=(Math.abs(1-r)-0.1);
                }
            }  
            elie.css( {WebkitTransform: 'rotate(' + degree + 'deg)','-moz-transform': 'rotate(' + degree + 'deg)'});  
            if ($("#ball1").is(':animated') || $("#ball2").is(':animated')) {
                setTimeout(function() {
                    if (dir1 === -1) {
                        if (degree < 0) degree = 360;
                        --degree; rotate(time, r);
                    }
                    else {
                        if (degree > 360) degree = 0;
                        ++degree; rotate(time, r)
                    }
                }, time);
            }
            else {
                $(".b1").css({"transform": ''});
                $(".b2").css({"transform": ''});
                return;
            }
            
            if($("#ball1").is(':animated') == false ){
                $(".b1").css({"transform": ''});
                return;
            }
            if($("#ball2").is(':animated') == false ){
                $(".b2").css({"transform": ''});
                return;
            }
        }
    };

    // Collision.
    function collision() {
        var ballwidth = 70;
        var x1 = Math.ceil( $("#ball1").offset().left / mashtab);
        var x2 = Math.ceil($("#ball2").offset().left / mashtab);

        if (x1 + ballwidth >= x2) {
           return true;
        } 
        else {
           return false;
        }
    };

    // Math random10 function.
    Math.round10 = function(value, exp) {
        return decimalAdjust('round', value, exp);
    };
    function decimalAdjust (type, value, exp) {
        if (typeof exp === 'undefined' || +exp === 0) {
          return Math[type](value);
        }
        value = +value;
        exp = +exp;
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
          return NaN;
        }
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    };

    // dot change to comma
    function changeDot(x) {
        return x.toString().split(".").join(",");
    }

// end   
})

// After window load show formula.
$(window).load(function(){
    $("#beforeMomentum").css({visibility: "visible"});
    $("#afterMomentum").css({visibility: "visible"});
})
