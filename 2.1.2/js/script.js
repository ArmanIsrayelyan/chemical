var zoomScale = 1;

var settings = {
    material: '',
    degree: 0,
    us: 0,
    uk: 0
};

var algorithm = {
    w1: 0,
    w2: 0,
    N: 0,
    F: 0,
    Fn: 0
};

// window.MathJax = {
//     tex2jax: {
//         inlineMath: [ ['$','$'] ],
//         processEscapes: true
//     }
// };

var slideCheck = false;
var enableReset = false;
var slider;

$( function() {
    $('.begin_helper_text').removeClass('hide');
    removeAllNot();
    start = false;

    $('.material-box').draggable({
        revert: "invalid",
        stack: ".material-box",
        helper: 'clone',
        cursor: 'pointer',
        start: function(event, ui) {
            hideNotifications();
            ui.position.left = 0;
            ui.position.top = 0;
        },
        drag: function(event, ui) {

            var changeLeft = ui.position.left - ui.originalPosition.left; // find change in left
            var newLeft = ui.originalPosition.left + changeLeft / (( zoomScale)); // adjust new left by our zoomScale

            var changeTop = ui.position.top - ui.originalPosition.top; // find change in top
            var newTop = ui.originalPosition.top + changeTop / zoomScale; // adjust new top by our zoomScale

            ui.position.left = newLeft;
            ui.position.top = newTop;

        }
    });

    $('.surface').droppable({
        accept: ".material-box",
        hoverClass: "ui-state-active",
        classes: {
            "ui-droppable-hover": "ui-state-hover"
        },
        drop: function(event, ui) {
            ui.helper.data('dropped', true);
		    var droppable = $(this);
            var draggable = ui.draggable;
            $(this).addClass("hide-bg");
            $('.materials .material-box').removeClass('items-hover');
            $(draggable).addClass('items-hover');
            var drag = draggable.clone().draggable({
                stack: ".material-box",
                helper: 'clone',
                cursor: 'pointer',
                disabled: true,
                start: function(event, ui) {
                    ui.helper.data('dropped', false);
                    // hideNotifications();
                },
                stop: function(event, ui) {
                    ui.helper[0].remove();
                    removeMaterial();
                    $('#rollar_free').empty();
                    $('#rollar_free').removeClass('finished');
                    enableCheck(false);
                    $(this).parent().droppable("enable");
                    this.remove();
                    settings.degree = 0;
                    $('.range1').val(0);
                    $('.surface').empty();
                    $('.choose-degree').attr({disabled: true});
                    $('.chosen').css('width', '0');
                    moveSurface(0, function() {});
                    $("#checkBtn").addClass("readyToCheck").prop('disabled', false);
                    checkavailability();
                }
            });
            // $(this).droppable("disable");
            $('.surface').empty();
            $('.choose-degree a').empty();
            $('.choose-degree input').attr({disabled: true});
            $('.chosen').css('width', '0');
            drag.appendTo(droppable);
            $('.box_helper_text').removeClass('hide');
            $('.chart-skills').addClass('hide-chart');
            $('.chart-number').addClass('hide-chart');
            removeAllNot();
            if(!$('.wooden-box').hasClass('active')){
            }
            $('.wooden-box').addClass('active');
            slideCheck = true;
            enableCheck(true);
            // $(this).children().removeClass("material-box").addClass("usedDraggableAtom");
            checkavailability()

        }
    });

    $('.wooden-box').on('touchstart click', function() {
        if($(this).hasClass('active')){
            hideNotifications();
            setTimeout(function () {
                showAngle();
            },1000);
            $('.surface').append('<div class="surface-box"></div>');
            $('.choose-degree input').attr({disabled: false});
            addSlide();
            showNumbers(settings.degree);
            $('.surface-box').addClass('show');
            $(this).removeClass('active');
            start = true;
            checkMaterials();
        }
    });

    $("#checkBtn").on('touchstart click', function() {
        if(enableReset){
            location.reload();
        }

    });

    $('body').on('touchstart click', function() {
        $('.box_choose_angle').addClass('hide');
    });
});

function addSlide() {
    if(slideCheck){
        $('.range1').on('touchstart input', function( e ) {
            var w = $(this).width()-($(this).width()*12)/100;
            var val_p = (e.target.value/60)*100
            $( ".degree-button" ).val( "$" + e.target.value );
            settings.degree = e.target.value;
            $('.line').css({width: (w * val_p)/100 + 'px'})
            hideNumbers();
            hideNotifications();
            if(!$('#rollar_free').hasClass('finished')){
                angleChart(e.target.value);
                checkMaterials();
                moveSurface(settings.degree, function() {

                });
            }
        });
        $('.range1').on('touchstart change', function(e) {
            if($('#rollar_free').hasClass('finished')) return;
            setTimeout(function () {
                friction(settings.material ,settings.degree);
            },500);
        })
        // $('.choose-degree').find('a').append('<img src="./img/Greenbutton.png" class="degree-button" alt="">');
        $( ".degree-button" ).val( "$" + $( "choose-degree input" ).val() );
    }
}

function removeMaterial(){
    $('.surface').removeClass('hide-bg');
    $('.material-box').removeClass('items-hover');
    $('.wooden-box').removeClass('active');
    $('.number-us').hide();
    $('.surface-box').removeClass('show');
    $('.box_helper_text').addClass('hide');
    $('.begin_helper_text').removeClass('hide');
    removeAllNot();
}

function countSlide(el) {
    var element = $( el ).attr('style');
    var str = element.slice(6, 15);
    var output = parseInt(str, 10);
    $('.chosen').css( 'width', output+'%' );
}

function friction(material, degree) {
    if(degree == 0){
        let angleOfFriction = 0;
        algorithm = {
            w1: 9.8,
            w2: 0,
            N: 9.8,
            F: 0,
            Fn: 0
        };
        checkAnimation(algorithm, 0);
    }else{
        if(material == 'wood'){
            let angleOfFriction = 17;
            getAlgorithm(degree, angleOfFriction);
        }else if(material == 'steel'){
            let angleOfFriction = 11;
            getAlgorithm(degree, angleOfFriction);
        }else if(material == 'zinc'){
            let angleOfFriction = 22;
            getAlgorithm(degree, angleOfFriction);
        }else if(material == 'concrete'){
            let angleOfFriction = 32;
            getAlgorithm(degree, angleOfFriction);
        }else if(material == 'glass'){
            let angleOfFriction = 45;
            getAlgorithm(degree, angleOfFriction);
        }else if(material == 'copper'){
            let angleOfFriction = 27;
            getAlgorithm(degree, angleOfFriction);
        }
    }
}

function getAlgorithm(currentDegree, angleOfFriction) {
    if(currentDegree < angleOfFriction){
        algorithm.w1 = 9.8 * Math.cos(currentDegree * Math.PI / 180);
        algorithm.w2 = 9.8 * Math.sin(currentDegree * Math.PI / 180);
        algorithm.N = algorithm.w1;
        algorithm.F = algorithm.w2;
        algorithm.Fn = algorithm.w2 - algorithm.F;
        checkAnimation(algorithm, 1);
    }else if(currentDegree == angleOfFriction){
        algorithm.w1 = 9.8 * Math.cos(currentDegree * Math.PI / 180);
        algorithm.w2 = 9.8 * Math.sin(currentDegree * Math.PI / 180);
        algorithm.N = algorithm.w1;
        algorithm.F = settings.us * algorithm.N;
        algorithm.w2 = algorithm.F;
        algorithm.Fn = algorithm.w2 - algorithm.F;
        checkAnimation(algorithm, 2);
    }else if(currentDegree > angleOfFriction){
        algorithm.w1 = 9.8 * Math.cos(currentDegree * Math.PI / 180);
        algorithm.w2 = 9.8 * Math.sin(currentDegree * Math.PI / 180);
        algorithm.N = algorithm.w1;
        algorithm.F = settings.uk * algorithm.N;
        algorithm.Fn = algorithm.w2 - algorithm.F;
        checkAnimation(algorithm, 3);
    }
}

function checkAnimation(numbers, move) {
    if(move == 1){
        activateArrows(numbers, move);
    }else if(move == 2){
        activateArrows(numbers, move);
    }else if(move == 3){
        activateArrows(numbers, move);
        lockSlider();
        setTimeout(function () {
            makeAnMove(numbers);
        },2000)
    }else if(move == 0){
        showNumbers(move);
    }
}

function activateArrows(numbers, move) {
    var elements = [
        'n-arrow',
        'w-arrow',
        'f-arrow',
        'w2-arrow',
        'fn-arrow'
    ];
    var symbols = [
        'N',
        'w_\\perp\\',
        'f',
        "w_\\parallel\\",
        'F_{net}'
    ];
    var nArrow = numbers.N;
    var w1Arrow = numbers.w1;
    var fArrow = numbers.F;
    var w2Arrow = numbers.w2;
    var fnArrow = numbers.Fn;
    var n = nArrow.toFixed(2).replace('.', ',');
    var w1 = w1Arrow.toFixed(2).replace('.', ',');
    var f = fArrow.toFixed(2).replace('.', ',');
    var w2 = w2Arrow.toFixed(2).replace('.', ',');
    var fn = fnArrow.toFixed(2).replace('.', ',');
    $('.surface-box').append('<div class="arrows">' +
        '<div class="n-arrow"><i class="up"></i><p><span></span> = '+ n +' <text class="n-symbol">N</text></p></div>' +
        '<div class="w-arrow"><i class="down"></i><p><span></span> = '+ w1 +' <text class="n-symbol">N</e></p></div>' +
        '<div class="f-arrow"><i class="left"></i><p><span></span> = '+ f +' <text class="n-symbol">N</text></p></div>' +
        '<div class="w2-arrow"><i class="right"></i><p><span></span> = '+ w2 +' <text class="n-symbol">N</text></p></div>' +
        '<div class="fn-arrow"><i class="right"></i><p><span></span> = '+ fn +' <text class="n-symbol">N</text></p></div>' +
        '</div>');
    for(var i = 0; i < 5; i++){
        mathEditor(elements[i], symbols[i])
    }
    setTimeout(function () {
        $('.arrows').show();
    },700)

    if(fnArrow == 0){
        $('.fn-arrow').addClass('fn-0');
    }else {
        var uk = settings.uk;
        $('.fn-arrow').removeClass('fn-0');
        $('.fn-arrow').css('width', fnArrow*20+'px');
        $('.number-us').empty().hide();
        $('.number-us').append(`<p><span></span> = ${uk.toFixed(2).replace('.', ',')}</p>`);
        mathEditor('number-us', '\\mu_\\mathrm{k}\\');

        setTimeout(function () {
            $('.number-us').show();
        },700)
    }
    $('.w-arrow').css('height', w1Arrow*20+'px');
    $('.n-arrow').css('height', nArrow*20+'px');
    $('.w2-arrow').css('width', w2Arrow*20+'px');
    $('.f-arrow').css('width', fArrow*20+'px');
}

function mathEditor( el, sym ) {

    var script = document.createElement('script');
    script.setAttribute('type', 'math/tex');
    script.textContent = sym;
    $(`.${el} p span`).html(script);

}

function makeAnMove(checkByNumbers) {
    var time = 0;
    var degree = settings.degree;

    if (degree == 0) {
        time = 0;
    }
    else if (degree == 5) {
        time = 4;
    }
    else  if (degree == 10) {
        time = 3.6;
    }
    else if (degree == 15) {
        time = 3.2
    }
    else if (degree == 20) {
        time = 2.8
    }
    else if (degree == 25) {
        time = 2.5
    }
    else if (degree == 30) {
        time = 2.1
    }
    else if (degree == 35) {
        time = 1.7
    }
    else if (degree == 40) {
        time = 1.3
    }
    else if (degree == 45) {
        time = 1
    }
    else if (degree == 50) {
        time = 0.6
    }
    else if (degree == 55) {
        time = 0.3
    }
    else if (degree == 60) {
        time = 0.1
    }

    if (time>0 && start && checkByNumbers.Fn !== 0) {
        var movedElement =  document.querySelector('.surface-box');
        movedElement.style.transition = 'all '+time+'s cubic-bezier(0.72, -0.07, 1, 1) 0s';
        movedElement.style.transform = 'translateX(900%)';

        setTimeout(function(){
            $('#rollar_free').css("left","790px");
            $('.surface-box').css({"transform":"translateX(0%)","left":"0"});
            $("#rollar_free").addClass('finished');
            $("#rollar_free").append($(".surface-box"));
            $(".surface-box").empty();
            enableCheck(true);
        },time*1000)
        start= false;
    }
}

function checkavailability() {
    let atoms = $(".material-box");
    for (var i=0; i<atoms.length; i++) {
        if (!atoms[i]['childElementCount']) {
            $("#checkBtn").removeClass("enoughElements");
            return;
        }
    }

    var connects = $(".droppable3Hand")
    for (i=0; i<connects.length; i++) {
        if (!connects[i]['childElementCount']) {
            $("#checkBtnw").removeClass("enoughElements");
            return;
        }
    }
    $("#checkBtn").addClass("enoughElements");
}

function enableCheck(flag){
    if(flag){
        $("#checkBtn").css("opacity","1");
        $("#checkBtn").css("cursor","pointer");
        enableReset = true;
    }else{
        $("#checkBtn").css("opacity",".5");
        $("#checkBtn").css("cursor","default");
        enableReset = false;
    }

}

function hideNotifications() {
    $(".begin_helper_text").addClass('hide');
    $(".box_helper_text").addClass('hide');
    $(".number-us").hide().empty();
}

function checkMaterials() {
    if($('.surface .material-box').hasClass('wood')){
        settings.us = 0.3;
        settings.uk = 0.2;
        settings.material = 'wood';
    }else if($('.surface .material-box').hasClass('steel')){
        settings.us = 0.2;
        settings.uk = 0.15;
        settings.material = 'steel';
    }else if($('.surface .material-box').hasClass('zinc')){
        settings.us = 0.4;
        settings.uk = 0.3;
        settings.material = 'zinc';
    }else if($('.surface .material-box').hasClass('concrete')){
        settings.us = 0.62;
        settings.uk = 0.45;
        settings.material = 'concrete';
    }else if($('.surface .material-box').hasClass('glass')){
        settings.us = 1;
        settings.uk = 0.4;
        settings.material = 'glass';
    }else if($('.surface .material-box').hasClass('copper')){
        settings.us = 0.5;
        settings.uk = 0.4;
        settings.material = 'copper';
    }
    var us = settings.us;
    $('.number-us').empty().hide();
    $('.number-us').append(`<p><span></span> = ${us.toFixed(2).replace('.', ',')}</p>`);
    mathEditor('number-us', '\\mu_\\mathrm{s}\\');

    setTimeout(function () {
        if(settings.degree !== 0){
            $('.number-us').show();
        }
    },700);
}

function showNumbers(degree) {
    if(degree == 0){
        var elements = [
            'n-arrow',
            'w-arrow'
        ];
        var symbols = [
            'N',
            'w'
        ];

        $('.surface-box').append('<div class="arrows">' +
            '<div class="n-arrow"><i class="up"></i><p><span></span> = 9,8 N</p></div>' +
            '<div class="w-arrow"><i class="down"></i><p><span></span> = 9,8 N</p></div>' +
            '</div>');

        for(var i = 0; i < 2; i++){
            mathEditor(elements[i], symbols[i])
        }
        setTimeout(function () {
            $('.arrows').show();
        },300)
    }
}

function hideNumbers() {
    $('.surface-box').empty();
}

function moveSurface(degree, callback) {
    if(degree == 0){
        $('.surface').css('-webkit-transform','rotate(0deg)');
        callback();
    }else if(degree == 5) {
        $('.surface').css('-webkit-transform','rotate(4deg)');
        callback();
    }else if(degree == 10) {
        $('.surface').css('-webkit-transform','rotate(8deg)');
        callback();
    }else if(degree == 15) {
        $('.surface').css('-webkit-transform','rotate(13deg)');
        callback();
    }else if(degree == 20) {
        $('.surface').css('-webkit-transform','rotate(17deg)');
        callback();
    }else if(degree == 25) {
        $('.surface').css('-webkit-transform','rotate(22deg)');
        callback();
    }else if(degree == 30) {
        $('.surface').css('-webkit-transform','rotate(26deg)');
        callback();
    }else if(degree == 35) {
        $('.surface').css('-webkit-transform','rotate(31deg)');
        callback();
    }else if(degree == 40) {
        $('.surface').css('-webkit-transform','rotate(36deg)');
        callback();
    }else if(degree == 45) {
        $('.surface').css('-webkit-transform','rotate(40deg)');
        callback();
    }else if(degree == 50) {
        $('.surface').css('-webkit-transform','rotate(45deg)');
        callback();
    }else if(degree == 55) {
        $('.surface').css('-webkit-transform','rotate(50deg)');
        callback();
    }else if(degree == 60) {
        $('.surface').css('-webkit-transform','rotate(55deg)');
        callback();
    }
}

function angleChart(checkNumber) {
    if(checkNumber !== 0){
        $('.chart-skills').removeClass('hide-chart');
        $('.chart-number').removeClass('hide-chart');
    }else{
        $('.chart-skills').addClass('hide-chart');
        $('.chart-number').addClass('hide-chart');
    }
    if(checkNumber == 5){
        $('.chart-skills li').css('transform', 'rotate(8deg)');
        mathEditor('chart-number', '\\ 5^{o} \\');
        $('.chart-number').addClass('chart-number-5');
    }else if(checkNumber == 10){
        $('.chart-skills li').css('transform', 'rotate(11deg)');
        mathEditor('chart-number', '\\ 10^{o} \\');
        $('.chart-number').addClass('chart-number-10');
    }else if(checkNumber == 15){
        $('.chart-skills li').css('transform', 'rotate(15deg)');
        mathEditor('chart-number', '\\ 15^{o} \\');
        $('.chart-number').addClass('chart-number-10');
    }else if(checkNumber == 20){
        $('.chart-skills li').css('transform', 'rotate(18deg)');
        mathEditor('chart-number', '\\ 20^{o} \\');
        $('.chart-number').addClass('chart-number-10');
    }else if(checkNumber == 25){
        $('.chart-skills li').css('transform', 'rotate(22deg)');
        mathEditor('chart-number', '\\ 25^{o} \\');
        $('.chart-number').addClass('chart-number-10');
    }else if(checkNumber == 30){
        $('.chart-skills li').css('transform', 'rotate(26deg)');
        mathEditor('chart-number', '\\ 30^{o} \\');
        $('.chart-number').addClass('chart-number-10');
    }else if(checkNumber == 35){
        $('.chart-skills li').css('transform', 'rotate(30deg)');
        mathEditor('chart-number', '\\ 35^{o} \\');
        $('.chart-number').addClass('chart-number-10');
    }else if(checkNumber == 40){
        $('.chart-skills li').css('transform', 'rotate(34deg)');
        mathEditor('chart-number', '\\ 40^{o} \\');
        $('.chart-number').addClass('chart-number-10');
    }else if(checkNumber == 45){
        $('.chart-skills li').css('transform', 'rotate(37deg)');
        mathEditor('chart-number', '\\ 45^{o} \\');
        $('.chart-number').addClass('chart-number-10');
    }else if(checkNumber == 50){
        $('.chart-skills li').css('transform', 'rotate(41deg)');
        mathEditor('chart-number', '\\ 50^{o} \\');
        $('.chart-number').addClass('chart-number-10');
    }else if(checkNumber == 55){
        $('.chart-skills li').css('transform', 'rotate(45deg)');
        mathEditor('chart-number', '\\ 55^{o} \\');
        $('.chart-number').addClass('chart-number-10');
    }else if(checkNumber == 60){
        $('.chart-skills li').css('transform', 'rotate(50deg)');
        mathEditor('chart-number', '\\ 60^{o} \\');
        $('.chart-number').addClass('chart-number-10');
    }
}

function removeAllNot() {
    setTimeout(function () {
        hideNotifications();
    }, 4000);
}

function removeAngle() {
    setTimeout(function () {
        $('.box_choose_angle').addClass('hide');
    }, 4000);
}

function showAngle() {
    $('.box_choose_angle').removeClass('hide');
    removeAngle();
}

function lockSlider() {
    $('.choose-degree input').attr({'disabled': true});
    $('.degree-button').css('cursor', 'auto');
}
lockSlider()
function openSlider(){
    $('.choose-degree input ').attr({disabled: false});
}
