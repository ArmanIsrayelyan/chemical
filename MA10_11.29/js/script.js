var zoomScale = 1;
var Page = function () {
    this.selectorText = ['parallelogram','rectangle','rhombus','square','trapezoid','kite'];
    this.anotherCheckbox = [true,true];
    this.popupText = {
        parallelogram:{
            rectangle: 'You have made a rectangle! Now try to make a rhombus or square.',
            rhombus: 'You have made a rhombus! Now try to make a rectangle or square.',
            square: 'You have made a square! Now test one of the other quadrilaterals.',
            parallelogram: 'You have made a parallelogram! Now try to make a rhombus, rectangle or a square.'
        },
        rectangle:{
            square: 'You have made a square! Now test one of the other quadrilaterals.',
            parallelogram: 'You have made a parallelogram! Now try to make a rhombus, rectangle or a square.',
        },
        rhombus:{
            square: 'You have made a square! Now test one of the other quadrilaterals.'
        },
        trapezoid:{
            parallelogram: 'You have made a parallelogram! Now try to make a rhombus, rectangle or a square.',
            rhombus: 'You have made a rhombus! Now try to make a rectangle or a square.',
            rectangle: 'You have made a rectangle! Now try to make a rhombus or a square.',
            square: 'You have made a square! Now test one of the other quadrilaterals.'
        },
        kite:{
            rhombus: 'You have made a rhombus! Now try to make a square.',
            square: 'You have made a square! Now test one of the other quadrilaterals.',
            parallelogram: 'You have made a parallelogram! Now try to make a rhombus, rectangle or a square.',
        }
    };
    this.sidePX = 0;
    this.degreeS = 0;
    this.paramForControll = {
        minDegree : 10,
        maxDegree : 260,
        minSide : 150,
        maxSide : 1000,
        minAreaSurface : 18000,
        minDiagonal: 70
    }
    this.degreeCalculate = 0;
    this.sideColor = ['black','red','blue'];
    this.kordinatDefault = {
        parallelogram:{
            a:{x:280,y:140},
            b:{x:610,y:140},
            c:{x:210,y:380},
            d:{x:540,y:380},
        },
        rectangle:{
            a:{x:250,y:140},
            b:{x:570,y:140},
            c:{x:250,y:370},
            d:{x:570,y:370},
        },
        rhombus:{
            a:{x: 343.6534030418985, y: 147},
            c:{x: 219, y: 374},
            b:{x: 601, y: 146},
            d:{x: 476.3465969581015, y: 373}
        },
        square:{
            a:{x:270,y:120},
            b:{x:550,y:120},
            c:{x:270,y:400},
            d:{x:550,y:400},
        },
        trapezoid:{
            a:{x:310,y:140},
            b:{x:530,y:140},
            c:{x:230,y:370},
            d:{x:610,y:370},
        },
        kite:{
            a:{x:270,y:220},
            b:{x:410,y:80},
            d:{x:550,y:220},
            c:{x:410,y:440},
        }
    }
    this.quadrilateralType = 'parallelogram';
}
Page.prototype = {
    init: function () {
        this.cleareBackgroundContent();
        this.addBackgroundContent();
        $('.angle .checkboxA').click();
        $('.diagonal .checkboxA').click();
        $('.parallelogram .checkbox').click();
    },
    addMainBlock: function (contents) {
        var self = this;
        var content = $('#contentImg');
        if(content.length > 0) {
            self.mainContent = content;
            content.append(contents);
            return true;
        } else {
            console.error('Not found div#contentImg block');
            return false;
        }
    },
    cleareBackgroundContent: function () {
        var self = this;
        var content = $('#contentImg');
        if(content.length > 0) {
            $('#contentImg').html('');
        }else {
            console.error('Not found div#contentImg block');
        }
    },
    addBackgroundContent: function () {
        var self = this;
        template = '<div class="left-block"></div>\
                    <div class="right-block"></div>';
        if(self.addMainBlock(template)) {
            self.addRightBlockContent();
            self.addLeftBlockContent();
        }
    },
    addRightBlockContent: function () {
        var self = this;
        for(var i=0;i<self.selectorText.length;i++){
            var text =  self.selectorText[i];
            var text2 = text.charAt(0).toUpperCase() + text.slice(1);
            var template = '<div class="'+text+' checkbox-text">\
                                <span class="checkbox"></span>\
                                <span class="text">'+text2+'</span>\
                            </div>'
            $('.right-block').append(template);
        }
        $('.right-block').append('<div class="another-checkbox">\
                                    <div class="angle">\
                                        <span class="checkboxA"></span>\
                                        <span class="text">Show angles</span>\
                                    </div>\
                                    <div class="diagonal">\
                                        <span class="checkboxA"></span>\
                                        <span class="text">Show diagonals</span>\
                                    </div>\
                                </div>')
        self.addEventCheckbox();
        self.addEventCheckboxA();
    },
    makeSvgTag: function (tag,attr,value){
        var el = document.createElementNS('http://www.w3.org/2000/svg', tag);
        for (var k in attr)
            el.setAttribute(k, attr[k]);
        el.innerHTML = value;
        return el;
    },
    toTop: function (selector) {
        var el = selector.clone();
        var elP = selector.parent();
        selector.remove();
        elP.append(el);
    },
    addLeftBlockContent: function () {
        var self = this;
        var template = '<div class="quadrilateral">\
                            <svg id="quadrilateralSVG">\
                                <path main/>\
                                <line diagonal1></line>\
                                <line diagonal2></line>\
                            </svg>\
                            <div id="degrees"></div>\
                        </div>'
        $('.left-block').append(template);
        for(var i=1;i<5;i++){
            var letter = self.changeNumToLetter(i);
            var letterR = letter;
            if(letter=='c') letterR = 'd';
            else if(letter=='d') letterR = 'c';
            $('#quadrilateralSVG').append(self.makeSvgTag("path",{['degree'+i]: '' }));
            $('#quadrilateralSVG').append(self.makeSvgTag("line",{['side'+i]:''}));
            $('#quadrilateralSVG').append(self.makeSvgTag("ellipse",{ 'degree-sign':letter},''));
        }
        self.toTop($('circle[net=a]'));
        self.toTop($('text[circle-text=a]'));
        self.toTop($('line[diagonal1]'));
        self.toTop($('line[diagonal2]'));
        self.toTop($('line[side1]'));
        self.toTop($('line[side2]'));
        self.toTop($('line[side3]'));
        self.toTop($('line[side4]'));
        for(var i=1;i<5;i++){
            var letter = self.changeNumToLetter(i);
            var letterR = letter;
            if(letter=='c') letterR = 'd';
            else if(letter=='d') letterR = 'c';
             $('#quadrilateralSVG').append(self.makeSvgTag("circle",{ 'net' : letter}));
            $('#quadrilateralSVG').append(self.makeSvgTag("text",{['circle-text']:letter,'net':letter},letterR.toUpperCase()));
            $('#quadrilateralSVG').append(self.makeSvgTag("rect",{ 'net' : letter}));
            $('#quadrilateralSVG').append(self.makeSvgTag("text",{ 'angle-text':letter,'net' : letter},''));
            $('#quadrilateralSVG').append(self.makeSvgTag("ellipse",{ 'degree-sign':letter},''));
        }
        this.changeDotToDefault('parallelogram');
        this.createQuadrilateral();
        this.addEventNet();
    },
    checkCorrectQuadrilateral: function (dots){
        var self = this;
        var valid = true;
        var width = $('#quadrilateralSVG').width();
        var height = $('#quadrilateralSVG').height();
        var allValue = this.getAllValue(dots)
        for(var i=1;i<5;i++){
            var letter = self.changeNumToLetter(i);
            var dot = dots[letter+'Net']
            if(!(allValue['side'+i+'H']>this.paramForControll.minSide&&allValue['side'+i+'H']<this.paramForControll.maxSide)){
                valid = false;
                break;
            }
            if(!(allValue['degree'+i]>this.paramForControll.minDegree&&allValue['degree'+i]<this.paramForControll.maxDegree)){
                valid = false;
                break;   
            }
            if(!(dot.x>20 && dot.x<width-20 && dot.y>20 && dot.y<height-20)){
                valid = false;
                break;
            }
            if(!self.checkTrueDot(letter,dot,dots)){
                valid = false;
                break;
            }
        }
        if(allValue.diagonal1<self.paramForControll.minDiagonal || allValue.diagonal2<self.paramForControll.minDiagonal){
            valid = false;
        }

        // for rhombus validation 

        if(dots['aNet'].x > dots['cNet'].x || dots['bNet'].x > dots['dNet'].x) {
            valid = false;
        }


        return valid;
    },
    getQuadrilateralArea: function(allValue){
        var a = Number(allValue.side1H);
        var b = Number(allValue.side2H);
        var c = Number(allValue.side3H);
        var d = Number(allValue.side4H);
        var diagonal1 = Number(allValue.diagonal1);
        var diagonal2 = Number(allValue.diagonal2);
        var S1 = (a+b+diagonal2)/2;
        var S2 = (d+c+diagonal1)/2;
        var Striangle1 = Math.sqrt(Math.abs(S1*(S1-a)*(S1-diagonal2)*(S1-b)));
        var Striangle2 = Math.sqrt(Math.abs(S2*(S2-c)*(S2-diagonal1)*(S2-d)));
        return Math.round(Striangle2+Striangle1);
    },
    checkTrueDot: function (letter,dot,dots){
        var self = this;
        var valid = false;
        switch(letter){
            case 'a':
            if(dot.x<dots.cNet.x && dot.y<dots.bNet.y) valid = true;
            if(this.quadrilateralType != 'kite' && dot.y<dots.dNet.y) valid = true;
            break;
            case 'b':
            if(dot.x<dots.dNet.x && dot.y>dots.aNet.y) valid = true;
            if(this.quadrilateralType != 'kite' && dot.y>dots.cNet.y) valid = true;
            break;
            case 'c':
            if(dot.x>dots.aNet.x && dot.y<dots.dNet.y) valid = true;
            if(this.quadrilateralType != 'kite' && dot.y<dots.bNet.y) valid = true;
            break;
            case 'd':
            if(dot.x>dots.bNet.x && dot.y>dots.cNet.y) valid = true;
            if(this.quadrilateralType != 'kite' && dot.y>dots.aNet.y) valid = true;
            break;
        }
        return valid;
    },
    addEventNet: function () {
        var self = this;
        $('circle,text').on('mousedown touchstart',function(e){
            var net = $(e.target).attr('net');
            $('circle').attr({class:''});
            $('circle[net='+net+']').attr({class:'active-circle'});
            self.moveNet = true;
            self.moveNetIndex = net;
            self.exDot = self[net+'Net']
        })
        $('#quadrilateralSVG').on('mousemove touchmove',function(e){
            if(!self.moveNet) return;
            var left = +$('.left-block').css('left').replace('px', '');
            var top = +$('.left-block').css('top').replace('px', '');
            var x = e.offsetX || (e.originalEvent.touches[0].pageX - $(this).offset().left) / zoomScale;
            var y = e.offsetY || (e.originalEvent.touches[0].pageY - $(this).offset().top ) / zoomScale;
            self.changeQuadrilateral(self.moveNetIndex,{x: x , y: y });
        })
        $('body').on('mouseup touchend',function(e){
            if(!self.moveNet) return;
            $('circle').attr({class:''});
            self.moveNet = false;
            self.checkQuadrilateral();
        })
    },
    createQuadrilateral: function (angleChange=true,sideChange=true) {
        var self = this;
        var mainPathStr = 'M ';
        for(var i=1;i<5;i++){
            var mainDot = this[self.changeNumToLetter(i)+'Net']; 
            var nextDot = this[self.changeNumToLetter(i+1)+'Net'];
            var prevDot = this[self.changeNumToLetter(i-1)+'Net'];
            var finalDot = this[self.changeNumToLetter(i+2)+'Net'];
            var letter = this.changeNumToLetter(i);
            mainPathStr+=mainDot.x+' '+mainDot.y+' L';
            $('line[side'+i+']').attr({
                'x1':mainDot.x,
                'y1':mainDot.y,
                'x2':nextDot.x,
                'y2':nextDot.y,
            });
            $('circle[net='+letter+']').attr({
                'cx':mainDot.x,
                'cy':mainDot.y
            });
            $('text[net='+letter+']').attr({
                'x':mainDot.x-10,
                'y':mainDot.y+10
            });
            if($('.angle').hasClass('active')){
                var pathText = this.getPathValue(i,mainDot,nextDot,prevDot,finalDot,angleChange);
                var diagonal = self.sideHeight(mainDot,finalDot);
                var angleTextKordinat = this.getCorrectPosition(mainDot,finalDot,35).obj
                var angleText = this.checkAngle(i)
                var anglePos = {};

                if(diagonal-70-2*78<0 || self.sideHeight(mainDot,nextDot)<200 || self.sideHeight(mainDot,prevDot)<200 || angleText<30 ){
                    if(this.quadrilateralType === 'kite' && letter == 'c' && mainDot.y < 80) {
                        anglePos = this.getAnglePosition(letter,angleTextKordinat,angleText);
                    } else {
                        anglePos = self.forsMajorDiagonal(letter,mainDot,angleText);
                    }
                }else {
                    anglePos = this.getAnglePosition(letter,angleTextKordinat,angleText);
                }
                $('rect[net='+letter+']').attr(anglePos.rectP).css({visibility:'visible'})
                $('text[angle-text='+letter+']').attr(anglePos.textP).css({visibility:'visible'})
                if(angleChange){
                    $('text[angle-text='+letter+']').html(String(angleText).replace('.',','));
                }
                $('path[degree'+i+']').attr('d',pathText).css({visibility:'visible'});
                $('ellipse[degree-sign='+letter+']').attr({cx:anglePos.degreeSignPos.x,cy:anglePos.degreeSignPos.y}).css({visibility:"visible"})    
            }else{
                $('path[degree'+i+']').css({visibility:'hidden'});
                $('rect[net='+letter+']').css({visibility:'hidden'});
                $('text[angle-text='+letter+']').css({visibility:'hidden'});
                $('ellipse[degree-sign='+letter+']').css({visibility:'hidden'})
            }
        }
        if($('.diagonal').hasClass('active')){
            $('line[diagonal1]').attr({'x1':this.aNet.x,'y1':this.aNet.y,'x2':this.dNet.x,'y2':this.dNet.y}).css({visibility:'visible'});
            $('line[diagonal2]').attr({'x1':this.cNet.x,'y1':this.cNet.y,'x2':this.bNet.x,'y2':this.bNet.y}).css({visibility:'visible'});
            this.checkForDiagonal();
        }else{
            $('line[diagonal1]').css({visibility:'hidden'});
            $('line[diagonal2]').css({visibility:'hidden'});
        }
        if($('.angle').hasClass('active')){
            this.checkForColorAngle();
        }
        $('path[main]').attr('d',mainPathStr.substring(0,mainPathStr.length - 1));
        if(!angleChange){
            $('path[degree1]').attr('class','color1');
            $('path[degree2]').attr('class','color1');
            $('path[degree3]').attr('class','color1');
            $('path[degree4]').attr('class','color1'); 
            $('line[diagonal1]').attr('class','diagonal-color1');
            $('line[diagonal2]').attr('class','diagonal-color1')
        }
        if(sideChange){
            this.checkForColor();
        }else {
            $('line[side1]').attr('class','side-color1');
            $('line[side2]').attr('class','side-color1');
            $('line[side3]').attr('class','side-color1');
            $('line[side4]').attr('class','side-color1');
        }
    },
    changeNumToLetter: function (num) {
        var letter = '';
        if(num>4) num = num-4;
        else if(num<1) num = num+4;
        switch(num){
            case 1:
                letter = 'a';
                break;
            case 2:
                letter = 'b';
                break;
            case 3:
                letter = 'd';
                break;
            case 4:
                letter = 'c';
                break;
        }
        return letter;
    },
    getPathValue: function (i,mainDot,nextDot,prevDot,finalDot,angleChange) {
        var self = this;
        var circleRadius = 43;
        var angle = self.checkAngle(i);
        var middleText = ' A '+ circleRadius+' '+circleRadius+' 0 0 0 '
        if(Math.abs(angle-90)<=self.degreeS || !angleChange){ 
            var dot = self.getCorrectPosition(
                self.getCorrectPosition(mainDot,nextDot,circleRadius).obj,
                self.getCorrectPosition(prevDot,finalDot,circleRadius).obj,circleRadius).obj;
            middleText = ' L'+dot.x+' '+dot.y+' L '
        }else if(Math.abs(angle)>180){
            middleText = ' A '+ circleRadius+' '+circleRadius+' 0 1 0 '
        }
        var pathText = 'M'+
            mainDot.x+' '+mainDot.y
            +' L'+
            this.getCorrectPosition(mainDot,nextDot,circleRadius).str
            +middleText+
            this.getCorrectPosition(mainDot,prevDot,circleRadius).str;
        return pathText;
    },
    getAnglePosition: function (letter,angleTextKordinat,text) {
        var rectP,textP;
        var heightBox = Number($('rect').css('height').replace('px',''));
        var widthBox = Number($('rect').css('width').replace('px',''));
        var widthText = Number($('text[angle-text='+letter+']').width());
        var changePX = 9;
        var changePxW = 4;
        if(letter=='a'){
            rectP = {x:angleTextKordinat.x,y:angleTextKordinat.y}
            textP = {x:angleTextKordinat.x+changePxW,y:angleTextKordinat.y+(heightBox)-changePX}
        }else if(letter=='b'){
            rectP = {x:angleTextKordinat.x,y:angleTextKordinat.y-(heightBox)}
            textP = {x:angleTextKordinat.x+changePxW,y:angleTextKordinat.y-changePX}
        }else if(letter=='c'){
            rectP = {x:angleTextKordinat.x-widthBox,y:angleTextKordinat.y}
            textP = {x:angleTextKordinat.x-widthBox+changePxW,y:angleTextKordinat.y+heightBox-changePX}
        }else if(letter=='d'){
            rectP = {x:angleTextKordinat.x-widthBox,y:angleTextKordinat.y-heightBox}
            textP = {x:angleTextKordinat.x-widthBox+changePxW,y:angleTextKordinat.y-changePX}
        }
        var widthText = String(text).replace('.','').length*15
        var degreeSignPos = {x:rectP.x+widthText+6,y:rectP.y+5};
        return {rectP,textP,degreeSignPos};
    },
    forsMajorDiagonal: function(letter,dot,text){
        var rectP,textP;
        var heightBox = Number($('rect').css('height').replace('px',''));
        var widthBox = Number($('rect').css('width').replace('px',''));
        var widthText = Number($('text[angle-text='+letter+']').width());
        var heightText = Number($('text[angle-text='+letter+']').height());
        var changePX = 9;
        var changePxW = 4;
        if(letter=='a'){
            rectP = {x:dot.x-widthBox,y:dot.y-30-heightBox}
            textP = {x:dot.x+changePxW-widthBox,y:dot.y-(heightBox)-changePxW}
        }else if(letter=='b'){
            rectP = {x:dot.x-widthBox,y:dot.y+30}
            textP = {x:dot.x+changePxW-widthBox,y:dot.y+heightBox+30-changePX}
        }else if(letter=='c'){
            rectP = {x:dot.x,y:dot.y-30-heightBox}
            textP = {x:dot.x+changePxW,y:dot.y-(heightBox)-changePxW}
        }else if(letter=='d'){
            rectP = {x:dot.x,y:dot.y+30}
            textP = {x:dot.x+changePxW,y:dot.y+(heightBox)+30-changePX}
        }
        if((letter=='a'||letter=='b')&&dot.x<70){
            rectP.x+=70;
            textP.x+=70;
        }else if((letter=='d'||letter=='c')&&(dot.x+70)>$('#quadrilateralSVG').width()-10){
            rectP.x-=70;
            textP.x-=70;
        }
        if((letter=='a'||letter=='c')&&dot.y<70){
            rectP.y+=70;
            textP.y+=70;
        }else if((letter=='b'||letter=='d')&&(dot.y+70)>$('#quadrilateralSVG').height()-10){
            rectP.y-=70;
            textP.y-=70;
        }
        if(letter=='c'&&dot.y<70){
            rectP.x+=30;
            textP.x+=30;
        } else if(letter=='a'&&dot.y<70){
            rectP.x-=30;
            textP.x-=30;
        } else if(letter=='d'&&(dot.y+70)>$('#quadrilateralSVG').height()-10) {
            rectP.x+=30;
            textP.x+=30;
        }
        else if(letter=='b'&&(dot.y+70)>$('#quadrilateralSVG').height()-10){
            rectP.x-=30;
            textP.x-=30;
        }
        var widthText = String(text).replace('.','').length*15
        var degreeSignPos = {x:rectP.x+widthText+6,y:rectP.y+5};
        return {letter, rectP,textP,degreeSignPos};
    },
    changeLetterToNum: function(letter){
        switch(letter){
            case 'a':
                return 1;
            case 'b':
                return 2;
            case 'd':
                return 3;
            case 'c':
                return 4;
        }
    },
    drawParallelogram: function(index,dot){
        var self = this;
        var dots = self.getDots();
        self.exDot = self[index+'Net'];
        var secondIndex = (index=='a')?'c':(index=='c'?'a':(index=='b'?'d':'b'))
        dots[index+'Net'] = dot;
        dots[secondIndex+'Net'].x += dot.x-self.exDot.x;
        dots[secondIndex+'Net'].y += dot.y-self.exDot.y;
        return dots;
    },
    drawRhombus: function(letter,dot){
        var self = this;

        // self.changeDotToDefault('rhombus');

        var dots =self.getDots();
        var index = self.changeLetterToNum(letter);
        var mainDot = self[letter+'Net'];
        var finalDotL = self.changeNumToLetter(index+2);
        var finalDot = self[finalDotL+'Net'];
        var sideIndex = (letter=='a')?'c':(letter=='c'?'a':(letter=='b'?'d':'b'));
        var frontIndex = (finalDotL=='a')?'c':(finalDotL=='c'?'a':(finalDotL=='b'?'d':'b'));
        
        var w = $('svg').width();
        var h = $('svg').height();

        // var size = -(self.sideHeight(mainDot, self[frontIndex+'Net']));
        
        // if(letter=='a'||letter=='b') size=-(size);
        // self.exDot = {x: mainDot.x, y: mainDot.y};

        // dots[sideIndex+'Net'] = { x: mainDot.x + size, y: mainDot.y };
        // dots[frontIndex+'Net'] = { x: finalDot.x - size, y: finalDot.y };
        var sideIndexX = (Math.pow(w - dot.x, 2) - Math.pow(dot.x, 2) + Math.pow(h - 2 * dot.y, 2)) / (2 * (w - 2 * dot.x));
        // console.log(sideIndexX, Math.pow(w - dot.x, 2) , Math.pow(dot.x, 2) , Math.pow(h - 2 * dot.y))
        dots[finalDotL+'Net'] = { x: w - dot.x, y: h - dot.y } 
        dots[letter+'Net'] = { x: dot.x, y: dot.y };
        dots[sideIndex + 'Net'] =  { x: sideIndexX, y: dot.y };
        // dots[sideIndex + 'Net'].y = dot.y;
        dots[frontIndex + 'Net'] =  { x: w - sideIndexX, y: h - dot.y };
        // console.log(letter, sideIndex, frontIndex, finalDotL)
        // $('ellipse[degree-sign=a]').attr({cx: dots.aNet.x, cy: dots.aNet.y});
        // $('ellipse[degree-sign=b]').attr({cx: dots.bNet.x, cy: dots.bNet.y});
        // $('ellipse[degree-sign=c]').attr({cx: dots.cNet.x, cy: dots.cNet.y});
        // $('ellipse[degree-sign=d]').attr({cx: dots.dNet.x, cy: dots.dNet.y});
        return dots;
    },
    drawSquare: function (letter,dot){
        var self = this;
        var dots = self.getDots();
        var type = '';
        var mainDot = dots[letter+'Net'];
        changedX = dot.x-self.exDot.x;
        changedY = dot.y-self.exDot.y;
        if(letter=='a'){
            if(changedX<0||changedY<0) type='big';
            else type = 'small'
        }else if(letter=='b'){
            if(changedX<0||changedY>0) type='big';
            else type = 'small'
        }else if(letter=='c'){
            if(changedX>0||changedY<0) type='big';
            else type = 'small'
        }else if(letter=='d'){
            if(changedX>0||changedY>0) type='big';
            else type = 'small'
        }
        var size = 0;
        if(type=="big"){
            size = 1;
            size = (Math.abs(changedY)>Math.abs(changedX))?changedY:changedX;
            size = Math.abs(size);
        }else {
            size = (Math.abs(changedY)>Math.abs(changedX))?changedY:changedX;
            size = -Math.abs(size);
        }
        self.exDot = dot;
        dots['aNet'] = {x:dots['aNet'].x-size,y:dots['aNet'].y-size};
        dots['bNet'] = {x:dots['bNet'].x-size,y:dots['bNet'].y+size};
        dots['cNet'] = {x:dots['cNet'].x+size,y:dots['cNet'].y-size};
        dots['dNet'] = {x:dots['dNet'].x+size,y:dots['dNet'].y+size};  
        return dots;
    },
    drawRectangle: function(letter,dot){
        var self = this;
        var center = {x:$('#quadrilateralSVG').width()/2,y:$('#quadrilateralSVG').height()/2};
        var sizeW = self.sideHeight(dot,{x:center.x,y:dot.y});
        var sizeH = self.sideHeight(dot,{y:center.y,x:dot.x});
        var dots = self.getDots();
        dots['aNet'] = {x:center.x-sizeW,y:center.y-sizeH};
        dots['bNet'] = {x:center.x-sizeW,y:center.y+sizeH};
        dots['cNet'] = {x:center.x+sizeW,y:center.y-sizeH};
        dots['dNet'] = {x:center.x+sizeW,y:center.y+sizeH};
        return dots;
    },
    drawTrapezoid: function(index,dot){
        var self = this;
        var dots = self.getDots();
        self.exDot = self[index+'Net'];
        var secondIndex = (index=='a')?'c':(index=='c'?'a':(index=='b'?'d':'b'))
        dots[index+'Net'] = dot;
        dots[secondIndex+'Net'].y += dot.y-self.exDot.y;
        return dots;
    },
    drawKite: function(letter,dot){
        var self = this;
        var dots = self.getDots();
        var sideDotIndex = (letter=='a')?'d':(letter=='d'?'a':(letter=='b'?'c':'b'));
        if(letter=='a'||letter=='d'){
            dots[sideDotIndex+'Net'].x = dot.x-2*(dot.x-dots['cNet'].x);
            dots[letter+'Net'].x = dot.x;
        }else {
            dots[letter+'Net'].y = dot.y;
        }
        self.exDot = dot;
        return dots;
    },
    getDots: function(){
        var self = this;
        var dots = {
            aNet:{
                x:self.aNet.x,
                y:self.aNet.y
            },
            bNet:{
                x:self.bNet.x,
                y:self.bNet.y
            },
            cNet:{
                x:self.cNet.x,
                y:self.cNet.y
            },
            dNet:{
                x:self.dNet.x,
                y:self.dNet.y
            }
        };
        return dots;
    },
    changeQuadrilateral: function (index,dot) {
        var self = this;
        var dots = self.getDots();
        var angleChange = true;
        var sideChange = true;
        switch(self.quadrilateralType){
            case 'parallelogram':
                dots = self.drawParallelogram(index,dot);
                break;
            case 'rectangle':
                dots = self.drawRectangle(index,dot);
                angleChange = false;
                break;
            case 'rhombus':
                dots = self.drawRhombus(index,dot);
                sideChange = false;
                break;
            case 'square':
                dots = self.drawSquare(index,dot);
                angleChange = false;
                sideChange = false;
                break;
            case 'trapezoid':
                dots = self.drawTrapezoid(index,dot);
                break;
            case 'kite':
                dots = self.drawKite(index,dot);
                break;
        }
        if(dots==null) return;
        if(self.checkCorrectQuadrilateral(dots)){
            self.aNet = dots.aNet;
            self.cNet = dots.cNet;
            self.bNet = dots.bNet;
            self.dNet = dots.dNet;
            self.createQuadrilateral(angleChange,sideChange);
         } 
         // else if(self.quadrilateralType == 'rhombus') {
         //    self.aNet = dots.aNet;
         //    self.cNet = dots.cNet;
         //    self.bNet = dots.bNet;
         //    self.dNet = dots.dNet;
         // //    $('ellipse[degree-sign=a]').attr({cx: dots.aNet.x, cy: dots.aNet.y});
         // //    $('ellipse[degree-sign=b]').attr({cx: dots.bNet.x, cy: dots.bNet.y});
         // //    $('ellipse[degree-sign=c]').attr({cx: dots.cNet.x, cy: dots.cNet.y});
         // //    $('ellipse[degree-sign=d]').attr({cx: dots.dNet.x, cy: dots.dNet.y});
         // //    console.log(dots)
         // //    self.exDot = dot;
         // }
    },
    getCorrectPosition: function (mainDot,secondDot,radius){
        var degree = this.angleDegreeWithDots(secondDot,mainDot,{x:mainDot.x,y:secondDot.y});
        var realW = Math.sin(degree*Math.PI/180)*radius;
        var realH = Math.cos(degree*Math.PI/180)*radius;
        var x,y;
        if(mainDot.x>secondDot.x){
            x=mainDot.x-realW;
        }else if(mainDot.x<secondDot.x){
            x=mainDot.x+realW;
        }else{
            x=mainDot.x;
        }
        if(mainDot.y>secondDot.y){
            y=mainDot.y-realH;
        }else if(mainDot.y<secondDot.y){
            y=mainDot.y+realH;
        }else{
            y=mainDot.y;
        }
        x = Math.round(x)
        y = Math.round(y)
        return {str:x+' '+y,obj:{x,y}};
    },
    angleDegreeWithDots: function (a,b,c){
        return Math.acos(this.sideHeight(c,b)/this.sideHeight(a,b))*180/Math.PI;
    },
    sideHeight: function(a,b){
        return Math.sqrt(Math.abs(Math.pow(a.x-b.x,2)+Math.pow(a.y-b.y,2)));
    },
    angleDegree: function(a,b,c){
        return Math.acos((Math.pow(a,2)+Math.pow(c,2)-Math.pow(b,2))/(2*a*c))/Math.PI*180;
    },
    checkQuadrilateral: function () {
        var allValue = this.getAllValue();
        var type = '';
        var degreeS = this.degreeS;
        var sidePX = this.sidePX;
        if(Math.abs(allValue.side1H-allValue.side3H)<=sidePX&&Math.abs(allValue.side2H-allValue.side4H)<=sidePX){
            type = 'parallelogram';
            var rhombus = rectangle = false;
            if(Math.abs(allValue.degree1-90)<=degreeS&&Math.abs(allValue.degree2-90)<=degreeS){
                rectangle = true;
                type = 'rectangle';
            } 
            if((Math.abs(allValue.side1H-allValue.side2H)<=sidePX && Math.abs(allValue.degree1-allValue.degree3)<=degreeS && Math.abs(allValue.degree2-allValue.degree4)<=degreeS) || this.quadrilateralType == 'rhombus'){
                rhombus = true
                type = 'rhombus';
            }
            if(rhombus&&rectangle){
                type = 'square';
            }
            if(!rhombus && 
                (Math.abs(allValue.side1H) - Math.abs(allValue.side3H)) <= sidePX && 
                (Math.abs(allValue.side2H) - Math.abs(allValue.side4H)) <= sidePX && 
                Math.abs(allValue.degree1-90)>degreeS){
                type = 'parallelogram';
            }
            if(this.addPopupQuadrilateral(type)) return;
        }
        if(this.addPopupQuadrilateral(type)) return;
        if(Math.abs(Math.round(allValue.degree1+allValue.degree2)-180)<=degreeS){
            type = "trapezoid";
        }
        if(this.addPopupQuadrilateral(type)) return;
        if(Math.abs(allValue.side1H-allValue.side2H)<=sidePX&&Math.abs(allValue.side3H-allValue.side4H)<=sidePX || Math.abs(allValue.side3H-allValue.side2H)<=sidePX&&Math.abs(allValue.side1H-allValue.side4H)<=sidePX){
            type = 'kite';
        }
        this.addPopupQuadrilateral(type);
    },
    checkDot: function(letter,dot){
        var self = this;
        var valid = false;
        var w = $('.quadrilateral').width()/2;
        var h = $('.quadrilateral').height()/2;
        var minSide = self.paramForControll.minSide/2;
        switch(letter){
            case 'a':
                if(dot.x<(w-minSide)&&dot.y<(h-minSide)){
                    valid = true;
                }
                break;
            case 'b':
                if(dot.x<(w-minSide)&&dot.y>(h+minSide)){
                    valid = true;
                }
                break;
            case 'd':
                if(dot.x>w+minSide&&dot.y>h+minSide){
                    valid = true;
                }
                break;
            case 'c':
                if(dot.x>(w+minSide)&&dot.y<(h-minSide)){
                    valid = true;
                }
                break;
        }
        return valid;
    },
    getAllValue: function (dots=null){
        var obj = {
            aNet : this.aNet,
            bNet : this.bNet,
            cNet : this.cNet,
            dNet : this.dNet
        }
        if(dots!==null){
            obj = dots;
        }
        var side1H = this.sideHeight(obj.aNet,obj.bNet).toFixed();
        var side2H = this.sideHeight(obj.aNet,obj.cNet).toFixed();
        var side3H = this.sideHeight(obj.cNet,obj.dNet).toFixed();
        var side4H = this.sideHeight(obj.dNet,obj.bNet).toFixed();
        var diagonal1 = this.sideHeight(obj.aNet,obj.dNet);
        var diagonal2 = this.sideHeight(obj.bNet,obj.cNet);
        var degree1 = this.checkAngle(1,obj);
        var degree2 = this.checkAngle(2,obj);
        var degree3 = this.checkAngle(3,obj);
        var degree4 = this.checkAngle(4,obj);
        return  {side1H,side2H,side3H,side4H,diagonal1,diagonal2,degree1,degree2,degree3,degree4};
    },
    correctI: function (i){
        if(i<1) return 5-(1-i);
        else if(i>4) return (i-4);
        else return i;
    },
    setColor: function (redColor,blueColor) {
        var self = this;
        $('line[side1]').attr('class','side-color3');
        $('line[side2]').attr('class','side-color3');
        $('line[side3]').attr('class','side-color3');
        $('line[side4]').attr('class','side-color3');
        for(var i=0;i<redColor.length;i++){
            var n = redColor[i];
            if(n==2) n = 4;
            else if(n==4) n = 2;
            $('line[side'+n+']').attr('class','side-color1');
        }
        for(var i=0;i<blueColor.length;i++){
            var n = blueColor[i];
            if(n==2) n = 4;
            else if(n==4) n = 2;
            $('line[side'+n+']').attr('class','side-color2');
        }
    },
    checkForDiagonal: function (){
        var allValue = this.getAllValue();
        $('line[diagonal1]').attr('class','diagonal-color1');
        $('line[diagonal2]').attr('class','diagonal-color2');
        if(Math.abs(allValue.diagonal2-allValue.diagonal1)<=this.sidePX){
            $('line[diagonal2]').attr('class','diagonal-color1');
        }
    },
    checkForColor: function(){
        var self = this;
        var allValue = self.getAllValue();
        var redColor = [],blueColor = [],degreeColorA=[],degreeColorB=[];
        var mainArray = [2,3,4];
        var sidePX = this.sidePX;
        var check = function(firstN,secondN){
            var firstSide = allValue['side'+firstN+'H']
            var secondSide = allValue['side'+secondN+'H']
            if(Math.abs(firstSide-secondSide)<=sidePX){
                if(Math.abs(allValue.side1H-firstSide)<=sidePX){
                    redColor.push(firstN,secondN)
                    degreeColorA.push(firstN,secondN)
                }else{
                    blueColor.push(firstN,secondN)
                    degreeColorB.push(firstN,secondN)
                }
            }else {
                if(Math.abs(firstSide-allValue.side1H)<=sidePX){
                    redColor.push(firstN)
                }
                if(Math.abs(secondSide-allValue.side1H)<=sidePX){
                    redColor.push(secondN)
                }
            }
        }
        if(Math.abs(allValue.side1H-allValue.side2H)<=sidePX){
            redColor.push(1,2)
            check(3,4)
        }else if(Math.abs(allValue.side1H-allValue.side3H)<=sidePX){
            redColor.push(1,3)
            check(2,4)
        }else if(Math.abs(allValue.side1H-allValue.side4H)<=sidePX){
            redColor.push(1,4)
            check(3,2)
        }
        self.setColor(redColor,blueColor);
    },
    checkForColorAngle: function (){
        var self = this;
        var allValue = self.getAllValue();
        var degree1=[],degree2=[];
        var mainArray = [2,3,4];
        var degreeS = this.degreeS;
        self.setDefaultColor();
        var angls = [allValue.degree1, allValue.degree2, allValue.degree3, allValue.degree4];
        for(var l = 0; l < angls.length; l++){
            for(var k = 0; k < angls.length; k++) {
                if(angls[l] - angls[k] <= degreeS && angls[l] - angls[k] >= 0) {
                    self.setColorDegree([l+1, k+1]);
                }
            }
        }
        
    },
    setDefaultColor: function () {
        $('path[degree1]').attr('class','color1');
        $('path[degree2]').attr('class','color2');
        $('path[degree3]').attr('class','color3');
        $('path[degree4]').attr('class','color4');
    },
    checkAngle: function (i,obj){
        var self = this;
        if(i==4){
            return Math.round((360-this.checkAngle(1)-this.checkAngle(2)-this.checkAngle(3))*10)/10;    
        }
        var mainDot = this[self.changeNumToLetter(i)+'Net']; 
        var nextDot = this[self.changeNumToLetter(i+1)+'Net'];
        var prevDot = this[self.changeNumToLetter(i-1)+'Net'];
        var anotherDot = this[self.changeNumToLetter(i-2)+'Net'];
        if(obj){
            mainDot = obj[self.changeNumToLetter(i)+'Net']; 
            nextDot = obj[self.changeNumToLetter(i+1)+'Net'];
            prevDot = obj[self.changeNumToLetter(i-1)+'Net'];
            anotherDot = obj[self.changeNumToLetter(i-2)+'Net'];
        }
        var side1 = self.sideHeight(mainDot,nextDot);
        var side2 = self.sideHeight(anotherDot,nextDot);
        var side3 = self.sideHeight(mainDot,anotherDot)
        var degree1 = this.angleDegree(side1,side2,side3);
        var side1 = self.sideHeight(mainDot,prevDot);
        var side2 = self.sideHeight(anotherDot,prevDot);
        var side3 = self.sideHeight(mainDot,anotherDot)
        var degree2 = this.angleDegree(side1,side2,side3);
        var degreeF = degree1+degree2;
        var degreeP = 360-self.degreeCalculate;
        var side1 = self.sideHeight(mainDot,nextDot);
        var side2 = self.sideHeight(mainDot,prevDot);
        var side3 = self.sideHeight(nextDot,prevDot);
        var mainDegree = this.angleDegree(side1,side3,side2)
        return Math.round(((degreeF>180)?degreeF:mainDegree)*10)/10;
    },
    setColorDegree: function (degree1, color){
        var self = this;
        var colorN = '';
        for(var i=0;i<degree1.length;i++){ 
            var n = degree1[i];
            if(i==0) {
                colorN = $('path[degree'+n+']').attr('class');
            }
            $('path[degree'+ degree1[i]+']').attr('class',colorN);
        }
    },
    addPopupQuadrilateral: function(type){
        var self = this;
        var exit = false;
        if(self.popupText[self.quadrilateralType]!==undefined){
            if(self.popupText[self.quadrilateralType][type]!==undefined){
                exit = true;
                $('.popup-quadro').remove();
                var template = '<div class="popup-quadro"><span class="close"></span><span class="text">'+
                                self.popupText[self.quadrilateralType][type]
                                +'</span></div>';
                self.addMainBlock(template)
                self.addEventClosePopup();
                self.mainContent.on('mousedown',function(){
                    $('.popup-quadro').remove();
                })
                // self.quadrilateralType = type;
                // $('.checkbox-text').removeClass('active');
                // $('.'+type+' ').addClass('active');

                // if(['rectangle', 'rhombus', 'square'].indexOf(type) != -1){
                //     $('.parallelogram').addClass('active');
                // }
                if(['rectangle', 'rhombus'].indexOf(type) == -1){
                    self.quadrilateralType = type;
                }
            }
        }
        return exit;
    },
    addEventClosePopup :function () {
        var self = this;
        $('.popup-quadro span.close').on('click',function(){
            $('.popup-quadro').remove();
        })
    },
    addEventCheckbox: function () {
        var self = this;
        $(document).off('click','.checkbox').on('click','.checkbox',function(e){
            var elem = $(e.target).parent()
            var className = elem.attr('class').split(' ')[0];
            if(elem.hasClass('rectangle')||elem.hasClass('rhombus')||elem.hasClass('square')){
                if(!elem.hasClass('active')){
                    $('.checkbox-text').removeClass('active');
                    elem.addClass('active');
                }else {
                    $('.checkbox-text').removeClass('active');
                    className = 'parallelogram';
                }
                $('.checkbox-text.parallelogram').addClass('active');
            }else{
                $('.checkbox-text').removeClass('active');
                elem.addClass('active');
            }
            self.changeDotToDefault(className);
            var sideChange = true;
            var angleChange = true;
            if(className=='rhombus'||className=='square'){
                sideChange = false
            }
            self.createQuadrilateral(angleChange,sideChange);
            self.quadrilateralType = className
        })
    },
    changeDotToDefault: function (name){
        this.aNet = this.kordinatDefault[name].a;
        this.bNet = this.kordinatDefault[name].c;
        this.cNet = this.kordinatDefault[name].b;
        this.dNet = this.kordinatDefault[name].d;
    },
    addEventCheckboxA: function () {
        var self = this;
        $(document).off('click','.checkboxA').on('click','.checkboxA',function(e){
            var elem = $(e.target).parent()
            var className = elem.attr('class').split(' ')[0];
            if(elem.hasClass('active')){
                elem.removeClass('active');
            }else{
                elem.addClass('active');
            }
            self.createQuadrilateral();
        })
    },
    setInstructionHelp: function (select) {
        var self = this;
        var helpList = self.getInstructionList();
        $(select || self.pageHelpText).html('');
        self.pageHelpText = select || self.pageHelpText;
        self.addMathText(self.pageHelpText, helpList);
        $(select || self.pageHelpText).parent().removeClass('help_list_' + self.page - 1).addClass('help_list_' + self.page);  
    },
    getInstructionList: function () {
        var self = this;
        helpList = ['Choose one of the types of quadrilaterals.',
                    'Move the vertices of the chosen quadrilateral to change its shape. The shape will change based on the conditions imposed on the quadrilateral. For example, a parallelogram can be changed to a rhombus, rectangle or square but not to a trapezoid or kite.',
                    'Test the different quadrilaterals to understand the relationship between them.',
                    'Click RESET to start again.'];
        return helpList;
    },
    addMathText: function (pageHelpText, helpList) {
        var ol = document.createElement('ol');
        ol.className = 'help-list';
        $(pageHelpText).html(ol);
        for(var i = 0; i< helpList.length; i++) {
            var id = 'li'+i+(new Date().getTime());
            MathJax.HTML.addElement(ol, "li", {id: id}, [helpList[i]]);
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, id]);
        }
    }
}