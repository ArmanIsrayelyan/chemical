var add_branch = document.querySelector('.buttons span:last-child');
var switch_controller = document.querySelector('.buttons span:first-child');

var add_branch_counter = 1;

var switchCounter = 1;

var isOpen = true;

// var containers = [...document.querySelectorAll('.drag_to')];


add_branch.addEventListener('click', function (event) {
    if (add_branch_counter > 2) {
        helperAlert(1, 'You have reached the maximum number of branches.')
        return
    };
    add_branch_counter++
    var left_side_container = document.querySelector('.left_side_image');
    left_side_container.style.backgroundImage = `url(./img/C${add_branch_counter}.png)`;
    var container = document.querySelectorAll('.drag_to.none');
    container[add_branch_counter - 2].style.display = 'block';

    if (add_branch_counter == 3) {
        add_branch.style.boxShadow = '1px 1px 1px 1px grey';
        add_branch.style.cursor = 'initial';
        add_branch.style.background = '#b5e6cd';
    }
    hide_content();
    if (!isOpen) {
        show_content()
    }

})

switch_controller.addEventListener('click', function (event) {
    if (isOpen) {

        switch_controller.innerHTML = 'Open switch';
        isOpen = false;
        closeSwitch()
    } else {
        switch_controller.innerHTML = 'Close switch';
        openSwitch()
        isOpen = true
    }
    switchCounter++
})


function reset() {
    hide_content();
    if (!isOpen) {
        // add_branch.click();
    }

    document.querySelector('.left_side_image').style.backgroundImage = 'url(./img/C1.png)';
    document.querySelector('.switch').style.transform = 'rotate(-40deg)';
    document.querySelector('.buttons span:last-child').style.boxShadow = '3px 3px 1px 1px grey';
    document.querySelector('.buttons span:last-child').style.opacity = '1';
    document.querySelector('.buttons span:last-child').style.cursor = 'pointer';


    var dropContainers  = [...document.querySelectorAll('.drag_to.none')]
    dropContainers.forEach(elem=>{
        elem.style.display = 'none'
    })

    add_branch_counter = 1;

    switchCounter = 1;

    isOpen = true;


    var containerImages = [...document.querySelectorAll('.img img')];
    containerImages.forEach(img => {
        var parentSelector = img.getAttribute('data-parent');
        var parent = document.querySelector('[data-p="' + parentSelector + '"]');
        img.style.transform = 'rotate(0deg)';
        parent.appendChild(img)
    })

   var toDrag =  [...document.querySelectorAll('.drag_to')];


   toDrag.forEach(elem=>{
       elem.style.background = 'transparent'
   })
}

document.addEventListener('click', function (event) {
    var target = event.target;
    if (!target.closest('#checkBtn')) return;
    
    window.location =  window.location
    // reset();
})
$(document).ready(function () {
    $('.disable_all').on('mousedown touchstart',function(event){
        this.style.display = 'none';
        document.querySelector('.alertContainer').remove();
    })
})
