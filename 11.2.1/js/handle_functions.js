function closeSwitch() {
    var switcher = document.querySelector('.switch');
    switcher.style.transform = 'rotate(0deg)';
    show_content()
}
function openSwitch() {
    var switcher = document.querySelector('.switch');
    switcher.style.transform = 'rotate(-40deg)';
    hide_content();
}


function show_content() {
    show_a()
    show_V()
}
function hide_content() {
    hide_v();
    hide_a();
}

function show_a() {
    var a = document.querySelectorAll('.a');
    for (var i = 0; i < add_branch_counter; i++) {
        a[i].style.display = 'block';
    }
    if (add_branch_counter == 2) {
        a[2].style.display = 'block';
    }
    if (add_branch_counter == 3) {
        a[3].style.display = 'block';
    }
}


function show_V() {
    var bulbs = document.querySelectorAll('.img .bulb');

    for (var i = 0; i < bulbs.length; i++) {
        var parent = bulbs[i].closest('.img');
        parent.nextElementSibling.style.display = 'block';
    }
}
function hide_v() {
    var bulbs = document.querySelectorAll('.checked_orange');
    for (var i = 0; i < bulbs.length; i++) {
        bulbs[i].style.display = 'none'
    }
}
function hide_a() {
    var a = document.querySelectorAll('.a');

    for (var i = 0; i < add_branch_counter + 1; i++) {
        a[i].style.display = 'none';
    }
}

function check_if_chain_disconnected() {

    var images_containers = document.querySelectorAll('.img');
    if (!images_containers[0].innerHTML || !images_containers[1].innerHTML) {
        return true
    }
    if (
        !images_containers[2].innerHTML && !images_containers[3].innerHTML && !images_containers[4].innerHTML
    ) {
        return true;
    }
    return false
}


function helperAlert(type, text, endCallback) {


    var disableContainer = document.querySelector('.disable_all');
    disableContainer.style.display = 'block';
    var alertContainer = document.createElement('div');
    alertContainer.classList.add('alertContainer');
    var alertText = document.createElement('p');
    alertText.innerHTML = text;

    if (type != 1) {
        var warning_img = document.createElement('img');
        warning_img.setAttribute('src', './img/warning.JPG');
        alertContainer.appendChild(warning_img);
        alertContainer.classList.add('warning');
    }

    alertContainer.appendChild(alertText)
    wrapper.appendChild(alertContainer);

    setTimeout(function () {

        // alertContainer.style.top = '280px';
        alertContainer.style.opacity = '1';
    })
    setTimeout(function () {

        // alertContainer.style.top = '280px';
        alertContainer.style.opacity = '0';

        setTimeout(function () {
            alertContainer.remove()
            disableContainer.style.display = 'none';


            endCallback ? endCallback() : null;
        }, 1100)
    }, 4000)
}




helperAlert(1, 'Use the tiles to complete the circuit. ')

function calculateResult() {
    var dragContainers = document.querySelectorAll('.drag_to .img')


    var isEmpty = check_if_is_empty(dragContainers);

    if (isEmpty) {
        setAllToZero()
    } else {
        setCharges(dragContainers)
    }

}



function check_if_is_empty(elements) {

    if (!elements[0].innerHTML) {
        return true
    } else if (!elements[1].innerHTML) {
        return true
    } else if (
        !elements[2].innerHTML &&
        !elements[3].innerHTML &&
        !elements[4].innerHTML

    ) {
        return true
    }
    return false
}

function setAllToZero() {
    var a = [...document.querySelectorAll('.a')];
    var v_w = [...document.querySelectorAll('.checked_orange')];


    a.forEach(elem => {
        elem.innerHTML = `<span class="number_1">0 </span> <span>A </span>`;
    })
    v_w.forEach(elem => {
        elem.innerHTML = `	<span class="number_1">0</span> <span>V ;</span> <span class="number_2">0</span> <span>W</span>`
    })
}


function setCharges(dragContainers) {


    var centralElements = [...document.querySelectorAll('.rotate_orange')];

    centralElements.forEach(elem=>{
        var image = elem.querySelector('.img');
        var a = document.querySelector('.'+elem.getAttribute('data-a'));
        a.querySelector('.number_1').innerHTML = 0
        if(elem.querySelector('.img img')){

            image.nextElementSibling.innerHTML = `<span class="number_1">6</span> <span>V ;</span> <span class="number_2">12</span> <span>W</span>`
            

            a.querySelector('.number_1').innerHTML = '2';
        }
    })

    var images = []

    dragContainers.forEach(elem => {
        var img = elem.querySelector('img');
        if (img) {
            images.push(img)
        }
    })

    var central_img1 = dragContainers[2].querySelector('img')
    var central_img2 = dragContainers[3].querySelector('img')
    var central_img3 = dragContainers[4].querySelector('img')

    if (images.length == 3 || images.length > 3 && (

        central_img1 && central_img1.classList.contains('wire') ||
        central_img2 && central_img2.classList.contains('wire') ||
        central_img3 && central_img3.classList.contains('wire')
    )) {

        firstChecking(dragContainers)
    }else if(images.length == 4){
        secondChecking(dragContainers)
    } else if(images.length == 5){
        thirdChecking(dragContainers)
    }
    //  else if (add_branch_counter == 2) {
    //     secondChecking(dragContainers)
    // } else {
    //     thirdChecking(dragContainers)
    // }
}
function firstChecking(dragContainers, branchesCount) {

    var dropElements = [];
    var bulbCount = 0;
    dragContainers




    dragContainers.forEach(elem => {
        if (elem.innerHTML) {
            var img = elem.querySelector('img')
            dropElements.push(img)
            if (img.classList.contains('bulb')) {
                bulbCount++
            }
        }
    })

    var aValue;
    var vValue;
    if (bulbCount == 1) {
        vValue = `	<span class="number_1">6</span> 
                    <span>V ;</span> 
                    <span class="number_2">12</span> 
                    <span>W</span></h1>`;
        aValue = 2
    } else if (bulbCount == 2) {
        vValue = `	<span class="number_1">3</span> 
        <span>V ;</span> 
        <span class="number_2">3</span> 
        <span>W</span></h1>`;
        aValue = 1
    }


    else if (bulbCount == 3) {
        vValue = `	<span class="number_1">2</span> 
        <span>V ;</span> 
        <span class="number_2">1,34</span> 
        <span>W</span></h1>`
        aValue = '0,67'
    }

    document.querySelector('.a1 .number_1').innerHTML = aValue;

    dropElements.forEach(elem => {
        if (elem.classList.contains('bulb')) {
            elem.closest('.img').nextElementSibling.innerHTML = vValue;
        }
    })

    if (branchesCount == 2) {
        document.querySelector('.a2 .number_1').innerHTML = aValue
    }



    var img_1 = dragContainers[0].querySelector('img');
    var img_2 = dragContainers[1].querySelector('img');
    var img_3 = dragContainers[2].querySelector('img');
    var img_4 = dragContainers[3].querySelector('img');
    var img_5 = dragContainers[4].querySelector('img');


    var zeroValue = `<span class="number_1">0</span> 
                        <span>V ;</span> 
                        <span class="number_2">0</span> 
                    <span>W</span></h1>`
    if (
        img_3 && img_3.classList.contains('wire') ||
        img_4 && img_4.classList.contains('wire') ||
        img_5 && img_5.classList.contains('wire')
    ) {

        dragContainers[2].nextElementSibling.innerHTML = zeroValue;
        dragContainers[3].nextElementSibling.innerHTML = zeroValue;
        dragContainers[4].nextElementSibling.innerHTML = zeroValue;



        var sidesVoltage

        if (img_1.classList.contains('bulb') || img_2.classList.contains('bulb')) {
            sidesVoltage = `<span class="number_1">6</span> 
                                    <span>V ;</span> 
                                    <span class="number_2">12</span> 
                                <span>W</span></h1>`

            document.querySelector('.a1 .number_1').innerHTML = 2;
            if (img_1.classList.contains('bulb') && img_2.classList.contains('bulb')) {
                sidesVoltage = `<span class="number_1">3</span> 
                                    <span>V ;</span> 
                                    <span class="number_2">3</span> 
                                <span>W</span></h1>`
                document.querySelector('.a1 .number_1').innerHTML = 1;
            }
        }


        dragContainers[0].nextElementSibling.innerHTML = sidesVoltage;
        dragContainers[1].nextElementSibling.innerHTML = sidesVoltage;

        var has_v_arr = []

        if(img_3 && img_3.classList.contains('wire')){
            has_v_arr.push(img_3)
        }
        if(img_4 && img_4.classList.contains('wire')){
            has_v_arr.push(img_4)
        }
        if(img_5 && img_5.classList.contains('wire')){
            has_v_arr.push(img_5)
        }

        var aElems =  [...document.querySelectorAll('.a')];
            aElems.shift();
        aElems.forEach(elem=>{
            elem.querySelector('.number_1').innerHTML = 0
        })
        var a_value =  document.querySelector('.a1 .number_1').innerHTML / has_v_arr.length
        var a_value_string = a_value.toString().replace('.', ',');

        has_v_arr.forEach(elem=>{
           var aElement =  document.querySelector('.'+elem.closest('.drag_to').getAttribute('data-a') + ' .number_1');
           aElement.innerHTML = a_value_string;
        })
    }

}

function secondChecking(dragContainers) {


    var central_img1 = dragContainers[0].querySelector('img')
    var central_img2 = dragContainers[1].querySelector('img')
    


    var sideElem
    var centralElements = [...document.querySelectorAll('.rotate_orange')];
    if(central_img1.classList.contains('bulb') || central_img2.classList.contains('bulb') ){
        if(central_img1.classList.contains('bulb')){
            sideElem = central_img1
        }else{
            sideElem = central_img2
        }
        console.log(sideElem);
        document.querySelector('.a1 .number_1').innerHTML = '1,33';
        sideElem.closest('.img').nextElementSibling.innerHTML = `<span class="number_1">4</span> <span>V ;</span> <span class="number_2">5,33</span> <span>W</span>`;
       

        centralElements.forEach(elem=>{
            var image = elem.querySelector('.img');
            var a = document.querySelector('.'+elem.getAttribute('data-a'));
            a.querySelector('.number_1').innerHTML = 0
            if(elem.querySelector('.img img')){
                image.nextElementSibling.innerHTML = `<span class="number_1">2</span> <span>V ;</span> <span class="number_2">1,33</span> <span>W</span>`
                

                a.querySelector('.number_1').innerHTML = '0,67';
            
            }
        })
    }else{
        document.querySelector('.a1 .number_1').innerHTML = '4';

        centralElements.forEach(elem=>{
            var image = elem.querySelector('.img');
            var a = document.querySelector('.'+elem.getAttribute('data-a'));
            a.querySelector('.number_1').innerHTML = 0
            if(elem.querySelector('.img img')){

                image.nextElementSibling.innerHTML = `<span class="number_1">6</span> <span>V ;</span> <span class="number_2">12</span> <span>W</span>`
                

                a.querySelector('.number_1').innerHTML = '2';
            }
        })

    }

}

function thirdChecking(dragContainers) {
    
    var centralElements = [...document.querySelectorAll('.rotate_orange')];
    document.querySelector('.a1 .number_1').innerHTML = '6';

    centralElements.forEach(elem=>{
        var image = elem.querySelector('.img');
        var a = document.querySelector('.'+elem.getAttribute('data-a'));
        a.querySelector('.number_1').innerHTML = 0
        if(elem.querySelector('.img img')){

            image.nextElementSibling.innerHTML = `<span class="number_1">6</span> <span>V ;</span> <span class="number_2">12</span> <span>W</span>`
            

            a.querySelector('.number_1').innerHTML = '2';
        }
    })
}