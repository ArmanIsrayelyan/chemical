var mashtab = 1
function offset(el) {
    var rect = el.getBoundingClientRect(),
        scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
        scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
}

// function percCulc (price, sale) {


//     var result = (price * sale) / 100;
//     //var saleprice = price - savings;
//     return result.toLocaleString();


// }


$(document).on('mousedown touchstart', function (e) {


    var parent
    var dragElement = e.target;

    if (!dragElement.closest('img') || !dragElement.closest('div')) return;

    var coords, shiftX, shiftY, firstX, firstY, endX, endY;

    dragElement.style.transition = 'all 0s linear'

    startDrag(e.originalEvent.touches? e.originalEvent.touches[0].clientX : e.clientX,
        e.originalEvent.touches? e.originalEvent.touches[0].clientY :  e.clientY);
    function m(clientX, clientY){
        moveAt(clientX, clientY);
        show_background(endY, endX, dragElement)
    }
    $(document).off('mousemove').on('mousemove', function (e) {
        m(e.clientX, e.clientY);
    });
    $(document).on('touchmove', function (e) {
        var clientX1 = e.originalEvent.touches[0].clientX;
        var clientY1 = e.originalEvent.touches[0].clientY;
        m(clientX1, clientY1)
    })

    $(dragElement).on('mouseup touchend', function () {
        finishDrag();
        $(document).off('mousemove');
    });


    function startDrag(clientX, clientY) {

        shiftX = clientX - dragElement.getBoundingClientRect().left ;
        shiftY = clientY - dragElement.getBoundingClientRect().top  - document.documentElement.scrollTop;

        dragElement.style.position = 'absolute';


        if (!dragElement.getAttribute('data-parent')) {
            var p = dragElement.closest('div').getAttribute('data-p')
            dragElement.setAttribute('data-parent', p)
        }

        var pData = dragElement.getAttribute('data-parent');
        parent = document.querySelector('div[data-p="' + pData + '"]');


        var parentPosition = offset(parent);
        firstX = parentPosition.left;
        firstY = parentPosition.top;

        document.body.appendChild(dragElement);

        moveAt(clientX, clientY);
    };

    function finishDrag() {



        var dropTo = myFinishDrag(endY, endX, dragElement);
        
        dragElement.style.transition = 'all 0.2s linear'

        if (dropTo) {

            dragElement.style.top = dropTo.top + 'px';
            dragElement.style.left = dropTo.left + 'px';

            if (dropTo.rotate) {
                dragElement.style.transform = 'rotate(90deg)';
            } else {
                dragElement.style.transform = 'rotate(0deg)';
            }
            parent = dropTo.parent
            if (dropTo.parent.innerHTML) {
               
                var last_image = dropTo.parent.querySelector('.img img');
                var parent_selector = last_image.getAttribute('data-parent')
                var last_parent = document.querySelector('div[data-p="'+parent_selector+'"]');
                var p_position = offset(last_parent);
                
                
                document.body.appendChild(last_image);
                setTimeout(function(){
                    last_image.style.transition = 'all 0.2s linear';
                
                    last_image.style.position = 'absolute';
                    last_image.style.left = p_position.left + 'px';
                    last_image.style.top = p_position.top + 'px';

                    
                    last_image.style.transform = 'rotate(0deg)';
                    setTimeout(function(){
                        last_parent.appendChild(last_image);
                        last_image.style.position = 'static';
                        
                        
                    },500)
                },0)
                
             
            }
            

        } else {

            dragElement.style.top = firstY + 'px';
            dragElement.style.left = firstX + 'px';
            dragElement.style.transform = 'rotate(0deg)';

        }


        document.onmousemove = null;
        dragElement.onmouseup = null;

        setTimeout(function () {
           
            parent.appendChild(dragElement)
            calculateResult()
            if(!isOpen){
                hide_content();
                show_content();
            }
            dragElement.style.position = 'initial'
        }, 500)


    }

    function moveAt(clientX, clientY) {

        var newX = clientX - shiftX;
        var newY = clientY - shiftY;

        var newBottom = newY + dragElement.offsetHeight;
        if (newBottom > document.documentElement.clientHeight) {
            var docBottom = document.documentElement.getBoundingClientRect().bottom;

            var scrollY = Math.min(docBottom - newBottom, 10);

            if (scrollY < 0) scrollY = 0;

            window.scrollBy(0, scrollY);

            newY = Math.min(newY, document.documentElement.clientHeight - dragElement.offsetHeight);
        }


        if (newY < 0) {

            var scrollY = Math.min(-newY, 10);
            if (scrollY < 0) scrollY = 0;

            window.scrollBy(0, -scrollY);

            newY = Math.max(newY, 0);
        }

        if (newX < 0) newX = 0;
        if (newX > document.documentElement.clientWidth - dragElement.offsetWidth) {
            newX = document.documentElement.clientWidth - dragElement.offsetWidth;
        }

        dragElement.style.left = newX + 'px';
        dragElement.style.top = newY + 'px';

        endX = newX
        endY = newY

    }

    return false;
})

function myFinishDrag(top, left, dragElement) {
    var dropElements = [...document.querySelectorAll('.drag_to')];

    var isDropable
    dropElements.forEach(elem => {
        var elemPosition = offset(elem);
        // console.log(top, left,elemPosition)
        if (
            left > elemPosition.left - 20 &&
            left < elemPosition.left + 20 &&
            top > elemPosition.top - 20 &&
            top < elemPosition.top + 20
        ) {
            if (
                elem.closest('.rotate_blue') && dragElement.closest('.wire') ||
                elem.closest('.rotate_orange') && dragElement.closest('.bulb')

            ) {
                elemPosition.rotate = true;
            }


            elemPosition.parent = elem.closest('div').querySelector('.img')
            isDropable = elemPosition

        }

    })
    return isDropable
}



function show_background(top, left, dragElement){
    var dropElements = [...document.querySelectorAll('.drag_to')];

    var isDropable
    dropElements.forEach(elem => {
        var elemPosition = offset(elem);
        // console.log(top, left,elemPosition)
        if (
            left > elemPosition.left - 20 &&
            left < elemPosition.left + 20 &&
            top > elemPosition.top - 20 &&
            top < elemPosition.top + 20
        ) {
            

            elem.style.background = '#ffffff7a'
    

        }else{

            elem.style.background = 'transparent'
        }

    })
}
