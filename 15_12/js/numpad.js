var mashtabControl = 1
var Numpad = function (inputs, nompad_block) {
    this.active_input_index = 0;
    this.plusMinusValue = [];
    this.inputs = inputs; // array
    this.block = nompad_block; // html element
    this.draggable = false;
    this.buttons = [
        {
            value: '7',
            class_name: 'num row1 column1',
            clickEvent: this.sevenClick
        },
        {
            value: '8',
            class_name: 'num row1 column2',
            clickEvent: this.eightClick
        },
        {
            value: '9',
            class_name: 'num row1 column3',
            clickEvent: this.nineClick
        },
        {
            value: '4',
            class_name: 'num row2 column1',
            clickEvent: this.fourClick
        },
        {
            value: '5',
            class_name: 'num row2 column2',
            clickEvent: this.fiveClick
        },
        {
            value: '6',
            class_name: 'num row2 column3',
            clickEvent: this.sixClick
        },
        {
            value: '1',
            class_name: 'num row3 column1',
            clickEvent: this.oneClick
        },
        {
            value: '2',
            class_name: 'num row3 column2',
            clickEvent: this.twoClick
        },
        {
            value: '3',
            class_name: 'num row3 column3',
            clickEvent: this.threeClick
        },
        {
            value: '0',
            class_name: 'num row4 column1',
            clickEvent: this.zeroClick
        },
        {
            value: '',
            class_name: 'sign row2 column4',
            clickEvent: this.plusMinusClick
        },
        {
            value: '',
            class_name: 'comma row4 column2',
            clickEvent: this.commaClick
        },
        {
            value: '',
            class_name: 'degree_of_ten row4 column3',
            clickEvent: this.multiplyBy10PowXClick
        },
        {
            value: '',
            class_name: 'remove-button row1 column4',
            clickEvent: this.removeClick
        }
    ]
};

Numpad.prototype.init = function () {
    this.block.append(this.getHeader());
    this.block.append(this.getButtons());
    this.block.append(this.getDone());
    this.addNumpadCloseEvent();
    this.addEventbuttons();
    this.addEventDone();
    this.initInputsevent();
    setTimeout(this.initDraggableEvent.bind(this), 0);
    $('.degree_of_ten').removeClass('able').addClass('disable');
    this.block.find('.button').on('mousedown', function (e) {
        e.preventDefault();
        e.stopPropagation();
    })
    this.block.find('.done-button-block').on('mousedown', function (e) {
        e.preventDefault();
        e.stopPropagation();
    })
};

Numpad.prototype.initDraggableEvent = function() {
    var self = this;
    this.block[0].addEventListener('mousedown', self.eventMouseDown.bind(self));
    this.block[0].addEventListener('touchstart', self.eventMouseDown.bind(self));

    document.addEventListener('mousemove', self.eventMouseMove.bind(self));
    document.addEventListener('touchmove', self.eventMouseMove.bind(self));

    document.addEventListener('mouseup', self.eventMouseUp.bind(self));
    document.addEventListener('touchend', self.eventMouseUp.bind(self));
};

Numpad.prototype.eventMouseDown = function(e) {
    this.draggable = true;
};

Numpad.prototype.eventMouseMove = function(event) {
    if(!this.draggable) return;

    var cursor_position = {
        x: (this.block.width() / 2) * 0.8, // added scale value
        y: 10 * 0.8 // added scale value
    };

    this.block[0].style.left = ((event.clientX || event.touches[0].clientX) / mashtabControl) - w/mashtabControl - cursor_position.x * mashtabControl + 'px';
    this.block[0].style.top = (event.clientY || event.touches[0].clientY) / mashtabControl - h/mashtabControl - cursor_position.y * mashtabControl+ 'px';
};

Numpad.prototype.eventMouseUp = function(e) {
    this.draggable = false;
};

Numpad.prototype.focusEvent = function (i) {
    this.active_input_index = i;
    this.showNumpad();
}

Numpad.prototype.initInputsevent = function() {
    var self = this;
    for(let i = 0; i < this.inputs.length; i++) {
        self.inputs[i].addEventListener('focus', function() {
            self.focusEvent.call(self, i);
        })
    }
};

Numpad.prototype.getButton = function (button_text, class_name = '') {
        return "<div class='button able "+ class_name +"' > \
                    <div>" + button_text + "</div> \
                </div>";
};

Numpad.prototype.getButtons = function () {
    var buttons = '';
    for(var i = 0; i < this.buttons.length; i++) {
        buttons += this.getButton(this.buttons[i].value, this.buttons[i].class_name);
    }
    return buttons;
};

Numpad.prototype.getHeader = function () {
   return '<div  class="header-block"> \
                <div class="close">\
                <div>\
            </div>';
};

Numpad.prototype.getDone = function () {
        return "<div class='done-button-block column2 row5'>\
                    <div></div>\
                </div>";
};

Numpad.prototype.addEventDone = function (f =function() {}) {
    var self = this;
    document.querySelector('.done-button-block div').addEventListener('click', function (e) {
        self.active_input_index++;
        if(self.active_input_index >= self.inputs.length) {
            self.closeNumpad();
        } else {

        }
        f.call(self, e);
    })
};

 Numpad.prototype.addEventbuttons = function (f =function() {}) {
    var buttons = document.querySelectorAll('.button div');
    for(var i = 0; i < buttons.length; i++) {
        this.addEventbutton(buttons[i], this.buttons[i].clickEvent);
    }
};

Numpad.prototype.addEventbutton = function (elem, f =function() {}) {
    var self = this;
    elem.addEventListener('click', function (e) {
        f.call(self, e);
    })
};

Numpad.prototype.closeNumpad = function() {
    this.block.css({display: 'none'});
};

Numpad.prototype.showNumpad = function() {
     this.block.css({display: 'block'});
}

Numpad.prototype.addNumpadCloseEvent = function () {
    var self = this;
    document.querySelector('.header-block .close').addEventListener('click', function (e) {
        self.closeNumpad.call(self);
    })
};

Numpad.prototype.addSimbolOnInput = function (simbole, e, f = function() {}) {
   var input = this.inputs[this.active_input_index];
   input.value += simbole;
   if(this.inputs[this.active_input_index].value.indexOf(',') != -1 ||
        this.inputs[this.active_input_index].value.length == 0) {
       // disable comma
       $('.comma').removeClass('able').addClass('disable');
   } else {
        $('.comma').removeClass('disable').addClass('able');
   }
   f();
   e.preventDefault();
   e.stopPropagation();
};

// buttons event

Numpad.prototype.zeroClick = function (e) {
    this.addSimbolOnInput('0', e);
};

Numpad.prototype.oneClick = function (e) {
    this.addSimbolOnInput('1', e);
};

Numpad.prototype.twoClick = function (e) {
    this.addSimbolOnInput('2', e);
};

Numpad.prototype.threeClick = function (e) {
    this.addSimbolOnInput('3', e);
};

Numpad.prototype.fourClick = function (e) {
    this.addSimbolOnInput('4', e);
};

Numpad.prototype.fiveClick = function (e) {
    this.addSimbolOnInput('5', e);
};

Numpad.prototype.sixClick = function (e) {
    this.addSimbolOnInput('6', e);
};

Numpad.prototype.sevenClick = function (e) {
    this.addSimbolOnInput('7', e);
};

Numpad.prototype.eightClick = function (e) {
    this.addSimbolOnInput('8', e);
};

Numpad.prototype.nineClick = function (e) {
    this.addSimbolOnInput('9', e);
};

Numpad.prototype.commaClick = function (e) {
    var str = this.inputs[this.active_input_index].value
    if(str.indexOf(',') != -1 || str.length == 0) {
        return;
    }
    if(str.length == 1 && str == '-') {
        // or added zero
        return;
    }
    this.addSimbolOnInput(',', e);
};

Numpad.prototype.plusMinusClick = function (e){
    if(this.plusMinusValue[this.active_input_index]) {
        this.inputs[this.active_input_index].value = this.inputs[this.active_input_index].value.toString().replace('-', '');
        this.plusMinusValue[this.active_input_index] = false;
    } else {
        this.plusMinusValue[this.active_input_index] = true;
        this.inputs[this.active_input_index].value = '-' + this.inputs[this.active_input_index].value
    }
}
Numpad.prototype.multiplyBy10PowXClick = function (e){
    // this.addSimbolOnInput('0');
};

Numpad.prototype.removeClick = function (e) {
    var str = this.inputs[this.active_input_index].value;
    str = str.split('');
    var s = str[str.length-1];
    delete str[str.length-1];
    this.inputs[this.active_input_index].value = str.join('');
    if(s == '-') {
        this.plusMinusValue[this.active_input_index] = false;
    }
    if(s == ',') {
        $('.comma').removeClass('disable').addClass('able');
    }
    e.preventDefault();
    e.stopPropagation();
};

