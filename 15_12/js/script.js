var Page = function () {
    this.slideMove = false;
    this.slideMoveIndex = 0;
    this.radius = 20;
}
Page.prototype = {
    init: function () {
        this.cleareBackgroundContent();
        this.addBackgroundContent();
        this.changeSlideEvent();
        $('.checkbox-1')[0].checked = true
        $('.checkbox-1').closest('div').find('.formula div.content').addClass('show');

    },
    addMainBlock: function (contents) {
        var self = this;
        var content = $('#contentImg');
        if(content.length > 0) {
            self.mainContent = content;
            content.append(contents);
            return true;
        } else {
            console.error('Not found div#contentImg block');
            return false;
        }
    },
    cleareBackgroundContent: function () {
        var self = this;
        var content = $('#contentImg');
        if(content.length > 0) {
            $('#contentImg').html('');
        }else {
            console.error('Not found div#contentImg block');
        }
    },
    addBackgroundContent: function () {
        var self = this;
        template = '<div class="left-block"></div>\
                    <div class="right-block"></div>';
        if(self.addMainBlock(template)) {
            self.addRightBlockContent();
            self.addLeftBlockContent();
            self.addTopFormulaBlock();
            self.addBottomFormulaBlock();
            self.addSlideBlock();
        }
    },
    addRightBlockContent: function () {
        var self = this;
        $('.right-block').append('<div class="top-formula"></div>\
                                    <div class="bottom-formula"></div>');
    },
    getFormulaTemplate: function (i) {
        return '<label class="container">\
                  <input type="checkbox" class="checkbox-'+i+'">\
                  <span class="checkmark"></span>\
                </label>'
    },
    addEventShowFormula: function (i) {
        var self = this;
        $('.checkbox-'+i).change(function () {
            if(!$('.checkbox-'+(3-i))[0].checked){
                $(this)[0].checked = true
                return
            }
            if($(this)[0].checked) {
                self['addFormula'+i+'Content']();
                $(this).closest('div').find('.formula div.content').addClass('show');
            } else {
                $(this).closest('div').find('.formula div.content').removeClass('show');
            }
        })
        $('.checkbox-'+i).change(function () {
            self.changeImagePath();
        })
    },
    changeImagePath: function () {
        if($('.checkbox-1')[0].checked && $('.checkbox-2')[0].checked){
            $('.sphere-block img').attr('src', 'img/Sphere1.png');
        } else if ($('.checkbox-1')[0].checked) {
            $('.sphere-block img').attr('src', 'img/Sphere2.png');
        } else if ($('.checkbox-2')[0].checked){
            $('.sphere-block img').attr('src', 'img/Sphere3.png');
        }
    },
    addTopFormulaBlock: function () {
        var template = this.getFormulaTemplate(1);
        template += '<div class="formula"></div>';
        $('.top-formula').append(template);
        this.addEventShowFormula(1);
        this.addFormula1();
    },
    addBottomFormulaBlock: function () {
        var template = this.getFormulaTemplate(2);
        template += '<div class="formula"></div>';
        $('.bottom-formula').append(template);
        this.addEventShowFormula(2);
        this.addFormula2();
    },
    addFormula1: function () {
        var template = '<div style="visibility: hidden;">\
                            <div class="header">` \\class{text-font}{\\text {Surface area}}=4 \\pi \\color{#029BFF}{r}^{2} `</div>\
                            <div class="content"></div>\
                        </div>'
        $('.top-formula .formula').html(template);
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output], function (){
            if($('.top-formula .formula > div  span').length > 0) {
                $('.top-formula .formula > div').css({visibility: 'visible'});
            }
        })
    },
    changeRadius: function () {
        var self = this;
        $('.sphere-block img').css({width: (+self.radius+250)+'px'});
        self.addFormula1Content();
        self.addFormula2Content();
    },
    addFormula1Content: function() {
        var template = '$$ \\begin{array}{l}{=4 \\times \\pi \\times(\\color{#029BFF}{'+spaceNumber(this.radius)+'})^{2}} \\\\  \\\\ {='+spaceNumber((4*Math.pow(Number(this.radius),2)*Math.PI).toFixed(2))+' \\class{content-text-font}{\\text { square units }}}\\end{array} $$'
        $('.top-formula .content').css({visibility: 'hidden'});
        $('.top-formula .content').html('')
        MathJax.HTML.addElement($('.top-formula .content')[0], "p", {id: 'top_formula_1'}, [template]);
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'top_formula_1'], function (){
            if($('#top_formula_1 span').length > 0) {
                $('.top-formula .content').css({visibility: 'visible'});
            }
        })
        
    },
    addFormula2Content: function() {
        var template = '$$ \\begin{array}{l}{=\\frac{4}{3} \\times \\pi \\times(\\color{#029BFF}{'+spaceNumber(this.radius)+'})^{3}} \\\\ \\\\ {='+spaceNumber(((4/3)*Math.pow(Number(this.radius),3)*Math.PI).toFixed(2))+' \\class{content-text-font}{\\text { cubic units }}}\\end{array} $$'
        $('.bottom-formula .content').css({visibility: 'hidden'});
        $('.bottom-formula .content').html('')
        MathJax.HTML.addElement($('.bottom-formula .content')[0], "p", {id: 'bottom_formula_1'}, [template]);
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'bottom_formula_1'], function (){
            if($('#bottom_formula_1 span').length > 0) {
                $('.bottom-formula .content').css({visibility: 'visible'});
            }
        })
        
    },
    addFormula2: function () {
        var template = '<div style="visibility: hidden;">\
                            <div class="header">` \\class{text-font}{\\text {Volume}}=\\frac{4}{3} \\pi \\color{#029BFF}{r}^{3} `</div>\
                            <div class="content"></div>\
                        </div>';
        $('.bottom-formula .formula').html(template);
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, window.output], function (){
            if($('.bottom-formula .formula > div  span').length > 0) {
                $('.bottom-formula .formula > div').css({visibility: 'visible'});
            }
        });
    },
    addLeftBlockContent: function () {
        var self = this;
        var template = '<div class="sphere-block">\
                            <img src="img/Sphere2.png" />\
                        </div>\
                        <div class="slide-block">\
                            <input type="text" class="radius_input">\
                        </div'
        $('.left-block').append(template);
    },
    addEventCheckbox: function () {
        var self = this;
        $(document).off('click','.checkbox').on('click','.checkbox',function(e){
            var elem = $(e.target).parent()
        })
    },
    addSlideBlock: function () {
        var self = this;
        var template = '<input id="range-1" type="range" class="radius" min="1" max="100" step="1" value="1">';

        $('.slide-block').append(template);
        self.addEventRadiusInput();
    },
    addEventRadiusInput: function () {
        var self = this;
        var changeControl = false;
        $(".radius").val(self.radius)
        $('.radius_input').val($(".radius").val());

        $(".radius").off("input").on("input", function(e){
            if(changeControl) {
                changeControl = false;
                return;
            }
            $('.radius_input').val($(this).val().replace('.', ','));
            self.changeSlideEvent.call(self);
        })
        $('.radius_input').off("keydown").on("keydown", function (e) {
            if (e.key === 'Backspace') return;
            var val = ((e.target.value).toString()+e.key).split(',');
            if( val[0] > 100 || (val[1] && val[1].length > 2)) {
                e.preventDefault();
            }
            if(e.key == '.'){
                if(e.target.value.indexOf(',') == -1){
                    e.target.value += ',';
                    $(".radius").val($(this).val().replace(',', '.') || 1);
                    self.changeSlideEvent.call(self);
                }
                e.preventDefault();
            }
        })
        $('.radius_input').off("input").on("input", function(e){
            changeControl = true;
            $(".radius").val($(this).val().replace(',', '.') || 1);
            self.changeSlideEvent.call(self);
        })
    },
    
    changeSlideEvent: function () {
        this.radius = $(".radius_input").val().replace(',', '.') || 1;
        this.changeRadius();
    },
    setInstructionHelp: function (select) {
        var self = this;
        var helpList = self.getInstructionList();
        $(select || self.pageHelpText).html('');
        self.pageHelpText = select || self.pageHelpText;
        self.addMathText(self.pageHelpText, helpList);
        $(select || self.pageHelpText).parent().removeClass('help_list_' + self.page - 1).addClass('help_list_' + self.page);  
    },
    getInstructionList: function () {
        var self = this;
        helpList = ['Change the radius of the sphere and observe the change in the surface area and volume.',
                    'Deselect the volume or the surface area boxes.'];
        return helpList;
    },
    addMathText: function (pageHelpText, helpList) {
        var ol = document.createElement('ol');
        ol.className = 'help-list';
        $(pageHelpText).html(ol);
        for(var i = 0; i< helpList.length; i++) {
            var id = 'li'+i+(new Date().getTime());
            MathJax.HTML.addElement(ol, "li", {id: id}, [helpList[i]]);
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, id]);
        }
    }
}
function spaceNumber(x){
    var z = x.toString();
    var b = z.split('.')[1];
    z = z.split('.')[0];
    if (z.length >= 4) {
        var c = z.substring(0,z.length-3);
        var d = '';
        if(c.length > 3){
            d = c.substring(0,c.length-3)
            c = c.substring(c.length-3,c.length)
            d+="\\space";
        }
        var y = z.substring(z.length-3, z.length);
        if(b) y = y+','+b; 
        return d+c+"\\space"+y;
    } else {
        if(b) return z + ',' + b;
        return z;
    }
}