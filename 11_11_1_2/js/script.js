var zoomScale = 1;
let Page = function () {
    this.calculateiIndex = 1;
    this.plotIndex = 1;
    this.nextStep = true;
    this.page = 1;
    this.pageHeader = "";
    this.pageDescription = "";
    this.pageHelpText = "";
    this.mainContent = null;
    this.enableCheck = function () {};
    this.nextCalculateControl = true;
    this.paginationPageNumber = {prevPage:null,nextPage:1};
    this.paramsForGraph = {graphPoints:[]};
    this.slideMove = false;
    this.leftValue = 0;
    this.switchButton = false;
    this.volt = ['3.00', '3.75', '4.50', '5.25', '6.00'];
    this.amper = ['2.0','2.5', '3.0', '3.5', '4.0'];
    this.resistor = ['1.5','1.5', '1.5', '1.5', '1.5'];
    this.voltForGraph = ['3.00', '4.00', '4.50', '5.50', '6.00'];
    this.stepIndex = 0;
    this.calculatedObj = {row:0,colume:0};
    this.paramsForMeasurements = {inputsVA:[],rowNum:1,inputsI:0,calculateInputBool:false}
    this.yValues = [3, 4, 4.5, 5.5, 6];
	this.xValues = [2, 2.5, 3, 3.5, 4];
	this.findColumeNameWithArrayNum = ['amper', 'volt', 'resistor'];
    this.setTimeEvent = null;
    this.gradientSelect = false;
	this.calculateValue = null;
	this.ifs = true;
    this.plotBlock = true;
    this.switchControl = false;
    this.inputActive = true;
    this.lineGraph = false;
}
Page.prototype = {
    init: function () {
        this.cleareBackgroundContent();
        this.addBackgroundContent();
    },
    addMainBlock: function (contents) {
        let self = this;
        let content = $('#contentImg');
        if(content.length > 0) {
            self.mainContent = content;
            content.append(contents);
            return true;
        } else {
            console.error('Not found div#contentImg block');
            return false;
        }
    },
    addBackgroundContent: function () {
        let self = this;
        template = '<div class="left-block"></div>\
                    <div class="right-block"></div>'
        if(self.addMainBlock(template)) {
             self.addTestContent();
             self.addRightContent();
             self.addSwitchBlock();
             self.addSlideContent();
             self.addArrow();
             self.addPopupSwitch();
            self.closePopupTwoSeconds('.switch-popup');
             self.addVoltAmperValue();
           	self.addPopupText();
            self.addEventInputs();
            self.showTableRowNumber();
            self.setVoltMeter('0.00');
            self.setAmperMeter('0.0');
        }
    },
    addForGraphTextTable: function () {
        $('table tr:nth-child(1) th:nth-child(2)').append('<span class="for-graph">(for graph)</span> <br />');
        $('table tr:nth-child(1) th:nth-child(3)').append('<span class="for-graph">(for graph)</span> <br />');
        $('table tr:nth-child(1) th:nth-child(4)').append(' <br />');
    },
    addTableInputsValuesForGraph: function(){
    	$('table input[data-id]').attr({'disabled':false,'readonly':true});
    	for(let i=0;i<5;i++){
    		$('table input[data-id=1_'+(i+1)+']').val(self.amper[i].toString().replace('.', ','));
    		$('table input[data-id=2_'+(i+1)+']').val(self.voltForGraph[i].toString().replace('.', ','));
    		$('table input[data-id=3_'+(i+1)+']').val(self.resistor[i].toString().replace('.', ','));
    	}
    },
    addTableInputsValuesForMeasurements: function(){
        $('table input[data-id]').attr({'disabled':false,'readonly':true});
        for(let i=0;i<5;i++){
            $('table input[data-id=1_'+(i+1)+']').val(self.amper[i].toString().replace('.', ','));
            $('table input[data-id=2_'+(i+1)+']').val(self.volt[i].toString().replace('.', ','));
            $('table input[data-id=3_'+(i+1)+']').val(self.resistor[i].toString().replace('.', ','));
        }  
    },
    addTestContent: function () {
    	$('.left-block').html('');
    	let template = '<div class="test-content">\
    						<div class="slide"></div>\
    					</div>';
    	$('.left-block').append(template);

    },
    addSwitchBlock: function () {
    	let template = '<div class="switch-content">\
    						<div class="switch-button"></div>\
    					</div>';
    	this.addMainBlock(template);
    	this.addSwitchEvent();
    },
    addSwitchEvent: function (callback = function () {}) {
		let self = this;
    	$(document).off('click','.switch-content').one('click','.switch-content', function (e) {
            clearInterval(self.setTimeEvent);
			let left = $('.switch-button').css('left').replace('px', '');
    		if(left == 4) {
    			left = 64;
    			self.switchButton = true;
    			self.addPopupEnter(function () {
                    self.addTableInputs();
		    	 	self.removePopup('.enter-popup');
		    	 });
                self.closePopupTwoSeconds('.enter-popup',function () {
                    self.addTableInputs();
                 });
    			self.setStep();
                $('.switch-content').addClass('pointer-unset');
                $('.slide-block').addClass('pointer-unset');
    		} else {
    			left = 4;
    			self.switchButton = false;
    			self.setVoltMeter('0.00');
				self.setAmperMeter('0.0');
                $('.slide-block').removeClass('pointer-unset');
    		}
    		$('.switch-button').css('left', left);
    		self.ChangePowerButton();
            setTimeout(function () {
                callback();
            }, 200);
    		
     	})
    },
    addTableInputs: function (value = null,row = null){
    	self = this;
    	let step = '';
    	$('table tbody input[data-id]').attr('readonly',true);
    	if(value !== null) step = value;
    	else step = self.stepIndex + 1;
        if(!self.inputActive) return;
    	if(row == null){
    		$('table tbody tr:nth-child('+step+') td:nth-child(2) input[data-id]').attr('disabled',false).attr('readonly',false);
    		$('table tbody tr:nth-child('+step+') td:nth-child(3) input[data-id]').attr('disabled',false).attr('readonly',false);
    	}else{
    		$('table tbody tr:nth-child('+row+') td:nth-child(4) input[data-id]').attr('disabled',false).attr('readonly',false)
    	}
    },
    nextRowFunc: function(position){
    	let self = this;
    	input = $('table input[data-id='+position[0]+'_'+position[1]+']')
    	self.paramsForMeasurements.inputsVA.push(self.paramsForMeasurements.rowNum);
		if(self.paramsForMeasurements.inputsVA.length == 5){
			self.paramsForMeasurements.inputsI++;
			$('input[data-id]').attr('disabled',false);
			//self.addCalculateContent();
            self.enableCalculateButton();
			self.addSwitchEvent();
    		$('.switch-content').trigger('click');
		}else{
            let callback = function() {
                $('input[data-id]').attr('readonly',true)
                self.removePopup('.change-rheostat-popup');
                self.addEventSlideButton();
				self.addSwitchEvent();
                if($('.switch-button').css('left') == '64px'){
					$('.switch-content').trigger('click');
				}
                
			}
    		self.addPopupChangeRheostat(callback);
            self.closePopupTwoSeconds('.change-rheostat-popup', callback);
		}
	},
    addEventInputs: function (){
    	self = this;
    	$('table tbody tr input[data-id]').off('change').on('change',function (){
    		let position = $(this).attr('data-id').split('_');
    		if(position[0]<3){
    			self.checkValidateInputsVA(position);
    		}else{
  				self.checkValidateInputsI(position);
   			}
    	})
    },
    checkValidateInputsI: function (position){
    	let self = this;
    	let input = $('input[data-id='+position[0]+'_'+position[1]+']');
    	if(self.checkValue(input.val() , self.resistor[position[1]-1])){
    		if(self.paramsForMeasurements.inputsI != 6){
    		 	input.removeClass('not-valid');
    		 	self.paramsForMeasurements.inputsI++;
	    		self.addTableInputs(null, self.paramsForMeasurements.inputsI);
	    		if(self.paramsForMeasurements.inputsI==6){
	    			self.enableGraphButton();
	    			self.addEventGraphButton();
                    self.addPopupNextPage();
                    self.closePopupTwoSeconds('.next-page-popup');
	    		}
			}
			input.val(input.val().replace('.', ','));
    	}else{
    		$(input).addClass("not-valid");
    		self.addPopupError(position[0],position[1])
    	}	
	},
	checkValue: function(value1, value2) {
		value1 = value1.replace('.',',');
		value2 = value2.replace('.',',');
		if(value1 == value2) {
			return true;
		} else {
			return false;
		}
	},
    checkValidateInputsVA: function (position){
    	self = this;
    	input = $('table tbody input[data-id='+position[0]+'_'+position[1]+']');
    	if(self.checkValue(input.val() , self[self.findColumeNameWithArrayNum[Number(position[0]-1)]][(position[1]-1)])) {
			input.removeClass('not-valid');
    		if(self.checkAllInput(position)) {
    			$(input).closest('tr').find('input').attr('readonly', true);
    			self.nextRowFunc(position);
			}
			input.val(input.val().replace('.', ','));
            input.attr('readonly','readonly');
    	} else {
    		$(input).addClass("not-valid");
    		self.addPopupError(position[0],position[1]);
    	} 
    },
    ChangePowerButton: function () {
    	let self = this;
    	if(this.switchButton){
    		$('.test-content').css({backgroundImage: 'url(img/setup2.png)'})
    	} else {
    		$('.test-content').css({backgroundImage: 'url(img/setup1.png)'})
    	}
    	$('.switch-popup').remove();
    },
    addArrow: function () {
    	let template = "<div class='arrow-v-a v-arrow'></div><div class='arrow-v-a a-arrow'></dit>";
    	$('.test-content').append(template);
    },
    addSlideContent: function () {
    	template = '<div class="slide-block pointer-unset">\
    					<div class="slide-button"></div>\
    				</div>';
    	this.addMainBlock(template);
    	// this.addEventSlideButton();
    },
    getCorectPosition: function (leftValue = null,type = null) {
    	var left = parseInt($('.slide-button').css('left').replace('px', ''));
    	if(leftValue){
    		left = leftValue
    	}
		var newLeft = 0;
		var newLeft1_5 = 0;
		if (left < 25){
			newLeft = -14;
			newLeft1_5 = 5;
		} else if ( (left >= 25 && left <= 45) || (left >= 45 && left <= 77)) {
			newLeft = 45;
			newLeft1_5 = 4;
		} else if ((left >= 77 && left <= 105) || (left >= 105 && left <= 137) ) {
			newLeft = 105;
			newLeft1_5 = 3;
		} else if ((left >= 137 && left <= 164) || (left >= 164 && left <= 199) ) {
			newLeft = 164;
			newLeft1_5 = 2;
		} else if ((left >= 199 && left <= 226) || (left >= 226 && left <= 242) ) {
			newLeft = 223;
			newLeft1_5 = 1;
		} else if (left > 220) {
			newLeft = 223;
			newLeft1_5 = 0;
		}
		this.leftValue = newLeft;
		if(type == null) return newLeft;
		else return newLeft1_5;
    },
    addPopupCalculate: function (f){
    	let self = this;
    	let template = '<div class="calculate-popup"></div>';
    	self.addMainBlock(template);
        $('.calculate-popup').append($('#calculate_popup_text').clone());
    	setTimeout(function () {
    		self.mainContent.one('click', function(){
	    		self.removePopup('.calculate-popup')
                f();
	    	});
    	},200)
    },
    addEventCalculate: function (){
        setTimeout(function () {
            $('.slide-block').addClass('pointer-unset'); 
        }, 300);
    	self = this;
    	$(document).off('click','.calculate-button-block').one('click','.calculate-button-block',function(){
    		 $('.slide-block').addClass('pointer-unset');
            self.removePopup('.calculate-popup');
    		self.addPopupCalculate(function (){
                self.addTableInputs(null,self.paramsForMeasurements.inputsI);
            });
            self.closePopupTwoSeconds('.calculate-popup');
            $('.calculate-button-block > div').removeClass('enable').addClass('active')
    	})
    },
    addEventSlideButton: function () {
    	let self = this;
    	$(document).on('mousedown touchstart', '.slide-button', function () {
    		self.slideMove = true;
    	})
    	$(document).on('mousemove touchmove', '.slide-block', function (e) {
			var clientX = e.originalEvent.touches ? e.originalEvent.touches[0].clientX : e.originalEvent.clientX;
			var r = $('.slide-block')[0].getBoundingClientRect();
    		let newLeft = clientX/zoomScale - r.left / zoomScale;
    		let blockWidth = $('.slide-block').width();
    		if(newLeft > blockWidth-20){
    			newLeft = blockWidth-15;
    		} else if (newLeft < 5){
    			newLeft = 0;
    		}
    		if(self.slideMove) {
    			$('.slide-button').css({left: newLeft+'px'});
    		}
    	})
    	$(document).on('mouseup touchend', 'body', function (e) {
    		// if(!$(e.target).hasClass('slide-block') && !$(e.target).hasClass('slide-button')){
    			self.slideMove = false;
	    		let newLeft = self.getCorectPosition();
	    		$('.slide-button').css({left: newLeft+'px'});
	    		self.addChangeRheostat();
	    		var value = self.getCorectPosition(null,"getValue1_5");
	    		self.stepIndex = value-1;
	    		if(self.paramsForMeasurements.inputsVA.indexOf(value) == -1){
                    if(self.switchControl) return;
                    self.switchControl = true
			    	self.addSwitchEvent(function(){
						var value = self.getCorectPosition(null,"getValue1_5");
				    	self.addTableInputs(value);
				    	self.paramsForMeasurements.rowNum = value;
				    	self.removeEventSlideButton();
                        self.switchControl = false
				    });
		    	}else{
		    		//self.removeEventSwitch();
		    	}
    		// }
    	})
    	$(document).on('click', '.slide-block', function (e) {
            setTimeout(function () {
                let value = self.getCorectPosition(null,"getValue1_5");
                if(self.paramsForMeasurements.inputsVA.indexOf(value) == -1){
                    self.removePopup('.switch-popup');
                    self.addPopupSwitch();
                    self.closePopupTwoSeconds('.switch-popup');
                    $('.switch-content').removeClass('pointer-unset');
                } else {
					$('.switch-content').addClass('pointer-unset'); 
                }
            }, 200)
    		let left = parseInt(e.offsetX)
    		if($(e.target).hasClass('slide-button')) {
    			left = parseInt($(e.target).css('left').replace('px', ''));
    		}
    		self.slideMove = false;
    		let newLeft = self.getCorectPosition(left);
    		$('.slide-button').css({left: newLeft+'px'});
    		self.addChangeRheostat();
    	})
    },
    removeEventSlideButton: function(){
    	$(document).off('mousedown', '.slide-button');
		$(document).off('mouseup', 'body');
		$(document).off('click', '.slide-block')
    },
    removeEventSwitch: function(){
    	$(document).off('click','.switch-content');
    },
    setStep: function () {
    	this.setVoltMeter(this.volt[this.stepIndex]);
    	this.setAmperMeter(this.amper[this.stepIndex]);
    },
    setVoltMeter: function (value) {
    	this.setVoltValue(value);
    	let newValue = 1;
    	if(value == 0) {
    		$('.v-arrow').css({transform: 'rotate('+(-44)+'deg)'});
    	} else {
    		newValue = (value/7.5 * 88) + -44;
    		$('.v-arrow').css({transform: 'rotate('+newValue+'deg)'});
    	}
// max 7.5
// min 0
// 88deg
    },
    setAmperMeter: function (value) {
    	this.setAmperValue(value);
    	let newValue = 1;
    	if(value == 0) {
    		$('.a-arrow').css({transform: 'rotate('+(-48)+'deg)'});
    	} else {
    		newValue = (value/5 * 90) + -48;
    		$('.a-arrow').css({transform: 'rotate('+newValue+'deg)'});
    	}
// max 5
// min 0
// 92deg
    },
    setVoltValue: function (value) {
    	$('.volt-value').html(value.toString().replace('.',','))
    },
    setAmperValue: function (value) {
    	$('.amper-value').html(value.toString().replace('.',','))
    },
	checkAllInput: function (position) {
		let self = this;
		let input = $('table tbody input[data-id='+position[0]+'_'+position[1]+']');
		let allInputRow = input.closest('tr').find('input');
		let control = false;
		for(let i = 0; i < allInputRow.length - 1; i++) {
			if(self.checkValue($(allInputRow[i]).val() , self[self.findColumeNameWithArrayNum[i]][position[1]-1])) {
				control = true;
			} else {
				control = false;
				break;
			}
		}
		return control;
	},
	addPopupChangeRheostat: function (f) {
		let self = this;
        self.addMainBlock('<div class="change-rheostat-popup" ></div>');
        $('.switch-content').removeClass('pointer-unset')
		setTimeout(function(){
            self.mainContent.one('click', f);
            $('.switch-content').addClass('pointer-unset')
        },200);
	},
	addPopupError: function (colume,row) {
		let self = this;
		let template = '<div class="popup-valume-error"></div>';
		let popupText = '';
		self.addMainBlock(template);
		 let attr = {id:"error_popup_id"};
		 if(self.page == 1) {
		 	popupText = $('#volumetextpopup1').clone();
		 	$('.popup-valume-error').addClass('volume');
		 	$('.popup-valume-error').append(popupText);
		 } else {
		 	popupText =$('#volumetextpopup2').clone();
			popupText.addClass('calculate-error-text');
		 	$('.popup-valume-error').append(popupText);
		 }
		let buttonBlock = '<div>\
								<input type="button" class="try_again">\
								<input type="button" class="correct_plot correct_value">\
							<div>';
		$('.popup-valume-error').append(buttonBlock);
        $('.popup-valume-error').css({visibility: 'visible'})
		self.addEventTryAgein();
		self.addEventCorrectPlotByColuemRow(colume, row);
		self.inputActive = false
        $('input[data-id]').attr('readonly', 'readonly');
	},
	addEventTryAgein: function(){
		$(document).on('click', '.try_again', function () {
            self.inputActive = true;
			self.removePopup('.popup-valume-error')
			if(self.paramsForMeasurements.inputsVA.length < 5) {
                let elem_1 = $('table tbody input[data-id='+2+'_'+(self.stepIndex+1)+']');
                let elem_2 = $('table tbody input[data-id='+1+'_'+(self.stepIndex+1)+']');
				if(!elem_2.hasClass('not-valid')) elem_1.attr('readonly',false);
				if(!elem_1.hasClass('not-valid')) elem_2.attr('readonly',false);
			} else {
				$('table tbody input[data-id='+3+'_'+(self.paramsForMeasurements.inputsI)+']').attr('readonly',false);
			}	
		})
	},
	addChangeRheostat: function () { //51 112 170
		switch(this.leftValue) {
			case 223:
				$('.test-content .slide').css({left: '66px', top: '82px'});
			break;
			case 164:
				$('.test-content .slide').css({left: '64px', top: '105px'});
			break;
			case 105:
				$('.test-content .slide').css({left: '61px', top: '128px'});
			break;
			case 45:
				$('.test-content .slide').css({left: '57px', top: '151px'});
			break;
			case -14:
				$('.test-content .slide').css({left: '55px', top: '173px'});
			break;
		}

	},
	addEventCorrectPlotByColuemRow:function(colume,row){
		let self = this;
		$(document).off('click','.correct_plot').on("click",'.correct_plot',function(){
            self.inputActive = true;
			let input = $('input[data-id='+colume+'_'+row+']');
			let val = self[self.findColumeNameWithArrayNum[colume-1]][row-1]
			input.val(val.replace('.', ','));
			self.removePopup('.popup-valume-error')
			if(self.paramsForMeasurements.inputsVA.length < 5){
                if(self.inputActive){
                    $('table tbody input[data-id='+2+'_'+(self.stepIndex+1)+']').attr('readonly',false);
                    $('table tbody input[data-id='+1+'_'+(self.stepIndex+1)+']').attr('readonly',false);
				}
				if(self.checkAllInput([colume,row])) {
    				$(input).closest('tr').find('input').attr('readonly', true);
    				self.nextRowFunc([colume,row]);
    			}
			} else {
                if(self.inputActive){
				    $('table tbody input[data-id='+3+'_'+(self.paramsForMeasurements.inputsI)+']').attr('readonly',false);
				}
                input.removeClass('not-valid');
				self.paramsForMeasurements.inputsI++;
				self.addTableInputs(null, self.paramsForMeasurements.inputsI);
				if(self.paramsForMeasurements.inputsI==6){
    				self.enableGraphButton();
    				self.addEventGraphButton();
                    self.addPopupNextPage();
                    self.closePopupTwoSeconds('.next-page-popup');
    			}
			}
			input.removeClass('not-valid');
            input.attr('readonly','readonly');
		})
	},
	addPopupText: function () {
		let template = '<div class="error-popup-texts"></div>';
		this.addMainBlock(template);
		let eroorText = 'Carefully read the `I` value on the horizontal axis and the `V` value on the vertical axis.'
		MathJax.HTML.addElement(document.querySelector('.error-popup-texts'), "p", {id: 'graphtextpopup'}, [eroorText]);
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'graphtextpopup']);

		eroorText = 'Enter the correct reading in each column. Do not round.'
		MathJax.HTML.addElement(document.querySelector('.error-popup-texts'), "p", {id: 'volumetextpopup1'}, [eroorText]);
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'volumetextpopup1']);

        calculatePopupText = 'Calculate `V/I` for each row of the table.'
        MathJax.HTML.addElement(document.querySelector('.error-popup-texts'), "p", {id: 'calculate_popup_text'}, [calculatePopupText]);
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'calculate_popup_text']);
		
		
	},
    addGraphContent: function () {
		this.cleareLeftContent();
		$('.left-block').append('<div class="graphs-block full-screen">\
									<div class="graph not-active">\
										<div class="points"></div>\
									</div>\
								</div>');
		this.addMainBlock('<div class="draw-gradient-block" >\
										<div class="draw-button"></div>\
										<div class="gradient-button"></div>\
									</div>')
    },
    addEventGraphButton: function (){
		let self = this;
		$(document).off('click', '.enable-graph').one('click', '.enable-graph', function () {
			self.page = 2
			self.enableMeasurementsButton();
			self.activeGraphButton();
			self.addGraphContent();
			self.showTableRowNumber();
			self.initTableSelectRow();
			self.setInstructionHelp();
			self.setDescription();
			$('.calculate-button-block').remove();
			self.addTableInputsValuesForGraph();
            self.addTabsEvent();
            self.addForGraphTextTable();
            self.activateTableColumn();
		})
    },
    addEventDrawButton: function () {
		let self = this;
		self.enableDrawButton();
        self.removePopup('.plot-block');
        self.plotBlock = false;
        $('.active-calculate').removeClass('active-calculate');
		$(document).off('click', '.draw-button').one('click', '.draw-button', function () {
            self.lineGraph = true;
			self.activeDrawButton();
			self.addlineGraph();
			self.enableGradientButton();
			self.addEventGradient();
		})
    },
    addlineGraph: function() {
		let self = this;
		let template = '<div class="graph-line">\
						</div>';
		$('.graph .points').prepend(template)
    },
	addEventGradient: function () {
		let self = this;
		$(document).off('click', '.gradient-button').one('click', '.gradient-button', function () {
			self.addlineGraphGradient();
			self.activeGradientButton();
			self.removePopup('.plot-block');
			//self.disableInptsEvent();
			self.addCalculateBlockContent();
			if(self.calculateValue){
                $('.hidden-img').css({visibility: 'visible'});
                $('.calculate-block .status').css({visibility: 'visible'});
                $('.calculate-block input').attr('readonly','readonly');
                let calc = self.calculateValue.toString().replace(',', '.');
                if(calc <= 1.8 && calc >= 1.2){
                    $('.calculate-block .status').attr('src','img/right.png');
                }
            }
            self.initCloseButton();
            self.gradientSelect = true;
		})
    },
    addRightContent: function () {
		let self = this;
		self.cleareRightContent();
		self.addButtonsContent();
		self.addTableContent();
        self.addCalculateContent();
    },
    cleareBackgroundContent: function () {
		let self = this;
		let content = $('#contentImg');
		if(content.length > 0) {
			$('#contentImg').html('');
		}else {
			console.error('Not found div#contentImg block');
		}
    },
    cleareLeftContent: function () {
		$('.left-block').html('');
	},
	cleareRightContent: function () {
		$('.right-block').html('');
    },
    addButtonsContent: function () {
		let template = '<div class="menu-button">\
								<div class="active-measurements measurements"></div>\
								<div class="not-active-graph graph"></div>\
								<div class="not-active-conclusion conclusion"></div>\
							</div>'
		this.addMainBlock(template)
    },
    addTableContent: function () {
		let tamplate = '<div class="table-content">\
							<table>\
								<thead>\
									<tr class="table-head-row">\
										<th></th>\
										<th></th>\
										<th></th>\
										<th></th>\
									</tr>\
								</thead>\
								<tbody>';
		for (let colum = 1; colum < 6; colum++) {
			tamplate+='<tr data-row="'+colum+'"><td>'+colum+'</td>';
			for (let row = 1; row < 4; row++) {
				tamplate+='<td><input type="text" data-id="' + row + '_' + colum + '" disabled /></td>';
			}
			tamplate+='</tr>';
		}
		tamplate+='<tbody>\
				<table>\
			</div>';
		$('.right-block').append(tamplate);
		this.addHeadMathTh($('.table-head-row th:nth-child(2)')[0], "span", {id: 'th1'}, ['Current, `I`         `(A)`']);
		this.addHeadMathTh($('.table-head-row th:nth-child(3)')[0], "span", {id: 'th2'}, ['Potential difference, `V` `(V)`']);
		this.addHeadMathTh($('.table-head-row th:nth-child(4)')[0], "span", {id: 'th3'}, ['`V/I`']);
    },
    addHeadMathTh: function (elem, tag, attr, mathText) {
		MathJax.HTML.addElement(elem, tag, attr, mathText);
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, attr.id]);
    },
    addCalculateContent: function () {
		let self = this;
		$('.right-block').append('<div class="calculate-button-block">\
									<div class="not-active"></div>\
								</div>');
	},
    addCalculateBlockContent: function () {
		let self = this
		$('.calculate-block').remove();
		let times = 0
		$('.right-block').html('<div class="calculate-block">\
								<div class="close-button"></div>\
							</div>');
        let calculatePopupText1 = 'Calculate `V/I` for each row of the table.';
        let id = Math.random()*10;
        MathJax.HTML.addElement($('.calculate-block')[0], "p", {id: 'calculate_popup_text'+id}, [calculatePopupText1]);
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'calculate_popup_text'+id]);

		$('.calculate-block').append('<p>Enter your answer below.</p>\
									<div>\
										<input type="text" value="'+(self.calculateValue? self.calculateValue : '')+'">\
										<img src="img/wrong.png" class="status">\
										<span class="show_answer">Show answer</span>\
										<div class="hidden-img"></div>\
									</div>');

        let hiddenImgText = '`m = (\y_\(2) - \y_\(1)) / (\\x_\(2) - \\x_\(1))`';
         id = +(Math.random()*10);
        MathJax.HTML.addElement($('.hidden-img')[0], "p", {id: 'hidden_img_text'+id}, [hiddenImgText]);
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'hidden_img_text'+id]);

        let hiddenImgText2 = '`= (6 - 3) / (4 - 2)`';
        id = +(Math.random()*10);
        MathJax.HTML.addElement($('.hidden-img')[0], "p", {id: 'hidden_img_text2'+id}, [hiddenImgText2]);
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'hidden_img_text2'+id]);

        let hiddenImgText3 = '`= 1,5`';
        id = +(Math.random()*10);
        MathJax.HTML.addElement($('.hidden-img')[0], "p", {id: 'hidden_img_text3'+id}, [hiddenImgText3]);
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'hidden_img_text3'+id]);


		if(self.calculateValue) {
			$('.hidden-img').css({visibility: 'visible'})
			$('.calculate-block img').css({visibility: 'visible'});
		 } else{
			$('.hidden-img').css({visibility: 'hidden'})
		 } 
		self.initEventCalculateBlockInput();
    },
    initEventCalculateBlockInput: function () {
		let self = this;
        $('.slide-block').addClass('pointer-unset');
		$(document).off('change','.calculate-block input').on('change','.calculate-block input', function (e) {
			$('.slide-block').addClass('pointer-unset');
            let value = $(e.target).val();
			if(Number(value.replace(',', '.')) <= 1.8 && Number(value.replace(',', '.')) >= 1.2) {
                $('.calculate-block input').attr('readonly','readonly');
                $('.calculate-block img').attr('src', 'img/right.png');
				self.enableConclusionButton();
				self.addEventConclusion();
                self.addPopupNextPage();
                self.closePopupTwoSeconds('.next-page-popup');
				$('.hidden-img').css({visibility: 'visible'});
				$('.calculate-block span.show_answer').css({visibility: 'hidden'});
				$(e.target).val($(e.target).val().replace('.', ','));
                self.calculateValue = $(e.target).val();
			} else {
                $('.calculate-block input').val($('.calculate-block input').val().toString().replace('.', ','));
				self.calculateValue = $('.calculate-block input').val();
                self.initEventShowAnswer();
				$('.calculate-block span.show_answer').css({visibility: 'visible'});
			}
			$('.calculate-block img.status').css({visibility: 'visible'});
			
		})
    },
    initEventShowAnswer: function () {
		let self = this;
		$(document).off('click', '.calculate-block .show_answer').one('click', '.calculate-block .show_answer', function () {
			$('.calculate-block input').attr('readonly','readonly');
			$('.calculate-block img').css({visibility: 'visible'});
			self.enableConclusionButton();
			self.addEventConclusion();
			$('.calculate-block span.show_answer').css({visibility: 'hidden'})
			$('.hidden-img').css({visibility: 'visible'});
		})
    },
    addPopupPlot: function () {
		let self = this;
        if(!self.plotBlock) return;
		$('.plot-block').remove();
		let template = '<div class="plot-block plot_index_'+self.plotIndex+'">\
							<p>Plot this point.</p>\
						</div>'
		self.addMainBlock(template);
		self.activeTableRow();
    },
    addPopupSwitch: function () {
    	let template = '<div class="switch-popup" ><p>Close the switch.</p></div>';
    	this.addMainBlock(template);
        setTimeout(function(){
            self.mainContent.one('click', function () {
                    self.removePopup('.switch-popup');
                });
            },200);
    },
    addPopupNextPage: function () {
        let self = this;
        let template = '<div class="next-page-popup">Proceed to the next tab.</div>';
        this.addMainBlock(template);
        setTimeout(function(){
            self.mainContent.one('click', function () {
                    self.removePopup('.next-page-popup');
                 });
            },200);
    },
    addPopupEnter: function (f) {
        let self = this;
    	let template = '<div class="enter-popup" ><p>Enter the ammeter and voltmeter readings in the table.</p> </div>';
    	this.addMainBlock(template);
        setTimeout(function(){
    	   self.mainContent.one('click', f);
        }, 200);
    },
    addVoltAmperValue: function () {
    	let template = '<div class="volt-value" ></div><div class="amper-value" ></div>';
    	this.addMainBlock(template);
    },
    activeTableRow: function () {
		let self = this;
		$('.right-block table tr').removeClass('active-calculate');
		$('.right-block table tr:nth-child('+self.plotIndex+')').addClass('active-calculate');
	},
	showTableRowNumber: function () {
		$('.right-block table td:nth-child(1)').css({visibility: 'visible'});
	},
    hideTableRowNumber: function () {
        $('.right-block table td:nth-child(1)').css({visibility: 'hidden'});
    },
	enableGraphButton: function (){
		$('.menu-button .graph').removeClass('not-active-graph active-graph').addClass('enable-graph');
	},
	disableGraphButton: function (){
		$('.menu-button .graph').removeClass('enable-graph active-graph').addClass('not-active-graph');
	},
	activeGraphButton: function (){
		$('.menu-button .graph').removeClass('enable-graph not-active-graph').addClass('active-graph');
	},
	activeConclusionButton: function (){
		$('.menu-button .conclusion').removeClass('not-active-conclusion active-conclusion').addClass('enable-conclusion');
		this.enableGraphButton();
		this.enableMeasurementsButton();
	},
	disableConclusionButton: function (){
		$('.menu-button .conclusion').removeClass('enable-conclusion active-conclusion').addClass('not-active-conclusion');
	},
	enableConclusionButton: function (){
		$('.menu-button .conclusion').removeClass('enable-conclusion not-active-conclusion').addClass('active-conclusion');
		this.addEventConclusion();
        this.addPopupNextPage();
        this.closePopupTwoSeconds('.next-page-popup');
	},
	enableMeasurementsButton: function (){
		$('.menu-button .measurements').removeClass('not-active-measurements active-measurements').addClass('enable-measurements');
	},
	activeMeasurementsButton: function (){
		$('.menu-button .measurements').removeClass('enable-measurements not-active-measurements').addClass('active-measurements');
	},
	activeGradientButton: function (){
		$('.gradient-button').removeClass("enable").addClass('active');
	},
	enableGradientButton: function (){
		$('.gradient-button').removeClass("active").addClass('enable');
	},
	disableGradientButton: function (){
		$('.gradient-button').removeClass('active enable')
	},

	activeDrawButton: function (){
		$('.draw-button').removeClass('enable').addClass('active');
	},
	disableDrawButton: function (){
		$('.draw-button').removeClass('enable  active');
	},
	enableDrawButton: function (){
		$('.draw-button').removeClass('active').addClass('enable');
		this.addEventConclusion();
    },
    enableCalculateButton: function (){
        $('.calculate-button-block > div').removeClass('not-active').addClass('enable');
        this.addEventCalculate();
    },
    activateTableColumn: function () {
		$('table tr td:nth-child(2)').addClass('graph-active-td');
		$('table tr:nth-child(1) th:nth-child(3)').addClass('active-th-y');
		$('table tr:nth-child(1) th:nth-child(2)').addClass('active-th-y');
        $('table tr td:nth-child(3)').addClass('graph-active-td');
       // $('table tr th:nth-child(2)').addClass('active-th-y');
    },
    initTableSelectRow: function () {
		let self = this;
        $('table td:nth-child(1)').addClass('cursor-pointer')
		$(document).off('click', '.right-block table tr td:nth-child(1)').on('click', '.right-block table tr td:nth-child(1)', function (e) {
			if($('.active-measurements').length > 0) return;
            let indexRow = $(e.target).closest('tr').attr('data-row');
			self.plotIndex = indexRow;
            $('.plot-block').remove();
			if(self.paramsForGraph.graphPoints.indexOf(self.plotIndex) == -1) self.addPopupPlot();
			self.activateTableColumn();
			self.initGraphPointsEvent();
            $(e.target).removeClass('cursor-pointer');
		})
    },
    initGraphPointsEvent: function () {
		let self = this;
        $(document).off('click', '.graphs-block .graph .points').on('click', '.graphs-block .graph .points', function (e) {
            if(self.pointControl) return;
            let graphHassThisPlot = false;
            for(let i=0;i<self.paramsForGraph.graphPoints.length;i++){
                if(self.paramsForGraph.graphPoints[i]==self.plotIndex){
                	graphHassThisPlot = true;
                	break;
                }
            }
            if(!graphHassThisPlot) self.addPointsToGraph($('.graph .points')[0], e, self.checkValidePoints(e));
 		})
    },
    addEventTryAgeinGraph: function (point) {
		let self = this;
		$(document).off('click','.graph.try_again').on('click','.graph.try_again', function () {
			self.removePopup('.popup-valume-error');
			point.remove();
			self.pointControl = false;
		})
    },
    addEventCorrectPlotGraph: function (select, point) {
		let self = this;
		let block_width = $(select).width();
		let block_height = $(select).height();
		let mashtab = block_height/7.5;
		$(document).off('click', '.graph.correct_plot_graph').on('click', '.graph.correct_plot_graph', function () {
            $(point).css({top: ((block_height - mashtab * self.yValues[self.plotIndex - 1])-5)+'px', left: (((self.xValues[self.plotIndex - 1])*mashtab)-5) + 'px'});
            self.paramsForGraph.graphPoints.push(self.plotIndex);
            self.addPointXYText({top: (block_height - mashtab * self.yValues[self.plotIndex - 1]),left: ((self.xValues[self.plotIndex - 1])*mashtab)});
            if(self.paramsForGraph.graphPoints.length == 5) {
                self.addEventDrawButton();
                self.removePopup('.plot-block');
            }
			self.pointControl = false;
			$(point).removeClass('error');
			self.removePopup('.popup-valume-error');
            $('.plot-block').remove();
		})
		
    },
    removePopup:function (namePopup){
    	$(namePopup).remove();
    },
    addPopupGraphError: function (select, e, point) {
		let self = this;
		let template = '<div class="popup-valume-error"></div>';
		let eroorText = '';
		let newClass = ''
		if($('.graph2').hasClass('active')) {
			eroorText = 'Carefully read the `1/p` value on the horizontal axis and the `V` value on the vertical axis.';
			newClass = 'over'
		} else {
			eroorText = 'Carefully read the `I` value on the horizontal axis and the `V` value on the vertical axis.';
		}
		
		let graphErrorButton ='<div>\
									<input type="button" class="graph try_again">\
									<input type="button" class="graph correct_plot_graph">\
								<div>';
		self.addMainBlock(template);
		MathJax.HTML.addElement(document.querySelector('.popup-valume-error'), "p", {id: 'graph_error_text', className: 'graph-error-text'}, [eroorText]);
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'graph_error_text']);
		$('.popup-valume-error').append(graphErrorButton);
        setTimeout(function () {
            $('.popup-valume-error').css({visibility:'visible'});
        },1000)
		$('.popup-valume-error').addClass(newClass);
		self.addEventTryAgeinGraph(point);
		self.addEventCorrectPlotGraph(select, point);
    },
    checkValidePoints: function (e) {
		let self = this;
		let block_width = $(e.target).width();
		let block_height = $(e.target).height();
		let valide = false;
		let mashtab = block_height/7.5;
		let yValueM = (block_height-e.offsetY)/mashtab;
        let xValueM = e.offsetX/mashtab;
        if(yValueM <= self.yValues[self.plotIndex-1]+0.2
            && yValueM >= self.yValues[self.plotIndex-1]-0.2 
            && xValueM <= self.xValues[self.plotIndex-1]+0.2
            && xValueM >= self.xValues[self.plotIndex-1]-0.2
        ){
            valide = true;	
            self.paramsForGraph.graphPoints.push(self.plotIndex);
            self.addPointXYText({left:e.offsetX,top:e.offsetY});
            if(self.paramsForGraph.graphPoints.length == 5){
            	self.addEventDrawButton();
                self.removePopup('.plot-block');
            }
        }
		
		return valide;
    },
    addPointsToGraph: function (select, e, valide) {
		let self = this;
		let block_height = $(e.target).height();
		let mashtab = block_height/7.5;
		var top = valide? block_height - self.yValues[self.plotIndex-1] * mashtab - 5 : e.offsetY-5;
		var left = valide? self.xValues[self.plotIndex-1] * mashtab - 5 : e.offsetX-5;
		let point = $('<div class="point ' + (valide? '' : 'error') + '" style="top:'+top+'px; left:'+left+'px;"></div>')
		$(select).append(point)
		if(!valide) {
			self.pointControl = true;
			self.addPopupGraphError(select, e, point);
		} else {
			self.pointControl = false;
            $('.plot-block').remove();
		}
    },
    addlineGraph: function () {
		let self = this;
		let template = '<div class="graph-line">\
						</div>';
		$('.graph .points').prepend(template)
    },
    addlineGraphGradient: function () {
		let self = this;
		let template = '<div class="gradient-line">\
						</div>';
		$('.graph .points').prepend(template)
    },
    addMathText: function (pageHelpText, helpList) {
		let ol = document.createElement('ol');
		ol.className = 'help-list';
		$(pageHelpText).html(ol);
		for(let i = 0; i< helpList.length; i++) {
			let id = 'li'+i+(new Date().getTime());
			MathJax.HTML.addElement(ol, "li", {id: id}, [helpList[i]]);
			MathJax.Hub.Queue(["Typeset", MathJax.Hub, id]);
		}
	},
	// set and get header, description, help list
	setInstructionHelp: function (select) {
		let self = this;
		let helpList = self.getInstructionList();
		$(select || self.pageHelpText).html('');
		self.pageHelpText = select || self.pageHelpText;
		self.addMathText(self.pageHelpText, helpList);
		$(select || self.pageHelpText).parent().removeClass('help_list_' + self.page - 1).addClass('help_list_' + self.page);
		
	},
	setDescription: function (select, description) {
		let self = this;
		$(select || self.pageDescription).html('');
		var description = self.getDescription();
		let id = Math.random()*(new Date().getTime());
		MathJax.HTML.addElement($(select || self.pageDescription)[0], "span", {id: 'description'}, [description]);
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'description']);
		self.pageDescription = select || self.pageDescription;
		// $(select).parent().removeClass('description_text_' + self.page - 1).addClass('description_text_' + self.page);
	},
	getDescription: function () {
		let self = this;
		let description = "";
		switch (self.page){
			case 1:
				description = "Adjust the rheostat and measure the current and voltage to complete the table.";
			break;
			case 2:
				description = "Click on each row number to plot `(I;V)`. Click on the correct spots on the graph paper and draw a line of best fit.";
			break;
			case 3:
				description = "Click on play button to see the conclusion that can be drawn from this experiment.";
			break;
		}
		return description;
	},
	getInstructionList: function () {
		let self = this;
		let helpList = "";
		switch (self.page){
			case 1:
			helpList = ['Close the switch.',
						'Read the current through and the potential difference over the resistor. Enter the values in the correct blocks in the table.',
						'Open the switch and choose a lower setting on the rheostat. This increases the current through the resistor.',
						'Repeat Steps 1 to 4 until the first two columns of the table are complete.',
						'Click on the CALCULATE button to complete the last column of the table.',
						'For each row of the table, calculate `V/I`. Enter the value in the correct blocks in the table.',
						'Go to the GRAPH tab to continue with the experiment.'];
			break;
			case 2:
                helpList = ['You should first work through the MEASUREMENTS tab before you continue. ',
                            'Click on a row number to plot `(I;V)`. Find the `I` value on the horizontal axis and the `V` value on the vertical axis.',
                            'Click on the correct spot on the graph paper to show where the point must be plotted.',
                            'Repeat Steps 3 and 4 until all the points are plotted. ',
                            'Click on the DRAW button. A line of best fit will be drawn through your plotted points. ',
                            'Click on the GRADIENT button for calculations about the gradient of the graph.',
                            'Go to the CONCLUSION tab to continue with the experiment.'];
			break;
			case 3:
                helpList = ['You should first work through the MEASUREMENTS and GRAPH tabs before you continue.',
                            'Click on the CONCLUSION button to see how we can interpret the table and the graph obtained in this experiment. ',
                            'Click on the TABLE or GRAPH button any time when you want to look at the table or the graphs again.',
                            'Click on the RESET button to start again. '];
			break;
		}
		return helpList;
	},
	setMainHeader: function (select) {
		let self = this;
		let header = self.getMainHeader();
		$(select || self.pageHeader).html(header);
		self.pageHeader = select || self.pageHeader;
		// $(select).parent().removeClass('description_text_' + self.page - 1).addClass('description_text_' + self.page);
	},
	getMainHeader: function () {
		let self = this;
		let header = "Verify Ohm’s Law";
		return header;
	},
	//////////
	clearMain : function () {
		let self = this;
		$('.left-block').html('')
		$('.right-block').html('');
		$('.popup-show-block').css('visibility','hidden');

	},
	createPageConclusion : function () {
		let self = this;
		self.addMainBlock('<div id="conclusion"></div>');
		$('#conclusion').append('<div class="pageNumber">1.</div>');
		$('#conclusion').append('<div class="conclusionImage"><img id="nextPage"><img id="conclusionImage"></div>');
		$('#conclusion').append('<div class="pagePagination">\
			<button class="pagePaginationButton" id="pageChange_prev" disabled="disabled">\<</button>\
			<button class="pagePaginationButton paginationButtonClicked" id="pageChange_1">1</button>\
			<button class="pagePaginationButton" id="pageChange_2">2</button>\
			<button class="pagePaginationButton" id="pageChange_3">3</button>\
			<button class="pagePaginationButton" id="pageChange_next">\></button>\
			</div>')
		$('#conclusionImage').attr('src','img/t1.png');
		self.addEventPagePagination()
	},
	addEventConclusion: function(){
		let self = this;
		$(document).off('click','.active-conclusion' ).on('click','.active-conclusion' , function () {
			self.clearMain();
			self.createPageConclusion();
			self.activeConclusionButton();
			self.page = 3;
			self.setInstructionHelp();
			self.setDescription();
			//self.setMainHeader();
			self.enableCheck(true);
            self.addTabsEvent();
		})
	},
	pageAnimate: function (){
		let self = this;
        self.animateDo = true
		$('#pageChange_'+self.paginationPageNumber.prevPage).removeClass('paginationButtonClicked');
		$('#pageChange_'+self.paginationPageNumber.nextPage).addClass('paginationButtonClicked');
		$('#nextPage').attr('src','img/t'+self.paginationPageNumber.nextPage+'.png');
		setTimeout(function(){
			$('#nextPage').css('visibility','visible');
		},100)
		$( "#conclusionImage" ).animate({opacity: 0}, 1000, function() {
    		$('#conclusionImage').attr('src','img/t'+self.paginationPageNumber.nextPage+'.png');
			$('#nextPage').css('visibility','hidden');
			$('#conclusionImage').css('opacity','1');
            self.animateDo = false;
		});
	},


	addEventPagePagination: function (){
		let self = this;
		$(document).off('click','.pagePaginationButton' ).on('click','.pagePaginationButton' , function () {
            if(self.animateDo) return;
			let buttonVal = $(this).text();
			$('#pageChange_prev').attr('disabled', false);
			$('#pageChange_next').attr('disabled', false);
			switch(buttonVal){
				case "1":
					self.paginationPageNumber.prevPage = self.paginationPageNumber.nextPage;
					self.paginationPageNumber.nextPage = 1;
					$('#pageChange_prev').attr('disabled', true);
					self.pageAnimate();
				break;
				case "2":
					self.paginationPageNumber.prevPage = self.paginationPageNumber.nextPage;
					self.paginationPageNumber.nextPage = 2;
					self.pageAnimate();
				break;
				case "3":
					self.paginationPageNumber.prevPage = self.paginationPageNumber.nextPage;
					self.paginationPageNumber.nextPage = 3;
					$('#pageChange_next').attr('disabled', true);
					self.pageAnimate();
				break;
				case "<":
					if(!(self.paginationPageNumber.nextPage==1)){
						self.paginationPageNumber.prevPage = self.paginationPageNumber.nextPage;
						self.paginationPageNumber.nextPage -=1;
						self.pageAnimate();
					}
                    if(self.paginationPageNumber.nextPage==1){
                        $('#pageChange_prev').attr('disabled', true);
                    }
				break;
				case ">":
					if(!(self.paginationPageNumber.nextPage==3)){
						self.paginationPageNumber.prevPage = self.paginationPageNumber.nextPage;
						self.paginationPageNumber.nextPage +=1;
						self.pageAnimate();
                       
					}
                    if(self.paginationPageNumber.nextPage==3){
                         $('#pageChange_next').attr('disabled', true);
                    }
				break;
			}
			$('.pageNumber').html(self.paginationPageNumber.nextPage+".")
		})
    },
    addPointXYText: function (position,index) {
		position.top -= 10;
        let plotIndex = index||self.plotIndex;
		if(position.left-100 < 0) {
			position.left = position.left + 20
            position.top = position.top+30
		}else {
			position.left = position.left - 100
		}
		let text = '(' + self.amper[plotIndex-1].replace('.', ',') + ' ; ' + self.voltForGraph[plotIndex-1].replace('.', ',') + ')';
		$('.graph .points').append('<span class="points-text" style="top:'+position.top+'px; left:'+position.left+'px">'+text+'</span>');
	},
	initCloseButton: function () {
		let self = this;
		$(document).off('click','.close-button').on('click','.close-button', function () {
			$(this).parent().remove();
			self.addTableContent();
			self.addTableInputsValuesForGraph();
			self.showTableRowNumber();
			self.initTableSelectRow();
			self.addEventGradient();
            self.activateTableColumn();
		})
	},
    closePopupTwoSeconds: function (select, f = function (){}) {
        let self = this;
        self.setTimeEvent = setTimeout(function () {
            self.removePopup(select);
            f()
        }, 2000);
    },
    addTabsEvent: function () {
        if(this.page == 2) {
            this.addEventTab1();
        }
        if (this.page >= 2){
            this.addEventTab2();
        }
    },
    addEventTab1: function () {
        let self = this
        $(document).off('click', '.enable-measurements').on('click', '.enable-measurements', function () {
            $('.slide-block').addClass('pointer-unset');
            self.activeMeasurementsButton();
            $('.table-content').remove();
            self.addTableContent();
            self.showTableRowNumber();
            self.addTableInputsValuesForMeasurements();
            self.addTestContent();
            $('.draw-gradient-block').remove();
            self.enableGraphButton();
            self.addArrow();
            $('#conclusion').remove();
            $('.calculate-block').remove();
            let pag = self.page
            self.page = 1
            $('#instPanel').removeClass('help_list_2 help_list_3 ')
            self.setInstructionHelp();
            self.setDescription();
            self.page = pag
            if(self.gradientSelect) {
                $('.menu-button .conclusion').removeClass('enable-conclusion not-active-conclusion').addClass('active-conclusion');
            }
            self.removePopup('.plot-block');
            if($('.popup-valume-error').length > 0){
                $('.popup-valume-error .try_again').click();
            }
        })
    },
    addEventTab2: function () {
        let self = this
        $(document).off('click', '.enable-graph').on('click', '.enable-graph', function () {
            $('.slide-block').addClass('pointer-unset');
            self.enableMeasurementsButton();
            self.activeGraphButton();
            $('.table-content').remove();
            self.addTableContent();
            self.showTableRowNumber();
            self.addTableInputsValuesForGraph();
            self.activateTableColumn();
            let pag = self.page
            self.page = 2
            $('#instPanel').removeClass('help_list_3')
            self.setInstructionHelp();
            self.setDescription();
            self.page = pag
            $('.calculate-button-block').remove();
            self.addGraphContent();
            self.addAllPointsToGraph();
            self.addForGraphTextTable();
            $('#conclusion').remove();
            if(self.lineGraph){
                self.activeDrawButton();
                self.addlineGraph();
                self.enableGradientButton();
                self.addEventGradient();
            }
            if (self.gradientSelect){
                self.addEventConclusion();
                self.activeGradientButton();
                self.addlineGraphGradient();
                self.activeGradientButton();
                self.removePopup('.plot-block');
                //self.disableInptsEvent();
                self.addCalculateBlockContent();
                self.initCloseButton();
                $('.menu-button .conclusion').removeClass('enable-conclusion not-active-conclusion').addClass('active-conclusion');
                if(self.calculateValue){
                    $('.calculate-block input').attr('readonly','readonly');
                    $('.calculate-block input').val(self.calculateValue)
                    $('.calculate-block .hidden-img ').css({visibility: 'visible'});
                    $('.calculate-block .status').css({visibility: 'visible'});
                    let calc = self.calculateValue.toString().replace(',', '.');
                    if(calc <= 1.8 && calc >= 1.2){
                        $('.calculate-block .status').attr('src','img/right.png');
                    }
                }
            }
            if(self.paramsForGraph.graphPoints.length == 5 && !self.lineGraph){
                self.addEventDrawButton();
            }
        })
    },
    addAllPointsToGraph: function () {
        let block_width = $('.graph .points').width();
        let block_height = $('.graph .points').height();
        let mashtab = block_height/7.5;
        for(let i = 0; i < self.paramsForGraph.graphPoints.length; i++) {
            let point = $('<div class="point " ></div>');
            $('.graph .points').append(point)
            let index = self.paramsForGraph.graphPoints[i]-1;
            point.css({top: ((block_height - mashtab * self.yValues[index])-5)+'px', left: (((self.xValues[index])*mashtab)-5) + 'px'});
            self.addPointXYText({top: (block_height - mashtab * self.yValues[index]),left: ((self.xValues[index])*mashtab)},index);
        }
    }
}