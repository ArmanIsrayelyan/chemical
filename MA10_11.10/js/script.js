var zoomScale = 1;
var Page = function () {
    this.size_controll = 50;
    this.svgElementSize = {w: 0, h: 0};
    this.typePath = 'triangle';
    this.triangle = {};
    this.quadrilateral = {};
    this.triangleBuilt = {};
    this.quadrilateralBuilt = {};
    this.validError = false;
    this.pointDirectionBeforeError = {};
    this.pathCode = '0 0 0';
    this.circleR = 17;
    this.dotsPositionForQuadrilateral = [
        [false,true],
        [false,true],
        [false,true],
        [false,true]
    ]
    this.test = 0;
    this.pathConstruction1 = [
        {
            start:{
                path:'main-path',
            }
        },
        {path:{attr:'degree'}},
        {line:null},
        {circle: null},
        {text:{attr:'dot',uniqueLetter:['b','a','c'], uniqueLetter2: ['c','b','a','d']}},
        {rect:null},
        {text:{attr:'degree'}},
        {ellipse:null}
    ]
    this.pathConstruction2 = [
        {
            start:{
                path:'main-path',
            }
        },
        {path:{attr:'degree'}},
        {line:null},
        {circle: null},
        {text:{attr:'dot',uniqueLetter:['q','p','r'], uniqueLetter2: ['r','q','p','s']}},
        {rect:null},
        {text:{attr:'degree'}},
        {ellipse:null}
    ]
    this.movingPath = '';
    this.similarityRatio = 1;
    this.maxRadius = 170;
    this.sidePX = 2;
    this.degreeS = 0;
    this.paramForControll = {
        minDegree : 20,
        maxDegree : 175,
        minSide : 70,
        maxSide : 650,
        minAreaSurface : 18000,
        minDiagonal: 100,
        minAltitude: 80
    }
    this.correctedRadius = 100;
    this.colorForAngles = ['#9462E1','#ED3037','#1FB472','#FFCC00']
}
Page.prototype = {
    init: function () {
        this.cleareBackgroundContent();
        this.addBackgroundContent();
        this.setSliderValue(1);
        $('.checkbox.triangle').click();
        // this.circleR = (this.circleR / 12.8 * (window.innerWidth / 100));
    },
    addMainBlock: function (contents) {
        var self = this;
        var content = $('#contentImg');
        if(content.length > 0) {
            self.mainContent = content;
            content.append(contents);
            return true;
        } else {
            console.error('Not found div#contentImg block');
            return false;
        }
    },
    cleareBackgroundContent: function () {
        var self = this;
        var content = $('#contentImg');
        if(content.length > 0) {
            $('#contentImg').html('');
        }else {
            console.error('Not found div#contentImg block');
        }
    },
    addBackgroundContent: function () {
        var self = this;
        template = '<div class="top-block"></div>\
                    <div class="bottom-block"></div>';
        if(self.addMainBlock(template)) {
            //self.addRightBlockContent();
            self.addTopBlockContent();
            self.addBottomBlockContent();
        }
    },
    addTopBlockContent: function () {
        var self = this;
        var template = '<svg id="basicSVG"></svg>\
                        <svg id="builtSVG"></svg>'
        $('.top-block').append(template);
        this.createPath('#basicSVG',3,'triangle',self.maxRadius,self.pathConstruction1);
        this.changePath('#basicSVG','triangle');
        this.createPath('#builtSVG',3,'triangleBuilt',self.maxRadius*self.similarityRatio,self.pathConstruction2);
        this.changePath('#builtSVG','triangleBuilt');
        this.addEventNet('#basicSVG',"triangle");
        this.addEventNet('#builtSVG',"triangle");
        this.svgElementSize = {w :$('#basicSVG').width(), h: $('#basicSVG').height()};
    },
    addBottomBlockContent: function (){
        var self = this;
        var template = '<div class="slider-box">\
                            <span>Similarity ratio</span>\
                            <div class="slider">\
                                <input type="range" class="range1" min="0.5" value="1" max="1.5" step="0.1" >\
                                <div class="line"></div>\
                            </div>\
                            <input type="text" min="0" max="1.5" value="1">\
                        </div>\
                        <div class="checkboxs">\
                            <div class="triangle checkbox"></div>\
                            <div class="quadrilateral checkbox"></div>\
                        </div>'
        $('.bottom-block').append(template);
        self.addEventCheckbox();
        self.addEventSlide();
    },
    getCorrectedPoint: function( point, center, k, newCenter ){
        var self = this
        var pointDistanceOfCenter = self.sideHeight( point, center )
        var newPointDistanceOfCenter = pointDistanceOfCenter * k
        var horizonitalPoint = {
            x : self.svgElementSize.w,
            y : center.y
        }
        var degreeWithCenter = self.angleDegreeWithP( point, center, horizonitalPoint )
        degreeWithCenter = ( point.y > center.y ) ? 360 - degreeWithCenter : degreeWithCenter
        var newPoint = self.getCorrectPositionWithDegree( degreeWithCenter || 0, newCenter, newPointDistanceOfCenter )
        return newPoint
    },
    addEventSlide: function (){
        var self = this;
        $('.slider-box .slider input').on('input change', function (e) {
            self.point.call(self, e.target.value);
        });
        $(document).on('change','.slider-box input',function(e){
            if(String(Number(e.target.value))=='NaN'){
                return;
            }
            var value = Number(String($(e.target).val()).replace(',','.'));
            if(String(value)=='NaN') return;
            if(value>1.5){
                e.target.value = '1,5';
                value = 1.5;
            }
            else if(value<0.5) {
                e.target.value = '0,5'
                value = 0.5;
            }else e.target.value = String(value).replace('.',',')
            self.setSliderValue(value);
        })
        $(document).on('keydown','input',function(e){
            var value = '';
            if(e.key == 'Enter' || e.key == 'ArrowLeft' || e.key == 'ArrowRight') {
                return;
            }
            if(e.key == 'Backspace'){
                value = (e.target.value.slice(0,e.target.selectionStart-1)+e.target.value.slice(e.target.selectionStart,e.target.value.length)).replace(',','.');
                // if(value[value.length-1]=='.') value = value.replace('.','');
            }else if(e.key == 'Delete'){
                value = e.target.value.slice(0,e.target.selectionStart)+e.target.value.slice(e.target.selectionStart+1,e.target.value.length);    
            }else{
                value = e.target.value.slice(0,e.target.selectionStart)+e.key+e.target.value.slice(e.target.selectionStart,e.target.value.length);
            }
            value = value.replace(',','.')
            if((String(Number(value))=='NaN' && (!(String(value)[value.length-1]=='.') || !(String(value)[0]=='-'))) ||
                Number(value)<e.target.min  ||
                Number(value)>e.target.max){
                console.log('no')
                e.preventDefault();
            }
        })
    },
    point: function (value) {
        var self = this;
        
        var similarityRatio = value;
        
        self.similarityRatio = similarityRatio

        $('.slider-box input[type=text]').val(self.similarityRatio)

        // var center = self.getTranscribedCircleCenter( '#basicSVG', self.typePath )
        // var radius = self.sideHeight( center, self[self.typePath]['dot1'] )
        
        // var centerOfSVG = {
        //     x: self.svgElementSize.w/2,
        //     y: self.svgElementSize.h/2
        // }  
        // for(dotName in self[ self.typePath ]){
        //     var point = self[ self.typePath ][dotName]
        //     var newPointB = self.getCorrectedPoint( Object.assign({}, point), center, self.similarityRatio * self.correctedRadius/radius, centerOfSVG )
        //     self[self.typePath + 'Built'][dotName] = Object.assign({}, newPointB)
        //     var newPoint = self.getCorrectedPoint(  Object.assign({}, point), center, self.correctedRadius/radius, centerOfSVG )
        //     self[self.typePath][dotName] =  Object.assign({}, newPoint)
        // }
        // self.changePath( '#basicSVG', self.typePath )
        // self.changePath( '#builtSVG', self.typePath + 'Built' )
        

        var center = self.getPathCenter(self.typePath);
        var n = Object.keys(self[self.typePath]).length;
        var newCenter = {
            x: self.svgElementSize.w / 2,
            y: self.svgElementSize.h / 2
        }

        var newPointsPos = [];

        //get all dots alfa(degree line of point and center with horizontal)

        for (var i = 1; i <= n; i++) {
            var point = self[self.typePath]['dot' + i];
            var d = self.sideHeight(point, center);

            var alfa = self.angleDegreeWithP(point, center, {x: self.svgElementSize.w, y:center.y});
            alfa = (point.y > center.y) ? 360 - alfa : alfa;
            newPointsPos.push({alfa, d});
        }

        //get max distance from center

        var max = newPointsPos[0].d;

        for(var i = 1; i < n; i++){
            if(newPointsPos[i].d > max){
                max = newPointsPos[i].d;
            }
        }

        max = (max * self.similarityRatio > max) ? (max * self.similarityRatio) : max; 

        //set new point poses

        var distanceConst = max / self.maxRadius;

        for (var i = 0; i < n; i++) {
            var newDistance = newPointsPos[i].d / distanceConst;

            self[self.typePath]['dot' + (i + 1)] = self.getCorrectPositionWithDegree(newPointsPos[i].alfa, newCenter, newDistance);
            self[self.typePath + 'Built']['dot' + (i + 1)] = self.getCorrectPositionWithDegree(newPointsPos[i].alfa, newCenter, newDistance * self.similarityRatio);
        }

        //draw updated path

        self.changePath('#basicSVG', self.typePath);
        self.changePath('#builtSVG', self.typePath + 'Built');

    },
    addEventCheckbox: function (){
        var self = this;
        $(document).off('click','.checkbox').on('click','.checkbox',function(e){
            var elem = $(e.target);
            var className = elem.attr('class').split(' ')[0];
            if(elem.hasClass('active')) return;
            $('.top-block').removeClass('quadrilateral_checked').removeClass('triangle_checked');
            $('.top-block').addClass(className+'_checked')
            $('.checkbox').removeClass('active');
            elem.addClass('active');
            self.typePath = className;
            self.pathCode = '0 0 0'
            var n = (className=='quadrilateral')?4:3;
            self.typePath = className;
            self.createPath('#basicSVG',n,className,self.maxRadius,self.pathConstruction1);
            self.changePath('#basicSVG',className);
            self.createPath('#builtSVG',n,className+'Built',self.maxRadius*self.similarityRatio,self.pathConstruction2);
            self.changePath('#builtSVG',className+'Built');
            self.point(self.similarityRatio);
        })
    },
    addEventNet: function (svgSelector,nameObj){
        var self = this;
        $(document).off('mousedown touchstart', svgSelector + ' circle,' + svgSelector + ' text').on('mousedown touchstart', svgSelector+' circle,'+svgSelector+' text', function(e){
            var index = $(e.target).attr('number');
            $(svgSelector+' circle').attr({class:''});
            $(svgSelector+' circle[number='+index+']').attr({class:'active-circle'});
            self.movingPath = svgSelector;
            self.moveNetIndex = index;
            self.exDot = self[index+'Net']
            if(self.typePath == 'quadrilateral') {
                self.saveDotsPositionWithLine('quadrilateral');
            }
        })
        $(document).off('mousemove touchmove', svgSelector)
        $(document).on('mousemove touchmove', svgSelector,function(e){
            if(self.movingPath=='' || svgSelector != self.movingPath) return;
            var left = $('.top-block').css('left').replace('px', '');
            var top = $('.top-block').css('top').replace('px', '');
            var changeX = (self.movingPath == '#builtSVG') ? ($(svgSelector).width()) : 0;
            var x = e.offsetX || Math.floor(e.originalEvent.touches[0].clientX/zoomScale - w/zoomScale - left - changeX);
            var y = e.offsetY || Math.floor(e.originalEvent.touches[0].clientY/zoomScale - h/zoomScale - top);
            self.changePoint({x: x,y: y},self.typePath);
            // self.changePoint({x: x,y: y},self.typePath);
        })
        $('body').off('mouseup touchend').on('mouseup touchend',function(e){
            if(self.movingPath=='') return;
            $(self.movingPath+' circle').attr({class:''});
            if(self.typePath == 'quadrilateral') {
                self.saveDotsPositionWithLine('quadrilateral');
            }
            // var center = self.getTranscribedCircleCenter('#basicSVG',self.typePath);
            // if(self.typePath=='triangle'){
            //     self.correctPath('#basicSVG',self.typePath,self.correctedRadius);
            //     self.correctPath('#builtSVG',self.typePath+'Built',self.correctedRadius*self.similarityRatio);
            // }
            self.movingPath = '';
        })
    },
    setSliderValue: function (value){
        var self = this;
        // self.similarityRatio = value;
        $('.slider-box .slider input').val(value);
        self.point(value);
    },
    makeSvgTag: function (tag,attr,value){
        if(value==undefined) value = '';
        var el = document.createElementNS('http://www.w3.org/2000/svg', tag);
        for (var k in attr){
            el.setAttribute(k, attr[k]);
        }
        el.innerHTML = value;
        return el;
    },
    getCorrectPositionWithDegree: function(degree,centerPoint,r){
        var self = this;
        var degreeR = degree;
        if(degree>90) degreeR = 180-degree;
        var x = r*Math.cos(degreeR/180*Math.PI);
        var y = r*Math.sin(degreeR/180*Math.PI);
        if(degree==0||degree==180){
            return { x:centerPoint.x+((degree==0)?r:(-r)),
                    y:centerPoint.y}
        }else if(degree==90||degree==270){
            return { x:centerPoint.x,
                    y:centerPoint.y+((degree==90)?(-r):r)}
        }
        if(degree>=90){
            y=-y;
            x=-x;
        }else{
            y = -y;
        }
        return {x:centerPoint.x+x,y:centerPoint.y+y}
    },
    createSVG: function (svg,points,pathConstruction){
        var self = this;
        svg.html('');
        var mainPathM = 'M ',mainPath=null;
        for(elementIndex in pathConstruction){
            for(elem in pathConstruction[elementIndex]){
                if(elem=='start'){
                    for(elem in pathConstruction[elementIndex].start){
                        var attrValue = pathConstruction[elementIndex].start[elem];
                        svg.append(self.makeSvgTag(elem,{[attrValue]:''},''));
                        if(attrValue=='main-path') mainPath = svg.find('path[main-path]');
                    }
                    continue;
                }
                for(point in points){
                    if(mainPath!==null) mainPathM += points[point].x+' '+points[point].y+' L';
                    var index = (point).replace('dot','');
                    var nameUniqAtr = 'none',value = '',attrObj = {number:index};
                    if(pathConstruction[elementIndex][elem]!==null){
                        var point_label_name = 'uniqueLetter';
                        if(Object.keys(points).length > 3) point_label_name = 'uniqueLetter2';
                        if(!(pathConstruction[elementIndex][elem][point_label_name]==undefined)){
                            value = pathConstruction[elementIndex][elem][point_label_name][index-1];
                        }
                        attrObj[pathConstruction[elementIndex][elem].attr] = '';
                    }
                    svg.append(self.makeSvgTag(elem,attrObj,(value).toUpperCase()));
                }
                if(mainPath!==null){
                    mainPath.attr('d',mainPathM.substring(0,mainPathM.length - 1));
                    mainPath=null;
                }
            }
        }
    },
    createPath: function (svgSelector,n,nameObj,r,pathConstruction) {
        var self = this;
        var svg = $(svgSelector);
        var centerPoint = {x: self.svgElementSize.w/2,y: self.svgElementSize.h/2};
        self[nameObj] = {};
        var degreeS = (n==4)?45:90;
        for(var i=0;i<n;i++){
            self[nameObj]['dot'+(i+1)] = this.getCorrectPositionWithDegree((360/n*i)+degreeS,centerPoint,r);
        }
        self.createSVG(svg,self[nameObj],pathConstruction);
    },
    correctIndex: function(index,max=3){
        return (index>max)?Number(index-max):((index<=0)?Number(index+max):index);
    },
    correctPath: function (svgSelector,nameObj,r,pointsObj=null,center,centerOfBody = null){
        var self = this;
        var points =  self[nameObj];
        var i = 0;
        var centerSVG =  {x: self.svgElementSize.w/2,y: self.svgElementSize.h/2}
        var centerRadius = (svgSelector == '#builtSVG') ? self.sideHeight(centerSVG,center)*self.similarityRatio : self.sideHeight(centerSVG,center)/self.similarityRatio
        var degreeRadius = self.angleDegreeWithP(center,centerSVG,{x: self.svgElementSize.w,y:centerSVG.y});
        degreeRadius = (centerSVG.y<center.y) ? 360-degreeRadius : degreeRadius
        var centerD = centerOfBody || self.getCorrectPositionWithDegree(degreeRadius||0,centerSVG,centerRadius);
        for(point in points){
            i++;
            if(i==3 && self.typePath=='quadrilateral') continue;
            var degree = self.angleDegreeWithP(self[pointsObj][point],center,{x: self.svgElementSize.w,y:center.y});
            degree = (self[pointsObj][point].y>center.y)?360-degree : degree;
            self[nameObj][point] = self.getCorrectPositionWithDegree(degree||0,centerD,r) ;
        }
        // $('#builtSVG ellipse[number=1]').attr({cx:centerD.x,cy:centerD.y})
        // $('#builtSVG ellipse[number=2]').attr({cx:centerD.x,cy:centerD.y,rx:r,ry:r})
    },
    changePath: function(svgSelector,nameObj){
        var self = this;
        var points = self[nameObj]
        var n = Object.keys(points).length;
        var mainPath = 'M ';
        var angles = [];
        var degreeSize = 0;
        for(point in points){
            var index = Number(point.replace('dot',''));
            var mainDot = points['dot'+index];
            var nextDot = points['dot'+self.correctIndex(index+1,n)];
            var prevDot = points['dot'+self.correctIndex(index-1,n)]; 
            var degree = Math.round(self.angleDegreeWithP(nextDot,mainDot,prevDot)*100)/100;
            if(index==n){
                degree = Math.round((((n-2)*180)-degreeSize)*100)/100;
                degreeSize = 0;
            }else{
                degreeSize += degree;
            }
            var objForAngle = self.getAllForAngle({index,nameObj,degree,mainDot,prevDot,nextDot});
            angles.push({degree:degree,index:index});
            $(svgSelector+' line[number='+index+']').attr({
                'x1':mainDot.x,
                'y1':mainDot.y,
                'x2':nextDot.x,
                'y2':nextDot.y,
            });
            $(svgSelector+' circle[number='+index+']').attr({
                'cx':mainDot.x,
                'cy':mainDot.y
            });
            $(svgSelector+' text[number='+index+'][dot]').attr({
                'x':mainDot.x-17/2,
                'y':mainDot.y+17/2
            });
            $(svgSelector+' path[number='+index+']').attr('d',objForAngle.pathText);
            $(svgSelector+' rect[number='+index+']').attr(objForAngle.rect);
            $(svgSelector+' text[number='+index+'][degree]').attr({...objForAngle.text,...{['dominant-baseline']:"middle",
            ['text-anchor']:"middle"}}).html(String(degree+'&#730;').replace('.',','));
            // $(svgSelector+' ellipse[number='+index+']').attr({cx:objForAngle.degreeSignPos.x,cy:objForAngle.degreeSignPos.y,rx:3,ry:3})
            mainPath += mainDot.x+' '+mainDot.y+' L';
        }
        // self.setColorAngles(angles);
        $(svgSelector+' path[main-path]').attr('d',mainPath.substring(0,mainPath.length - 1))
    },
    setColorAngles: function (angles){
        var self = this;
        var colors = [];
        for(index in angles){

            var degree = angles[index].degree;
            angles.find(function(angle){
                if(Math.abs(angle.degree-degree)>=self.degreeS){
                    $('path[number='+index+']').css({fill:self.colorForAngles[index-1]});
                    $('path[number='+angle.index+']').css({fill:self.colorForAngles[index-1]});
                }
            })
        }
        // for(var i=0;i<angles.length;i++){
        //     if(Math.abs(angles[i].degree-angles[self.correctIndex((i+2),angles.length)-1].degree)<=self.degreeS){
        //         $('path[number='+i+']').attr({fill:self.colorForAngles[n]});
        //         $('path[number='+self.correctIndex(i+1,angles.length)+']').attr({fill:self.colorForAngles[n]});
        //         var anglesCreated = 
        //         setColorAngles(arr.splice(i+1,2))
        //     }
        // }
    },
    getStructureCenter: function(center){
        var self = this;
        var movedPath = (self.movingPath == '#basicSVG') ? self.typePath : self.typePath + 'Built'
        var stayedPath = (self.movingPath == '#basicSVG') ? self.typePath + 'Built' : self.typePath
        var centerSVG =  {x: self.svgElementSize.w/2,y: self.svgElementSize.h/2}
        var centerRadius = (self.movingPath == '#basicSVG') ? self.sideHeight(centerSVG,center)*self.similarityRatio : self.sideHeight(centerSVG,center)/self.similarityRatio
        var degreeRadius = self.angleDegreeWithP(center, centerSVG, {x: self.svgElementSize.w, y: centerSVG.y});
        degreeRadius = (centerSVG.y<center.y) ? 360-degreeRadius : degreeRadius
        var centerD = self.getCorrectPositionWithDegree(degreeRadius||0,centerSVG,centerRadius);
        if(centerD.x == center.x && centerD.y==center.y) return center
        return centerD
    },
    changePoint: function(dot,nameObj){
        var self = this
        var n = Object.keys(self[nameObj]).length
        var movedPath = self.movingPath
        var stayedPath = (self.movingPath == '#builtSVG') ? '#basicSVG' : '#builtSVG'
        var stayedPathObj = (self.movingPath == '#builtSVG') ? nameObj : nameObj + 'Built'
        var movedPathObj = (self.movingPath == '#builtSVG') ? nameObj + 'Built' : nameObj
        
        self.exDot = {  x : self[ movedPathObj ][ 'dot' + self.moveNetIndex ].x,
                        y : self[ movedPathObj ][ 'dot' + self.moveNetIndex ].y }
        self.exDotStayed = {  x : self[ stayedPathObj ][ 'dot' + self.moveNetIndex ].x,
                              y : self[ stayedPathObj ][ 'dot' + self.moveNetIndex ].y }
        self[movedPathObj]['dot'+self.moveNetIndex] = dot;
        var center = self.getTranscribedCircleCenter(movedPath,movedPathObj)
        var movedRadius = self.sideHeight( center, self[movedPathObj]['dot1'])
        var stayedRadius = (self.movingPath == '#basicSVG') ? (movedRadius*self.similarityRatio) : (movedRadius/self.similarityRatio)
        if(Object.keys(self[nameObj]).length==4){
            self.correctPoint( movedPathObj, 3, center )
            
            self.correctPath( stayedPath, stayedPathObj, stayedRadius, movedPathObj, center )
        }else{
            self.correctPath( stayedPath, stayedPathObj, stayedRadius, movedPathObj, center )
        }

        if(!self.checkValidate(movedPath,nameObj)){
            self[movedPathObj]['dot'+self.moveNetIndex] = self.exDot;
            self[stayedPathObj]['dot'+self.moveNetIndex] = self.exDotStayed;
            if(self.validError) return;
            self.validError = true;
            self.pointDirectionBeforeError = { index : self.moveNetIndex, direction : self.getDotPosWithFrontLine(nameObj, self.moveNetIndex) }
        }else{
            if( self.validError ){
                if( self.pointDirectionBeforeError.index == self.moveNetIndex ){
                    
                    var direction = self.getDotPosWithFrontLine(nameObj, self.moveNetIndex)
                    if( String(direction) != String(self.pointDirectionBeforeError.direction ) ){
                        if(self.typePath != 'quadrilateral'){
                            self.pathCode = ( self.pathCode == '0 0 0' ) ? '0 0 1' : '0 0 0';
                        }
                    }
                }
                self.pointDirectionBeforeError = {}
                self.validError = false
            }
            self.changePath(movedPath,movedPathObj)
            self.changePath(stayedPath,stayedPathObj)
        }
        // $(movedPath+' ellipse[number=2]').attr({ cx : center.x, cy : center.y,
        //                                         rx : stayedRadius, ry : stayedRadius })
    },
    getAngleDegreeWithTwoTriangles: function(nameObj, i) {
        var self = this;
        var n = 4;

        var mainDot = self[nameObj]['dot'+i];
        var nextDot = self[nameObj]['dot'+self.correctIndex(i+1,n)];
        var prevDot = self[nameObj]['dot'+self.correctIndex(i-1,n)];
        var otherDot = self[nameObj]['dot'+self.correctIndex(i-2,n)];
        
        var angle1 = self.angleDegreeWithP(nextDot, mainDot, otherDot);
        var angle2 = self.angleDegreeWithP(prevDot, mainDot, otherDot);

        return angle1+angle2;
            
    },
    getDotPosWithFrontLine: function(nameObj, i){
        var self = this
        var n = Object.keys(self[nameObj]).length
      
        var mainDot = Object.assign({}, self[nameObj][ 'dot' + i ])
        var nextDot = Object.assign({}, self[nameObj][ 'dot' + self.correctIndex( Number(i) + 1, n )]);
        var prevDot = Object.assign({}, self[nameObj][ 'dot' + self.correctIndex( Number(i) - 1, n )]);
        var height = $('#basicSVG').height();
      
        var lineAngleWithHorizonital = self.angleDegreeWithP( nextDot, prevDot, { x : $('#basicSVG').width(), y : prevDot.y });
        if( nextDot.y > prevDot.y ){
            lineAngleWithHorizonital = self.angleDegreeWithP( prevDot, nextDot, { x : $('#basicSVG').width(), y : nextDot.y });
        }
        var x = mainDot.x
        var y = mainDot.y
        var k = Math.tan( lineAngleWithHorizonital / 180 * Math.PI );
        var b = (height - prevDot.y) - (prevDot.x * k);
        var dotY = k * x + b;
        var dotX = (y - b)/k;
        return (dotY > height - mainDot.y)
    },
    saveDotsPositionWithLine: function(nameObj) {
        var self = this;
        var n = Object.keys(self[nameObj]).length
        for(var i = 0; i < n; i++) {
            var posWithLine = self.getDotPosWithFrontLine(nameObj, i+1);
            self.dotsPositionForQuadrilateral[i][0] = posWithLine[0];
            self.dotsPositionForQuadrilateral[i][1] = posWithLine[1];
        }
    },
    getPathCenter: function(nameObj){
        var self = this;
        var n = Object.keys(self[nameObj]).length;
        var xSum = 0, ySum = 0;

        for(var i = 1; i <= n; i++) {
            xSum += self[nameObj]['dot' + i].x;
            ySum += self[nameObj]['dot' + i].y;
        }

        return {x: xSum / n, y: ySum / n}
    },
    correctPoint: function(nameObj,index,center){
        var self = this;
        var centerD = self.getStructureCenter(center)
        var point = self[nameObj]['dot'+index];
        var movedPath = (self.movingPath == '#basicSVG') ? self.typePath : self.typePath + 'Built'
        var stayedPath = (self.movingPath == '#basicSVG') ? self.typePath + 'Built' : self.typePath
        // var centerSVG = {x:$('#builtSVG').width()/2,y:$('#builtSVG').height()/2}
        var side = self.sideHeight(center,point)
        var degree = self.angleDegreeWithP( self[movedPath]['dot' + index], center, { x : self.svgElementSize.w, y : center.y });
        var radius = (self.movingPath == '#basicSVG') ? side*self.similarityRatio : side/self.similarityRatio;
        degree = (point.y>center.y)?360-degree : degree;
        var testDot = self.getCorrectPositionWithDegree( degree || 0, centerD, radius );
        self[stayedPath]['dot'+index] = testDot;
        //$(self.movingPath+' ellipse[number=1]').attr({cx:testDot.x,cy:testDot.y})
    },
    checkValidate: function (svgSelector,nameObj){
        var self = this;

        var valid = true;
        
        var width = self.svgElementSize.w;
        var height = self.svgElementSize.h;
        
        var allValue = this.getAllValue(nameObj)
        var n = Object.keys(self[nameObj]).length;
        var areaTriangle = allValue['side1'] * allValue['side2'] * Math.sin(Math.PI / 180 * allValue['degree1']);
        
        var movedPath = (self.movingPath == '#basicSVG') ? self.typePath : self.typePath + 'Built';
        var builtObj = (self.movingPath == '#basicSVG') ? self.typePath + 'Built' : self.typePath;

        for(var i=1; i <= n; i++){
            var dot = self[movedPath]['dot' + i];
            var dot2 = self[builtObj]['dot' + i];
            
            if(self.typePath == 'triangle') {
                if(!(allValue['degree'+i] > this.paramForControll.minDegree &&
                     allValue['degree'+i] < this.paramForControll.maxDegree)) {
                        valid = false;
                        break;   
                }
                if(areaTriangle * 2 / allValue['side' + i] < this.paramForControll.minAltitude ||
                    areaTriangle * 2 / allValue['side' + i] * self.similarityRatio < this.paramForControll.minAltitude){
                    valid = false;
                    break;
                }
            } else {
                var angleDegree = self.getAngleDegreeWithTwoTriangles('quadrilateral', i);
                if(!(angleDegree > this.paramForControll.minDegree &&
                    angleDegree < this.paramForControll.maxDegree)) {
                        valid = false;
                        break;
                }
            }

            if (svgSelector == '#basicSVG'){
                if(!(allValue['side'+i] * self.similarityRatio > this.paramForControll.minSide &&
                   allValue['side'+i] * self.similarityRatio < this.paramForControll.maxSide)) {
                        valid = false;
                        break;
                }            
            } else {
                if(!(allValue['side'+i] / self.similarityRatio > this.paramForControll.minSide &&
                   allValue['side'+i] / self.similarityRatio < this.paramForControll.maxSide)) {
                        valid = false;
                        break;
                }
            }

            if(!(dot.x > self.circleR && dot.x < width - self.circleR 
                && dot.y > self.circleR && dot.y < height - self.circleR) ||
                !(dot2.x > self.circleR && dot2.x < width - self.circleR 
                && dot2.y > self.circleR && dot2.y < height - self.circleR)) {
                    valid = false;
                    break;
            }

 
            if( !(allValue['side'+i] > this.paramForControll.minSide &&
                allValue['side'+i] < this.paramForControll.maxSide)) {
                    valid = false;
                    break;
            }
        }
    
        if(self.typePath == 'quadrilateral') {
            var height = $('#basicSVG').height();
            var A = Object.assign({}, self[self.typePath]['dot1']);
            var B = Object.assign({}, self[self.typePath]['dot2']);
            var C = Object.assign({}, self[self.typePath]['dot3']);
            var D = Object.assign({}, self[self.typePath]['dot4']);

            var diagonal1 = self.getEquationOfLine(A, C);
            var diagonal2 = self.getEquationOfLine(B, D);

            var diagonalLength1 = self.sideHeight(A, C);
            var diagonalLength2 = self.sideHeight(B, D);

            var crossingPoint = self.getLinesCrossingPoint(diagonal1, diagonal2);
            
            var isCrossingPointLocatedInDiagonal1 = self.isPointLocatedInExcerpt(A, C, crossingPoint);
            var isCrossingPointLocatedInDiagonal2 = self.isPointLocatedInExcerpt(B, D, crossingPoint);

            if(!isCrossingPointLocatedInDiagonal1 || !isCrossingPointLocatedInDiagonal2) {
                valid = false;
            }
            if(diagonalLength1 < this.paramForControll.minDiagonal || diagonalLength2 < this.paramForControll.minDiagonal) {
                valid = false;
            }

        }


        return valid;
    },
    gerTriangleCenter: function(nameObj){
        var self = this;
        var A = self[nameObj]['dot1'];
        var B = self[nameObj]['dot2'];
        var C = self[nameObj]['dot3'];
        return {x: (A.x+B.x+C.x)/3,y:(A.y+B.y+C.y)/3};
    },
    getLinesCrossingPoint: function(aLine, bLine){
        var x = (aLine.b - bLine.b) / (bLine.k - aLine.k);
        var y = aLine.k * x + aLine.b;

        return {x, y};
    },
    getDotFromLineWithX: function(line, x){
        var y = line.k * x + line.b;
        return y;
    },
    getEquationOfLine: function(dot1, dot2){
        var lineAngle, self = this;
        var width = $('#basicSVG').width();
        var height = $('#basicSVG').height();

        //shrjum enq keteri y-nery
        dot1.y = height - dot1.y;
        dot2.y = height - dot2.y;
        
        if(dot1.y > dot2.y){
            lineAngle = self.angleDegreeWithP( dot1, dot2, { x : width, y : dot2.y })
        } else {
            lineAngle = self.angleDegreeWithP( dot2, dot1, { x : width, y : dot1.y })
        }

        var k = Math.tan(lineAngle / 180 * Math.PI);
        var b = dot1.y - (dot1.x * k);
        return {k, b, lineAngle};
    },
    isPointLocatedInExcerpt: function(A, B, F) {
        var self = this;
        
        var lengthExcerpt1 = Math.floor(self.sideHeight(A, F));
        var lengthExcerpt2 = Math.floor(self.sideHeight(B, F));
        var lengthAB = Math.floor(self.sideHeight(A, B));
        
        return (Math.abs(lengthAB - (lengthExcerpt1 + lengthExcerpt2)) <= 3);
    },
    gerQuadrilateralCenter: function(nameObj){
        var self = this;
        var A = self[nameObj]['dot1'];
        var B = self[nameObj]['dot2'];
        var C = self[nameObj]['dot3'];
        var D = self[nameObj]['dot4'];
        return {x: (A.x+B.x+C.x+D.x)/4,y:(A.y+B.y+C.y+D.y)/4};
    },
    checkValidSize: function (nameObj) {
        var size_controll = this.movingPath == '#basicSVG' ? this.size_controll * 1/this.similarityRatio : this.size_controll * this.similarityRatio;
        var center;
        var A = this[nameObj]['dot1'];
        var B = this[nameObj]['dot2'];
        var C = this[nameObj]['dot3'];
        
        if(nameObj=='quadrilateral' || nameObj=='quadrilateralBuilt'){
            center = this.gerQuadrilateralCenter(nameObj);
            var D = this[nameObj]['dot4'];
            var d_center = this.sideHeight(D, center);
            size_controll *= 1.3;
        } else {
            var d_center = 1000;
            center = this.gerTriangleCenter(nameObj);
        }
        var a_center = this.sideHeight(A, center);
        var b_center = this.sideHeight(B, center);
        var c_center = this.sideHeight(C, center);
        if(a_center < size_controll || b_center < size_controll || c_center < size_controll || d_center < size_controll) {
            return false;
        } else {
            return true;
        }
    },
    getAllForAngle:function (objData) {
        var self = this;
        var rectWidth = 72;
        var rectHeight = 30;
        var pathText = this.getPathValue(objData);
        var rect = this.getRectPosition(objData.index,objData.nameObj);
        var text = {x:rect.x+rectWidth/2,y:rect.y+rectHeight/2+3};
        return {pathText,rect,text}
    },
    getRectPosition: function (index,nameObj) {
        var self = this; 
        var rectWidth = 72
        var rectHeight = 30
        var circleRadius = 17
        var changedPX = 72
        var center = self.gerTriangleCenter( nameObj );
        var mainDot = self[ nameObj ][ 'dot' + index];
        var degree = self.angleDegreeWithP( mainDot, center, { x : self.svgElementSize.w, y : center.y } )
        degreeForCircle = (mainDot.y>center.y)?360-degree:degree;
        var dotWithLongRadius = self.getCorrectPositionWithDegree( degreeForCircle, center, self.sideHeight( center, mainDot ) + changedPX )
        var dotWithShortRadius = self.getCorrectPositionWithDegree( degreeForCircle, center, self.sideHeight( center, mainDot ) - changedPX )
        
        var checkRectValidate = ( dot )=>{
            if( (Math.abs(dot.y - mainDot.y) < circleRadius + rectHeight / 2) 
                && (Math.abs(dot.x - mainDot.x) < circleRadius + rectWidth / 2) ){
                return dotWithShortRadius
            }else{
                return dot
            }
        }

        if( dotWithLongRadius.x - rectWidth / 2 < 10 ){
            dotWithLongRadius.x = 10 + rectWidth / 2
        }else if( dotWithLongRadius.x + rectWidth / 2 > self.svgElementSize.w - 10){
            dotWithLongRadius.x = self.svgElementSize.w - (10 + rectWidth / 2 )
        }

        if( dotWithLongRadius.y - rectHeight / 2 < 10 ){
            dotWithLongRadius.y = 10 + rectHeight / 2
        }else if( dotWithLongRadius.y + rectHeight / 2 > self.svgElementSize.h - 10){
            dotWithLongRadius.y = self.svgElementSize.h - (10 + rectHeight / 2)
        }
        var returnedDot = checkRectValidate(dotWithLongRadius)
        return {
            x : returnedDot.x - rectWidth / 2,
            y : returnedDot.y - rectHeight / 2 }
    },
    getTranscribedCircleCenter:function (svgSelector = '#basicSVG',nameObj){
        var self = this;
        var n = (nameObj=='triangle'||nameObj=='triangleBuilt')?3:4;
        var points = self[nameObj]
        var index = 1
        var mainDot = points['dot'+index];
        var nextDot = points['dot'+self.correctIndex(index-1,n)];
        var prevDot = points['dot'+self.correctIndex(index+1,n)]; 
        var radius = self.sideHeight(prevDot,nextDot)/Math.sin(self.angleDegreeWithPoints(1,nameObj)/180*Math.PI)/2;
        var middleDot = {x:(prevDot.x+nextDot.x)/2,y:(prevDot.y+nextDot.y)/2};
        var height = Math.sqrt((Math.pow(radius,2)-Math.pow((self.sideHeight(nextDot,prevDot)/2),2)));
        var degree = 180-self.angleDegreeWithP(prevDot,middleDot,{x:self.svgElementSize.w,y:middleDot.y})
        degreeForCircle = (prevDot.y<middleDot.y)?360-(90+degree):360-(90-degree);
        var centerDot = {};
        for(var i=0;i<3;i++){
            degree = (i==0)?degreeForCircle:(i==1?(180-degreeForCircle):180+degreeForCircle)
            centerDot = self.getCorrectPositionWithDegree(degree,middleDot,height);
            radius = Math.round(radius*100)/100;            
            if(Math.round(self.sideHeight(mainDot,centerDot)*100)/100==radius&&
                Math.round(self.sideHeight(nextDot,centerDot)*100)/100==radius&&
                Math.round(self.sideHeight(prevDot,centerDot)*100)/100==radius){
                break;
            }
        }
        return centerDot;
    },
    getPathValue: function (objData){
        var self = this;
        var points = self[objData.nameObj];
        var n = Object.keys(points).length;
        var circleRadius = 35;
        var correctDotOfNext = this.getCorrectPositionForLine(objData.mainDot,objData.nextDot,circleRadius);
        var correctDotOfPrev = this.getCorrectPositionForLine(objData.mainDot,objData.prevDot,circleRadius);
        var middleText = ' A '+ circleRadius+' '+circleRadius+' ' + this.pathCode + ' '
        if(Math.abs(objData.degree)>180){
            // middleText = ' A '+ circleRadius+' '+circleRadius+' 0 1 0 '
        }else if(Math.abs(objData.degree-90)<=self.degreeS){ 
            var dotOther = {
                x:Number(Number(correctDotOfNext.obj.x)+Number(correctDotOfPrev.obj.x))/2,
                y:Number(Number(correctDotOfNext.obj.y)+Number(correctDotOfPrev.obj.y))/2
            }
            var degree = self.angleDegreeWithP(dotOther,objData.mainDot,{x: self.svgElementSize.w,y:objData.mainDot.y})
            degree = (objData.mainDot.y>dotOther.y)?degree:(360-degree);
            var dot = self.getCorrectPositionWithDegree(degree,objData.mainDot,circleRadius*Math.sqrt(2));
            middleText = ' L'+dot.x+' '+dot.y+' L '
        }
        var pathText = 'M'+
            objData.mainDot.x+' '+objData.mainDot.y
            +' L'+
            correctDotOfNext.str
            +middleText+
            correctDotOfPrev.str+' Z'

        return pathText;
    },
    getCorrectPositionForLine: function (mainDot,secondDot,radius){
        var degree = this.angleDegreeWithDots(secondDot,mainDot,{x:mainDot.x,y:secondDot.y});
        var realW = Math.sin(degree*Math.PI/180)*radius;
        var realH = Math.cos(degree*Math.PI/180)*radius;
        var x,y;
        if(mainDot.x>secondDot.x){
            x=mainDot.x-realW;
        }else if(mainDot.x<secondDot.x){
            x=mainDot.x+realW;
        }else{
            x=mainDot.x;
        }
        if(mainDot.y>secondDot.y){
            y=mainDot.y-realH;
        }else if(mainDot.y<secondDot.y){
            y=mainDot.y+realH;
        }else{
            y=mainDot.y;
        }
        x = Math.round(x)
        y = Math.round(y)
        return {str:x+' '+y,obj:{x,y}};
    },
    getAllValue: function (nameObj) {
        var self = this;
        var obj = self[nameObj];
        var transmittedObj = {},i=1;
        var n = (nameObj=='quadrilateral')?4:3;
        for(dot in obj){
            var mainDot = obj['dot'+i];
            var nextDot = obj['dot'+self.correctIndex(i+1,n)];
            var prevDot = obj['dot'+self.correctIndex(i-1,n)];
            transmittedObj['side'+i] = self.sideHeight(mainDot,nextDot);
            transmittedObj['degree'+i] = self.angleDegreeWithP(mainDot,nextDot,prevDot);
            i++;
        }
        return transmittedObj;
    },
    angleDegreeWithDots: function (a,b,c){
        return Math.acos(this.sideHeight(c,b)/this.sideHeight(a,b))*180/Math.PI;
    },
    sideHeight: function(a,b){
        return Math.sqrt(Math.abs(Math.pow(a.x-b.x,2)+Math.pow(a.y-b.y,2)));
    },
    angleDegree: function(a,b,c){
        var degree = Math.acos((Math.pow(a,2)+Math.pow(c,2)-Math.pow(b,2))/(2*a*c))/Math.PI*180;
        degree = String(degree) == 'NaN' ? ( a + c == b ? 180 : 0) : Number(degree)
        return degree;
    },
    angleDegreeWithPoints: function (index,nameObj){
        var self = this;
        var points = self[nameObj]
        var n = (nameObj=='triangle'||nameObj=='triangleBuilt')?3:4;
        mainDot = points['dot'+index];
        prevDot = points['dot'+self.correctIndex(index-1,n)];
        nextDot = points['dot'+self.correctIndex(index+1,n)];
        return self.angleDegree(self.sideHeight(mainDot,nextDot),self.sideHeight(nextDot,prevDot),self.sideHeight(prevDot,mainDot));
    },
    angleDegreeWithP : function (A,B,C){
        var self = this;
        return self.angleDegree(self.sideHeight(B,C),self.sideHeight(A,C),self.sideHeight(B,A));
    },
    setInstructionHelp: function (select) {
        var self = this;
        var helpList = self.getInstructionList();
        $(select || self.pageHelpText).html('');
        self.pageHelpText = select || self.pageHelpText;
        self.addMathText(self.pageHelpText, helpList);
        $(select || self.pageHelpText).parent().removeClass('help_list_' + self.page - 1).addClass('help_list_' + self.page);  
    },
    getInstructionList: function () {
        var self = this;
        helpList = ['Set the similarity ratio. ',
                    'Manipulate one shape by moving its vertices.',
                    'Observe how the sides and the angles of the other shape change to maintain similarity.',
                    'Repeat the steps for quadrilaterals.'];
        return helpList;
    },
    addMathText: function (pageHelpText, helpList) {
        var ol = document.createElement('ol');
        ol.className = 'help-list';
        $(pageHelpText).html(ol);
        for(var i = 0; i< helpList.length; i++) {
            var id = 'li'+i+(new Date().getTime());
            MathJax.HTML.addElement(ol, "li", {id: id}, [helpList[i]]);
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, id]);
        }
    }
}