// ============================= Way ==========================
//−
var mashtab = 1
jQuery.fn.onPositionChanged = function (trigger, millis) {
    if (millis == null)
        millis = 100;
    let o = $(this[0]);
    if (o.length < 1)
        return o;
    let lastPos = null;
    let lastOff = null;
    setInterval(function () {
        if (o == null || o.length < 1)
            return o;
        if (lastPos == null)
            lastPos = o.position();
        if (lastOff == null)
            lastOff = o.offset();
        let newPos = o.position();
        let newOff = o.offset();
        if (lastPos.top != newPos.top || lastPos.left != newPos.left) {
            $(this).trigger('onPositionChanged', { lastPos: lastPos, newPos: newPos });
            if (typeof (trigger) == "function")
                trigger(lastPos, newPos);
            lastPos = o.position();
        }
        if (lastOff.top != newOff.top || lastOff.left != newOff.left) {
            $(this).trigger('onOffsetChanged', { lastOff: lastOff, newOff: newOff });
            if (typeof (trigger) == "function")
                trigger(lastOff, newOff);
            lastOff = o.offset();
        }
    }, millis);
    return o;
};
function second() {
    $div1 = $('.bot-car');
    $div2 = $('.car');
    let x1 = ($div1.offset().left/mashtab );
    let y1 = $div1.offset().top;
    let h1 = $div1.outerHeight(true);
    let w1 = $div1.outerWidth(true);
    let b1 = y1 + h1;
    let r1 = x1 + w1;
    let x2 = $div2.offset().left/mashtab;
    let y2 = $div2.offset().top;
    let h2 = $div2.outerHeight(true);
    let w2 = $div2.outerWidth(true);
    let b2 = y2 + h2;
    let r2 = x2 + w2;
    if (b1 < y2 || y1 > b2 || r1 < x2 || x1 > r2) {
        return false;
    }
    return true;
}
let audio = $(".myAudio");
let k = 1
$(".car").onPositionChanged(() => {
    let carMargin = $('.car').position().left;
    let SecondCarMargin = $('.bot-car').position().left;
    if (second()) {
        $(".car").css("-webkit-animation-play-state", "paused");
        $(".bot-car").css("-webkit-animation-play-state", "paused");
        if (k <= 1) {
            audio.trigger('play')
            let crashedCar = $(".car").attr("src");
            crashedCar = `${crashedCar.slice(0, crashedCar.length - 4)}_d.png`;
            let crashedSecondCar = $(".bot-car").attr("src");
            crashedSecondCar = `${crashedSecondCar.slice(0, crashedSecondCar.length - 4)}_d.png`;
            $(".car").attr("src", crashedCar);
            $(".bot-car").attr("src", crashedSecondCar);
            k = 2;
        }
    }
    // if (carMargin < -399 && SecondCarMargin < -399 || carMargin >= 1279 && SecondCarMargin >= 1279 || carMargin < -399 && SecondCarMargin >= 1279) {
    //     $(".car").css("-webkit-animation-play-state", "paused");
    //     $(".bot-car").css("-webkit-animation-play-state", "paused");
    //     // window.location.reload(true);
    // }
});
$(".bot-car").onPositionChanged(() => {
    let carMargin = $('.car').position().left;
    let SecondCarMargin = $('.bot-car').position().left;
    if (second()) {
        $(".car").css("-webkit-animation-play-state", "paused");
        $(".bot-car").css("-webkit-animation-play-state", "paused");
        if (k <= 1) {
            audio.trigger('play')
            let crashedCar = $(".car").attr("src");
            crashedCar = `${crashedCar.slice(0, crashedCar.length - 4)}_d.png`;
            let crashedSecondCar = $(".bot-car").attr("src");
            crashedSecondCar = `${crashedSecondCar.slice(0, crashedSecondCar.length - 4)}_d.png`;
            $(".car").attr("src", crashedCar);
            $(".bot-car").attr("src", crashedSecondCar);
            k = 2;
        }
    }
    if (carMargin < -399 && SecondCarMargin < -399 || carMargin >= 1279 && SecondCarMargin >= 1279 || carMargin < -399 && SecondCarMargin >= 1279) {
        $(".car").css("-webkit-animation-play-state", "paused");
        $(".bot-car").css("-webkit-animation-play-state", "paused");
        // window.location.reload(true);
    }
});
// ========================== Filter ==============================
let count1 = 0;
$('.first-car-flt .change-direction-btn').on('click', () => {
    if (count1 % 2 == 0) {
        let text = $('.before_collision .firstCar-speed').text().replace('−','-')
        let speedCount = $('.carSpeedCount').text().replace('−','-')
        let val = parseInt($('.before_collision .firstCar-mass').text().replace(/\s/g, '').replace('−', '-'))
        let range_val = -parseInt($('.before_collision .firstCar-speed').text().replace(/\s/g, '').replace('−', '-'))
        $('.before_collision .firstCar-speed').text((-text).toString().replace('-', '−'))
        $('.carSpeedCount').text(speedCount.toString().replace('-', '−'))
        $('.before_collision .firstCar__value').text(((val) * (range_val)).toString().replace('-', '−'))
        let firstCar__value = $('.before_collision .firstCar__value').text().replace(/\s/g, '').replace('−', '-');
        let SecondCar__value = $('.before_collision .SecondCar__value').text().replace(/\s/g, '').replace('−', '-');
        $('.before_collision .final-value').text((parseInt(firstCar__value) + parseInt(SecondCar__value)).toString().replace('-', '−'))
    }
    else if (count1 % 2 !== 0) {
        let text = $('.before_collision .firstCar-speed').text().replace('−', '-')
        let speedCount = $('.carSpeedCount').text().replace('−', '-')
        let val = parseInt($('.before_collision .firstCar-mass').text().replace(/\s/g, '').replace('−', '-'))
        let range_val = -parseInt($('.before_collision .firstCar-speed').text().replace(/\s/g, '').replace('−', '-'))
        $('.before_collision .firstCar-speed').text((-text).toString().replace('-', '−'))
        $('.carSpeedCount').text(speedCount.toString().replace('-', '−'))
        $('.before_collision .firstCar__value').text(((val) * (range_val)).toString().replace('-', '−'))
        let firstCar__value = $('.before_collision .firstCar__value').text().replace(/\s/g, '').replace('−', '-');
        let SecondCar__value = $('.before_collision .SecondCar__value').text().replace(/\s/g, '').replace('−', '-');
        $('.before_collision .final-value').text((parseInt(firstCar__value) + parseInt(SecondCar__value)).toString().replace('-', '−'))
    }
    count1++;
})

let secondCount1 = 0;
$('.second-car-flt .change-direction-btn').on('click', () => {
    if (secondCount1 % 2 == 0) {
        let text = $('.before_collision .SecondCar-speed').text().replace('−', '-')
        let speedCount = $('.SecondCarSpeedCount').text().replace('−', '-')
        let val = parseInt($('.before_collision .SecondCar-mass').text().replace(/\s/g, '').replace('−', '-'))
        let range_val = -parseInt($('.before_collision .SecondCar-speed').text().replace(/\s/g, '').replace('−', '-'))
        $('.before_collision .SecondCar-speed').text((-text).toString().replace('-', '−'))
        $('.SecondCarSpeedCount').text(speedCount.toString().replace('-', '−'))
        $('.before_collision .SecondCar__value').text(((val) * (range_val)).toString().replace('-', '−'))
        let firstCar__value = $('.before_collision .firstCar__value').text().replace(/\s/g, '').replace('−', '-');
        let SecondCar__value = $('.before_collision .SecondCar__value').text().replace(/\s/g, '').replace('−', '-');
        $('.before_collision .final-value').text((parseInt(firstCar__value) + parseInt(SecondCar__value)).toString().replace('-', '−'))
    }
    else if (secondCount1 % 2 !== 0) {
        let text = $('.before_collision .SecondCar-speed').text().replace('−', '-')
        let speedCount = $('.SecondCarSpeedCount').text().replace('−', '-')
        let val = parseInt($('.before_collision .SecondCar-mass').text().replace(/\s/g, '').replace('−', '-'))
        let range_val = -parseInt($('.before_collision .SecondCar-speed').text().replace(/\s/g, '').replace('−', '-'))
        $('.before_collision .SecondCar-speed').text((- text).toString().replace('-', '−'))
        $('.SecondCarSpeedCount').text(speedCount.toString().replace('-', '−'))
        $('.before_collision .SecondCar__value').text(((val) * (range_val)).toString().replace('-', '−'))
        let firstCar__value = $('.before_collision .firstCar__value').text().replace(/\s/g, '').replace('−', '-');
        let SecondCar__value = $('.before_collision .SecondCar__value').text().replace(/\s/g, '').replace('−', '-');
        $('.before_collision .final-value').text((parseInt(firstCar__value) + parseInt(SecondCar__value)).toString().replace('-', '−'))
    }
    secondCount1++;
})

let countI = 1;
$('.first-car-flt .cars-mass-block .wcar').on('click', function () {
    $(".car").attr("src", "./assets/img/Cars/car1.png");
})

$('.first-car-flt .cars-mass-DirectionChange-block .wcar').on('click', function () {
    $(".car").attr("src", "./assets/img/Cars/car1F.png");
})

$('.first-car-flt .wcar').on('click', function () {
    $(".car").css({ 'max-height': '70px' });
    let val = $(this).data('value');
    $('.mass-count').text(750 + ' kg');
    $('.firstCar-mass').text(val)
    if ($('.car').hasClass('carDirectionChange')) {
        var range_val = -$('.FirstCar__Speed').val()
    }
    else {
        var range_val = $('.FirstCar__Speed').val()
    }
    $('.before_collision .firstCar__value').text(((val) * (range_val)).toString().replace('-', '−'))
    let firstCar__value = $('.before_collision .firstCar__value').text().replace(/\s/g, '').replace('−', '-');
    let SecondCar__value = $('.before_collision .SecondCar__value').text().replace(/\s/g, '').replace('−', '-');
    $('.before_collision .final-value').text((parseInt(firstCar__value) + parseInt(SecondCar__value)).toString().replace('-', '−'))
})

$('.first-car-flt .cars-mass-block .jeep').on('click', function () {
    $(".car").attr("src", "./assets/img/Cars/jeep1.png");
})

$('.first-car-flt .cars-mass-DirectionChange-block .jeep').on('click', function () {
    $(".car").attr("src", "./assets/img/Cars/jeep1F.png");
})

$('.first-car-flt .jeep').on('click', function () {
    $(".car").css({ 'max-height': '90px' })
    let val = $(this).data('value');
    $('.mass-count').text('1 700' + ' kg');
    $('.firstCar-mass').text(val.toString().replace('-', '−'))
    if ($('.car').hasClass('carDirectionChange')) {
        var range_val = -$('.FirstCar__Speed').val()
    }
    else {
        var range_val = $('.FirstCar__Speed').val()
    }
    $('.before_collision .firstCar__value').text(((val) * (range_val)).toString().replace('-', '−'))
    let firstCar__value = $('.before_collision .firstCar__value').text().replace(/\s/g, '').replace('−', '-');
    let SecondCar__value = $('.before_collision .SecondCar__value').text().replace(/\s/g, '').replace('−', '-');
    $('.before_collision .final-value').text((parseInt(firstCar__value) + parseInt(SecondCar__value)).toString().replace('-', '−'))
})

$('.first-car-flt .cars-mass-block .truck').on('click', function () {
    $(".car").attr("src", "./assets/img/Cars/tr1.png");
})

$('.first-car-flt .cars-mass-DirectionChange-block .truck').on('click', function () {
    $(".car").attr("src", "./assets/img/Cars/tr1F.png");
})

$('.first-car-flt .truck').on('click', function () {
    $(".car").css({ 'max-height': '150px' })
    let val = $(this).data('value');
    $('.mass-count').text('5 000' + ' kg');
    $('.firstCar-mass').text(val)
    if ($('.car').hasClass('carDirectionChange')) {
        var range_val = -$('.FirstCar__Speed').val()
    }
    else {
        var range_val = $('.FirstCar__Speed').val()
    }
    $('.before_collision .firstCar__value').text(((val) * (range_val)).toString().replace('-', '−'))
    let firstCar__value = $('.before_collision .firstCar__value').text().replace(/\s/g, '').replace('−', '-');
    let SecondCar__value = $('.before_collision .SecondCar__value').text().replace(/\s/g, '').replace('−', '-');
    $('.before_collision .final-value').text((parseInt(firstCar__value) + parseInt(SecondCar__value)).toString().replace('-', '−'))
})

$('.second-car-flt .cars-mass-block .wcar').on('click', function () {
    $(".bot-car").attr("src", "./assets/img/Cars/car2F.png");
})

$('.second-car-flt .cars-mass-DirectionChange-block .wcar').on('click', function () {
    $(".bot-car").attr("src", "./assets/img/Cars/car2.png");
})

$('.second-car-flt .wcar').on('click', function () {
    $(".bot-car").css({ 'max-height': '70px' })
    let val = $(this).data('value');
    $('.SecondMass-count').text(val + ' kg');
    $('.SecondCar-mass').text(val.toString().replace('-', '−'))
    if ($('.bot-car').hasClass('SecondCarDirectionChange') == false) {
        var range_val = -$('.SecondCar__Speed').val()
    }
    else {
        var range_val = $('.SecondCar__Speed').val()
    }
    $('.before_collision .SecondCar__value').text(((val) * (range_val)).toString().replace('-', '−'))
    let firstCar__value = $('.before_collision .firstCar__value').text().replace(/\s/g, '').replace('−', '-');
    let SecondCar__value = $('.before_collision .SecondCar__value').text().replace(/\s/g, '').replace('−', '-');
    $('.before_collision .final-value').text((parseInt(firstCar__value) + parseInt(SecondCar__value)).toString().replace('-', '−'))
})

$('.second-car-flt .cars-mass-block .jeep').on('click', function () {
    $(".bot-car").attr("src", "./assets/img/Cars/jeep2F.png");
})

$('.second-car-flt .cars-mass-DirectionChange-block .jeep').on('click', function () {
    $(".bot-car").attr("src", "./assets/img/Cars/jeep2.png");
})

$('.second-car-flt .jeep').on('click', function () {
    $(".bot-car").css({ 'max-height': '90px' })
    let val = $(this).data('value');
    $('.SecondMass-count').text('1 700' + ' kg');
    $('.SecondCar-mass').text(val.toString().replace('-', '−'))
    if ($('.bot-car').hasClass('SecondCarDirectionChange') == false) {
        var range_val = -$('.SecondCar__Speed').val()
    }
    else {
        var range_val = $('.SecondCar__Speed').val()
    }
    $('.before_collision .SecondCar__value').text(((val) * (range_val)).toString().replace('-', '−'))
    let firstCar__value = $('.before_collision .firstCar__value').text().replace(/\s/g, '').replace('−', '-');
    let SecondCar__value = $('.before_collision .SecondCar__value').text().replace(/\s/g, '').replace('−', '-');
    $('.before_collision .final-value').text((parseInt(firstCar__value) + parseInt(SecondCar__value)).toString().replace('-', '−'))
})

$('.second-car-flt .cars-mass-block .truck').on('click', function () {
    $(".bot-car").attr("src", "./assets/img/Cars/tr2F.png");
})

$('.second-car-flt .cars-mass-DirectionChange-block .truck').on('click', function () {
    $(".bot-car").attr("src", "./assets/img/Cars/tr2.png");
})

$('.second-car-flt .truck').on('click', function () {
    $(".bot-car").css({ 'max-height': '150px' })
    let val = $(this).data('value');
    $('.SecondMass-count').text('5 000' + ' kg');
    $('.SecondCar-mass').text(val.toString().replace('-', '−'))
    if ($('.bot-car').hasClass('SecondCarDirectionChange') == false) {
        var range_val = -$('.SecondCar__Speed').val()
    }
    else {
        var range_val = $('.SecondCar__Speed').val()
    }
    $('.before_collision .SecondCar__value').text(((val) * (range_val)).toString().replace('-', '−'))
    let firstCar__value = $('.before_collision .firstCar__value').text().replace(/\s/g, '').replace('−', '-');
    let SecondCar__value = $('.before_collision .SecondCar__value').text().replace(/\s/g, '').replace('−', '-');
    $('.before_collision .final-value').text((parseInt(firstCar__value) + parseInt(SecondCar__value)).toString().replace('-', '−'))
})



$(".first-car-flt .car-mass-btn img").click(function (event) {
    event.preventDefault();
    if ($(".car-mass-btn").is(":disabled")) {
        return false
    }
    else {
        $(".first-car-flt .car-mass-btn img.active").removeClass("active");
        $(this).addClass("active");
        $('.first-car-flt .car-mass-btn').each(function () {
            let carPasive = $(this).children().attr('src');
            if (carPasive.length == 45) {
                carPasive = `${carPasive.slice(0, carPasive.length - 5)}.png`;
                $(this).children().attr("src", carPasive);
            }
            else if (carPasive.length == 46) {
                carPasive = `${carPasive.slice(0, carPasive.length - 6)}.png`;
                $(this).children().attr("src", carPasive);
            }
        });
        let carActive = $(".first-car-flt .car-mass-btn img.active").attr("src");
        carActive = `${carActive.slice(0, carActive.length - 4)}_.png`;
        $('.first-car-flt .car-mass-btn img.active').attr("src", carActive);
    }
})

$(".first-car-flt .cars-mass-DirectionChange-block img").click(function (event) {
    event.preventDefault();
    if ($(".car-mass-btn").is(":disabled")) {
        return false
    }
    else {
        $(".first-car-flt .car-mass-DirectionChange-btn img.active").removeClass("active");
        $(this).addClass("active");
        $('.first-car-flt .car-mass-DirectionChange-btn').each(function () {
            let carPasive = $(this).children().attr('src');
            if (carPasive.length == 45) {
                carPasive = `${carPasive.slice(0, carPasive.length - 4)}.png`;
                $(this).children().attr("src", carPasive);
            }
            else if (carPasive.length == 46) {
                carPasive = `${carPasive.slice(0, carPasive.length - 6)}F.png`;
                $(this).children().attr("src", carPasive);
            }
        });
        let carActive = $(".first-car-flt .car-mass-DirectionChange-btn img.active").attr("src");
        carActive = `${carActive.slice(0, carActive.length - 5)}_F.png`;
        $('.first-car-flt .car-mass-DirectionChange-btn img.active').attr("src", carActive);
    }
})

$('.first-car-flt .car-mass-btn').on('click', function () {
    let val = $(this).data('value')
    $('.first-car-flt .car-mass-DirectionChange-btn').each(function () {
        let carPasive = $(this).children().attr('src');
        if (carPasive.length == 46) {
            carPasive = `${carPasive.slice(0, carPasive.length - 6)}F.png`;
            $(this).children().attr("src", carPasive);
        }
        if (val == $(this).data('value')) {
            let carActive = $(this).children().attr("src");
            carActive = `${carActive.slice(0, carActive.length - 5)}_F.png`;
            $(this).children().attr("src", carActive);
        }
    })
})

$('.first-car-flt .car-mass-DirectionChange-btn').on('click', function () {
    let val = $(this).data('value')
    $('.first-car-flt .car-mass-btn').each(function () {
        let carPasive = $(this).children().attr('src');
        if (carPasive.length == 45) {
            carPasive = `${carPasive.slice(0, carPasive.length - 5)}.png`;
            $(this).children().attr("src", carPasive);
        }
        if (val == $(this).data('value')) {
            let carActive = $(this).children().attr("src");
            carActive = `${carActive.slice(0, carActive.length - 4)}_.png`;
            $(this).children().attr("src", carActive);
        }
    })
})

$(".second-car-flt .car-mass-btn img").click(function (event) {
    event.preventDefault();
    if ($(".car-mass-btn").is(":disabled")) {
        return false
    }
    else {
        $(".second-car-flt .car-mass-btn img.active").removeClass("active");
        $(this).addClass("active");
        $('.second-car-flt .car-mass-btn').each(function () {
            let carPasive = $(this).children().attr('src');
            if (carPasive.length == 45) {
                carPasive = `${carPasive.slice(0, carPasive.length - 4)}.png`;
                $(this).children().attr("src", carPasive);
            }
            else if (carPasive.length == 46) {
                carPasive = `${carPasive.slice(0, carPasive.length - 5)}.png`;
                $(this).children().attr("src", carPasive);
            }
        });
        let carActive = $(".second-car-flt .car-mass-btn img.active").attr("src");
        carActive = `${carActive.slice(0, carActive.length - 4)}_.png`;
        $('.second-car-flt .car-mass-btn img.active').attr("src", carActive);
    }
});

$(".second-car-flt .cars-mass-DirectionChange-block img").click(function (event) {
    event.preventDefault();
    if ($(".car-mass-btn").is(":disabled")) {
        return false
    }
    else {
        $(".second-car-flt .car-mass-DirectionChange-btn img.active").removeClass("active");
        $(this).addClass("active");
        $('.second-car-flt .car-mass-DirectionChange-btn').each(function () {
            let carPasive = $(this).children().attr('src');
            if (carPasive.length == 46) {
                carPasive = `${carPasive.slice(0, carPasive.length - 4)}.png`;
                $(this).children().attr("src", carPasive);
            }
            else if (carPasive.length == 47) {
                carPasive = `${carPasive.slice(0, carPasive.length - 6)}F.png`;
                $(this).children().attr("src", carPasive);
            }
        });
        let carActive = $(".second-car-flt .car-mass-DirectionChange-btn img.active").attr("src");
        carActive = `${carActive.slice(0, carActive.length - 5)}_F.png`;
        $('.second-car-flt .car-mass-DirectionChange-btn img.active').attr("src", carActive);
    }
})

$('.second-car-flt .car-mass-btn').on('click', function () {
    let val = $(this).data('value')
    $('.second-car-flt .car-mass-DirectionChange-btn').each(function () {
        let carPasive = $(this).children().attr('src');
        if (carPasive.length == 47) {
            carPasive = `${carPasive.slice(0, carPasive.length - 6)}F.png`;
            $(this).children().attr("src", carPasive);
        }
        if (val == $(this).data('value')) {
            let carActive = $(this).children().attr("src");
            carActive = `${carActive.slice(0, carActive.length - 5)}_F.png`;
            $(this).children().attr("src", carActive);
        }
    })
})

$('.second-car-flt .car-mass-DirectionChange-btn').on('click', function () {
    let val = $(this).data('value')
    $('.second-car-flt .car-mass-btn').each(function () {
        let carPasive = $(this).children().attr('src');
        if (carPasive.length == 46) {
            carPasive = `${carPasive.slice(0, carPasive.length - 5)}.png`;
            $(this).children().attr("src", carPasive);
        }
        if (val == $(this).data('value')) {
            let carActive = $(this).children().attr("src");
            carActive = `${carActive.slice(0, carActive.length - 4)}_.png`;
            $(this).children().attr("src", carActive);
        }
    })
})

$('.reset').on('click', () => {
    window.location.reload(true);
})

let i = 1;
var Vf1New;
$('.go').on('click', () => {
    $(".car").css("-webkit-animation-play-state", "running");
    $(".bot-car").css("-webkit-animation-play-state", "running");
    i++;
    if (i > 1) {
        $('.change-direction-btn, .car-mass-DirectionChange-btn, .car-mass-btn, .go, .FirstCar__Speed, .SecondCar__Speed').attr('disabled', 'disabled');
    }
    let m1 = parseInt($('.before_collision .firstCar-mass').text().replace(/\s/g, '').replace('−','-'));
    let m2 = parseInt($('.before_collision .SecondCar-mass').text().replace(/\s/g, '').replace('−','-'));
    let v1 = parseInt($('.before_collision .firstCar-speed').text().replace(/\s/g, '').replace('−','-'));
    let v2 = parseInt($('.before_collision .SecondCar-speed').text().replace(/\s/g, '').replace('−','-'));
    let a = 1 + m1 / m2;
    let b = -2 * ((m1 * v1 + m2 * v2) / m2);
    let c = m2 / m1 * Math.pow(((m1 * v1 + m2 * v2) / m2), 2) - Math.pow(v1, 2) - m2 / m1 * Math.pow(v2, 2);
    let Vf1Plus = (-b + Math.sqrt(Math.pow(b, 2) - (4 * a * c))) / (2 * a);
    let Vf1Minus = (-b - Math.sqrt(Math.pow(b, 2) - (4 * a * c))) / (2 * a);
    Vf1Minus = Vf1Minus.toFixed(0)
    let Vf1_20 = ((Vf1Minus) * 20) / 100;
    let Vf1 = (Vf1Minus) - (Vf1_20)
    if (Vf1 < -20) {
        Vf1 = Vf1 - Vf1_20;
    }
    Vf1New = Vf1 = Vf1.toFixed(0)
    let Vf2 = ((m1 * v1 + m2 * v2) - m1 * Vf1) / m2;

    function animation_cars(Vf1_local, Vf2_local){
        var position1 = 0,  position2 = 0, duration1 = 0, duration2 = 0;
        if(Vf1_local > 0){
            position1 = $('.car').position().left + 1500;
        } else if(Vf1_local < 0){
            position1 = $('.car').position().left - 1500;
        }
        if(Vf2_local > 0){
            position2 = $('.bot-car').position().left - 1500;
        } else if(Vf2_local < 0) {
            position2 = $('.bot-car').position().left + 1500;
        }
        Vf1_local == 0? duration1 = 0 : duration1 = Math.abs(position1/Vf1_local/11);
        Vf2_local == 0? duration2 = 0 : duration2 = Math.abs(position2/Vf2_local/11);
        // console.log(duration1, position1, Vf1_local)
        // console.log(duration2, position2, Vf2_local)
        $('.car').css({ 'transition': duration1 + 's', 'margin-left': position1 + "px" , "transition-timing-function": "linear"});
        $('.bot-car').css({ 'transition': duration2 + 's', 'margin-right': position2 + "px", "transition-timing-function": "linear" });
        $('.car.carDirectionChange').css({ 'transition': duration1 + 's', 'margin-right': (-position1) + "px" , "transition-timing-function": "linear"});
        $('.bot-car.SecondCarDirectionChange').css({ 'transition': duration2 + 's', 'margin-left': (-position2) + "px", "transition-timing-function": "linear" });
    }
    var count = 0
    if ($(".car").hasClass('carStart')) {
        $(".car").onPositionChanged(() => {
            if (second()) {
                if(count > 0) return ;
                count++
                animation_cars(Vf1, Vf2);
            }
        });
    }
    else {
        $(".bot-car").onPositionChanged(() => {
            if (second()) {
                if(count > 0) return;
                count++
                animation_cars(Vf1, Vf2);
            }
        });
    }
    let j = 1
    $(".car").onPositionChanged(() => {
        if (second()) {
            if (j <= 1) {
                let FinalValueForSecond =  parseInt($('.after_collision .firstCar-mass').text().replace(/\s/g, '').replace('−','-') * (Vf1));
                let AfterFinalValueForSecond = parseInt($('.after_collision .SecondCar-mass').text().replace(/\s/g, '').replace('−','-') * (Vf2));
                let EInExcel = parseInt((FinalValueForSecond)) + parseInt((AfterFinalValueForSecond));
                let AInExcel = parseInt($('.before_collision .firstCar-mass').text().replace(/\s/g, '').replace('−','-'));
                let GInExcel = parseInt(Vf1)
                let CInExcel = parseInt($('.before_collision .SecondCar-mass').text().replace(/\s/g, '').replace('−','-'));
                let Vf2ForSecond = ((EInExcel) - (AInExcel) * (GInExcel)) / CInExcel
                let Vf1ToFixed = Vf1
                let Vf2ToFixed = Number(Vf2ForSecond.toFixed(2)).toString().replace('.', ',');
                $('.AfterfirstCar-speed').text(Vf1ToFixed.replace('.', ','));
                $('.After_SecondCar-speed').text(Vf2ToFixed.replace('.', ','));
                let After_firstCar__value = parseInt($('.after_collision .firstCar-mass').text().replace(/\s/g, '').replace('−','-') * (Vf1));
                $('.after_collision .After_firstCar__value').text(After_firstCar__value.toString().replace('-', '−'))
                let After_SecondCar__value = parseInt($('.after_collision .SecondCar-mass').text().replace(/\s/g, '').replace('−','-') * (Vf2));
                $('.after_collision .After_SecondCar__value').text(After_SecondCar__value)
                let After_final_value = parseInt((After_firstCar__value)) + parseInt((After_SecondCar__value));
                $('.After_final-value').text(After_final_value.toString().replace('-', '−'))
                j = 2;
                let AfterfirstCarSpeed = Vf1ToFixed.replace(/\s/g, '');
                let AfterSecondCarSpeed = Vf2ToFixed.replace(/\s/g, '');
                let AfterFirstCarValue = $('.After_firstCar__value').text().replace(/\s/g, '').replace('−','-');
                let AfterSecondCarValue = $('.After_SecondCar__value').text().replace(/\s/g, '').replace('−','-');
                let AfterFinalValue = $('.After_final-value').text().replace(/\s/g, '').replace('−','-');
                if (parseInt(AfterfirstCarSpeed) < 0) {
                    $('.AfterfirstCar-speed').text(spaceNumber(AfterfirstCarSpeed));
                    $('.AfterfirstCar-speed').addClass('active');
                }
                if (parseInt(AfterfirstCarSpeed) >= 0) {
                    $('.AfterfirstCar-speed.active').removeClass('active');
                }
                if (parseInt(AfterSecondCarSpeed) < 0) {
                    $('.After_SecondCar-speed').text(spaceNumber(AfterSecondCarSpeed));
                    $('.After_SecondCar-speed').addClass('active');
                }
                if (parseInt(AfterSecondCarSpeed) >= 0) {
                    $('.After_SecondCar-speed.active').removeClass('active');
                }
                if (parseInt(AfterFirstCarValue) < 0) {
                    $('.After_firstCar__value').text(spaceNumber(AfterFirstCarValue));
                    $('.After_firstCar__value').addClass('active');
                }
                if (parseInt(AfterFirstCarValue) >= 0) {
                    $('.After_firstCar__value.active').removeClass('active');
                }
                if (parseInt(AfterSecondCarValue) < 0) {
                    $('.After_SecondCar__value').text(spaceNumber(AfterSecondCarValue));
                    $('.After_SecondCar__value').addClass('active');
                }
                if (parseInt(AfterSecondCarValue) >= 0) {
                    $('.After_SecondCar__value.active').removeClass('active');
                }
                if (parseInt(AfterFinalValue) < 0) {
                    $('.After_final-value').text(spaceNumber(AfterFinalValue));
                    $('.After_final-value').addClass('active');
                }
                if (parseInt(AfterFinalValue) >= 0) {
                    $('.After_final-value.active').removeClass('active');
                }
                let AfterFirstCarMassAddSpace = $('.after_collision .firstCar-mass').text().replace('−','-');
                $('.after_collision .firstCar-mass').text(spaceNumber(AfterFirstCarMassAddSpace));

                let AfterSecondCarMassAddSpace = $('.after_collision .SecondCar-mass').text().replace('−','-');
                $('.after_collision .SecondCar-mass').text(spaceNumber(AfterSecondCarMassAddSpace));
                
                let AfterFirstCarValueAddSpace = $('.after_collision .After_firstCar__value').text().replace('−','-');
                $('.after_collision .After_firstCar__value').text(spaceNumber(AfterFirstCarValueAddSpace));
                
                let AfterSecondCarValueAddSpace = $('.after_collision .After_SecondCar__value').text().replace('−','-');
                $('.after_collision .After_SecondCar__value').text(spaceNumber(AfterSecondCarValueAddSpace));
                
                let AfterFinalValueAddSpace = $('.after_collision .After_final-value').text().replace('−','-');
                $('.after_collision .After_final-value').text(spaceNumber(AfterFinalValueAddSpace));
                
                $('.mass-text, .SecondMass-text, .carSpeed, .SecondCarSpeed').css({'display' : 'none'})
            }
        }
    });
    $(".bot-car").onPositionChanged(() => {
        if (second()) {
            if (j <= 1) {
                let FinalValueForSecond =  parseInt($('.after_collision .firstCar-mass').text().replace(/\s/g, '').replace('−','-') * (Vf1));
                let AfterFinalValueForSecond = parseInt($('.after_collision .SecondCar-mass').text().replace(/\s/g, '').replace('−','-') * (Vf2));
                let EInExcel = parseInt((FinalValueForSecond)) + parseInt((AfterFinalValueForSecond));
                let AInExcel = parseInt($('.before_collision .firstCar-mass').text().replace(/\s/g, '').replace('−','-'));
                let GInExcel = parseInt(Vf1New.replace(/\s/g, ''));
                let CInExcel = parseInt($('.before_collision .SecondCar-mass').text().replace(/\s/g, '').replace('−','-'));
                let Vf2ForSecond = ((EInExcel) - (AInExcel) * (GInExcel)) / CInExcel
                let Vf1ToFixed = Vf1
                let Vf2ToFixed = Vf2ForSecond.toFixed(2)
                $('.AfterfirstCar-speed').text(Vf1ToFixed.replace('.', ','));
                $('.After_SecondCar-speed').text(Vf2ToFixed.replace('.', ','));
                let After_firstCar__value = parseInt($('.after_collision .firstCar-mass').text().replace(/\s/g, '').replace('−','-') * (Vf1));
                $('.after_collision .After_firstCar__value').text(After_firstCar__value)
                let After_SecondCar__value = parseInt($('.after_collision .SecondCar-mass').text().replace(/\s/g, '').replace('−','-') * (Vf2));
                $('.after_collision .After_SecondCar__value').text(After_SecondCar__value)
                let After_final_value = parseInt((After_firstCar__value)) + parseInt((After_SecondCar__value));
                $('.After_final-value').text(After_final_value)
                j = 2;
                let AfterfirstCarSpeed = $('.AfterfirstCar-speed').text().replace(/\s/g, '').replace('−','-');
                let AfterSecondCarSpeed = $('.After_SecondCar-speed').text().replace(/\s/g, '').replace('−','-');
                let AfterFirstCarValue = $('.After_firstCar__value').text().replace(/\s/g, '').replace('−','-');
                let AfterSecondCarValue = $('.After_SecondCar__value').text().replace(/\s/g, '').replace('−','-');
                let AfterFinalValue = $('.After_final-value').text().replace(/\s/g, '').replace('−','-');
                if (parseInt(AfterfirstCarSpeed) < 0) {
                    $('.AfterfirstCar-speed').text(spaceNumber(AfterfirstCarSpeed));
                    $('.AfterfirstCar-speed').addClass('active');
                }
                if (parseInt(AfterfirstCarSpeed) >= 0) {
                    $('.AfterfirstCar-speed.active').removeClass('active');
                }
                if (parseInt(AfterSecondCarSpeed) < 0) {
                    $('.After_SecondCar-speed').text(spaceNumber(AfterSecondCarSpeed));
                    $('.After_SecondCar-speed').addClass('active');
                }
                if (parseInt(AfterSecondCarSpeed) >= 0) {
                    $('.After_SecondCar-speed.active').removeClass('active');
                }
                if (parseInt(AfterFirstCarValue) < 0) {
                    $('.After_firstCar__value').text(spaceNumber(AfterFirstCarValue));
                    $('.After_firstCar__value').addClass('active');
                }
                if (parseInt(AfterFirstCarValue) >= 0) {
                    $('.After_firstCar__value.active').removeClass('active');
                }
                if (parseInt(AfterSecondCarValue) < 0) {
                    $('.After_SecondCar__value').text(spaceNumber(AfterSecondCarValue));
                    $('.After_SecondCar__value').addClass('active');
                }
                if (parseInt(AfterSecondCarValue) >= 0) {
                    $('.After_SecondCar__value.active').removeClass('active');
                }
                if (parseInt(AfterFinalValue) < 0) {
                    $('.After_final-value').text(spaceNumber(AfterFinalValue));
                    $('.After_final-value').addClass('active');
                }
                if (parseInt(AfterFinalValue) >= 0) {
                    $('.After_final-value.active').removeClass('active');
                }
                let AfterFirstCarMassAddSpace = $('.after_collision .firstCar-mass').text().replace('−','-');
                $('.after_collision .firstCar-mass').text(spaceNumber(AfterFirstCarMassAddSpace));

                let AfterSecondCarMassAddSpace = $('.after_collision .SecondCar-mass').text().replace('−','-');
                $('.after_collision .SecondCar-mass').text(spaceNumber(AfterSecondCarMassAddSpace));
                
                let AfterFirstCarValueAddSpace = $('.after_collision .After_firstCar__value').text().replace('−','-');
                $('.after_collision .After_firstCar__value').text(spaceNumber(AfterFirstCarValueAddSpace));
                
                let AfterSecondCarValueAddSpace = $('.after_collision .After_SecondCar__value').text().replace('−','-');
                $('.after_collision .After_SecondCar__value').text(spaceNumber(AfterSecondCarValueAddSpace));
                
                let AfterFinalValueAddSpace = $('.after_collision .After_final-value').text().replace('−','-');
                $('.after_collision .After_final-value').text(spaceNumber(AfterFinalValueAddSpace));
                
                $('.mass-text, .SecondMass-text, .carSpeed, .SecondCarSpeed').css({'display' : 'none'})
            }
        }
    });
})

let firstCount = 0;
let secondCount = 1;

$('.first-car-flt .change-direction-btn').on('click', () => {
    if (firstCount % 2 === 0) {
	    let crashedCar = $(".car").attr("src");
        crashedCar = `${crashedCar.slice(0, crashedCar.length - 4)}F.png`;
        $(".car").attr("src", crashedCar);
        $(".car").addClass('carDirectionChange')
        $('.first-car-flt .cars-mass-block.d_none').removeClass('d_none');
        $('.first-car-flt .cars-mass-block').addClass('d_none');
        $('.first-car-flt .cars-mass-DirectionChange-block.d_none').removeClass('d_none');
    }
    else if (firstCount % 2 !== 0) {
	    let crashedCar = $(".car").attr("src");
	    crashedCar = `${crashedCar.slice(0, crashedCar.length - 5)}.png`;
        $(".car").attr("src", crashedCar);
        $(".car.carDirectionChange").removeClass('carDirectionChange')
        $('.first-car-flt .cars-mass-DirectionChange-block.d_none').removeClass('d_none');
        $('.first-car-flt .cars-mass-DirectionChange-block').addClass('d_none');
        $('.first-car-flt .cars-mass-block.d_none').removeClass('d_none');
    }

	firstCount++;
})

$('.second-car-flt .change-direction-btn').on('click', () => {
    if (secondCount % 2 == 0) {
        let crashedSecondCar = $(".bot-car").attr("src");
        crashedSecondCar = `${crashedSecondCar.slice(0, crashedSecondCar.length - 4)}F.png`;
        $(".bot-car").attr("src", crashedSecondCar);
        $(".bot-car.SecondCarDirectionChange").removeClass('SecondCarDirectionChange')
        $('.second-car-flt .cars-mass-DirectionChange-block.d_none').removeClass('d_none');
        $('.second-car-flt .cars-mass-DirectionChange-block').addClass('d_none');
        $('.second-car-flt .cars-mass-block.d_none').removeClass('d_none');
    }
    else if (secondCount % 2 !== 0) {
        let crashedSecondCar = $(".bot-car").attr("src");
        crashedSecondCar = `${crashedSecondCar.slice(0, crashedSecondCar.length - 5)}.png`;
        $(".bot-car").attr("src", crashedSecondCar);
        $(".bot-car").addClass('SecondCarDirectionChange')
        $('.second-car-flt .cars-mass-block.d_none').removeClass('d_none');
        $('.second-car-flt .cars-mass-block').addClass('d_none');
        $('.second-car-flt .cars-mass-DirectionChange-block.d_none').removeClass('d_none');
    }

    secondCount++;
})
$('.FirstCar__Speed').on('input', function () {
    let FirstCar_mass = $('.before_collision .firstCar-mass').text().replace(/\s/g, '').replace('−','-');
    if ($('.car').hasClass('carDirectionChange')) {
        var val = -$('.FirstCar__Speed').val()
    }
    else {
        var val = $('.FirstCar__Speed').val()
    }
    let SpeedCountVal = $('.FirstCar__Speed').val();
    $('.carSpeedCount').text(SpeedCountVal.toString().replace('-', '−'))
    $('.firstCar-speed').text(val.toString().replace('-', '−'))
    $('.before_collision .firstCar__value').text(((FirstCar_mass) * (val)).toString().replace('-', '−'))
    let firstCar__value = $('.before_collision .firstCar__value').text().replace(/\s/g, '').replace('−','-');
    let SecondCar__value = $('.before_collision .SecondCar__value').text().replace(/\s/g, '').replace('−','-');

    $('.before_collision .final-value').text((parseInt(firstCar__value.replace('−','-')) + parseInt(SecondCar__value.replace('−','-'))).toString().replace('-', '−'))
    if (val == 0) {
        $('.car.carStart').removeClass('carStart')
    }
    else {
        $(".car").addClass('carStart')
    }
})

$('.SecondCar__Speed').on('input', function () {
    let SecondCar_mass = $('.before_collision .SecondCar-mass').text().replace(/\s/g, '').replace('−','-');
    if ($('.bot-car').hasClass('SecondCarDirectionChange') == false) {
        var val = -$('.SecondCar__Speed').val()
    }
    else {
        var val = $('.SecondCar__Speed').val()
    }
    let SecondSpeedCountVal = $('.SecondCar__Speed').val();
    $('.SecondCarSpeedCount').text(SecondSpeedCountVal.toString().replace('-', '−'))
    $('.SecondCar-speed').text(val.toString().replace('-', '−'))
    $('.before_collision .SecondCar__value').text(((SecondCar_mass) * (val)).toString().replace('-', '−'))
    let firstCar__value = $('.before_collision .firstCar__value').text().replace(/\s/g, '').replace('−','-')
    let SecondCar__value = $('.before_collision .SecondCar__value').text().replace(/\s/g, '').replace('−','-')

    $('.before_collision .final-value').text((parseInt(firstCar__value) + parseInt(SecondCar__value)).toString().replace('-', '−'));
    if (val == 0) {
        $('.bot-car.SecondCarStart').removeClass('SecondCarStart')
    }
    else {
        $(".bot-car").addClass('SecondCarStart')
    }
})
$('.FirstCar__Speed').on('change', function () {
    let val = $(this).val()
    $(".carStart").css({ "animation-duration": (30 - val) + "s" });
})

$('.SecondCar__Speed').on('change', function () {
    let val = $(this).val();
    $(".SecondCarStart").css({ "animation-duration": (30 - val) + "s" });
})

let DisabledCount = 1;

$('.first-car-flt .car-mass-btn').on('click', function () {
    if (DisabledCount < 2) {
        $('.first-car-flt .change-direction-btn').attr('disabled', false);
        DisabledCount = 3;
        $('.speed-bg.d_none').removeClass('d_none')
        $('.mass-text.d_none').removeClass('d_none')
        $('.FirstCar__Speed.d_none').removeClass('d_none')
        $('.carSpeed.d_none').removeClass('d_none')
        $('.firstCar-speed, .AfterfirstCar-speed').css({ 'color': 'inherit' })
    }
})

let SecondDisabledCount = 1;

$('.second-car-flt .car-mass-btn').on('click', function () {
    if (SecondDisabledCount < 2) {
        $('.second-car-flt .change-direction-btn').attr('disabled', false);
        SecondDisabledCount = 3;
        $('.SecondSpeed-bg.d_none').removeClass('d_none')
        $('.SecondMass-text.d_none').removeClass('d_none')
        $('.SecondCarSpeed.d_none').removeClass('d_none')
        $('.SecondCar__Speed.d_none').removeClass('d_none')
        $('.SecondCar-speed, .After_SecondCar-speed').css({ 'color': 'inherit' })
    }
})

let popupCount = 1;

$('.car-mass-btn').on('click', function () {
    if ($('.second-car-flt .change-direction-btn').prop("disabled") == false && $('.first-car-flt .change-direction-btn').prop("disabled") == false) {
        $('.go').attr('disabled', false);
        $('.final-value').css({ 'color': 'inherit' })
    }
})
function popups () {
    if (popupCount < 2) {
        $('.first_popup_block').addClass('d_flex')
        setTimeout(() => {
            $('.first_popup_block').removeClass('d_flex')
            $('.second_popup_block').addClass('d_flex')
        }, 2000)
        setTimeout(() => {
            $('.second_popup_block').removeClass('d_flex')
        }, 4000)
        setTimeout(() => {
            if ($('.first-car-flt img').hasClass('active') == false || $('.second-car-flt img').hasClass('active') == false) {
                $('.fourth_popup_block').addClass('d_flex')
            }
        }, 9000);
        setTimeout(() => {
            $('.fourth_popup_block.d_flex').removeClass('d_flex')
        }, 12000);
        popupCount = 3;
    }
}

$('.first-car-flt .car-mass-btn').on('click', function () {
    popups ();
})

$('.second-car-flt .car-mass-btn').on('click', function () {
    popups ();
})

$('.go').on('click', function () {
    let num = $('.car').css('animation-duration')
    num = `${num.slice(0, num.length - 1)}`;
    num = parseInt(num) + 3;
    let SecondNum = $('.bot-car').css('animation-duration')
    SecondNum = `${SecondNum.slice(0, SecondNum.length - 1)}`;
    SecondNum = parseInt(SecondNum) + 3;
    let height = $('.car').css('max-height')
    height = `${height.slice(0, height.length - 2)}`;
    let Secondheight = $('.bot-car').css('max-height')
    Secondheight = `${Secondheight.slice(0, Secondheight.length - 2)}`;
    if (parseInt(height) == 150) {
        $('.car').css({ 'animation-duration': num + 's' })
    }
    if (parseInt(height) == 90) {
        $('.car').css({ 'animation-duration': num - 2 + 's' })
    }
    if (parseInt(Secondheight) == 150) {
        $('.bot-car').css({ 'animation-duration': SecondNum + 's' })
    }
    if (parseInt(Secondheight) == 90) {
        $('.bot-car').css({ 'animation-duration': SecondNum - 2 + 's' })
    }
    $('.fourth_popup_block').addClass('d_main_none');
    $('.mass-text, .SecondMass-text, .carSpeed, .SecondCarSpeed').css({'display' : 'none'})
})

setTimeout(function () {
    $('.Before_preliminary, .After_preliminary').addClass('d_none')
    $('.after_collision.d_none, .before_collision.d_none').removeClass('d_none')
}, 1000)


$('body').on('click', function () {
    let AfterfirstCarSpeed = $('.AfterfirstCar-speed').text().replace(/\s/g, '').replace( '−','-');
    let AfterSecondCarSpeed = $('.After_SecondCar-speed').text().replace(/\s/g, '').replace( '−','-');
    let AfterFirstCarValue = $('.After_firstCar__value').text().replace(/\s/g, '').replace( '−','-');
    let AfterSecondCarValue = $('.After_SecondCar__value').text().replace(/\s/g, '').replace( '−','-');
    let AfterFinalValue = $('.After_final-value').text().replace(/\s/g, '').replace( '−','-');
    let FirstCarValue = $('.firstCar__value').text().replace(/\s/g, '').replace( '−','-');
    let FirstCarSpeed = $('.firstCar-speed').text().replace(/\s/g, '').replace( '−','-');
    let SecondCarSpeed = $('.SecondCar-speed').text().replace(/\s/g, '').replace( '−','-');
    let SecondCarValue = $('.SecondCar__value').text().replace(/\s/g, '').replace( '−','-');
    let FinalValue = $('.final-value').text().replace(/\s/g, '').replace( '−','-');
    if (parseInt(FirstCarSpeed) < 0) {
        $('.firstCar-speed').text(spaceNumber(FirstCarSpeed));
        $('.firstCar-speed').addClass('active');
    }
    if (parseInt(FirstCarSpeed) >= 0) {
        $('.firstCar-speed.active').removeClass('active');
    }
    if (parseInt(SecondCarSpeed) < 0) {
        $('.SecondCar-speed').text(spaceNumber(SecondCarSpeed));
        $('.SecondCar-speed').addClass('active');
    }
    if (parseInt(SecondCarSpeed) >= 0) {
        $('.SecondCar-speed.active').removeClass('active');
    }
    if (parseInt(FirstCarValue) < 0) {
        $('.firstCar__value').text(spaceNumber(FirstCarValue));
        $('.firstCar__value').addClass('active');
    }
    if (parseInt(FirstCarValue) >= 0) {
        $('.firstCar__value.active').removeClass('active');
    }
    if (parseInt(SecondCarValue) < 0) {
        $('.SecondCar__value').text(spaceNumber(SecondCarValue));
        $('.SecondCar__value').addClass('active');
    }
    if (parseInt(SecondCarValue) >= 0) {
        $('.SecondCar__value.active').removeClass('active');
    }
    if (parseInt(FinalValue) < 0) {
        $('.final-value').text(spaceNumber(FinalValue));
        $('.final-value').addClass('active');
    }
    if (parseInt(FinalValue) >= 0) {
        $('.final-value.active').removeClass('active');
    }
    if (parseInt(AfterfirstCarSpeed) < 0) {
        $('.AfterfirstCar-speed').text(spaceNumber(AfterfirstCarSpeed));
        $('.AfterfirstCar-speed').addClass('active');
    }
    if (parseInt(AfterfirstCarSpeed) >= 0) {
        $('.AfterfirstCar-speed.active').removeClass('active');
    }
    if (parseInt(AfterSecondCarSpeed) < 0) {
        $('.After_SecondCar-speed').text(spaceNumber(AfterSecondCarSpeed));
        $('.After_SecondCar-speed').addClass('active');
    }
    if (parseInt(AfterSecondCarSpeed) >= 0) {
        $('.After_SecondCar-speed.active').removeClass('active');
    }
    if (parseInt(AfterFirstCarValue) < 0) {
        $('.After_firstCar__value').text(spaceNumber(AfterFirstCarValue));
        $('.After_firstCar__value').addClass('active');
    }
    if (parseInt(AfterFirstCarValue) >= 0) {
        $('.After_firstCar__value.active').removeClass('active');
    }
    if (parseInt(AfterSecondCarValue) < 0) {
        $('.After_SecondCar__value').text(spaceNumber(AfterSecondCarValue));
        $('.After_SecondCar__value').addClass('active');
    }
    if (parseInt(AfterSecondCarValue) >= 0) {
        $('.After_SecondCar__value.active').removeClass('active');
    }
    if (parseInt(AfterFinalValue) < 0) {
        $('.After_final-value').text(spaceNumber(AfterFinalValue));
        $('.After_final-value').addClass('active');
    }
    if (parseInt(AfterFinalValue) >= 0) {
        $('.After_final-value.active').removeClass('active');
    }

    // Space before the thousands

    let firstCarMassAddSpace = $('.before_collision .firstCar-mass').text().replace('−', '-');
    $('.before_collision .firstCar-mass').text(spaceNumber(firstCarMassAddSpace));
    
    let SecondCarMassAddSpace = $('.before_collision .SecondCar-mass').text().replace('−', '-');
    $('.before_collision .SecondCar-mass').text(spaceNumber(SecondCarMassAddSpace));
    
    let FirstCarValueAddSpace = $('.before_collision .firstCar__value').text().toString().replace('-', '−');
    $('.before_collision .firstCar__value').text(spaceNumber(FirstCarValueAddSpace));
    
    let SecondCarValueAddSpace = $('.before_collision .SecondCar__value').text().replace('−', '-');
    $('.before_collision .SecondCar__value').text(spaceNumber(SecondCarValueAddSpace));
    
    let FinalValueAddSpace = $('.before_collision .final-value').text();
    $('.before_collision .final-value').text(spaceNumber(FinalValueAddSpace));
    
    let AfterFirstCarMassAddSpace = $('.after_collision .firstCar-mass').text().replace('−', '-');
    $('.after_collision .firstCar-mass').text(spaceNumber(AfterFirstCarMassAddSpace));
    
    let AfterSecondCarMassAddSpace = $('.after_collision .SecondCar-mass').text().replace('−', '-');
    $('.after_collision .SecondCar-mass').text(spaceNumber(AfterSecondCarMassAddSpace));
    
    let AfterFirstCarValueAddSpace = $('.after_collision .After_firstCar__value').text().replace('−', '-');
    $('.after_collision .After_firstCar__value').text(spaceNumber(AfterFirstCarValueAddSpace));
    
    let AfterSecondCarValueAddSpace = $('.after_collision .After_SecondCar__value').text().replace('−', '-');
    $('.after_collision .After_SecondCar__value').text(spaceNumber(AfterSecondCarValueAddSpace));
    
    let AfterFinalValueAddSpace = $('.after_collision .After_final-value').text().replace('−', '-');
    $('.after_collision .After_final-value').text(spaceNumber(AfterFinalValueAddSpace));
    
})


$(".car").onPositionChanged(() => {
    if (second()) {
        if (j <= 1) {
            let AfterfirstCarSpeed = $('.AfterfirstCar-speed').text().replace(/\s/g, '').replace('−', '-');
        }
    }
});
$(".bot-car").onPositionChanged(() => {
    if (second()) {
        if (j <= 1) {
            let AfterfirstCarSpeed = $('.AfterfirstCar-speed').text().replace(/\s/g, '').replace('−', '-');
        }
    }
});

// ===============================Popup============================

$('.first_popup_block').on('click', function () {
    $(this).addClass('d_none')
})

if ($('.first_popup_block').hasClass('d_none') == false) {
    setTimeout(function () {
        $('.first_popup_block').addClass('d_none')
    }, 2000)
}


$('.change-direction-btn').on('click', function () {
    if ($('.car').hasClass('carDirectionChange') && $('.bot-car').hasClass('SecondCarDirectionChange')) {
        $('.third_popup_block').addClass('d_flex')
        setTimeout(function () {
            $('.third_popup_block').removeClass('d_flex')
        }, 2000)
    }
})

$('.go').on('click', function () {
    if ($('.car').hasClass('carDirectionChange') == true && $('.FirstCar__Speed').val() >= parseInt($('.SecondCar__Speed').val())) {
        $('.third_popup_block').addClass('d_flex')
        setTimeout(function () {
            $('.third_popup_block').removeClass('d_flex')
        }, 2000)
    }
    if ($('.bot-car').hasClass('SecondCarDirectionChange') == true && $('.SecondCar__Speed').val() >= parseInt($('.FirstCar__Speed').val())) {
        $('.third_popup_block').addClass('d_flex')
        setTimeout(function () {
            $('.third_popup_block').removeClass('d_flex')
        }, 2000)
    }
    if ($('.car').hasClass('carStart') == false && $('.bot-car').hasClass('SecondCarStart') == false) {
        $('.third_popup_block').addClass('d_flex')
        setTimeout(function () {
            $('.third_popup_block').removeClass('d_flex')
        }, 2000)
    }
})

$('.first_popup_block').on('click', function () {
    $(this).addClass('d_none')
    $(this).removeClass('d_flex')
})

$('.second_popup_block').on('click', function () {
    $(this).addClass('d_none')
    $(this).removeClass('d_flex')
})

$('.third_popup_block').on('click', function () {
    $(this).addClass('d_none')
    $(this).removeClass('d_flex')
})

$('.fourth_popup_block').on('click', function () {
    $(this).addClass('d_none')
    $(this).removeClass('d_flex')
})
var fontSizeCount = 0;
var count = 0
var fontSize = setInterval(function(){
    count++
    $("span[id^='MathJax-Element-']").css("font-size", "86%")
    if (fontSizeCount === 20) {
        clearInterval(fontSize);
        fontSizeCount = 0
    }
}, 200);

function spaceNumber(x){
    x = x.replace(/\s/g, '')
    var z = x.toString();
    var b = z.split(',')[1];
    z = z.split(',')[0];
    if (z.length >= 4) {
        var c = z.substring(0,z.length-3);
        var d = '';
        if(c.length > 3){
            d = c.substring(0,c.length-3)
            c = c.substring(c.length-3,c.length)
            d+=" ";
        }
        var y = z.substring(z.length-3, z.length);
        if(b) y = y+','+b; 
        return (d+c+" "+y).toString().replace('-', '−');
    } else {
        if(b)
            return (z+','+b).toString().replace('-', '−');
        else return z.toString().replace('-', '−')
    }
}