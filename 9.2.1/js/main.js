var draggable = false;
var arrowContainer = document.querySelector('.arrowsContainer');
var draggableElement;
var arrows
var zoom_size

var sqrt = Math.sqrt;
var mashtabControl = 1;

function offset(el) {
    var rect = el.getBoundingClientRect()
    zoom_size = 1
    return { top: (rect.top) * zoom_size / mashtabControl - (h / mashtabControl), left: (rect.left) * zoom_size / mashtabControl - (w / mashtabControl) }
}


for (var i = 0; i < 11; i++) {

    var row = document.createElement('div');
    row.classList.add('row')
    for (var j = 0; j < 22; j++) {
        var item = document.createElement('div');
        item.classList.add('row_item')
        var img = document.createElement('img');
        img.setAttribute('src', "./img/Arrow.png");
        item.appendChild(img)
        row.appendChild(item)
    }

    arrowContainer.appendChild(row)
}


document.addEventListener('mousedown', function (event) {
    if (!event.target.closest('.absolute')) return;
    draggable = true;
    draggableElement = event.target.closest('.absolute');
})
document.addEventListener('touchstart', function (event) {
    if (!event.target.closest('.absolute')) return;
    draggable = true;
    draggableElement = event.target.closest('.absolute');
})

function mousedown(event) {
    var target = event.target;

    if (!target.closest('img')) return;

    if (target.style.opacity && +target.style.opacity < 1) {
        return
    }
    target.style.opacity = '1'
    if (target.closest('.charge')) {
        target.style.opacity = '0.5'
    }
    draggable = true;
    var cloneImage;
    var charge
    if (target.classList.contains('plus') || target.classList.contains('minus')) {
        cloneImage = document.createElement('img')
        cloneImage.setAttribute('src', target.getAttribute('src'))
        cloneImage.setAttribute('draggable', false)
        cloneImage.classList.add(target.classList[0])
        cloneImage.classList.add('absolute')

        draggableElement = cloneImage;
    }

    if (target.classList.contains('plus')) {
        charge = 1
        cloneImage.setAttribute('data-value', charge)
    } else if (target.classList.contains('minus')) {
        charge = -1
        cloneImage.setAttribute('data-value', charge)
    } else {
        cloneImageCharge = document.createElement('img')
        cloneImageCharge.setAttribute('src', target.getAttribute('src'))
        cloneImageCharge.setAttribute('draggable', false)

        var arrowContainer = document.createElement('div');
        arrowContainer.classList.add('svgArrowContainer');
        arrowContainer.classList.add('arrowSvg');
        arrowContainer.classList.add('absolute');



        document.querySelector('.svgArrowContainer') && document.querySelector('.svgArrowContainer').remove();
        var arrowImage = document.createElement('img');
        arrowImage.setAttribute('draggable', false)
        arrowImage.setAttribute('src', './img/Arrow_east.svg');
        arrowContainer.appendChild(cloneImageCharge)
        arrowContainer.appendChild(arrowImage)

        var minus_top = 0;
        var minus_left = 0
        if (arrowContainer.closest('.svgArrowContainer')) {
            minus_top = 130*mashtabControl
            // minus_left = 200
        }
        var left = (event.clientX || event.touches[0].clientX) / mashtabControl - ((offset(document.querySelector('.arrowsContainer')).left / zoom_size))- w/mashtabControl - 20;
        var top = ((event.clientY || event.touches[0].clientY) / mashtabControl - minus_top / mashtabControl/zoom_size - h/mashtabControl) - (20);


        // arrowContainer.style.left = ((event.clientX || event.touches[0].clientX) / mashtabControl) - 320 + 'px'
        // arrowContainer.style.top = (event.clientY || event.touches[0].clientY) / mashtabControl - 150 + 'px'

        arrowContainer.style.left = left + 'px'
        arrowContainer.style.top = top + 'px'
        

        document.querySelector('.hidden_outside_wrapper').appendChild(arrowContainer);

        draggableElement = arrowContainer
    }



    if (cloneImage) {
        var minus_top = 0;
        var minus_left = 0

        if (draggableElement.closest('.svgArrowContainer')) {
            minus_top = 130*mashtabControl
            // minus_left = 200
        }
        var left = (event.clientX || event.touches[0].clientX) / mashtabControl - ((offset(document.querySelector('.arrowsContainer')).left / zoom_size))- w/mashtabControl - 20;
        var top = ((event.clientY || event.touches[0].clientY) / mashtabControl - minus_top / mashtabControl/zoom_size - h/mashtabControl) - (20);

        // cloneImage.style.left = ((event.clientX || event.touches[0].clientX) / mashtabControl) - 320 + 'px'
        // cloneImage.style.top = (event.clientY || event.touches[0].clientY) / mashtabControl - 10 + 'px'
        cloneImage.style.left = left + 'px'
        cloneImage.style.top = top + 'px'
        
        document.querySelector('#wrapper').appendChild(cloneImage);
    }


}

document.querySelector('.thinks').addEventListener('mousedown', mousedown)
document.querySelector('.thinks').addEventListener('touchstart', mousedown)

var canInit = true;


function mouseMove(event) {
    var target = event.target;
    if (!draggable) return;

    var minus_top = 0;
    var minus_left = 0

    if (draggableElement.closest('.svgArrowContainer')) {
        minus_top = 130*mashtabControl
        // minus_left = 200
    }
    var left = (event.clientX || event.touches[0].clientX) / mashtabControl - ((offset(document.querySelector('.arrowsContainer')).left / zoom_size))- w/mashtabControl - 20;
    var top = ((event.clientY || event.touches[0].clientY) / mashtabControl - minus_top / mashtabControl/zoom_size - h/mashtabControl) - (20);
    
    var Width = draggableElement.offsetWidth;
    var height = draggableElement.offsetHeight;

    

    var draggableElements = [...document.querySelectorAll('.absolute')];

    var dragIsAvailable = true;
    if(!draggableElement.closest('.svgArrowContainer')){
        draggableElements.forEach(function(element){
            if(element == draggableElement || element.closest('.svgArrowContainer')) return;

            var elemLeft = parseInt(element.style.left);
            var elemTop = parseInt(element.style.top);
            var elemWidth = element.offsetWidth;
            var elemHeight = element.offsetHeight;
            if( 
                left-Width<elemLeft && 
                left > elemLeft - elemWidth &&
                top - height< elemTop  &&
                top>elemTop - elemHeight 
                ){
    
                    dragIsAvailable = false;
                }
        })
    }
    
    if(dragIsAvailable){
        draggableElement.style.left = left + 'px';
        draggableElement.style.top = top + 'px';
    }

    if (canInit) {
        setTimeout(function () {
            init()
            canInit = true
        }, 20)
        canInit = false
    }
}

document.addEventListener('mousemove', mouseMove)
document.addEventListener('touchmove', mouseMove)

function mouseup(event) {
    if (!draggable) return;

    setTimeout(function(){
        var absolute = document.querySelectorAll('.absolute');
        if(absolute.length){
            document.querySelector('#checkBtn').classList.add('anabel')
        }else{
            document.querySelector('#checkBtn').classList.remove('anabel')
        }

    },100)

    
    
    
    
    draggable = false;
    var container = document.querySelector('.arrowsContainer');
    var off = offset(container);

    off.left = off.left

    var bottomLine = (+container.offsetHeight/mashtabControl + off.top/mashtabControl) ;
    var topLine = (+container.offsetWidth/mashtabControl + off.left/mashtabControl) ;
    var mouseX, mouseY;
    //find a clientX and clientY in event object
    if( event.clientX ){
        mouseX = event.clientX; 
        mouseY = event.clientY; 
    }else if( event.touches[0] ){
        mouseX = event.touches[0].clientX; 
        mouseY = event.touches[0].clientY;
    }else if( event.changedTouches[0] ){
        mouseX = event.changedTouches[0].clientX; 
        mouseY = event.changedTouches[0].clientY;
    }else if( event.targetTouches[0] ){
        mouseX = event.targetTouches[0].clientX; 
        mouseY = event.targetTouches[0].clientY;
    }else {
        console.error('can`t find clientX and clientY');
        return;
    }


    var bottomLine = (off.top + $(container).height() + 20) * mashtabControl + h;
    var rightLine = (off.left + $(container).width() + 20) * mashtabControl + w;
    
    if (mouseY < off.top * mashtabControl + h
        || mouseX < off.left * mashtabControl + w
        || mouseY > bottomLine
        || mouseX > rightLine) {
        draggableElement.remove()

        if (draggableElement.classList.contains('minus')) {
            document.querySelector('.minus').style.opacity = '1';
        } else if (draggableElement.classList.contains('plus')) {
            document.querySelector('.plus').style.opacity = '1';
        } else {
            document.querySelector('.charge').style.opacity = '1';
            if (document.querySelector('.svgArrowContainer'))
                document.querySelector('.svgArrowContainer').remove()
        }
    }

    init()
}

document.addEventListener('mouseup', mouseup)
document.addEventListener('touchend', mouseup)

function init() {


    var minus
    var plus
    arrows = [...document.querySelectorAll('.row_item')]
    var uniqArrow = document.querySelector('.svgArrowContainer')
    if (uniqArrow) {
        arrows.push(uniqArrow)
    }


    arrows.forEach(oneArrow => {




        var oneArrowCoordinates = offset(oneArrow);
        var licqakir = document.querySelectorAll('img.absolute');
        // console.log(licqakir)
        if(!licqakir.length){
            oneArrow.style.transform = 'rotate(0deg)'
        }
        // array licqakirnerov - object (x,y,v(50,-50))

        // 
        var x = oneArrowCoordinates.left - 9 ;
        var y = oneArrowCoordinates.top - 12 ;
        var _ex = 0;
        var _ey = 0;

        var spec_arrow_length = 0
        var maximum = 0;

        for (let i = 0; i < licqakir.length; ++i) {
            maximum += 500

            if (licqakir[i].closest('.plus')) {
                licqakir[i].v = 50
            } else {
                licqakir[i].v = -50
            }
            dx = x - (licqakir[i].style.left.replace('px','')); // x koordinat
            dy = y - (licqakir[i].style.top.replace('px','')); // y koordinat

            // console.log(h)
            d = sqrt(dx * dx + dy * dy); // heravortyun tvyal ketic
            e = licqakir[i].v / (d * d); // tvyal ketum stexcac el. dasht

            var ex = e * (dx / d) 			// e-i proyekcia x arancqov
            var ey = e * (dy / d) 			// e-i proyekcia y arancqov
            _ex += ex;
            _ey += ey;
            if (oneArrow.closest('.svgArrowContainer')) {
                maximum -= d;
            }

        }

        e = sqrt(_ex * _ex + _ey * _ey);	//tvyal ketum el. dashti larvacucyan arjeq
        deltax = _ex / e; // shexman cos
        deltay = _ey / e; // shexman sin

        var x1 = x;
        var y1 = y;
        var x2 = x + deltax;
        var y2 = y + deltay;

        Q = (Math.atan2(x1 - x2, y2 - y1) * (180 / Math.PI) + 90)


        oneArrow.style.transform = 'rotate(' + Q + 'deg)';
        if (oneArrow.closest('.svgArrowContainer')) {

            var scale = (maximum / 100) * 5;
            if (scale < 0) {
                scale = 0
            }

            document.querySelectorAll('.svgArrowContainer img')[1].style.transform = `scaleX(${scale}) translateX(14px)`;
        }
    })





}

var checked = false;
var arrowCont = document.querySelector('.arrowsContainer')
document.querySelector('.checkbox_container').addEventListener('click', function (event) {

    if (!checked) {

        event.target.classList.add('checkbox_checked')
        checked = true;
        arrowCont.style.opacity = 1

    } else {
        checked = false;
        event.target.classList.remove('checkbox_checked')
        arrowCont.style.opacity = 0
    }
})


setTimeout(function () {

    document.querySelector('#checkBtn').addEventListener('click', function (event) {

        if(!this.classList.contains('anabel')) return;

        var thinksImages = [...document.querySelectorAll('.thinks img')];
        thinksImages.forEach(elem => {
            elem.style.opacity = 1
        })


        document.querySelector('.checkbox_container').classList.remove('checkbox_checked')
        document.querySelector('.arrowsContainer').style.opacity = '0';
        [].forEach.call(document.querySelectorAll('.row_item'), function (element) {
            element.style.transform = 'rotate(0deg)';
        });

        [].forEach.call(document.querySelectorAll('.absolute'), function (elem) {
            elem.remove()
        })
        checked = false
        this.classList.remove('anabel')
    })
}, 100)




