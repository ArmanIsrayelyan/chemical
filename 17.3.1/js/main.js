var checker = document.querySelector('.checker');
var switchImg = document.querySelector('.switch_container img');
var wrapper = document.querySelector('#wrapper');
var step = 1;
var choosed_meter

// checker.addEventListener('click', function (event) {
//     if (checker.classList.contains('off')) {
//         turnOn()



//     } else if (checker.classList.contains('on')) {
//         turnOff()

//     } else {
//         turnOn()
//     }
// })

var selected = false
var selected2 = false


document.querySelector('.met_container').addEventListener('click', function (event) {
    var target = event.target;
    if (!target.closest('.ammMeter') && !target.closest('.VMeter') || target.closest('.disabled')) return;


    if (step == 1 && !selected) {
        if (!target.closest('.ammMeter')) {
            helperAlert(2, 'This is not the correct meter to measure current.')
        } else {


            helperAlert(1, 'Decide where to connect the meter. Click on any suitable point.');


            metersFirstStepAnimation(target)
            selected = true
        }
    } else if (step == 2 && !selected2) {

        if (!target.closest('.VMeter')) {
            helperAlert(2, 'This is not the correct meter to measure potential difference.')
        } else {


            helperAlert(1, 'Decide where to connect the meter. Click on any suitable point.')
            metersFirstStepAnimation(target, true);
            turnOff();
            document.querySelector('.met_container').classList.add('disabled')
            selected2 = true
        }
    }

})



document.querySelector('.points').addEventListener('click', function (event) {
    var target = event.target;
    var container = target.closest('.wrap')
    if (!container) return;

    container.classList.add('selected');

    var place = container.getAttribute('data-place');
    var element_point = container.getAttribute('data-element');
    var left_point = container.getAttribute('data-left');

    if (step == 1) {
        if (!place || place != 1) {
            helperAlert(2, 'An ammeter cannot be connected in parallel.', function () {
                container.classList.remove('selected');
            });
        } else {


            metersSecondStepAnimation();
            container.classList.remove('selected');
            hidePoints1()
            container.style.display = 'flex';
            container.style.background = 'transparent';
            container.style.cursor = 'inherit';
            container.classList.add('choosing')
            helperAlert(1, 'Drag the connecting lead to connect the negative terminal of the meter.', function () {
                var arr = document.querySelector('.ammeterArrow');
                arr.style.opacity = 1;
                arr.style.visibility = 'visible';
                // voltmeter_arrow
            });


        }

        document.querySelector('.ammMeter').style.borderColor = 'transparent';

    } else if (step == 2) {
        if (!place || place != 2) {
            if (element_point != null) {
                helperAlert(2, 'This is not the correct place to measure the potential difference over the light bulb.', function () {
                    container.classList.remove('selected');
                });
            }
            else if (left_point != null) {
                helperAlert(2, 'A voltmeter cannot be connected in series.', function () {
                    container.classList.remove('selected');
                });
            }
        } else {

            vSecondStepAnimation();
            var points = document.querySelectorAll('.points > div');
            [...points].forEach(elem => {
                elem.style.display = 'none'
            })
            container.style.display = 'flex';
            container.style.background = 'transparent';
            container.classList.remove('selected');
            container.classList.add('choosing');

            setTimeout(function () {
                var black_croc = document.querySelector('.voltmeter_crocs .orange_croc');
                black_croc.style.top = 302 + 'px';
                setTimeout(function () {
                    var arr = document.querySelector('.voltmeter_arrow');
                    arr.style.opacity = 1;
                    arr.style.visibility = 'visible';
                }, 300)


            }, 500)
            helperAlert(1, 'Drag the connecting lead to connect the negative terminal of the meter.')

        }

        document.querySelector('.VMeter').style.borderColor = 'transparent';
    }


})

var drag = false;

$('.draggable').on('mousedown', function (event) {
    drag = true
    })

$('.circle_white').on('mousedown touchstart', function (event) {

    this.style.display = 'none'
    drag = true
    })

$(document).on('mouseup touchend', function (event) {
    if (!drag || disabled_croc__2_drag) return
    var croc = document.querySelector('.orange_croc img');
    var left = parseInt(croc.style.left);
    var top = parseInt(croc.style.top);


    if (left < 14 && top < 90 && top > 60) {
        crocFirstRightPosition()
        croc.classList.remove('draggable')
        helperAlert(1, 'Drag the connecting lead to connect the positive side of the meter. Choose the correct scale.');
        setTimeout(function () {
            document.querySelector('.other_croc').style.top = 285 + 'px';
        }, 500)

        var container = document.querySelector('.bottom_right_point');
        [...container.querySelectorAll('.onePoint')].forEach(elem => {
            elem.style.display = 'none';
        })
    }
    else if (left > 90 && top < 90 && top > 60) {
        helperAlert(2, 'Connect the negative terminal of the meter closest to the negative terminal of the battery.', returnCrocToInitial);



    } else {

        returnCrocToInitial();

    }

    drag = false;

})

function returnCrocToInitial() {
    var croc = document.querySelector('.draggable');
    croc.style.left = '30px';
    croc.style.top = '101px';
    var path = document.querySelector('svg path');
    path.setAttribute('d', 'M 541 241 C 509.379 281.373 456.746 394.371 537 416 C 697.254 466.629 835.216 399.298 742 581')

    var point = document.querySelector('.bottom_right_point .onePoint:last-child')
    point.style.opacity = '1'

    document.querySelector('.circle_white').style.display = 'block'
}

function crocFirstRightPosition() {
    var croc = document.querySelector('.draggable');
    croc.style.left = '-28.5px';
    croc.style.top = '77px';
    var path = document.querySelector('.orange_croc svg path');
    path.setAttribute('d', 'M 541 241 C 509.379 281.373 456.746 394.371 537 416 C 697.254 466.629 835.216 399.298 516 458 ');
}

var disable_drag_hard  = false


$(document).on('mousemove touchmove', function (event) {

    if (disabledDrag ) return;
    if (!drag) return;


    var croc = document.querySelector('.draggable');

    var parent = croc.closest('div');
    var pPos = offset(parent)

    var top = minusScroll((event.clientY || event.originalEvent.touches[0].clientY)/mashtab, 2) - pPos.top/mashtab;
    var left = minusScroll((event.clientX || event.originalEvent.touches[0].clientX)/mashtab, 1) - pPos.left/mashtab - 40;

    if (left < (-52.5)) return;
    if (left > 138) return;

    if (top < 50) {
        return
    }
    if (top > 151) {
        return
    }



    croc.style.left = left + 'px';
    croc.style.top = top + 'px';




    left = (left * 4) + 630;
    top = (top * 4) + 150;
    if (left < (-10)) {
        left -= 45;
    }
    if (left > 960) {
        left += 20;
    }
    var svgPath = `M 541 241 C 509.379 281.373 456.746 394.371 537 416 C 697.254 466.629 835.216 399.298 
    
    ${left} ${top}
    
    `;

    var path = document.querySelector('.orange_croc svg path');
    path.setAttribute('d', svgPath)


    var left = parseInt(croc.style.left);
    var top = parseInt(croc.style.top);

    var points_wrapper = document.querySelector('.bottom_right_point.wrap');
    var points = points_wrapper.querySelectorAll('.onePoint');
    points[0].style.opacity = '1'
    points[1].style.opacity = '1'


    if (left < 14 && top < 90 && top > 60) {
        points[0].style.opacity = '0.5'

    }
    else if (left > 90 && top < 90 && top > 60) {
        points[1].style.opacity = '0.5'
    }

})


var drag_orange = false;
$('.orange_drag').on('mousedown touchstart', function (event) {
        if (disabledDrag) return;
    drag_orange = true
})



var changeCounter = 1;

function moveOrangeElement(position, d) {


    var circle = document.querySelector('.orange_drag');
    var path = document.querySelector('.other_croc svg path');
    circle.style.left = position.left;
    circle.style.top = position.top;
    path.setAttribute('d', d)
}

var disabledDrag = false;
var lastSelectedPosition

$(document).on('mouseup touchend', function (event) {
    if (!drag_orange || disabledDrag) return;

    var dragElement = document.querySelector('.orange_drag');
    var x = parseInt(dragElement.style.left);
    var y = parseInt(dragElement.style.top);
    if (changeCounter == 1) {
        if (y > 5 && y < 30) {
            if (x < 20 && x > (-20)) {
                changeCounter++;

                moveOrangeElement({
                    top: '15px',
                    left: '1.5px'
                }, 'M 550 308 C 509.379 281.373 681.746 368.371 682 362 C 773.254 388.629 951.216 399.298 742 581')

                lastSelectedPosition = {
                    top: '15px',
                    left: '1.5px',
                    path: 'M 550 308 C 509.379 281.373 681.746 368.371 682 362 C 773.254 388.629 951.216 399.298 742 581',
                    amper: 5
                }

                var arrow = document.querySelector('.ammeterArrow');
                arrow.style.visibility = 'visible';
                arrow.style.opacity = '1';
                turnOn();

                var circleColored = document.querySelector('.under_circle');
                circleColored.style.background = '#EA5E00';
                document.querySelector('.amper_counter').style.display = 'flex';

                disabledDrag = true;

                setTimeout(function () {
                    helperAlert(1, 'Your scale is too large. Try the next scale.', function () {
                        disabledDrag = false;
                        circleColored.style.background = '#fff';
                    })
                }, 1500)

            }
            if (x < (-30) && x > (-45)) {
                moveOrangeElement({
                    top: '-40px',
                    left: '45px'
                }, 'M 744 102 C 509.379 281.373 681.746 368.371 682 362 C 773.254 388.629 951.216 399.298 742 581')
                helperAlert(2, 'Always connect to the largest scale first.')
            }
            if (x < (-75)) {
                moveOrangeElement({
                    top: '-40px',
                    left: '45px'
                }, 'M 744 102 C 509.379 281.373 681.746 368.371 682 362 C 773.254 388.629 951.216 399.298 742 581')
                helperAlert(2, 'Always connect to the largest scale first.')
            }
        }
        else{
            position = {
                top: '-40px',
                left: '45px'
            }
            itemMoveTo(dragElement, document.querySelector("#mainContent > div.ammeterWires > div.other_croc > svg > path"), position, 'M 744 102 C 509.379 281.373 681.746 368.371 682 362 C 773.254 388.629 951.216 399.298 742 581')
        }

    } else if (changeCounter == 2) {
        if (y > 10 && y < 25) {
            if (x < (-25) && x > (-50)) {

                lastSelectedPosition.left = '-40px';
                lastSelectedPosition.top = '16px';
                lastSelectedPosition.path = 'M 382 304 C 509.379 281.373 681.746 368.371 682 362 C 773.254 388.629 951.216 399.298 742 581';
                lastSelectedPosition.amper = 500
                document.querySelector('.ammeterArrow').style.transform = 'rotate(5deg)';
                animateAmmeterInCharge(500)
                moveOrangeElement(
                    {
                        top: lastSelectedPosition.top,
                        left: lastSelectedPosition.left
                    }
                    , lastSelectedPosition.path
                )
                changeCounter++;
              
                setTimeout(function () {
                    helperAlert(1, 'Click on the meter that must be used to measure the potential difference over the light bulb.')
                    step = 2;
                }, 1500)
                disable_drag_hard = true;

            } else if (x < (-75) ) {
                lastSelectedPosition.left = '-80px';
                lastSelectedPosition.top = '16px';
                lastSelectedPosition.path = "M 218 300 C 509.379 281.373 681.746 368.371 682 362 C 773.254 388.629 951.216 399.298 742 581";
                lastSelectedPosition.amper = 50
                animateAmmeterInCharge(50)

                moveOrangeElement(
                    {
                        top: lastSelectedPosition.top,
                        left: lastSelectedPosition.left
                    }
                    , lastSelectedPosition.path
                )
                helperAlert(1, 'Try the middle scale first.')
            } else if (x < 9 && x > (-8)) {
                lastSelectedPosition = {
                    top: '15px',
                    left: '1.5px',
                    path: 'M 550 308 C 509.379 281.373 681.746 368.371 682 362 C 773.254 388.629 951.216 399.298 742 581'
                }
                lastSelectedPosition.amper = 5
                animateAmmeterInCharge(5)
    
                moveOrangeElement(
                    {
                        top: lastSelectedPosition.top,
                        left: lastSelectedPosition.left
                    }
                    , lastSelectedPosition.path
                )
                helperAlert(1, 'Try the middle scale first.')
            }
        } else {
            moveOrangeElement(
                {
                    top: lastSelectedPosition.top,
                    left: lastSelectedPosition.left
                }
                , lastSelectedPosition.path
            )
        }

    } else if (changeCounter == 3) {

        if (y > 10 && y < 25) {
            if (x < 15 && x > (-15) ) {
                lastSelectedPosition = {
                    top: '15px',
                    left: '1.5px',
                    path: 'M 550 308 C 509.379 281.373 681.746 368.371 682 362 C 773.254 388.629 951.216 399.298 742 581'
                }
                lastSelectedPosition.amper = 5
                animateAmmeterInCharge(5)

                helperAlert(1, 'Try the middle scale first.', function () {
                    moveOrangeElement(
                        {
                            top: lastSelectedPosition.top,
                            left: lastSelectedPosition.left
                        }
                        , lastSelectedPosition.path
                    )
                })
            }
            if (x < (-30) && x > (-45)) {

                lastSelectedPosition.left = '-40px';
                lastSelectedPosition.top = '16px';
                lastSelectedPosition.path = 'M 382 304 C 509.379 281.373 681.746 368.371 682 362 C 773.254 388.629 951.216 399.298 742 581';
                document.querySelector('.ammeterArrow').style.transform = 'rotate(5deg)';
                lastSelectedPosition.amper = 500
                animateAmmeterInCharge(500)

            } else if (x < (-71) ) {
                lastSelectedPosition.left = '-80px';
                lastSelectedPosition.top = '16px';
                lastSelectedPosition.path = "M 218 300 C 509.379 281.373 681.746 368.371 682 362 C 773.254 388.629 951.216 399.298 742 581";
                lastSelectedPosition.amper = 50
                animateAmmeterInCharge(50)


                helperAlert(1, 'Try the middle scale first.', function () {
                    moveOrangeElement(
                        {
                            top: lastSelectedPosition.top,
                            left: lastSelectedPosition.left
                        }
                        , lastSelectedPosition.path
                    )
                })
            }
            moveOrangeElement(
                {
                    top: lastSelectedPosition.top,
                    left: lastSelectedPosition.left
                }
                , lastSelectedPosition.path
            )
        } else {
            moveOrangeElement(
                {
                    top: lastSelectedPosition.top,
                    left: lastSelectedPosition.left
                }
                , lastSelectedPosition.path
            )
        }


    }


    var dragElement = document.querySelector('.orange_drag');

    var circle = dragElement.querySelector('.under_circle');
    circle.style.background = '#fff'


    drag_orange = false
})
$(document).on('mousemove touchmove', function (event) {
    if (!drag_orange || disable_drag_hard) return;
    var target = document.querySelector('.orange_drag');
    var other_croc_container = document.querySelector('.other_croc');
    var container_position = offset(other_croc_container);
    var x = minusScroll((event.clientX || event.originalEvent.touches[0].clientX) / mashtab, 1) - container_position.left / mashtab;
    var y = minusScroll((event.clientY || event.originalEvent.touches[0].clientY) / mashtab, 2) - container_position.top / mashtab;

    if (y < -31) return;
    if (y > 180) return;
    if (x < -75) return;
    if (x > 150) return;

    target.style.left = x - 10 + 'px';
    target.style.top = y - 10 + 'px';

    var left = (x * 4) + 500;
    var top = (y * 4) + 200

    var path = document.querySelector('.other_croc path');


    var d = `M 
    
    ${left} ${top} 
    
    C 509.379 281.373 681.746 368.371 682 362 C 773.254 388.629 951.216 399.298 742 581`
    path.setAttribute('d', d)





    var dragElement = document.querySelector('.orange_drag');
    var x = parseInt(dragElement.style.left);
    var y = parseInt(dragElement.style.top);


    var circle = dragElement.querySelector('.under_circle');
    circle.style.background = '#fff'



    if (y > 10 && y < 25) {
        if (x < 15 && x > (-15)) {
            circle.style.background = 'rgb(117, 221, 22)'

        }
        if (x < (-30) && x > (-45)) {
            circle.style.background = 'rgb(117, 221, 22)'
        }
        if (x < (-75)) {
            circle.style.background = 'rgb(117, 221, 22)'
        }
    }


})



var secondCroc = document.querySelector('.voltmeter_crocs .orange_croc img:first-child');
var secondBlackDrag = false;

function itemMoveTo(element, path, position, d) {


    element.style.left = position.left;
    element.style.top = position.top;
    path.setAttribute('d', d)
}


$(secondCroc).on('mousedown touchstart', function (event) {
    if (disabled_croc__2_drag) return;
    secondBlackDrag = true;
    });



var disabled_croc__2_drag = false
$(document).on('mouseup touchend', function (event) {
    if (!secondBlackDrag) return;
    secondBlackDrag = false;

    p = document.querySelector('.voltmeter_crocs .orange_croc svg path');


    var left = parseInt(secondCroc.style.left);
    var top = parseInt(secondCroc.style.top);

    var position
    if (top > 60 && top < 91) {

        if (left > -60 && left < 20) {
            // ok 37 left top 76 path M 637 234 C 238.379 281.373 456.746 394.371 596.7 458;

            position = {
                left: -37 + 'px',
                top: 76 + 'px'
            }
            itemMoveTo(secondCroc, p, position, ' M 637 234 C 238.379 281.373 456.746 394.371 596.7 458');
            var points = document.querySelector('.bottom_left_point');
            points.style.display = 'none';
            disabled_croc__2_drag = true
            helperAlert(1, 'Drag the connecting lead to connect the positive side of the meter. Choose the correct scale.');
            var other_croc = document.querySelector('.voltmeter_crocs .other_croc');
            other_croc.style.top = 285 + 'px';


        } else if (left > 107 && left < 143) {
            // err     top: 69px; left: -82px;  M 637 234 C 238.379 281.373 456.746 394.371 411 461 ;
            position = {
                left: -82 + 'px',
                top: 69 + 'px'
            }

            helperAlert(2, 'Connect the negative terminal of the meter closest to the negative terminal of the battery.', function () {
                var points = document.querySelector('.bottom_left_point .onePoint');

                points[1] && (points[1].style.opacity = '1');
                itemMoveTo(secondCroc, p, position, ' M 637 234 C 238.379 281.373 456.746 394.371 411 461');
            })
        }

    }
})
$(document).on('mousemove touchmove', function (event) {
    if (!secondBlackDrag) return;

    var container = document.querySelector('.voltmeter_crocs .orange_croc');
    var p = document.querySelector('.voltmeter_crocs .orange_croc svg path');
    var pPos = offset(container)


    var top = minusScroll((event.clientY || event.originalEvent.touches[0].clientY) / mashtab, 2) - pPos.top/mashtab;
    var left = minusScroll((event.clientX || event.originalEvent.touches[0].clientX) / mashtab, 1) - pPos.left/mashtab - 40;

    if (left < (-85)) return;
    if (left > (165)) return;
    if (top > (150)) return;
    if (top < (-4)) return;
    secondCroc.style.left = left + 'px';
    secondCroc.style.top = top + 'px';


    left = left * 4.2 + 750;
    top = top * 4 + 150;
    var path = `M 637 234 C 238.379 281.373 456.746 394.371 
    
    ${left} ${top}`;
    p.setAttribute('d', path)




    var left = parseInt(secondCroc.style.left);
    var top = parseInt(secondCroc.style.top);
    var points = document.querySelectorAll('.bottom_left_point .onePoint');

    points[0].style.opacity = 1;
    points[1].style.opacity = 1;

    if (top > 60 && top < 91) {



        if (left > -60 && left < 20) {
            points[0].style.opacity = '0.5';



        } else if (left > 107 && left < 143) {
            points[1].style.opacity = '0.5';
        }

    }



})
var orange_drag_2 = document.querySelector('.voltmeter_crocs .orange_drag ');
orange_drag_2_drag = false;
$(orange_drag_2).on('mousedown touchstart', function (event) {
    orange_drag_2_drag = true;
    disabledDrag = true
})
$(document).on('mouseup touchend', function (event) {
    orange_drag_2_drag = false;
    if (changeCounter == 3)
        disabledDrag = false


})


$(document).on('mousemove touchmove', function (event) {
    if (!orange_drag_2_drag) return;

    var target = document.querySelector('.voltmeter_crocs .orange_drag');
    var other_croc_container = document.querySelector('.voltmeter_crocs .other_croc');
    var container_position = offset(other_croc_container);
    var x = minusScroll((event.clientX || event.originalEvent.touches[0].clientX) / mashtab, 1) - container_position.left / mashtab;
    var y = minusScroll((event.clientY || event.originalEvent.touches[0].clientY) / mashtab, 2) - container_position.top / mashtab;

    if (y < -31) return;
    if (y > 180) return;
    if (x < -75) return;
    if (x > 150) return;



    target.style.left = x - 10 + 'px';
    target.style.top = y - 10 + 'px';

    var left = (x * 4) + 500;
    var top = (y * 4) + 200

    var path = document.querySelector('.voltmeter_crocs .other_croc path');


    var d = `M 
    
    ${left} ${top} 
    
    C 509.379 281.373 681.746 368.371 682 362 C 773.254 388.629 951.216 399.298 742 581`
    path.setAttribute('d', d)






    var dragElement = document.querySelector('.voltmeter_crocs .orange_drag');
    var x = parseInt(dragElement.style.left);
    var y = parseInt(dragElement.style.top);

    var circ = dragElement.querySelector('.circ .under_circle')

    circ.style.background = '#fff'
    if (y > 0 && y < 30) {
        if (x < 15 && x > (-15)) {
            circ.style.background = 'rgb(117, 221, 22)'
        }
        if (x < (-35) && x > (-55)) {
            circ.style.background = 'rgb(117, 221, 22)'
        }
        if (x < (-75)) {
            circ.style.background = 'rgb(117, 221, 22)'
        }
    }

})


var changeCounter_2 = 1;
var disabledDrag_2 = false;

var last_position = 1

$(document).on('mouseup touchend', function (event) {
    if (disabledDrag_2) return;
    var path = document.querySelector('.voltmeter_crocs .other_croc path');
//voltmeter pointer mouseup comment
    var dragElement = document.querySelector('.voltmeter_crocs .orange_drag');
    var x = parseInt(dragElement.style.left);
    var y = parseInt(dragElement.style.top);
    var position


    var first_correct_position = {
        left: '-2.5px',
        top: '14px'
    }
    var first_correct_path = 'M  530 296 C 509.379 281.373 681.746 368.371 682 362 C 773.254 388.629 951.216 399.298 742 581';
    var sec_correct_position = {
        left: '-45.5px',
        top: '14px'
    }
    var sec_correct_path = 'M 358 296 C 509.379 281.373 681.746 368.371 682 362 C 773.254 388.629 951.216 399.298 742 581';

    var last_correct_position = {
        left: '-86.5px',
        top: '15.3px'
    }
    var last_correct_path = 'M 202 288 C 509.379 281.373 681.746 368.371 682 362 C 773.254 388.629 951.216 399.298 742 581';
    var arrow_orange = document.querySelector('.voltmeter_arrow');


    if (changeCounter_2 == 1) {
        if (y > 0 && y < 30) {
            if (x < 15 && x > (-15)) {
                itemMoveTo(dragElement, path, first_correct_position, first_correct_path);


                arrow_orange.style.visibility = 'visible';
                arrow_orange.style.opacity = 1;
                turnOn();
                disabledDrag_2 = true;
                voltmeterAnimation(300)
                setTimeout(function () {
                    helperAlert(1, 'Your scale is too large. Try the next scale.', function () { disabledDrag_2 = false })
                    changeCounter_2++
                }, 1500)
            }
            if (x < (-30) && x > (-55)) {
                disabledDrag_2 = true
                position = {
                    top: '-40px',
                    left: '45px'
                }
                itemMoveTo(dragElement, path, position, 'M 744 102 C 509.379 281.373 681.746 368.371 682 362 C 773.254 388.629 951.216 399.298 742 581')

                helperAlert(2, 'Always connect to the largest scale first.', function () {

                    disabledDrag_2 = false
                })
            }
            if (x < (-75)) {
                position = {
                    top: '-40px',
                    left: '45px'
                }
                itemMoveTo(dragElement, path, position, 'M 744 102 C 509.379 281.373 681.746 368.371 682 362 C 773.254 388.629 951.216 399.298 742 581')
                helperAlert(2, 'Always connect to the largest scale first.', function () {
                    disabledDrag_2 = false
                })
            }
        }

        // else{
        //     itemMoveTo(dragElement, path, position, 'M 744 102 C 509.379 281.373 681.746 368.371 682 362 C 773.254 388.629 951.216 399.298 742 581')
        // }



    } else if (changeCounter_2 == 2) {
        if (y > 0 && y < 35) {

            if (x < (-35) && x > (-55)) {
                changeCounter_2++;
                voltmeterAnimation(15);
                itemMoveTo(dragElement, path, sec_correct_position, sec_correct_path);
                last_position = 2

            } else if (x < (-75)) {
                itemMoveTo(dragElement, path, first_correct_position, first_correct_path);
                helperAlert(2, 'Try the middle scale first.')
            } else {
                itemMoveTo(dragElement, path, first_correct_position, first_correct_path);
                secondDragReturnTO(last_position, dragElement)
            }
        } else {
            itemMoveTo(dragElement, path, first_correct_position, first_correct_path);
            secondDragReturnTO(last_position, dragElement)

          

        }

    } else if (changeCounter_2 == 3) {

        if (y > 0 && y < 35) {

            if (x < 15 && x > (-15)) {
                itemMoveTo(dragElement, path, first_correct_position, first_correct_path);
                voltmeterAnimation(300);
                last_position = 1
                // animateAmmeterInCharge(5)
            } else if (x < (-30) && x > (-55)) {
                itemMoveTo(dragElement, path, sec_correct_position, sec_correct_path);
                voltmeterAnimation(15);
                last_position = 2
                // animateAmmeterInCharge(500)

            } else if (x < (-75)) {
                itemMoveTo(dragElement, path, last_correct_position, last_correct_path);
                voltmeterAnimation(3);
                last_position = 3
            } else {
                secondDragReturnTO(last_position, dragElement)

            }

        } else {
            secondDragReturnTO(last_position, dragElement)
        }


    }

    function secondDragReturnTO(position, dragElement) {

        switch (position) {
            case 1:
                itemMoveTo(dragElement, path, first_correct_position, first_correct_path);
                voltmeterAnimation(300);
                break;
            case 2:
                itemMoveTo(dragElement, path, sec_correct_position, sec_correct_path);
                voltmeterAnimation(15);
                break;
            case 3:
                itemMoveTo(dragElement, path, last_correct_position, last_correct_path);
                voltmeterAnimation(3);
                break;
        }
    }


    var circ = document.querySelector('.voltmeter_crocs .under_circle')

    circ.style.background = '#fff'
})




function voltmeterAnimation(voltage) {
    var arrow_orange = document.querySelector('.voltmeter_arrow');
    var vCount = document.querySelector('.Volt_count');
    vCount.style.display = 'block'
    switch (voltage) {
        case 300:
            arrow_orange.style.transform = ' rotate(-40deg)'
            vCount.innerHTML = '3 V'

            vCount.style.display = 'none'

            break;
        case 15:
            arrow_orange.style.transform = ' rotate(-28deg)'
            vCount.innerHTML = '3 V'
            break;
        case 3:
            arrow_orange.style.transform = ' rotate(46deg)'

            vCount.innerHTML = '3 V'

            break;
        default:
            break;
    }

}


document.addEventListener('click', function (event) {
    if (!event.target.closest('#checkBtn')) return;
    window.location.reload(true)

})

document.addEventListener('click', function (event) {
    if (event.target.closest('.disable_all')) {

        setTimeout(function () {

            document.querySelector('.disable_all').style.display = 'none';
           $('.alertContainer').remove();

            disabledDrag = disabledDrag_2 = false;
            alertCallback()
        })

    }
})