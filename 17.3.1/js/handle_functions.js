var mashtab = 1;
function offset(el) {
    var rect = el.getBoundingClientRect(),
        scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
        scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
}

function minusScroll(number,x_y){
    return number;
    if(x_y == 1){
        return number +  window.pageXOffset || document.documentElement.scrollLeft;
    }else{
        return number + window.pageYOffset || document.documentElement.scrollTop;
    }
}

var hasCharge = false;
var openPopup = false;

function turnOn() {
    hasCharge = true;

    checker.classList.add('on')
    checker.classList.remove('off')
    switchImg.setAttribute('src', './img/switchclose.png');
    var light = document.querySelector('.lamp_light');
    light.style.visibility = 'visible';
    light.style.opacity = '1';
    animateAmmeterInCharge(lastSelectedPosition.amper)
    
}

function turnOff() {
    hasCharge =  false
    checker.classList.add('off');
    checker.classList.remove('on');
    switchImg.setAttribute('src', './img/switchopen.png');
    var light = document.querySelector('.lamp_light');
    light.style.visibility = 'hidden';
    light.style.opacity = '0';

    animateAmmeterInCharge(null)
}

function helperAlert(type, text, endCallback) {
    
    var disableContainer = document.querySelector('.disable_all');
    disableContainer.style.display = 'block';
    var alertContainer = document.createElement('div');
    alertContainer.classList.add('alertContainer');
    var alertText = document.createElement('p');
    alertText.innerHTML = text;

    if (type != 1) {
        var warning_img = document.createElement('img');
        warning_img.setAttribute('src', './img/warning.JPG');
        alertContainer.appendChild(warning_img);
        alertContainer.classList.add('warning');
    }

    alertContainer.appendChild(alertText)
    wrapper.appendChild(alertContainer);

    setTimeout(function () {
        // alertContainer.style.top = '280px';
        alertContainer.style.opacity = '1';
    })

    
    alertCallback = endCallback|| function(){}
    setTimeout(function () {
        // alertContainer.style.top = '280px';
        alertContainer.style.opacity = '0';
        document.querySelector('.disable_all').style.display = 'none';
        alertContainer.remove();
        $('.bottom_left_point').removeClass('selected');
        $('.element_point').removeClass('selected');
        $('left_point').removeClass('selected')
        // disabledDrag = disabledDrag_2 = false;
        alertCallback();
    }, 2000)
}

var alertCallback

var start_button = document.querySelector('.start_button_container')
document.querySelector('.disable_all').style.display = 'none'
start_button.onclick = function(){
    var firstHelperText = 'Click on the meter that must be used to measure the current through the light bulb.';
    helperAlert(1, firstHelperText);
    var start_button = document.querySelector('.start_button_container').remove();
}




function metersFirstStepAnimation(element, show2) {
    if (document.querySelector('.moving_meter_container') && step == 1) return;
    if (document.querySelector('.moving_meter_container_2') && step == 2) return;

    var src = element.getAttribute('src');
    var top = offset(element).top/mashtab - h/mashtab - 17 ;
    var left = offset(element).left/mashtab - wrapper.offsetLeft/mashtab - w/mashtab;

    var container = document.createElement('div');
    if (step == 2) {

        container.classList.add('moving_meter_container_2');
    } else {

        container.classList.add('moving_meter_container');
    }
    container.style.left = left + 'px';
    container.style.top = top + 'px';

    var img = document.createElement('img');
    img.setAttribute('src', src);

    container.appendChild(img);


    var arrow = document.createElement('div')
    arrow.classList.add('ammeterArrow');
   

    wrapper.appendChild(container);
    if (step == 1) {
        setTimeout(function () {
            container.style.left = left - 234 + 'px';
            container.style.top = top + 35 + 'px';
            container.style.transform = 'scale(1.5)';
        })
        container.appendChild(arrow)
    }else if(step == 2){
        arrow.classList.add('voltmeter_arrow')
        container.appendChild(arrow)
        setTimeout(function () {
            container.style.left = left - 234 + 'px';
            container.style.top = '169.5938px';
            container.style.transform = 'scale(1.5)';
        })
    }


    element.closest('div').style.borderColor = '#fff';

    document.querySelector('.bottom_wire').style.top = 450 + 'px';

        showPoints1(null);

}

function metersSecondStepAnimation() {
    var meterContainer = document.querySelector('.moving_meter_container');
    meterContainer.style.top = '255px';
    meterContainer.style.left = '518.5px';
    meterContainer.style.transform = 'scale(1.4)';

    var blackCroc = document.querySelector('.orange_croc');
    blackCroc.style.top = '304px';
    blackCroc.style.left = '514px';


}


function vSecondStepAnimation(){
    var meterContainer = document.querySelector('.moving_meter_container_2');
    meterContainer.style.top = '255px';
    meterContainer.style.left = '238.5px';
    meterContainer.style.transform = 'scale(1.4)';
    document.querySelector('.top_wire').style.left = '137px';
}

function showPoints1(step) {
    if(step)step = 1;
    else step = 0;
    var points = document.querySelectorAll('.points > div');
    for (var i = 0; i < points.length+step - 1; i++) {
        points[i].style.display = 'flex'
    }

    if(window.step == 2){
        document.querySelector('.left_point').style.display = 'flex';
        document.querySelector('.top_wire').style.left = '94px';
    }

}

function hidePoints1() {
    var points = document.querySelectorAll('.points > div');
    for (var i = 0; i < points.length; i++) {
        points[i].style.display = 'none'
    }
}

// ammeterArrow voltmeter_arrow
function animateAmmeterInCharge(amper) {
    var ammArrow = document.querySelector('.moving_meter_container .ammeterArrow');
    var amCount = document.querySelector('.amper_counter');

    switch (amper) {
        case 5:
            ammArrow.style.transform = 'rotate(-44deg)';
            amCount.innerHTML = '0,3 A';

            break;
        case 50:
            ammArrow.style.transform = 'rotate(-33deg)';
            amCount.innerHTML = '100 mA';

            break;
        case 500:
            ammArrow.style.transform = 'rotate(5deg)';
            amCount.innerHTML = '300 mA';

            break;
        default:
            ammArrow.style.transform = 'rotate(-50deg)';
            amCount.innerHTML = '0';
            break;


    }
    
    if(!hasCharge) {
        ammArrow.style.transform = 'rotate(-50deg)';
        amCount.innerHTML = '0';
    } ;
}








