var ctx;
var seperatorWidth;
var seperatorHeight;
var xLine, yLine;

function drawGraph(canvasArea, ctx, xLines, yLines) {
    this.xLine = xLines;
    this.yLine = yLines;
    ctx.beginPath();
    ctx.strokeStyle = "#eab3b3";
    ctx.moveTo(0, 0);

    var width = ctx.canvas.width;
    var height = ctx.canvas.height;

    seperatorWidth = (width) / (xLines - 1);
  //  console.log("a",+seperatorWidth);
	//console.log("b",+width);

    for (var i = 0; i < xLines; i++) {
        var line = $('<div>').css({
            position: 'absolute',
            left: seperatorWidth * i,
            height: height,
            width: 1, //(i % 2) ? 1 : 2,
			
            background: "#eab3b3" //(i % 2) ? "#ffc7c7" : "#eab3b3"
        });
        canvasArea.append(line)

    }

    seperatorHeight = (height) / (yLines - 1);
    for (var i = 0; i < yLines; i++) {
        var line = $('<div>').css({
            position: 'absolute',
            top: seperatorHeight * i,
            height: 1, //(i % 2) ? 1 : 2,
            width: width,
            background: "#eab3b3" //(i % 2) ? "#ffc7c7" : "#eab3b3"
        });
        canvasArea.append(line)
    }

    ctx.beginPath();
    ctx.lineWidth  = 2;
    ctx.strokeStyle = "#005893";
    ctx.fillStyle = "#005893";
    ctx.moveTo(width / 2, 0);
    ctx.lineTo(width / 2 + 5, 10);
    ctx.lineTo(width / 2 - 5, 10);
    ctx.lineTo(width / 2, 0);
    ctx.fill();

    ctx.moveTo(width / 2, height);
    ctx.lineTo(width / 2 + 5, height - 10);
    ctx.lineTo(width / 2 - 5, height - 10);
    ctx.lineTo(width / 2, height);
    ctx.fill();
    ctx.stroke();

    ctx.fillStyle = "#005893";
    ctx.strokeStyle = "005893";
    ctx.moveTo(0, height / 2);
    ctx.lineTo(10, height / 2 + 5);
    ctx.lineTo(10, height / 2 - 5);
    ctx.lineTo(0, height / 2);
    ctx.fill();

    ctx.moveTo(width, height / 2);
    ctx.lineTo(width - 10, height / 2 + 5);
    ctx.lineTo(width - 10, height / 2 - 5);
    ctx.lineTo(width, height / 2);


    ctx.fill();

    canvasArea.append($('<div>x</div>').css({
        position: 'absolute',
        left: width - 20,
        top: height / 2 - 5,
        fontSize: 27,
        fontFamily: 'MathJax_Main-Italic'
    }));

    canvasArea.append($('<div>y</div>').css({
        position: 'absolute',
        left: width / 2 - 20,
        top: 5,
        fontSize: 27,
        fontFamily: 'MathJax_Main-Italic'
    }));

    /*
        ctx.font = "20px MathJax_Main-Italic";
        ctx.fillStyle = "black";
        ctx.fillText('X', width - 25, height / 2 + 20);
    */

    /* ctx.font = "20px MathJax_Main-Italic";
     ctx.fillStyle = "black";
     ctx.fillText('Y', width / 2 - 20, 20);*/


    ctx.beginPath();
    ctx.strokeStyle = "#005893";
    ctx.fillStyle = "#005893";
    ctx.moveTo(width / 2, 0);
    ctx.lineTo(width / 2, height);
    ctx.stroke();

    ctx.font = "15px Roboto";
    ctx.fillStyle = "black";
    for (var i = 1; i < (xLines - 1) / 2; i++) {
        if (i - (xLines - 1) / 4 != 0) {
            var indicator = $("<div/>").css({
                position: 'absolute',
                width: 2,
                height: 8,
                backgroundColor: 'black',
                top: height / 2 - 4,
                left: seperatorWidth * i * 2
            });

            canvasArea.append(indicator);
			var angle;
				if ((i - (xLines - 1) / 4) == 1){
					angle=90;					
				}else if ((i - (xLines - 1) / 4) == 2){
					angle=180;	
				}else if ((i - (xLines - 1) / 4) == 3){
					angle=270;					
				}else if ((i - (xLines - 1) / 4) == 4){
					angle=360;
				}else if ((i - (xLines - 1) / 4) == 5){
					angle=450;
				}else if ((i - (xLines - 1) / 4) == 6){
					angle=540;	
				}else if ((i - (xLines - 1) / 4) == -1){
					angle=-90;					
				}else if ((i - (xLines - 1) / 4) == -2){
					angle=-180;	
				}else if ((i - (xLines - 1) / 4) == -3){
					angle=-270;					
				}else if ((i - (xLines - 1) / 4) == -4){
					angle=-360;
				}else if ((i - (xLines - 1) / 4) == -5){
					angle=-450;
				}else if ((i - (xLines - 1) / 4) == -6){
					angle=-540;	
				}				
				
            if (i - (xLines - 1) / 4 < 0) {
                ctx.fillText(angle+"°", seperatorWidth * i * 2 - 10, height / 2 + 25);
				
            } else {
                ctx.fillText(angle+"°", seperatorWidth * i * 2 - 8, height / 2 + 25);
            }
        }
    }

    for (var i = 1; i < (yLines - 1) / 2; i++) {
        if ((yLines - 1) / 4 - i != 0) {
            ctx.fillText((yLines - 1) / 4 - i, width / 2 - 25, seperatorHeight * i * 2 + 5);
            var indicator = $("<div/>").css({
                position: 'absolute',
                height: 2,
                width: 8,
                backgroundColor: 'black',
                top: seperatorHeight * i * 2,
                left: width / 2 -4
            });
            canvasArea.append(indicator);
        }
    }

    ctx.beginPath();
    ctx.strokeStyle = "#005893";
    ctx.fillStyle = "#005893";

    ctx.moveTo(0, height / 2);
    ctx.lineTo(height, width / 2);
    ctx.stroke();
}

function getCoordinatesFromPosition(position) {
    var coordinates = {x: 0, y: 0};

    coordinates.x = position.left / (seperatorWidth * 2) - (xLine - 1) / 4;
    coordinates.y = (yLine - 1) / 4 - position.top / (seperatorHeight * 2);

    return coordinates;
}

function getPositionFromCoordinates(coordinates) {
    var position = {left: 0, top: 0};

    position.left = (coordinates.x + (xLine - 1) / 4) * seperatorWidth * 2;
    position.top = ((yLine - 1) / 4 - coordinates.y) * seperatorHeight * 2;
    //console.log("seperatorWidth", seperatorWidth);
    return position;
}