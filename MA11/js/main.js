var zoomScale = 1;
var Page = function () {
	this.mode = "prod";
	this.devType = 2;
	this.activityTitle = "Combined Transformations of Sine function";
	this.gradeTitle = "SUbject Grade <B>9</B>";
	this.q = 0;
	this.a = 1;
    this.p = 1;
    this.k = 1;
	this.traces = [];
	this.traceNo = 0;
	this.graphContainer;
	this.propertiesContainer;
	this.propertiesDivs = [];
	this.traceListDiv;
	this.showMainPlot = true;
	this.canvasArea;
	this.sliderC;
	this.sliderM;
    this.sliderP;
    this.sliderK;
	this.ctx;
	this.seperatorWidth;
	this.seperatorHeight;
	this.xLine;
	this.yLine;
    this.formula = "<span class='for-italic'>y</span> = <span class='for-italic'>a</span> sin(<span class='for-italic'>kx</span>+<span class='for-italic'>p</span>)+<span class='for-italic'>q</span>"
    this.correctFormula = '';
    this.activeFormula = 'main';
    this.activeYZero = [];
    this.setTimeOut_a = null;
    this.setTimeOut_p = null;
    this.setTimeOut_q = null;
    this.setTimeOut_k = null;
    this.circleAnimateObj = {
        index:0,
        x:0,
        y:0
    }
    this.circleAnimate = false;
    this.circleAnimateF = null;
}
const traceColors = ['#f94f4b', '#744585', '#3eb2a2']
    
Page.prototype = {
    pushTrace: function (){
        var self = this;
        var trace = {
            no: ++self.traceNo,
            color: self.getColor(self.traces),
            a: self.a,
            q: self.q,
            p: self.p,
            k: self.k,
            closed: true,
            show: true
        };
        self.traces.push(trace);
        return trace; 
    },
    addEventTrace: function(){
        var self = this;
        $(".traceBtn").click(function () {
            if (self.traces.length === 3) return;
            var trace = self.pushTrace.call(self);
            var sign = trace.q >= 0 ? "+" : '  −';
            var formula = "<div class='trace' index='"+self.traceNo+"''>"+self.correctFormula+"</div>"+
            "<span class='eyes' index='"+self.traceNo+"''></span>\
            <span class='remove' index='"+self.traceNo+"''></span>";
            
            var traceDiv = $("<div class='traceDiv' index='"+self.traceNo+"''>" +
                "<div class='traceCircle' index='"+self.traceNo+"'' style='position: absolute;display:flex;top: " + (80 + 55 * (self.traces.length - 1)+15) + "px;width: 20px;height: 20px;border-radius: 50%;border-style: solid;border-width: 2px;border-color: #666666;'>" +
                "<div id='traceDot" + trace.no + "' style='display:none;margin:auto;width: 16px;height: 16px;border-radius: 50%;background-color: " + trace.color + " '/>" +
                "</div>" +
                "<div class='traceValue' index='"+self.traceNo+"'' style='color:" + trace.color + ";position: absolute;top: " + (80 + 55 * (self.traces.length - 1)) + "px;font-size: 25px;left: 30px;' >" + formula + "</div>" +
                "</div>");
            self.traceListDiv.append(traceDiv);
            traceDiv.click(function (e) {
                if($(e.target).hasClass('eyes')||$(e.target).hasClass('remove')) return;
                var traceDot = $('#traceDot' + trace.no);
                self.removeAllChecked($(e.target).attr('index'));
                self.plotExpression();
            });
            self.plotExpression();
            self.reorder()
        });
    },
	init: function () {
		var self = this;
        self.addMathText();
        self.instPanelEvents();
		var sliderC = self.sliderC;
		var sliderM = self.sliderM;
        document.title = self.activityTitle;
        $("#main_header").html(self.activityTitle);
       
        $("#resetBtn").click(function () {
            self.q = 0;
            self.a = 1;
            self.k = 0;
            self.p = 1;
            self.traces = [];
            self.propertiesDivs = [];
            self.activeFormula = 'main';
            self.setPosions();
        });

        self.addEventTrace();
        self.setPosions();
    },
    checkInputValue: function(value){
        var valueR = Number(value);
        value = (value).toString();
        if( value=='') valueR = '0';
        if(String(valueR)=='NaN') return value;
        return valueR;
    },
    setCorectValue: function (input, f = function () {}){
        var self = this;
        var parent_id = $(input).parent().attr('id');
        if(self['setTimeOut_'+parent_id]) {
            clearInterval(self['setTimeOut_'+parent_id]);
        }

        self['setTimeOut_'+parent_id] = setTimeout(function(){
            if(parent_id=='a'){
                var val = String($(input).val()).replace(',','.').replace('–','-');
                if(val=='') {
                    $(input).val(1);
                    self.a = 1;
                    self.setSliderPosition(parent_id,self.a);
                }
                else if(val == '-0' || val == '-0.' || val == '-0.0' || val == '-0.00' || Number(val) == -0 ) {
                    $(input).val('–0,01');
                    self.a = -0.01;
                    self.setSliderPosition(parent_id,self.a);
                }else if(val == '0'|| val == '0.' || val == '0.0' || val == '0.00' || Number(val) == 0){
                    $(input).val('0,01');
                    self.a = 0.01;
                    self.setSliderPosition(parent_id,self.a);
                }

            } else {
                var valCorect = self.checkInputValue($(input).val().replace(',','.').replace('–','-'));
                var val = String($(input).val().replace(',','.').replace('–','-'));
                 if(String(valCorect)!==val){
                    $(input).val(String(valCorect).replace('-','–').replace('.',','));
                    self.setSliderPosition(parent_id,valCorect);
                    self.plotExpression();
                }
            }
            f();
            self.plotExpression.call(self)
        },1000);
    },
    removeAllChecked: function(n=null,e) {
        var elem = '';
        var self = this;
        self.activeFormula = n-1;
        if(n==null){
            elem = $('#dot')
            self.activeFormula = "main";
        }else{
            elem = $('#traceDot'+(n-1))
            self.activeFormula = n-1;
        }
        $('.active').removeClass('active')
        for(var i=0;i<4;i++){
            $('#traceDot'+i).css({display:'none'});
        }
        $('#dot').css({visibility:'hidden'})
        elem.css({display:'block',visibility:"visible"})
           $('.traceValue[index='+n+']').addClass('active')
        // setTimeout(function(){
        // },1)
    },
    changeText: function (text,type = true,corrected=false){
        var sign = "";
        sign = ((text<0)?'&#8211;':'+');
        if(!type && sign=='+'){
            sign = ''
        }
        if(corrected && (text==1||text==-1)){
            return sign=='&#8211;'?'&#8211;':'';
        }
        return sign+String(Math.abs(text)).replace('.',',');
    },
    setPropertiesFormula: function(){
        var self = this;
        var elem = $('#dot');
        var color = '#4B54FF'
        $('.active').removeClass('active')
        if(self.activeFormula=="main"){
            setTimeout(function () { $('#properties-formula').html(self.correctFormula).css({color:color}); }); 
        }else{
            var elemP = $($('.trace')[self.activeFormula]); 
            $('#properties-formula').html(elemP.html()).css({color:elemP.css('color')});
        }
    },
    getFormula: function (activeFormula){
        var self = this;
        var obj;
        if(activeFormula=='main') obj = self;
        else obj = self.traces[activeFormula];
        var start = '<span class="dataF">y</span> = ';
        var a = self.changeText(self.a,false,true);
        var middle1 = ' sin(';
        var k = self.changeText(self.k,false);
        var middle2 = '<span class="dataF">x</span>';
        var p = self.changeText(self.p);
        var middle3 = '&#176;)';
        var q = self.changeText(self.q);
        if(obj.a==0){
            return start+self.changeText(obj.q,false);
        }
        if(obj.k==0 && obj.p!==0 && obj.q!==0){
            return start+a+middle1.replace('(',' ')+self.changeText(obj.p,false)+'&#176;'+q;
        }
        if(obj.a!==0 && obj.k==0 && obj.p!==0 && obj.q==0){
            return start+a+middle1+self.changeText(obj.p,false)+middle3;
        }
        if(obj.a!==0 && obj.k==0 && obj.p==0){
            return start+q;   
        }
        if(obj.a!==0 && obj.k!==0 && obj.p!==0 && obj.q==0){
            return start+a+middle1+k+middle2+p+middle3;
        }
        if(obj.a==1 && obj.k!==0 && obj.p!==0 && obj.q==0){
            return start+middle1+k+middle2+p+middle3;
        }
        if(obj.a!==0 && obj.k==1 && obj.p!==0 && obj.q==0){
            return start+a+middle1+middle2+p+middle3;            
        }
        if(obj.a!==0 && obj.k!==0 && obj.p==0 && obj.q!==0){
            return start+a+middle1+k+middle2+')'+q;
        }
        if(obj.a!==0 && obj.k!==0 && obj.p==0 && obj.q==0){
            return start+a+middle1+k+middle2+')';
        }
        if(obj.a==1 && obj.k!==0 && obj.p==0 && obj.q==0){
            return start+middle1+k+middle2+middle3;
        }
        return start+a+middle1+k+middle2+p+middle3+q;
    },
    plotExpression: function() {
        var self = this;
        var a = self.a;
		var q = self.q;
		var sliderC = self.sliderC;
		var sliderM = self.sliderM;
        var sign = q < 0 ? '  −' : '+ ';
        self.correctFormula = self.getFormula('main');   
        $(".my-formula").html(self.correctFormula);
        self.setRangeText();
        self.setPeriodText();
        self.setAmplitudeText();
        self.setInterceptsText();
        self.setTurningPointText();
        var elem = $('#dot');
        var color = '#4B54FF'
        if(self.activeFormula=="main"){
            $('#properties-formula').html(self.correctFormula).css({color:color}); 
        }else{
            var elemP = $($('.trace')[self.activeFormula]); 
            $('#properties-formula').html(elemP.html()).css({color:elemP.css('color')});
        }
        var canvas = document.getElementById('lineCanvas');
        if (null == canvas || !canvas.getContext) return;
        ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        var activeObj = self;
        activeObj.show = true; 
        activeObj.closed = true; 
        activeObj.color = "rgb(75, 84, 255)"; 
        trace(activeObj, false)
        self.traces.forEach(function (data) {
            if (data.show&&data.closed){
                if(!(self.activeFormula==data.no)) trace(data);
                else activeObj = data;
            } else if(data.show && !data.closed && self.activeFormula == data.no){
                activeObj = data;
            }
        });

        trace(activeObj,true);

        if(!(self.activeFormula=='main')) {
            trace(activeObj,false)
        }
        function trace(data,active=false) {
            if(data.color==undefined) return;
            if(active) self.activeYZero = [];
            var color = data.color;
            var a = data.a;
            var p = data.p;
            var k = data.k;
            var q = data.q;
            var colorP = color;
            var width = 4;
            if(!active){
                colorP = String(color).replace('1)','0.5)');
                width = 2;
            }
            ctx.beginPath();
            ctx.setLineDash([]);
            var initialPositionLeft = getPositionFromCoordinates({x: -3*Math.PI, y: a * Math.sin(-3*Math.PI) + q });
            var index1 = (0 - 3*Math.PI) / 1;
            var y1 =  a * Math.sin(k*index+(p/180*Math.PI)) + q;
            var position1 = getPositionFromCoordinates({x: index1, y: y1});
            ctx.moveTo(position1.left, position1.top);
            var lastTop = 0;
            //var y =  a * Math.sin(k*index+(p/180*Math.PI)) + q;
            for (var i = 0, j=0; i < 3*Math.PI; i=i+0.01,j++) {
                                    // ctx.lineWidth = width;
                var index = (i - 3*Math.PI) / 1;
                var y =  a * Math.sin(k*index+(p/180*Math.PI)) + q;
                var x = index;
                var position = getPositionFromCoordinates({x: index, y: y});
                ctx.lineTo(position.left, position.top);
                if(active && self.circleAnimateObj.index==j && self.circleAnimate){
                    self.circleAnimateObj.x = position.left;
                    self.circleAnimateObj.y = position.top;
                    ctx.arc(self.circleAnimateObj.x, self.circleAnimateObj.y, 10, 0, 2 * Math.PI);
                }
                if(!active) continue;
                if(y<0.05 && y>-0.05){
                    self.activeYZero.push(position.left); 
                }else if(x<0.05 && x>-0.05){
                    self.activeXZero = position.top;
                }
            }
            // ctx.closePath();
            var lastNagetivePosition = getPositionFromCoordinates({x: -0.0001, y: a * Math.sin(k*index+(p/180*Math.PI)) + q});
            ctx.lineTo(lastNagetivePosition.left, lastNagetivePosition.top);
            lastTop = lastNagetivePosition.top;
            ctx.lineWidth = width;
            ctx.strokeStyle = colorP;
            ctx.stroke();
            ctx.beginPath();
            var initialPositionRight = getPositionFromCoordinates({x: 0.0001, y: a * Math.sin(k*index+(p/180*Math.PI)) + q});
            ctx.beginPath();
            ctx.moveTo(250, lastTop);
            ctx.lineTo(250, initialPositionRight.top);
            ctx.setLineDash([8, 8]);
            ctx.lineWidth = width;
            ctx.stroke();
            ctx.closePath();
            ctx.beginPath();
            ctx.setLineDash([]);
            ctx.moveTo(initialPositionRight.left, initialPositionRight.top);
            for (var i = 3*Math.PI+0.1,j=94; i < 6*Math.PI; i=i+0.01,j++) {
                var index = (i - 3*Math.PI) / 1;
                var y = a * Math.sin(k*index+(p/180*Math.PI)) + q;
                var x = index
                var position = getPositionFromCoordinates({x: index, y: y});
                ctx.lineTo(position.left, position.top);
                if(active && self.circleAnimateObj.index==j){
                    self.circleAnimateObj.x = position.left;
                    self.circleAnimateObj.y = position.top;
                    ctx.arc(self.circleAnimateObj.x, self.circleAnimateObj.y, 10, 0, 2 * Math.PI);
                    if(j == 94*2){
                        clearInterval(self.circleAnimateF);
                    }
                }
                if(!active) continue;
                if(y<0.05 && y>-0.05){
                    self.activeYZero.push(position.left); 
                }else if(x<0.05 && x>-0.05){
                    self.activeXZero = position.top;
                }
            }
            //tx.closePath();
            // if(self.circleAnimate){
            //     ctx.beginPath()
            //     ctx.arc(self.circleAnimateObj.x, self.circleAnimateObj.y, 10, 0, 2 * Math.PI);
            // }
            ctx.lineWidth = width;
            ctx.strokeStyle = colorP;
            ctx.stroke();
        }
        if($('#Intercepts').hasClass('active-property')){
            self.intercept();
        }
        if($('#TurningPoint').hasClass('active-property')){
            self.turningPoint();
        }
        self.amplitude();
        self.manageProperties();
        self.continuousNature();
        self.range();
        if(self.a === 0) {
            $('#q input').attr('disabled', 'disabled');
            $('#p input').attr('disabled', 'disabled');
        } else {
            $('#q input').attr('disabled', false);
            $('#p input').attr('disabled', false);
        }
    },
    camelize: function(str){
        return str.replace(/\W+(.)/g, function (match, chr) {
            return chr.toUpperCase();
        });
    },
    continuousNature: function(){
        var self = this;
        if(!$('#continuousNature').hasClass('active-property')) return;
        self.circleAnimate = true;
        self.circleAnimateObj = {
            x:0,
            y:0,
            index:0
        }
        self.circleAnimateF = setInterval(function(){
            self.circleAnimateObj.index++;
            self.plotExpression.call(self);
        },70)
    },
    amplitude: function () {
        var self = this;
        if(!$('#Amplitude').hasClass('active-property')) return;
        const canvas = document.getElementById('lineCanvas');
        const ctx = canvas.getContext('2d');
        var mashtab = ($(canvas).width())/12;
        var a = self.a;
        var q = self.q;
        if(self.activeFormula!=='main'){
            a = self.traces[self.activeFormula].a;
            q = self.traces[self.activeFormula].q;
        }
        if(self.k == 0) a = 0;
        var startmin = (6-q)*mashtab;
        var startmax = (6-q-Math.abs(a))*mashtab;

        ctx.beginPath();
        ctx.lineWidth = 3;
        ctx.strokeStyle = "#fd983e";

        ctx.setLineDash([15, 5]);
        ctx.moveTo(0, startmin);
        ctx.lineTo(mashtab*12, startmin);
        ctx.stroke();

        ctx.beginPath();
        ctx.setLineDash([15, 5]);
        ctx.moveTo(0, startmax);
        ctx.lineTo(mashtab*12, startmax);
        ctx.stroke();
    },
    range: function (){
        var self = this;
        if(!$('#Range').hasClass('active-property')) return;
        const canvas = document.getElementById('lineCanvas');
        const ctx = canvas.getContext('2d');
        var mashtab = ($(canvas).width())/12;
        var a = self.a;
        var q = self.q;
        if(self.activeFormula!=='main'){
            a = self.traces[self.activeFormula].a;
            q = self.traces[self.activeFormula].q;
        }
        if(self.k == 0) a = 0;
        var start = (6-q+a)*mashtab;
        ctx.beginPath();
        ctx.lineWidth = 2;
        ctx.strokeStyle = "black";
        ctx.setLineDash([15, 5]);
        ctx.moveTo(0, start);
        ctx.lineTo(mashtab*6, start);
        ctx.stroke();
        
        // Solid line
        ctx.beginPath()
        ctx.setLineDash([]);
        ctx.moveTo(mashtab*6, start);
        ctx.lineTo(mashtab*6,start-2*a*mashtab);
        ctx.stroke();

        ctx.beginPath();
        ctx.setLineDash([15, 5]);
        ctx.moveTo(mashtab*6,start-2*a*mashtab);
        ctx.lineTo(mashtab*12,start-2*a*mashtab);
        ctx.stroke();

    },
    turningPoint: function () {
        var maxPoint = this.getMaxPoint();
        var minPoint = this.getMinPoint();
        var mashtabX = $('#graphCanvas').width()/1080
        var mashtabY = $('#graphCanvas').width()/12
        var obj = this.getValue();
        for(var i = 0; i < maxPoint.length; i++){
            ctx.beginPath();
            ctx.strokeStyle = '#417dfa';
            ctx.fillStyle = '#3ff0f5';
            var x = (540+(maxPoint[i][0]))*mashtabX;
            var maxY =  (6 - (obj.a * Math.sin((obj.k * maxPoint[i][0] + obj.p)/180 * Math.PI) + obj.q)) * mashtabY;
            ctx.arc(x, maxY, 5, 0, 2 * Math.PI);
            ctx.fill();
            ctx.stroke();
        }
        for(var i = 0; i < minPoint.length; i++){
            ctx.beginPath();
            ctx.strokeStyle = '#829624';
            ctx.fillStyle = '#5ee23e';
            var x = (540+minPoint[i][0])*mashtabX
            var minY = (6 - (obj.a * Math.sin((obj.k * minPoint[i][0] + obj.p)/180*Math.PI) + obj.q)) * mashtabY;
            ctx.arc(x, minY, 5, 0, 2 * Math.PI);
            ctx.fill();
            ctx.stroke();
        }
    },
    intercept: function(){
        var self = this;
        if(!$('#Intercepts').hasClass('active-property')){
            self.plotExpression();
            return;
        }
        var obj = self.getValue();
        var alfa = Math.asin(-obj.q/obj.a)/Math.PI*180;
        var impuls = (1/obj.k)*360;
        for(var i=0;i<16;i++){
            var coordinatDefaultX = Math.round((1/obj.k)*(alfa-obj.p)*100)/100;
            var coordinatDefaultX2 = Math.round((1/obj.k)*((180-alfa)-obj.p)*100)/100;
            for(var j=1;j<3;j++){
                var n = (j==1)?i:-i;
                var degree1 = (coordinatDefaultX)+impuls*(n);
                var x = (540+(degree1))*($('#graphCanvas').width()/1080)
                ctx.beginPath();
                ctx.strokeStyle = '#be4f8a';
                ctx.fillStyle = '#f668b3';
                ctx.arc(x, $('#graphCanvas').height()/2, 5, 0, 2 * Math.PI);
                ctx.fill();
                ctx.stroke();
                var degree2 = (coordinatDefaultX2)+impuls*(n);
                var x = (540+(degree2))*($('#graphCanvas').width()/1080)
                ctx.beginPath();
                ctx.lineWidth = 2;
                ctx.strokeStyle = '#be4f8a';
                ctx.fillStyle = '#f668b3';
                ctx.arc(x, 250, 5, 0, 2 * Math.PI);
                ctx.fill();
                ctx.stroke();
            }
        }
        var y =  obj.a * Math.sin((obj.p/180*Math.PI)) + obj.q;
        y = (6-y)*($('#graphCanvas').height()/12)
        ctx.beginPath();
        ctx.lineWidth = 2;
        ctx.strokeStyle = '#982724';
        ctx.fillStyle = '#dd3b3a';
        ctx.arc($('#graphCanvas').width()/2, y, 5, 0, 2 * Math.PI);
        ctx.fill();
        ctx.stroke();
    },
    drawAll: function(type){
        var self = this;
        switch(type){
            case 'Range':
                self.range();
                break;
            case 'Intercepts':
                self.intercept();
                break;
            case 'ContinuousNature':
                self.continuousNature();
                break;
            case 'Amplitude':
                self.amplitude();
                break;
        }
    },
    propertyFunction: function(name, content){
        var self = this;
        var id = self.camelize(name);
        var property = $("<div id='" + id + "' style='width:250px; margin-top: 20px'>" +
            "<div style='display: flex;flex-direction: row;align-items: center;justify-content: space-between'>" +
            "<div style='font-size: 20px;'>" + name + "</div>" +
            "<img id='img" + id + "' src='./img/arrow.png' width='20' style='transform: rotate(180deg)'/>" +
            "</div>" +
            "<div id='content" + id + "' hidden style='margin: 10px;font-size: 18px'>" + content + "</div>" +
            "<div style='margin-top:10px;height:2px;background-color: #bcbcbc;width:250px'></div>" +
            "</div>");

        property.data("expanded", false);
        property.click(function (e) {
            var _self = this;
                if($(_self).hasClass('active-property')) $(_self).removeClass('active-property');
                else $(_self).addClass('active-property');
                self.addTextProperties($(_self).attr('id'));
                self.drawAll($(_self).attr('id'))
                self.plotExpression();
                if (property.css('opacity') !== '1')
                    return;
                var expanded = property.data("expanded");
                property.data("expanded", !expanded);
                $('#content' + id).toggle('fast', self.manageProperties.bind(self));
            e.preventDefault;
        });
        self.propertiesDivs.push(property);
        return property;
    },
    addTextProperties: function(id){
        var self = this;
        switch(id){
            case 'Range':
                self.setRangeText();
                break;
            case 'Intercepts':
                self.setInterceptsText();
                break;
            case 'Period':
                self.setPeriodText();
                break;
            case 'TurningPoint':
                self.setTurningPointText();
                break;
            case 'Amplitude':
                self.setAmplitudeText();
                break;
        }
    },
    getValue: function () {
        var obj;
        if(this.activeFormula=='main') obj = this;
        else obj = this.traces[this.activeFormula];
        return obj;
    },
    getXInterceptsPoint: function () {
        var obj = this.getValue();

        var asin_val = (-obj.q) / obj.a;
        if(asin_val < -1 || asin_val > 1 || (obj.a == 0 && obj.q != 0)) return [];
        var y = 0;
        var n_start = Math.ceil((Math.abs(obj.k) * (-540) + obj.p - Math.asin(-obj.q/obj.a)/Math.PI*180) / 360)-1;
        var n_end = Math.floor((Math.abs(obj.k) * 540 + obj.p - Math.asin(-obj.q/obj.a)/Math.PI*180) / 360);
        var x_axis_intercept = [];
        for(var n = n_start; n <= n_end && n < 100; n++){
            var pointval1 = (1/obj.k) * (Math.asin(-obj.q/obj.a)/Math.PI*180 - obj.p + 360 * n)
            if(pointval1 <= 540 && pointval1 >= -540) x_axis_intercept.push([pointval1, y]);
            var pointval2 = (1/obj.k) * (180 - Math.asin(-obj.q/obj.a)/Math.PI*180 - obj.p + 360 * n)
            if(pointval2 <= 540 && pointval2 >= -540) x_axis_intercept.push([pointval2, y]);
        }
        return x_axis_intercept;
    },
    getYInterceptsPoint: function () {
        var obj = this.getValue();
        var x = 0;
        var y_intercept = [];
        y_intercept[0] = x;
        y_intercept[1] = obj.a * Math.sin((obj.p/180*Math.PI)) + obj.q;
        return y_intercept;
    },
    setInterceptsText: function(){
        var self = this;
        var xIntercept,yIntercept;
        var x_axis = self.getXInterceptsPoint();
        var y_axis = self.getYInterceptsPoint();
        // console.log(x_axis);
        // console.log(y_axis);
        var x_text = '<span>The <span class="for-italic">x</span> - axis</span><br>', y_text = '<span>The <span class="for-italic"> y </span> - axis</span><br>';
        for(var l=0; l < x_axis.length; l++) {
            x_text += '<span style="font-family: MathJax">('+self.correctProperty(x_axis[l][0])+' ; '+self.correctProperty(x_axis[l][1])+')</span><br>';
        }
        if(x_axis.length == 0){
            x_text = '<span>No <span class="for-italic">x</span> - intercepts</span><br>';
        }
        y_text += '<span style="font-family: MathJax">('+self.correctProperty(y_axis[0])+' ; '+self.correctProperty(y_axis[1])+')</span><br>';
        $('#contentIntercepts').html(x_text+'<br>'+y_text);
    },
    getMaxPoint: function(){
        var obj = this.getValue();
        var max = [];
        var y = obj.a + obj.q;
        var n_start = Math.ceil((Math.abs(obj.k)*(-540) + obj.p - 90)/360)-1;
        var n_end = Math.round((Math.abs(obj.k)*(540) + obj.p - 90)/360);
        for(var n = n_start; n <= n_end && n < 100; n++){
            if(obj.a > 0  ) {
                var valmax = (90+360*n - obj.p)/obj.k;
                if(valmax > 540 || valmax < -540) continue;
                max.push([valmax, y]);
            } else {
                var valmax = (-90+360*n - obj.p)/obj.k;
                if(valmax > 540 || valmax < -540) continue;
                max.push([valmax, y]);
            }
        }
        return max;
    },
    getMinPoint: function(){
        var obj = this.getValue();
        var min = [];
        var y = -obj.a + obj.q;
        var n_start = Math.ceil((Math.abs(obj.k)*(-540) + obj.p + 90)/360)-1;
        var n_end = Math.round((Math.abs(obj.k)*(540) + obj.p + 90)/360);
        for(var n = n_start; n <= n_end && n < 100; n++){
            if(obj.a > 0 ) {
                var valmin = (-90+360*n - obj.p)/obj.k
                if(valmin > 540 || valmin < -540) continue;
                min.push([valmin, y]);
            } else {
                var valmin = (90+360*n - obj.p)/obj.k
                if(valmin > 540 || valmin < -540) continue;
                min.push([valmin, y]);
            }
        }
        return min;
    },
    setTurningPointText: function(){
        var self = this;
        var text = 'Maximum turning points:';
        var maxValue = self.getMaxPoint();
        var minValue = self.getMinPoint();
        var maxText = '', minText = '';

        for(var l=0; l < maxValue.length; l++) {
            maxText += '<span style="font-family: MathJax">('+self.correctProperty(maxValue[l][0])+' ; '+self.correctProperty(maxValue[l][1])+')</span><br>';
        }
        for(var l=0; l < minValue.length; l++) {
            minText += '<span style="font-family: MathJax">('+self.correctProperty(minValue[l][0])+' ; '+self.correctProperty(minValue[l][1])+')</span><br>';
        }
        // console.log(maxValue);
        // console.log(minValue);
        $('#contentTurningPoint').html('<span>Maximum turning points:</span><br>'+maxText+'<span>Minimum turning points:</span><br>'+minText)
    },
    setAmplitudeText: function(){
        var self = this;
        var obj = self.getValue();
        var text = '';
        if(obj.a>0) text = '<span class="for-italic" style="margin-left: 14px;">a</span><br><span> = <span style="font-family: MathJax">'+self.correctProperty(obj.a)+'</span></span>';
        else text = '<span class="for-italic" style="margin-left: 14px;">–a</span><br><span> = <span style="font-family: MathJax">'+self.correctProperty(Math.abs(obj.a))+'</span>';
        $('#contentAmplitude').html(text);
    },
    setPeriodText: function(){
        var self = this;
        var obj = self.getValue();
        var text = '<span>Period = <span id="formula_1"></span> <br><span style="margin-left: 57px;">= <span style="font-size: 119%;font-family: MathJax;">'+self.correctProperty(360/Math.abs(obj.k))+'°</span></span>';
        $('#contentPeriod').html(text);
        MathJax.HTML.addElement(document.getElementById('formula_1'), 'span', {id: 'formula_2'}, ['`{360°} / |k|`']);
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'formula_2']);
    },
    correctProperty: function(num){
        return String(Math.round(Number(num)*100)/100).replace('.',',').replace('-','–');
    },
    setRangeText: function(){
        var self = this;
        var obj = self.getValue();
        var text = $('#contentRange').html();
        if(obj.a>0) text = '<span>[<span class="for-italic"> –a </span> + <span class="for-italic"> q </span>; <span class="for-italic">a</span> + <span class="for-italic"> q </span>]</span><br><span>[<span style="font-family: MathJax;" >'+self.correctProperty(-obj.a+obj.q)+'</span>; <span style="font-family: MathJax;" >'+self.correctProperty(obj.a+obj.q)+'</span>]';
        else if(obj.a<0) text = '<span>[<span class="for-italic"> a </span> + <span class="for-italic"> q </span>; <span class="for-italic"> –a </span> + <span class="for-italic"> q </span>]</span><br><span>[<span style="font-family: MathJax;" >'+self.correctProperty(obj.a+obj.q)+'</span>; <span style="font-family: MathJax;" >'+self.correctProperty(-obj.a+obj.q)+'</span>]';
        else text = '<span><span class="for-italic" >y</span> = <span class="for-italic">q</span></span><br><span> <span style="font-family:MathJax" >y</span> = <span style="font-family:MathJax" >'+self.correctProperty(obj.q)+'</span>';
        if(obj.k == 0) text = '<span><span class="for-italic" >y</span> = <span class="for-italic">a</span> sin(<span class="for-italic">p</span>) + <span class="for-italic">q</span></span><br><span> <span class="for-italic" >y</span> = <span style="font-family:MathJax" >'+self.correctProperty(obj.a*Math.sin(obj.p/180*Math.PI)+obj.q)+'</span>';
        $('#contentRange').html(text);
    },
    setProperties: function(){
        var self = this;
        var propertiesListDiv = $("#propertiesListDiv").css({marginTop: 130});
        
        propertiesListDiv.append(self.propertyFunction.call(self,"Domain",
            "<div>{<span class='for-italic'>x</span>° : <span class='for-italic'>x</span> ∈ R}</div>"//"<div>𝑥:𝑥 ∈ R , 𝑥 ≠ 0</div>"
        ));
        propertiesListDiv.append(self.propertyFunction.call(self,"Range",
            "<div id='rangeDef'>y : y ∈ R , y ≠ <span class='for-italic'>q</span></div>" +
            "<div id='range'>y : y ∈ R - {<span class='for-italic'>q</span>}</div>"
        ));
        propertiesListDiv.append(self.propertyFunction.call(self,"Period",
            "<div>Period</div>"    
        ))
        propertiesListDiv.append(self.propertyFunction.call(self,"Amplitude",
            "<div>(<span class='for-italic'>𝑥</span>, 𝑦) = (0, <span class='for-italic'>q</span>)</div>" +
            "<div id='pointOfSymmetry' style='margin-left: 55px'>(0,0)</div>"
        ));
        propertiesListDiv.append(self.propertyFunction.call(self,"Intercepts",
            "<div>𝑥-Intercept: <span class='for-italic'> 𝑥 </span>= <span class='for-italic'> -a </span>/<span class='for-italic'> q </span>, <span class='for-italic'> q </span> ≠ 0 </div>" +
            "<div id='xIntercept'><span class='for-italic'> 𝑥 </span>-intercept does not exist</div>" +
            "<div id='yIntercept'>y-Intercept: No y intercept</div>"
        ));
        propertiesListDiv.append(self.propertyFunction.call(self,"Turning point",
            "<div>Horizontal: 𝑦 = <span class='for-italic'>q</span></div>" +
            "<div id='asymptoteY'>Horizontal: 𝑦 = 0</div>" +
            "<div>Vertical: <span class='for-italic'>𝑥</span> = 0</div>"
        ));
        propertiesListDiv.append(self.propertyFunction.call(self,"Continuous nature",
         "The function is continuous everywhere since it is defined everywhere."
        ));



    },
    setPosions: function() {
        var self = this;
        var sliderC = self.sliderC;
		var sliderM = self.sliderM;
        var graphBoard = $("#graphBoard").empty();
        var controlPanel = $("#controlPanel").empty();
		controlPanel.append("<div id='mainformula'>"+self.formula+"</div>")
        graphBoard.append("<div id='properties'>" +
			"<div id='properties-formula'>"+self.correctFormula+
            "</div>\
            <div id='propertiesListDiv' style='overflow-y: auto;overflow-x: hidden;width: 270px;height:520px'/>" +
            "</div>");
        self.addEventsTrace();
        self.setProperties();

        controlPanel.append("<div style='top: 105px;left: 24px;' class='adjuestControl'>" +
            "<div style='position: absolute;top:23px;left:10px'></div>" +
            "<div id='sliderC'><input type='range' class='range' min='-5' value='1' max='5' step='0.01'></div>" +
            "<div style='position: absolute;top:26px;left:270px;' id='q'><input type='text' min='-5' max='5'></div>" +
            "</div>");
        self.setSliderPosition('q',1);
        sliderC = $('#sliderC');
        $(document).on('input','#sliderC input[type=range]', function (e) {
                if($('#dot').css('visibility') != 'visible') {
                    // $('#dot').click();
                }
                self.k = +e.target.value;
                $('#q input').val(String(self.k).replace('-','–').replace('.',','));

                self.setCorectValue($('#q input'), function () {
                    self.k = $('#q input').val().replace('–','-').replace(',','.');
                    self.setSliderPosition('q',self.k);
                });
                 self.plotExpression.call(self);
            });

        controlPanel.append("<div style='top: 58px;left: 24px;' class='adjuestControl'>" +
            "<div style='position: absolute;top:20px;left:10px'></div>" +
            "<div id='sliderM'><input type='range' class='range' min='-5' value='0' max='5' step='0.01'></div>" +
            "<div style='position: absolute;top:23px;left:270px;' id='a'><input type='text' min='-5' max='5'></div>" +
            "</div>");
        sliderM = $('#sliderM');
        self.setSliderPosition('a',1);
        $('#sliderM input').val(1)
        $(document).on('input', '#sliderM input', function (e) {
                if($('#dot').css('visibility') != 'visible') {
                    // $('#dot').click();
                }
                self.exA = self.a;
                var val = +e.target.value;
                if(val==0){
                    if(self.exA>0) val = 0.05;
                    else val = -0.05;
                }
                self.a = val;
                $('#a input').val(String(self.a).replace('-','–').replace('.',','));
                self.setCorectValue($('#a input'), function () {
                    self.setSliderPosition('a',self.a);
                },(self.exA>self.a))
                self.plotExpression.call(self);
            });

        controlPanel.append("<div style='top: 159px;left: 24px;' class='adjuestControl'>" +
            "<div style='position: absolute;top:20px;left:10px'></div>" +
            "<div id='sliderP'><input type='range' class='range' min='-360' value='1' max='360' step='1'></div>" +
            "<div style='position: absolute;top:23px;left:270px;' id='p'><input type='text' float min='-360' max='360' type='number'><span id='degree-sign'>&#176;</span></div>" +
            "</div>");
        self.setSliderPosition('p',0);
        sliderP = $('#sliderP');
        $(document).on('input', '#sliderP input', function (e) {
                
                if($('#dot').css('visibility') != 'visible'){
                    // $('#dot').click();
                }
                self.p = +e.target.value;
                $('#p input').val(String(self.p).replace('-','–').replace('.',','));

                var left = String(self.p).length*4+40;
                $('#degree-sign').css({position:'absolute',top:'0px',left:left+'px'})
                self.plotExpression.call(self);
            });
            // stop: function () {
                // self.setCorectValue($('#p input'), function () {
                //     self.p = ($('#p input').val()).replace('–','-').replace(',','.');
                //     var left = String(self.p).length*4+40;
                //     $('#degree-sign').css({position:'absolute',top:'0px',left:left+'px'})
                //     self.plotExpression.call(self);
                //     self.setSliderPosition('p',self.p);
                // })
            // }

        controlPanel.append("<div style='top: 215px;left: 24px;' class='adjuestControl'>" +
            "<div style='position: absolute;top:20px;left:10px'></div>" +
            "<div id='sliderK'><input type='range' class='range' min='-5' value='0' max='5' step='0.01'></div>" +
            "<div style='position: absolute;top:23px;left:270px;' id='k'><input type='text' min='-5.5' max='5'></div>" +
            "</div>");
        sliderK = $('#sliderK');
        $(document).on('input', '#sliderK input',function (e) {
                if($('#dot').css('visibility') != 'visible'){
                    // $('#dot').click();
                }
                self.q = +e.target.value;
                
                $('#k input').val(String(self.q).replace('-','–').replace('.',','));
                self.setCorectValue($('#k input'), function () {
                    self.q = Number($('#k input').val().replace('–','-').replace(',','.'));
                    self.setSliderPosition('k',self.q);
                })
                self.plotExpression.call(self);
            });

        var mainPlot = $("<div style='font-family: MathJax_Main-Italic;top: 306px;left: -19px;width: 300px;position: absolute;border-radius: 5px;height: 50px;'>" +
            "<div id='dot' style='margin-left: 19px;margin-top: 14px;width: 14px;height: 14px;border-radius: 50%;background-color: #4B54FF'/>" +
            '<div class="my-formula">y = '+self.a+' sin('+self.k+'x'+self.p+')'+self.q+' </div>'+
            "</div>");
        controlPanel.append(mainPlot);
        self.setSliderPosition('k',0);

        if (self.showMainPlot)
            $("#dot").css({visibility:'visible'}).addClass('active');
        else
            $("#dot").css({visibility:'hidden'}).removeClass('active');

        mainPlot.click(function () {
            self.removeAllChecked.call(self)
            self.plotExpression();
        });


        controlPanel.append("<div id='tracedFunctions'>" +
            "<div id='traceListDiv' style='overflow-y: auto;overflow-x: hidden;width: 250px'/>" +
            "</div>");

        self.traceListDiv = $('#traceListDiv');
        $("#graphBoard").append("<div id='graphContainer'></div>");

        self.graphContainer = $("#graphContainer").append("<div id='canvasArea'></div>");
        self.graphContainer.append("<div id='propertiesContainer'/>");
        self.canvasArea = $("#canvasArea");
        self.propertiesContainer = $("#propertiesContainer");
        self.canvasArea.append("<canvas id='graphCanvas' style='position: absolute;z-index: 9999' width='500' height='500'></canvas>");
        self.canvasArea.append("<canvas id='lineCanvas' style='position: absolute;z-index: 9999' width='500' height='500'></canvas>");
        self.manageGraph();
        self.plotExpression();
        $('.adjuestControl input[type=text]').off('change').on('change',function(e){
            if(String(Number((e.target.value).replace(',','.').replace('–','-')))=='NaN'){
                e.target.value = '1';
            }

            self.setSliderPosition($(e.target).parent().attr("id"),$(e.target).val().replace(',','.').replace('–','-'));
            self.plotExpression.call(self);
        })
        self.keyDowned = false;
        $('input[type=text]').off('input').on('input',function(e){
            self.setSliderPosition($(e.target).parent().attr("id"),$(e.target).val().replace(',','.').replace('–','-'));
            self.setCorectValue(e.target,function(){});
            self.plotExpression.call(self);
        })

        $(document).off('keydown','input[type=text]').on('keydown','input[type=text]',function(e){
            var value = '';
            if(e.key == 'Backspace'){
                value = (e.target.value.slice(0,e.target.selectionStart-1)+e.target.value.slice(e.target.selectionStart,e.target.value.length)).replace(',','.');
            }else if(e.key == 'Delete'){
                value = e.target.value.slice(0,e.target.selectionStart)+e.target.value.slice(e.target.selectionStart+1,e.target.value.length);    
            }else{
                value = e.target.value.slice(0,e.target.selectionStart)+e.key+e.target.value.slice(e.target.selectionStart,e.target.value.length);
            }
            var to_fix = value.split(',');
            var parentId = $(e.target).parent().attr('id')

            if(to_fix.length > 1 && to_fix[1].length == 3 && parentId != 'p') {
                e.preventDefault();
                return;
            }

            value = value.replace(',','.').replace('–','-')
            if(parentId == 'p'){
                var valL = String($(e.target).val()).length;
                if(valL>5){
                    valL=5;
                }
                var left = valL*6+40;
                $('#degree-sign').css({position:'absolute',top:'0px',left:left+'px'})
            }
            if(e.target.value=='0'||e.target.value=='-0'){
                if(e.key!='.' && e.key!=','&&e.key!='Backspace'&&e.key!='Delete'){
                    e.preventDefault();
                    return;
                }
            }
            if(value.length==0||e.key=='ArrowRight'||e.key=='ArrowLeft'){
                return;
            }
            if((e.key=='.' || e.key==',') && e.target.hasAttribute('float')){
                 e.preventDefault();
                 return;
            }
            if(String(value)[value.length-1]=='.' && value.split('.').length==2){
                var newValue = value.replace('.','');
                if(String(Number(newValue))=='NaN' && !newValue.length==0){
                    e.preventDefault();
                }
                return;
            }
            
            if(Number(value)<e.target.min  ||
                Number(value)>e.target.max){
                e.preventDefault();
                return;
            }
            if(String(value)[0]=='-' && value.split('-').length==2){
                var newValue = value.replace('-','');
                if(String(Number(newValue))=='NaN' && !newValue.length==0){
                    e.preventDefault();
                }
                return;
            }
            if(String(Number(value))=='NaN'){
                e.preventDefault();
                return;
            }
            // if(e.key=='-'){
            //     value = String(value).replace('-','–');
            // }
            // e.target.value = value;
            // e.preventDefault();
        })



    },
    setSliderPosition: function(letter,value){
        var size = 5;
        var letterR = letter;
        if(letter == 'a') letterR = 'm';
        else if(letter == 'q') letterR = 'c';
        else if(letter == 'p'){
           size = 360;
        }
        if(value>size) value = size;
        else if(value<-size) value = -size;
        var width = $('#slider'+letterR.toUpperCase()).width();
        $('#slider'+letterR.toUpperCase()+' input[type=range]').val(value);
        $('#'+letter+' input').val(String(value).replace('-','–').replace('.',','));
        if(letter == 'p'){
            var left = String(value).length*4+40;
            $('#degree-sign').css({position:'absolute',top:'0px',left:left+'px'})
        }
        letter = letter == 'q' ? 'k' : (letter == 'k' ? 'q':letter);
        this[letter] = Number(value);
    },
    reorder: function () {
        for(var i=1;i<=this.traces.length;i++){
            var traceDiv = $($('.traceDiv')[i-1]);
            traceDiv.find('.eyes').attr({'index':i});
            traceDiv.find('.remove').attr({'index':i});
            traceDiv.find('.traceCircle').css({top:(i*55)+40+'px'}).attr({'index':i});
            traceDiv.find('.traceValue').css({top:(i*55)+25+'px'}).attr({'index':i});
            traceDiv.attr({'index':i});
            traceDiv.find('.trace').attr({'index':i});
            traceDiv.find('.traceCircle').children().attr('id',"traceDot"+(i-1))
            this.traces[i-1]['no'] = i-1;
        }
    },
    addEventsTrace: function () {
        var self = this
        $(document).off('click','.eyes').on('click','.eyes',function(e){
            var elem = $(e.target);
            var index = elem.attr('index');
            if(elem.hasClass('closed')){
                self.traces[index-1].closed = true;
                elem.removeClass('closed');
            }
            else {
                self.traces[index-1].closed = false;
                elem.addClass('closed');
            }
            self.plotExpression()
        })
        $(document).off('click','.remove').on('click','.remove',function(e){
            var index = $(e.target).attr("index")
            var color = $(e.target).parent().css('color')
            $(e.target).parent().parent().remove();
            self.reorder();
            setTimeout(function(){
                $('.active').removeClass('active')                
            },200)
            self.removeAllChecked();
            self.activeFormula = 'main'
            self.setPropertiesFormula();
            self.traces.splice(index-1,1)
            self.traceNo--;
            self.plotExpression();
        })
    },
	manageProperties:function() {
        var self = this;
        var a = self.a;
		var q = self.q;
		var sliderC = self.sliderC;
		var sliderM = self.sliderM;
        self.propertiesDivs.forEach(function (property) {
            if (a === 0 && property.prop('id') !== 'Range') {
                if (property.data("expanded")) {
                    // property.trigger("click")
                }
                property.css({opacity: 0.3, cursor: 'auto'});
            } else {
                property.css({opacity: 1, cursor: 'pointer'});
            }
        });
        self.propertiesContainer.empty();
    },
    manageGraph: function() {
        var self = this;
        var canvas = document.getElementById('graphCanvas');
        if (null == canvas || !canvas.getContext) return;
        ctx = canvas.getContext("2d");
        drawGraph(self.canvasArea, ctx, 25, 25);
    },
    randamiseArray: function(inpArr) {
        var self = this;
        var tempArr = [];
        while (inpArr.length > 0) {
            var randomIndex = Math.floor(Math.random() * inpArr.length);
            tempArr.push(inpArr[randomIndex]);
            inpArr.splice(randomIndex, 1);
        }
        return tempArr;
    },
    getColor: function(traces) {
        var colors = JSON.parse(JSON.stringify(traceColors));

        traces.map((item) => {
            var i = colors.indexOf(item.color);
            colors.splice(i, 1);
        })

        return colors[0];
    },
	drawGraph: function(canvasArea, ctx, xLines, yLines) {
		var self = this;
	    self.xLine = xLines;
	    self.yLine = yLines;
	    var ctx = self.ctx;
	    ctx.beginPath();
	    ctx.strokeStyle = "#eab3b3";
	    ctx.moveTo(0, 0);
	    var width = ctx.canvas.width;
	    var height = ctx.canvas.height;

	    self.seperatorWidth = (width) / (12*Math.PI);

	    for (var i = 0; i < xLines; i++) {
	        var line = $('<div>').css({
	            position: 'absolute',
	            left: (self.seperatorWidth+7.5) * i,
	            height: height,
	            width: 1 ,//(i % 2) ? 1 : 2,
				
	            background: "#eab3b3" //(i % 2) ? "#ffc7c7" : "#eab3b3"
	        });
	        canvasArea.append(line)

	    }

	    self.seperatorHeight = (height) / (yLines - 1);
	    for (var i = 0; i < yLines; i++) {
	        var line = $('<div>').css({
	            position: 'absolute',
	            top: self.seperatorHeight * i,
	            height: 1 ,//(i % 2) ? 1 : 2,
	            width: width,
	            background: "#eab3b3" //(i % 2) ? "#ffc7c7" : "#eab3b3"
	        });
	        canvasArea.append(line)
	    }

	    ctx.beginPath();
	    ctx.lineWidth  = 2;
	    ctx.strokeStyle = "#005893";
	    ctx.fillStyle = "#005893";
	    ctx.moveTo(width / 2, 0);
	    ctx.lineTo(width / 2 + 5, 10);
	    ctx.lineTo(width / 2 - 5, 10);
	    ctx.lineTo(width / 2, 0);
	    ctx.fill();

	    ctx.moveTo(width / 2, height);
	    ctx.lineTo(width / 2 + 5, height - 10);
	    ctx.lineTo(width / 2 - 5, height - 10);
	    ctx.lineTo(width / 2, height);
	    ctx.fill();
	    ctx.stroke();

	    ctx.fillStyle = "#005893";
	    ctx.strokeStyle = "005893";
	    ctx.moveTo(0, height / 2);
	    ctx.lineTo(10, height / 2 + 5);
	    ctx.lineTo(10, height / 2 - 5);
	    ctx.lineTo(0, height / 2);
	    ctx.fill();

	    ctx.moveTo(width, height / 2);
	    ctx.lineTo(width - 10, height / 2 + 5);
	    ctx.lineTo(width - 10, height / 2 - 5);
	    ctx.lineTo(width, height / 2);


	    ctx.fill();

	    canvasArea.append($('<div>x</div>').css({
	        position: 'absolute',
	        left: width - 20,
	        top: height / 2 - 5,
	        fontSize: 27,
	        fontFamily: 'MathJax_Main-Italic'
	    }));

	    canvasArea.append($('<div>y</div>').css({
	        position: 'absolute',
	        left: width / 2 - 20,
	        top: 5,
	        fontSize: 27,
	        fontFamily: 'MathJax_Main-Italic'
	    }));
        
	    ctx.beginPath();
	    ctx.strokeStyle = "#005893";
	    ctx.fillStyle = "#005893";
	    ctx.moveTo(width / 2, 0);
	    ctx.lineTo(width / 2, height);
	    ctx.stroke();

	    ctx.font = "15px Roboto";
	    ctx.fillStyle = "black";
	    for (var i = 1; i < (xLines - 1) / 2; i++) {
	        if (i - (xLines - 1) / 4 != 0) {
	            var indicator = $("<div/>").css({
	                position: 'absolute',
	                width: 2,
	                height: 8,
	                backgroundColor: 'black',
	                top: height / 2 - 4,
	                left: (self.seperatorWidth+7.5) * i * 2
	            });

	            canvasArea.append(indicator);
				var angle;
					if ((i - (xLines - 1) / 4) == 1){
						angle=90;					
					}else if ((i - (xLines - 1) / 4) == 2){
						angle=180;	
					}else if ((i - (xLines - 1) / 4) == 3){
						angle=270;					
					}else if ((i - (xLines - 1) / 4) == 4){
						angle=360;
					}else if ((i - (xLines - 1) / 4) == 5){
						angle=450;
					}else if ((i - (xLines - 1) / 4) == 6){
						angle=540;	
					}else if ((i - (xLines - 1) / 4) == -1){
						angle=-90;					
					}else if ((i - (xLines - 1) / 4) == -2){
						angle=-180;	
					}else if ((i - (xLines - 1) / 4) == -3){
						angle=-270;					
					}else if ((i - (xLines - 1) / 4) == -4){
						angle=-360;
					}else if ((i - (xLines - 1) / 4) == -5){
						angle=-450;
					}else if ((i - (xLines - 1) / 4) == -6){
						angle=-540;	
					}				
					
	            if (i - (xLines - 1) / 4 < 0) {
	                ctx.fillText(angle+"°", (self.seperatorWidth+7.5) * i * 2 - 10, height / 2 + 25);
					
	            } else {
	                ctx.fillText(angle+"°", (self.seperatorWidth+7.5) * i * 2 - 8, height / 2 + 25);
	            }
	        }
	    }

	    for (var i = 1; i < (yLines - 1) / 2; i++) {
	        if ((yLines - 1) / 4 - i != 0) {
	            ctx.fillText((yLines - 1) / 4 - i, width / 2 - 25, self.seperatorHeight * i * 2 + 5);
	            var indicator = $("<div/>").css({
	                position: 'absolute',
	                height: 2,
	                width: 8,
	                backgroundColor: 'black',
	                top: self.seperatorHeight * i * 2,
	                left: width / 2 -4
	            });
	            canvasArea.append(indicator);
	        }
	    }

	    ctx.beginPath();
	    ctx.strokeStyle = "#005893";
	    ctx.fillStyle = "#005893";

	    ctx.moveTo(0, height / 2);
	    ctx.lineTo(height, width / 2);
	    ctx.stroke();
	},
	getCoordinatesFromPosition: function(position) {
	    var coordinates = {x: 0, y: 0};

	    coordinates.x = position.left / (self.seperatorWidth * 2) - 3*Math.PI;
	    coordinates.y = (yLine - 1) / 4 - position.top / (self.seperatorHeight * 2);

	    return coordinates;
	},
	getPositionFromCoordinates: function(coordinates) {
	    var position = {left: 0, top: 0};

	    position.left = (coordinates.x + 3*Math.PI) * self.seperatorWidth * 2;
	    position.top = ((yLine - 1) / 4 - coordinates.y) * self.seperatorHeight * 2;
	    return position;
	},
    getInstructionList: function () {
        var self = this;
        helpList = ['You are shown the graph of the sine function.',
                    'You can change the parameters `k`, `p`, `a` or `q` of the function to see the effect on the shape of the graph.',
                    'Click the TRACE button to take a snapshot of the function and then make changes to any of the parameters to get a different function and compare with the snapshot.',
                    'The blue graph always shows the “live” function that you can modify.',
                    'You can show/hide any of the traced functions or even remove them.',
                    'The “Properties” section gives the properties of the selected function – either the “live” function or any of the traced functions.',
                    'Use the radio buttons to select a different function to view its properties.',
                    'Expand a property to view the formula and the value of that property for the selected function.',
                    'Click RESET to start again.'];
        return helpList;
    },
    addMathText: function () {
        var ol = document.createElement('ol');
        ol.className = 'help-list';
        var elem = $('#insTxtPanel ol');
        var helpList = this.getInstructionList();
        for(var i = 0; i< helpList.length; i++) {
            elem.append('<li>'+helpList[i]+'</li>')
        }
    },
    instPanelEvents: function (){
        $(document).off('click','#instBtn').on('click','#instBtn',function(){
            $('#instPanel').css('visibility',"visible")
        })
        $(document).off('click','#instCl').on('click','#instCl',function(){
            $('#instPanel').css('visibility',"hidden")
        })
    }
}
